# General Notes

Release instructions are in the releases directory.

Here we have some general notes about interacting with the database.

When you create a new database using pgAdmin, use `template0`.
Otherwise you may get lots of errors when you restore.
Also, as much as possible, use the same versions of PostgreSQL and PostGIS.

## GeoServer and Connection Pools

Note that GeoServer uses a pool of connections to the database.
In the future, the Django application may also use a connection pool.
This means that and when you stop the database or go into single-user mode,
you should suspend or stop the pools.  The simplest thing to do here is to
stop GeoServer - I usually just stop Tomcat. As we go into production we'll
probably have to refine this a bit.

> **TODO:** Include a link to the Tomcat administration document.


# DUMP and RESTORE

`pg_dump` and `pg_restore` exist in both pgAdmin and PostgreSQL.
For example, on my machine:

> `"C:\Program Files\PostgreSQL\8.3\bin\pg_dump.exe" --version`

produces

> `pg_dump (PostgreSQL) 8.3.7`

But the following command:

> `"C:\Program Files\pgAdmin III\1.10\pg_dump.exe" --version`

produces

> `pg_dump (PostgreSQL) 8.4.1`

Be sure to use the correct versions for the database instance with which you are working.
To control this you should probably use the command line.

## PL/pgSQL

We use PL/pgSQL, therefore before you perform a restore, be sure to create the PL/pgSQL language, otherwise PL/pgSQL functions cannot be created:

> `/usr/bin/createlang plpgsql -U postgres mad`

## pg_dump Examples

- `pg_dump --host localhost --port 5432 --username postgres --format custom --blobs --verbose --file "C:\tmp\mad_20100526_1707.backup" mad`
- `pg_dump --host localhost --port 5432 --username postgres --format plain --blobs --verbose --file "C:\tmp\mad_20100527_1908.backup" mad`
- `pg_dump --host localhost --port 5432 --username postgres --format plain --blobs --verbose --file "/mnt/data/load/data/mad_20101028.backup" mad`
- `pg_dump --host localhost --port 5432 --username postgres --format plain --blobs --verbose --file "/mnt/data/load/data/mad_156_20101028.backup" mad_156`
- `pg_dump --host localhost --port 5432 --username postgres --format custom --blobs --verbose --file "/mnt/data/load/data/mad-1_0-beta-1_patch.backup" mad`
- `pg_dump --host localhost --port 5432 --username postgres --format custom --blobs --verbose --file "/mnt/data/load/data/mad-1_0-prod.backup" mad_dev`

## pg_restore Examples

- `pg_restore --host localhost --port 5432 --username postgres --dbname mad /mnt/data/load/data/sfmad_final_20091213.backup > restore.out 2>&1`
- `pg_restore --host localhost --port 5432 --username postgres --dbname mad_156 /mnt/data/load/data/mad_20100929_1444.dump > restore.out 2>&1`
- `pg_restore --host localhost --port 5432 --username postgres --dbname mad /mnt/data/load/data/mad_20100929_1444.dump > restore.out 2>&1`
- `pg_restore --host localhost --port 5432 --username postgres --dbname dev /mnt/data/load/data/mad-1_0-beta-1_patch.backup > restore.out`
- `pg_restore --host localhost --port 5432 --username postgres --dbname mad /mnt/data/load/data/mad-1_0-prod.backup`

Alternatively, if you have an ASCII SQL dump file:

- `su - postgres`
- `psql dev < /mnt/data/load/data/mad-1_0-beta-1_patch.backup`

##### END OF FILE
