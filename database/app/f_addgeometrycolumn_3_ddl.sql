-- Function: addgeometrycolumn(character varying, character varying, integer, character varying, integer)

-- DROP FUNCTION addgeometrycolumn(character varying, character varying, integer, character varying, integer);

CREATE OR REPLACE FUNCTION addgeometrycolumn(character varying, character varying, integer, character varying, integer)
  RETURNS text AS
$BODY$
DECLARE
	ret  text;
BEGIN
	SELECT AddGeometryColumn('','',$1,$2,$3,$4,$5) into ret;
	RETURN ret;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE STRICT
  COST 100;
ALTER FUNCTION addgeometrycolumn(character varying, character varying, integer, character varying, integer) OWNER TO postgres;
