﻿
select p.blk_lot, count(*)
from 
	parcels p,
	address_x_parcels axp,
	addresses a
where 1=1
and p.parcel_id = axp.parcel_id
and axp.address_id = a.address_id
and axp.retire_tms is null
and a.retire_tms is null
group by p.blk_lot
having count(*) > 1
order by count(*) desc;

