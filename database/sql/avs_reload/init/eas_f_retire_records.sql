
-- DROP FUNCTION IF EXISTS avs_reload.retire_records();

CREATE OR REPLACE FUNCTION avs_reload.retire_records()
  RETURNS text AS
$BODY$
DECLARE

    _address_base_id    int;
    _address_base_ids   int[];
    _address_id         int;
    _unit_address_ids   int[];
    _addr_number        int;
    _addr_unit          varchar(10);
    _addr_streetname    character varying(100);
    _addr_streetsuffix  character varying(14);
    _addr_zip_spatial   character varying(14);
    _unq_adds_id        int;
    _subaddr_to_id      int;
    _current_tms        timestamp without time zone = now();
    _block_lot          character varying(10);
    _parcel_id          int;
    _message character varying(256);
    _row_count          int;

BEGIN

    ----- base address logic in one place
    update avs_reload.addr_retires ai
    set addr_base_flg = false;

    update avs_reload.addr_retires ai
    set addr_base_flg = true
    where (
        unq_adds_id::int = coalesce(subaddr_to::int, -1)
        or
        subaddr_to is null
    );

    update avs_reload.axp_retires ai
    set addr_base_flg = false;

    update avs_reload.axp_retires ai
    set addr_base_flg = true
    where (
        unq_adds_id::int = coalesce(subaddr_to::int, -1)
        or
        subaddr_to is null
    );
    -----


    ----- Retire the base addresses.
    declare cursorBaseAddress cursor for
        select 
            ar.addr_number::int,
            nullif(ar.addr_unit, '-1'),
            ar.addr_streetname,
            nullif(ar.addr_streetsuffix, '[NULL]'), -- you gotta love this - todo - is this valid here (was a copy-paste)
            ar.unq_adds_id::int,
            a.address_base_id,
            a.address_id
        from avs_reload.addr_retires as ar
        left outer join addresses a on (ar.unq_adds_id = a.unq_adds_id)
        where 1=1
        and ar.addr_base_flg = true
        and ar.exception_text is null
        and ar.success_text is null;
    begin
        open cursorBaseAddress;
        loop
            fetch cursorBaseAddress into _addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, _unq_adds_id, _address_base_id, _address_id;
            if not found then
                exit;
            end if;
            BEGIN

                _message = 'retiring base address';

                -- If the left outer join fails, we look for equivalents.
                if _address_base_id is null then
                    select into _address_base_ids public._eas_find_equiv_base_address_smart(_addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, null);
                    if array_upper(_address_base_ids, 1) is not null then
                        -- found 1 or more equivalents
                        if array_upper(_address_base_ids, 1) > 1 then
                            raise exception 'multiple equivalent records found';
                        end if;
                        _address_base_id = _address_base_ids[1];
                        select address_id into _address_id from addresses where address_base_id = _address_base_id and address_base_flg = true;
                        _message = 'retiring equivalent base address';
                    end if;
                end if;

                if _address_base_id is null then
                    raise exception 'unable to retire base address - no match found';
                end if;

                perform avs_reload.retire_base_address(_address_base_id, _current_tms);

                -- base address
                update avs_reload.addr_retires set success_text = _message where unq_adds_id = _unq_adds_id and addr_base_flg = true;
                -- AXPs for base address
                update avs_reload.axp_retires set success_text = 'part of cascade retire' where subaddr_to = _unq_adds_id and addr_base_flg = true;
                -- units
                update avs_reload.addr_retires set success_text = 'part of cascade retire' where subaddr_to = _unq_adds_id and addr_base_flg = false;
                -- AXPs for units
                update avs_reload.axp_retires set success_text = 'part of cascade retire' where subaddr_to = _unq_adds_id and addr_base_flg = false;

            EXCEPTION
                when others then
                    update avs_reload.addr_retires set exception_text = substring(SQLERRM from 1 for 256) where unq_adds_id = _unq_adds_id;
                    update avs_reload.addr_retires set exception_text = 'unable to retire parent base address' where subaddr_to = _unq_adds_id and addr_base_flg = false;
                    update avs_reload.axp_retires set exception_text = 'unable to retire parent unit address' where subaddr_to = _unq_adds_id and addr_base_flg = false;
            END;
        end loop;
        close cursorBaseAddress;
    end;
    -----


    ----- remaining unit addresses
    declare cursorRemainingUnitAddresses cursor for
        select distinct
            ar1.unq_adds_id,
            ar1.subaddr_to,
            ar1.addr_number,
            ar1.addr_unit,
            ar1.addr_streetname,
            nullif(ar1.addr_streetsuffix, '[NULL]'), -- you gotta love this
            a.address_id
        from avs_reload.addr_retires ar1
        left outer join public.addresses a on a.unq_adds_id = ar1.unq_adds_id::int
        where 1=1
        and ar1.addr_base_flg = false
        and ar1.exception_text is null
        and ar1.success_text is null;
    begin
        open cursorRemainingUnitAddresses;
        loop
            fetch cursorRemainingUnitAddresses into _unq_adds_id, _subaddr_to_id, _addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, _address_id;
            if not found then
                exit;
            end if;
            BEGIN
                _message = 'retiring unit address';
                
                -- If the left outer join fails, we look an equivalents.
                if _address_id is null then

                    -- First we try to find an equivalent base address.
                    select into _address_base_ids public._eas_find_equiv_base_address_smart(_addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, null);
                    if array_upper(_address_base_ids, 1) is not null then
                        -- found 1 or more equivalents
                        if array_upper(_address_base_ids, 1) > 1 then
                            raise exception 'multiple equivalent records found';
                        end if;
                        _address_base_id = _address_base_ids[1];
                    else
                        raise exception 'no equivalent base address found';
                    end if;
                
                    -- Now we find the correspnding unit address.
                    select into _unit_address_ids public._eas_find_equiv_unit_address(_address_base_id, _addr_unit);
                    if array_upper(_unit_address_ids, 1) is not null then
                        -- found 1 or more equivalents
                        if array_upper(_unit_address_ids, 1) > 1 then
                            raise exception 'multiple equivalent records found';
                        end if;
                        _address_id = _unit_address_ids[1];
                    else
                        raise exception 'no equivalent unit address found for unit address retire';
                    end if;

                    _message = 'retiring equivalent unit address';

                end if;

                perform avs_reload.retire_unit_address(_address_id, _current_tms);
                update avs_reload.addr_retires set success_text = _message where unq_adds_id = _unq_adds_id;
                update avs_reload.axp_retires set success_text = 'part of cascade retire' where unq_adds_id = _unq_adds_id and addr_base_flg = false;

            EXCEPTION
                when others then
                    update avs_reload.addr_retires set exception_text = substring(SQLERRM from 1 for 256) where unq_adds_id = _unq_adds_id;
                    update avs_reload.axp_retires set exception_text = 'parent unit address retirement failed' where unq_adds_id = _unq_adds_id and addr_base_flg = false;
            END;
        end loop;
        close cursorRemainingUnitAddresses;
    end;
    -----


    ----- The remaining AXPs rows have no parent row in the addr_retire table, and _may_ have a matching row in the addresses table.
    declare cursorOrphanedAxpRetires cursor for
        select 
            axpr.unq_adds_id,
            axpr.subaddr_to,
            axpr.blocklot,
            axpr.addr_number,
            axpr.addr_unit,
            axpr.addr_streetname,
            nullif(axpr.addr_streetsuffix, '[NULL]'), -- you gotta love this
            a.address_id,
            p.parcel_id
        from avs_reload.axp_retires as axpr
        left outer join addresses a on a.address_id = axpr.eas_address_id
        left outer join parcels p on p.blk_lot = axpr.blocklot
        where 1=1
        and axpr.exception_text is null
        and axpr.success_text is null;
    begin
        open cursorOrphanedAxpRetires;
        loop
            fetch cursorOrphanedAxpRetires into _unq_adds_id, _subaddr_to_id, _block_lot, _addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, _address_id, _parcel_id;
            if not found then
                exit;
            end if;
            BEGIN

                if _parcel_id is null then
                    raise exception 'unable to retire address parcel link - no matching parcel record';
                end if;

                -- If the left outer join fails, we look for equivalents.
                if _address_id is null then

                    _address_base_id = null;

                    -- try to find an equivalent base address
                    select into _address_base_ids public._eas_find_equiv_base_address_smart(_addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, null);
                    if array_upper(_address_base_ids, 1) is not null then
                        -- found 1 or more equivalents
                        if array_upper(_address_base_ids, 1) > 1 then
                            raise exception 'multiple equivalent records found';
                        end if;
                        _address_base_id = _address_base_ids[1];
                    else
                        raise exception 'no equivalent base address found for axp retire';
                    end if;

                     
                    -- Find the correct address_id.
                    -- To to this we need to know: does the AXP belong to the base or the unit?
                    if _unq_adds_id = coalesce(_subaddr_to_id, -1) then
                        -- base
                        select address_id into _address_id from addresses where address_base_id = _address_base_id and address_base_flg = true;
                    else
                        -- unit
                        select into _unit_address_ids public._eas_find_equiv_unit_address(_address_base_id::int, _addr_unit::text);
                        if array_upper(_unit_address_ids, 1) is not null then
                            -- found 1 or more equivalents
                            if array_upper(_unit_address_ids, 1) > 1 then
                                raise exception 'multiple equivalent records found';
                            end if;
                            _address_id = _unit_address_ids[1];
                        else
                            raise exception 'no equivalent unit address found for axp retire';
                        end if;
                    end if;

                end if;

                -- At this point we should be able to retire.
                
                update address_x_parcels
                set 
                    retire_change_request_id = 0,
                    last_change_tms = _current_tms,
                    retire_tms = _current_tms
                where address_id = _address_id
                and parcel_id = _parcel_id;

                GET DIAGNOSTICS _row_count = ROW_COUNT;

                if _row_count > 0 then
                    _message = 'retired address parcel link';
                else
                    _message = 'no address parcel link found';
                end if;

                update avs_reload.axp_retires set success_text = _message where unq_adds_id = _unq_adds_id and blocklot = _block_lot;

            EXCEPTION
                when others then
                    update avs_reload.axp_retires set exception_text = substring(SQLERRM from 1 for 256) where unq_adds_id = _unq_adds_id and blocklot = _block_lot;
            END;
        end loop;
        close cursorOrphanedAxpRetires;
    end;
    -----


    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION avs_reload.retire_records() OWNER TO postgres;
