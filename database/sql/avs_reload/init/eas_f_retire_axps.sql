-- Function: avs_reload.retire_axps()

DROP FUNCTION IF EXISTS avs_reload.retire_axps(
    _address_id int,
    _current_tms timestamp without time zone
);

CREATE OR REPLACE FUNCTION avs_reload.retire_axps(
    _address_id int,
    _current_tms timestamp without time zone
)
RETURNS void AS
$BODY$
DECLARE 
    _axp_id int;
BEGIN

    declare cursorAddressXParcel cursor for
        select id
        from public.address_x_parcels
        where address_id = _address_id;
    begin
        open cursorAddressXParcel;
        loop
            fetch cursorAddressXParcel into _axp_id;
            if not found then
                exit;
            end if;

            update address_x_parcels
            set 
                retire_change_request_id = 0,
                last_change_tms = _current_tms,
                retire_tms = _current_tms
            where id = _axp_id;
        end loop;
        close cursorAddressXParcel;
    end;

    return;


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs_reload.retire_axps(
    _address_id int,
    _current_tms timestamp without time zone
)
OWNER TO postgres;

