CREATE OR REPLACE FORCE VIEW "ADDRESS_PROCESSING"."VW_ADDR_INSERTS" AS
  SELECT a."ADDR_NUMBER",
    a."ADDR_UNIT",
    a."ADDR_STREETNAME",
    a."ADDR_STREETSUFFIX",
    a."ADDR_ZIP_SPATIAL",
    a."ADDR_ZIP_ORIG",
    a."ADDR_CONCATENATE",
    a."SRC_SFADDRS_ID",
    a."SRC_DWG_ID",
    a."SRC_DBIAVS_ID",
    a."SRC_DPW_ID",
    a."SRC_PLANNING_ID",
    a."SUBADDR_TO",
    a."GEOMETRY",
    a."ZONE_ID",
    a."XCOORD",
    a."YCOORD",
    a."SRC_DBIAVS2_ID",
    a."UNQ_ADDS_ID",
    a."EAS_ADDRESS_ID",
    a."CNN_FINAL",
    a."ADDR_STREETNAME_STD",
    a."ADDR_STREETSUFFIX_STD",
    b.CNN,
    b.dist_in_meters,
    b.addr_range_dif,
    b.jws_streetname,
    b.jws_suffix,
    b.pick_id
  FROM vw_unq_adds_to_process a
  LEFT OUTER JOIN unq_adds_nn b
  ON a.unq_adds_id = b.unq_adds_id
  WHERE 1 = 1
  AND b.pick_flag = 'Y'
  OR b.pick_flag IS NULL;

insert into user_sdo_geom_metadata (table_name, column_name, diminfo, srid)
select 'VW_ADDR_INSERTS', 'GEOMETRY', a.diminfo, a.srid
from user_sdo_geom_metadata a
where a.table_name='UNQ_ADDS'
and not exists (
  select 1
  from user_sdo_geom_metadata
  where table_name = 'VW_ADDR_INSERTS'
  and column_name = 'GEOMETRY'
);
commit;
