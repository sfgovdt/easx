
-- todo - discuss the following
-- 152 m is approximately 500 feet
--
-- todo - discuss jws threshhold
-- A value of 91 will provide matches for lots of rows such as 5th <-> 05th.
-- But it seems like we should handle the special case of these oddly named numbered street separately.

CREATE OR REPLACE PROCEDURE select_street_segment IS

    v_unq_adds_id number;
    v_test_char character;

    cursor cur is
    select unq_adds_id
    from unq_adds_nn
    where unq_adds_id in (
        select unq_adds_id
        from unq_adds_nn
        group by unq_adds_id, exclude_flag
        having exclude_flag = 'N'
    );


BEGIN

    update unq_adds_nn
    set exclude_flag = 'N',
        pick_flag = 'N',
        pick_id = null;


    ----- BEGIN - exclude the set if there are no streets within 500 feet
    -- 152 m is approx 500 f
    --    select *
    --    from unq_adds_nn
    --    where unq_adds_id in (
    --      select unq_adds_id
    --      from unq_adds_nn
    --      group by unq_adds_id
    --      having min(dist_in_meters) > 152
    --    );

    update unq_adds_nn
    set exclude_flag = 'Y'
    where unq_adds_id in (
      select unq_adds_id
      from unq_adds_nn
      group by unq_adds_id
      having min(dist_in_meters) > 152
    )
    and exclude_flag <> 'Y';
    ----- END - exclude the set if there are no streets within 500 feet


    ----- BEGIN - exclude the set if the JWS_streetname exceed a arbitrary threshhold.
    -- Based on visual inspection, at a JWS of 90, we get 99 good matches with 1 bad.
    -- This threshhold excludes about 28k rows from unq_adds_nn (or about 2800 potential matches).
    --select *
    --from unq_adds_nn
    --where unq_adds_id in (
    --  select unq_adds_id
    --  from unq_adds_nn
    --  group by unq_adds_id
    --  having max (jws_streetname) = 90
    --) order by unq_adds_id asc, jws_streetname desc;

    update unq_adds_nn
    set exclude_flag = 'Y'
    where unq_adds_id in (
      select unq_adds_id
      from unq_adds_nn
      group by unq_adds_id
      having max(jws_streetname) < 90
    )
    and exclude_flag <> 'Y';
    ----- END - exclude the set if the street name matches exceed a arbitrary threshhold.


    open cur;

        loop
            -- The most precise matches are tried first.
            -- If a query updates, the rest of the queries are skipped and we go to the next set.

            -- debug: this select shows sets where a segment has not been picked.
            /*
            select *
            from unq_adds_nn
            where unq_adds_id in (
               select unq_adds_id
                from unq_adds_nn
                where pick_flag = 'N'
                group by unq_adds_id, pick_flag
                having count(*) = 10
            )
            and rownum < 1000
            order by unq_adds_id asc;
            */

            fetch cur into v_unq_adds_id;
            exit when cur%NOTFOUND;

            update unq_adds_nn
            set pick_flag = 'Y',
                pick_id = 'A'
            where jws_streetname = 100
            and jws_suffix = 100
            and addr_range_dif = 0
            and unq_adds_id = v_unq_adds_id
            and dist_in_meters = (
              select min(dist_in_meters)
              from unq_adds_nn
              where jws_streetname = 100
              and jws_suffix = 100
              and addr_range_dif = 0
              and unq_adds_id = v_unq_adds_id
            )
            and rownum = 1;
            if SQL%ROWCOUNT = 1 then
                continue;
            end if;


            update unq_adds_nn
            set pick_flag = 'Y',
                pick_id = 'B'
            where jws_streetname = 100
            and jws_suffix = 100
            and addr_range_dif > 0
            and unq_adds_id = v_unq_adds_id
            and dist_in_meters = (
              select min(dist_in_meters)
              from unq_adds_nn
              where jws_streetname = 100
              and jws_suffix = 100
              and addr_range_dif > 0
              and unq_adds_id = v_unq_adds_id
            )
            and rownum = 1;
            if SQL%ROWCOUNT = 1 then
                continue;
            end if;


            -- suffix does not match
            update unq_adds_nn
            set pick_flag = 'Y',
                pick_id = 'C'
            where jws_streetname = 100
            and jws_suffix < 100
            and addr_range_dif = 0
            and unq_adds_id = v_unq_adds_id
            and dist_in_meters = (
              select min(dist_in_meters)
              from unq_adds_nn
              where jws_streetname = 100
              and jws_suffix < 100
              and addr_range_dif = 0
              and unq_adds_id = v_unq_adds_id
            )
            and rownum = 1;
            if SQL%ROWCOUNT = 1 then
                continue;
            end if;


            -- suffix does not match and addr_range_dif > 0
            update unq_adds_nn
            set pick_flag = 'Y',
                pick_id = 'D'
            where jws_streetname = 100
            and jws_suffix < 100
            and addr_range_dif > 0
            and unq_adds_id = v_unq_adds_id
            and dist_in_meters = (
              select min(dist_in_meters)
              from unq_adds_nn
              where jws_streetname = 100
              and jws_suffix < 100
              and addr_range_dif > 0
              and unq_adds_id = v_unq_adds_id
            )
            and rownum = 1;
            if SQL%ROWCOUNT = 1 then
                continue;
            end if;


            -- do not proceed to a fuzzy streetname match is this is a numeric streetname
            select substr(ADDR_STREETNAME, 1, 1)
            into v_test_char
            from unq_adds_nn
            where unq_adds_id = v_unq_adds_id
            and rownum = 1;
            if isNumber(v_test_char) = 1 then
                continue;
            end if;



            -- fuzzy streetname match
            update unq_adds_nn
            set pick_flag = 'Y',
                pick_id = 'E'
            where jws_streetname between 90 and 99
            and jws_suffix = 100
            and addr_range_dif = 0
            and unq_adds_id = v_unq_adds_id
            and dist_in_meters = (
              select min(dist_in_meters)
              from unq_adds_nn
              where jws_streetname between 90 and 99
              and jws_suffix = 100
              and addr_range_dif = 0
              and unq_adds_id = v_unq_adds_id
            )
            and rownum = 1;
            if SQL%ROWCOUNT = 1 then
                continue;
            end if;

            -- fuzzy streetname match with no constraint on suffix or addr range diff
            update unq_adds_nn
            set pick_flag = 'Y',
                pick_id = 'F'
            where jws_streetname between 90 and 99
            and unq_adds_id = v_unq_adds_id
            and dist_in_meters = (
              select min(dist_in_meters)
              from unq_adds_nn
              where jws_streetname between 90 and 99
              and unq_adds_id = v_unq_adds_id
            )
            and rownum = 1;
            if SQL%ROWCOUNT = 1 then
                continue;
            end if;



        end loop;


    close cur;

    commit;

END select_street_segment;

