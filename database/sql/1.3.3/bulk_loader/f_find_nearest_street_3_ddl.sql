
DROP FUNCTION IF EXISTS bulkloader.find_nearest_street_3(
    _street_name    character varying(64),
    _street_type    character varying(32),
    _use_street_type boolean,
    _parcel_id      int,
    _std_end_date   character varying(10),
    _category       character varying(5),
    out _street_segment_id int
);

CREATE OR REPLACE FUNCTION bulkloader.find_nearest_street_3(
    _street_name    character varying(64),
    _street_type    character varying(32),
    _use_street_type boolean,
    _parcel_id      int,
    _std_end_date   character varying(10),
    _category       character varying(5),
    out _street_segment_id int
)
  RETURNS int AS
$BODY$
DECLARE

    _row_count int;

BEGIN

    -- NOTE - We are on the verge of needing dynamic sql construction here because there is some repeated code.

    ----- Try to find and return the nearest un-retired most recent street segment.
    select street_segment_id into _street_segment_id
    from bulkloader.vw_streets_nearest
    where base_street_name = _street_name
    and category = _category
    and (
        (
            (
                (_street_type is not null and street_type = _street_type)
                or
                (_street_type is null and street_type is null)
            ) and _use_street_type
        ) 
        or not _use_street_type
    )
    and parcel_id = _parcel_id
    and date_dropped is null
    order by distance asc, date_added desc
    limit 1;

    if _street_segment_id is not null then
        return;
    end if;
    -----


    ----- Try to find and return the nearest retired street segment.
    -- Pick the segment whose date_dropped value most nearly precedes the address's std_end_date.
    select street_segment_id into _street_segment_id
    from bulkloader.vw_streets_nearest
    where base_street_name = _street_name
    and category = _category
    and (
        (
            (
                (_street_type is not null and street_type = _street_type)
                or
                (_street_type is null and street_type is null)
            ) and _use_street_type
        ) 
        or not _use_street_type
    )
    and parcel_id = _parcel_id
    and date_dropped is not null
    and date_dropped < _std_end_date::date
    order by distance asc, date_dropped desc
    limit 1;

    if _street_segment_id is not null then
        return;
    end if;
    -----


    ----- Try to find and return the nearest retired street segment.
    -- Pick the segment with the greatest date_dropped value.
    select street_segment_id into _street_segment_id
    from bulkloader.vw_streets_nearest
    where base_street_name = _street_name
    and category = _category
    and (
        (
            (
                (_street_type is not null and street_type = _street_type)
                or
                (_street_type is null and street_type is null)
            ) and _use_street_type
        ) 
        or not _use_street_type
    )
    and parcel_id = _parcel_id
    and date_dropped is not null
    order by distance asc, date_dropped desc
    limit 1;
    -----





END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION bulkloader.find_nearest_street_3(
    _street_name    character varying(64),
    _street_type    character varying(32),
    _use_street_type boolean,
    _parcel_id      int,
    _std_end_date   character varying(10),
    _category       character varying(5),
    out _street_segment_id int
) OWNER TO postgres;
