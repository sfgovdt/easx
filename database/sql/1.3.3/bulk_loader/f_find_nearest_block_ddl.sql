
DROP FUNCTION IF EXISTS bulkloader.find_nearest_block(
    _bulkloader_id         int,
    out _block             character varying(12) 
);

CREATE OR REPLACE FUNCTION bulkloader.find_nearest_block(
    _bulkloader_id         int,
    out _block             character varying(12)
)
  RETURNS character varying(12) AS
$BODY$
DECLARE

BEGIN

        select  block_num into _block from bulkloader.blocks_nearest where address_extract_id = _bulkloader_id order by distance limit 1;

END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION bulkloader.find_nearest_block(
    _bulkloader_id         int,
    out _block             character varying(12) 
) OWNER TO postgres;
