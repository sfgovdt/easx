
-- DROP VIEW vw_change_request;
-- select * from vw_change_request;

CREATE OR REPLACE VIEW vw_change_request AS

select 
    cr.change_request_id,
    cr.cr_change_request_id,
    cr.create_tms,
    crcr.requestor_user_id,
    crcr.requestor_comment,
    crcr.reviewer_user_id,
    crcr.reviewer_comment,
    au_req.first_name || ' ' ||  au_req.last_name as requestor_full_name,
    au_req.email requestor_email,
    au_rev.first_name || ' ' ||  au_rev.last_name as reviewer_full_name,
    au_rev.email reviewer_email
from change_requests cr
join cr_change_requests crcr on (cr.cr_change_request_id = crcr.change_request_id)
join auth_user au_req on (au_req.id = crcr.requestor_user_id)
join auth_user au_rev on (au_rev.id = crcr.reviewer_user_id)
order by cr.create_tms;

ALTER TABLE vw_change_request OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_change_request TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_change_request TO django;

