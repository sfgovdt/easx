-- Function: _eas_validate_addresses_after_etl(character varying)

-- DROP FUNCTION _eas_validate_addresses_after_etl(character varying);

CREATE OR REPLACE FUNCTION _eas_validate_addresses_after_etl(validation_scope character varying)
  RETURNS text AS
$BODY$
DECLARE

    -- There is probably some _simple_ and cool way to handle domain constants - maybe _you_ can show us?
    -- select * from d_invalid_address_types;
    PARCEL_RETIRED int = 1; -- parcel is retired
    STREET_RETIRED int = 5; -- street is retired
    ADDRESS_PARCEL_DISTANCE_GT_500 int = 4;
    ADDRESS_NUMBER_NOT_IN_RANGE int = 6; -- address number not with within street range

BEGIN


    if VALIDATION_SCOPE is null
       or
       VALIDATION_SCOPE not in ('PARCELS', 'STREETS') then
            raise exception 'expected one of (PARCELS, STREETS) but got %', coalesce(VALIDATION_SCOPE, 'NULL');
    end if;


    delete from invalid_addresses
    where invalid_type_id = STREET_RETIRED
    and VALIDATION_SCOPE = 'STREETS';

    delete from invalid_addresses
    where invalid_type_id in (PARCEL_RETIRED, ADDRESS_PARCEL_DISTANCE_GT_500, ADDRESS_NUMBER_NOT_IN_RANGE)
    and VALIDATION_SCOPE = 'PARCELS';

	-- parcel is retired
	insert into invalid_addresses (address_id, invalid_type_id, message)
    select axp.address_id, PARCEL_RETIRED, 'apn: ' || p.blk_lot
    from
        address_x_parcels axp,
        parcels p
    where 1=1
    and axp.parcel_id = p.parcel_id
    and axp.retire_tms is null
    and p.date_map_drop is not null
    and VALIDATION_SCOPE = 'PARCELS';



    -- street is retired
    insert into invalid_addresses (address_id, invalid_type_id, message)
    select distinct a.address_id, STREET_RETIRED, 'street: ' || sn.full_street_name
    from
        addresses a,
        address_base ab,
        street_segments ss,
        streetnames sn
    where ss.street_segment_id = ab.street_segment_id
    and ab.address_base_id = a.address_base_id
    and a.address_base_flg = 'TRUE'
    and ab.street_segment_id = sn.street_segment_id
    and sn.category = 'MAP'
    and ss.date_dropped is not null
    and ab.retire_tms is null
    and VALIDATION_SCOPE = 'STREETS';


    -- parcel is beyond 500 feet of the address point.
    insert into invalid_addresses (address_id, invalid_type_id, message)
    select  distinct a.address_id, ADDRESS_PARCEL_DISTANCE_GT_500, 'apn:' || p.blk_lot
    from
        address_base ab,
        addresses a,
        address_x_parcels axp,
        parcels p
    where ab.address_base_id = a.address_base_id
    and a.address_id = axp.address_id
    and axp.parcel_id = p.parcel_id
    and a.address_base_flg = true
    and ab.retire_tms is null
    and a.retire_tms is null
    and axp.retire_tms is null
    and p.date_map_drop is null
    and st_distance(p.geometry, ab.geometry) > 500
    and VALIDATION_SCOPE = 'PARCELS';

    -- address number not with within street range
    insert into invalid_addresses (address_id, invalid_type_id, message)
    select  address_id,
            ADDRESS_NUMBER_NOT_IN_RANGE,
            'min: ' || _eas_get_address_range_min(l_f_add, l_t_add, r_f_add, r_t_add)::text || ' max: ' || _eas_get_address_range_max(l_f_add, l_t_add, r_f_add, r_t_add)::text
    from vw_base_addresses
    where base_address_num NOT BETWEEN _eas_get_address_range_min(l_f_add, l_t_add, r_f_add, r_t_add) AND _eas_get_address_range_max(l_f_add, l_t_add, r_f_add, r_t_add)
    and VALIDATION_SCOPE = 'PARCELS';


    return 'success!';


END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_validate_addresses_after_etl(character varying) OWNER TO eas_dbo;
