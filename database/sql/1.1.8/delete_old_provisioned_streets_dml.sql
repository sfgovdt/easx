----- delete some old provisioned street records
delete from streetnames_staging where bsm_streetnameid = 9000;
delete from streetnames where bsm_streetnameid = 9000;

delete
from xmit_queue
where address_x_parcel_history_id in (
	select id
	from address_x_parcels_history
	where address_id in (
		select address_id
		from addresses_history
		where address_base_id in (
			select address_base_id
			from address_base_history
			where street_segment_id in (
			    18906,
			    18907,
			    18908,
			    18909,
			    18910
			)
		)
	)
);
delete
from address_x_parcels_history
where address_id in (
	select address_id
	from addresses_history
	where address_base_id in (
		select address_base_id
		from address_base_history
		where street_segment_id in (
		    18906,
		    18907,
		    18908,
		    18909,
		    18910
		)
	)
);
delete
from addresses_history
where address_base_id in (
	select address_base_id
	from address_base_history
	where street_segment_id in (
	    18906,
	    18907,
	    18908,
	    18909,
	    18910
	)
);
delete
from address_base_history
where street_segment_id in (
    18906,
    18907,
    18908,
    18909,
    18910
);
delete
from street_address_ranges
where street_segment_id in (
    18906,
    18907,
    18908,
    18909,
    18910
);
delete
from streetnames
where street_segment_id in (
    18906,
    18907,
    18908,
    18909,
    18910
);
delete
from street_segments
where street_segment_id in (
    18906,
    18907,
    18908,
    18909,
    18910
);
