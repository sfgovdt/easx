
-- Table: public.avs_addresses

-- DROP TABLE public.avs_addresses;

CREATE TABLE public.avs_addresses
(
  id double precision,
  blocklot character varying(10),
  block character varying(6),
  lot character varying(5),
  structure_number character varying(3),
  address_kind character varying(11),
  address_type character varying(11),
  street_number double precision,
  street_number_sfx character(1),
  avs_street_name character varying(26),
  avs_street_sfx character varying(7),
  avs_street_type character varying(17),
  avs_street_status character varying(10),
  unit character varying(5),
  unit_sfx character varying(11),
  end_date character varying(10),
  std_street_type character varying(32),
  std_end_date character varying(10),
  std_unit character varying(10),
  std_street_number_sfx character(1),
  street_segment_id integer,
  zone_id integer,
  address_base_id integer,
  address_id integer,
  address_x_parcel_id integer,
  exception_text character varying(256),
  sql character varying(1024)
)
WITH (
  OIDS=TRUE
);
ALTER TABLE public.avs_addresses OWNER TO eas_dbo;

-- Index: public.avs_idx_address_base_id

-- DROP INDEX public.avs_idx_address_base_id;

CREATE INDEX avs_idx_address_base_id
  ON public.avs_addresses
  USING btree
  (address_base_id);

-- Index: public.avs_idx_address_id

-- DROP INDEX public.avs_idx_address_id;

CREATE INDEX avs_idx_address_id
  ON public.avs_addresses
  USING btree
  (address_id);

-- Index: public.avs_idx_address_type

-- DROP INDEX public.avs_idx_address_type;

CREATE INDEX avs_idx_address_type
  ON public.avs_addresses
  USING btree
  (address_type);

-- Index: public.avs_idx_address_x_parcel_id

-- DROP INDEX public.avs_idx_address_x_parcel_id;

CREATE INDEX avs_idx_address_x_parcel_id
  ON public.avs_addresses
  USING btree
  (address_x_parcel_id);

-- Index: public.avs_idx_avs_street_name

-- DROP INDEX public.avs_idx_avs_street_name;

CREATE INDEX avs_idx_avs_street_name
  ON public.avs_addresses
  USING btree
  (avs_street_name);

-- Index: public.avs_idx_avs_street_type

-- DROP INDEX public.avs_idx_avs_street_type;

CREATE INDEX avs_idx_avs_street_type
  ON public.avs_addresses
  USING btree
  (avs_street_type);

-- Index: public.avs_idx_blocklot

-- DROP INDEX public.avs_idx_blocklot;

CREATE INDEX avs_idx_blocklot
  ON public.avs_addresses
  USING btree
  (blocklot);

-- Index: public.avs_idx_end_date

-- DROP INDEX public.avs_idx_end_date;

CREATE INDEX avs_idx_end_date
  ON public.avs_addresses
  USING btree
  (end_date);

-- Index: public.avs_idx_exception_text

-- DROP INDEX public.avs_idx_exception_text;

CREATE INDEX avs_idx_exception_text
  ON public.avs_addresses
  USING btree
  (exception_text);

-- Index: public.avs_idx_std_end_date

-- DROP INDEX public.avs_idx_std_end_date;

CREATE INDEX avs_idx_std_end_date
  ON public.avs_addresses
  USING btree
  (std_end_date);

-- Index: public.avs_idx_std_street_type

-- DROP INDEX public.avs_idx_std_street_type;

CREATE INDEX avs_idx_std_street_type
  ON public.avs_addresses
  USING btree
  (std_street_type);

-- Index: public.avs_idx_street_number

-- DROP INDEX public.avs_idx_street_number;

CREATE INDEX avs_idx_street_number
  ON public.avs_addresses
  USING btree
  (street_number);

-- Index: public.avs_unq_idx_id

-- DROP INDEX public.avs_unq_idx_id;

CREATE UNIQUE INDEX avs_unq_idx_id
  ON public.avs_addresses
  USING btree
  (id);


-- truncate public.avs_addresses;
INSERT INTO public.avs_addresses
select  *
from avs.avs_addresses;


DROP SCHEMA avs cascade;
