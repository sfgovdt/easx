-- temporary patch for http://code.google.com/p/eas/issues/detail?id=451

/*
select *
from streetnames_staging
where full_street_name = 'MISSION BAY BLVD SOUTH'
and category = 'MAP';

select *
from streetnames_staging
where full_street_name = 'MISSION BAY BLVD NORTH'
and category = 'MAP';

select *
from streetnames_staging
where seg_cnn in (
	3362000,
	3363000,
	3364000,
	3365000,
	3366000,
	3367000,
	3368000,
	3369000,
	3370000,
	3371000
)

*/

update streetnames_staging sns1
set
	base_street_name = 'MISSION BAY BLVD SOUTH',
	street_type = null,
	full_street_name = 'MISSION BAY BLVD SOUTH'
where sns1.base_street_name = 'MISSION BAY'
and sns1.street_type = 'BLVD'
and sns1.category = 'MAP'
and exists (
	select 1
	from streetnames_staging sns2
	where sns2.base_street_name = 'MISSION BAY BLVD SOUTH'
	and sns2.street_type is null
	and sns2.category = 'ALIAS'
	and sns2.seg_cnn = sns1.seg_cnn
);

update streetnames_staging sns1
set
	base_street_name = 'MISSION BAY BLVD NORTH',
	street_type = null,
	full_street_name = 'MISSION BAY BLVD NORTH'
where sns1.base_street_name = 'MISSION BAY'
and sns1.street_type = 'BLVD'
and sns1.category = 'MAP'
and exists (
	select 1
	from streetnames_staging sns2
	where sns2.base_street_name = 'MISSION BAY BLVD NORTH'
	and sns2.street_type is null
	and sns2.category = 'ALIAS'
	and sns2.seg_cnn = sns1.seg_cnn
);


update streetnames_staging sns1
set
	base_street_name = 'BUENA VISTA AVE EAST',
	street_type = null,
	full_street_name = 'BUENA VISTA AVE EAST'
where sns1.base_street_name = 'BUENA VISTA'
and sns1.street_type = 'AVE'
and sns1.category = 'MAP'
and seg_cnn in (
	3362000, 
	3363000,
	3364000,
	3365000,
	3366000
);


update streetnames_staging sns1
set
	base_street_name = 'BUENA VISTA AVE WEST',
	street_type = null,
	full_street_name = 'BUENA VISTA AVE WEST'
where sns1.base_street_name = 'BUENA VISTA'
and sns1.street_type = 'AVE'
and sns1.category = 'MAP'
and seg_cnn in (
	3367000,
	3368000,
	3369000,
	3370000,
	3371000
);

