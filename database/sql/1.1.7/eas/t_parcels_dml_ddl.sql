-- these changes precipitated by http://code.google.com/p/eas/issues/detail?id=805

alter table parcels alter blk_lot set not null;
alter table parcels alter block_num set not null;
alter table parcels alter lot_num set not null;

delete
from parcels
where (blk_lot = '5650054' and block_num = '5650' and lot_num = '034')
or (blk_lot = '5650053' and block_num = '5650' and lot_num = '033');

update parcels
set date_map_drop = null
where blk_lot = '8705009'
and block_num = '8705'
and lot_num = '009'
and date_map_drop is not null;

ALTER TABLE parcels ADD CONSTRAINT blk_lot CHECK (blk_lot = (block_num||lot_num));
