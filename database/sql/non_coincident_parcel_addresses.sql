﻿-- addresses with parcels that are not underneath the address point

-- base level
-- count = 1626
SELECT distinct
	ab.base_address_num,
	sn.base_street_name,
	sn.street_type
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
and ab.retire_tms is null
and a.retire_tms is null
and axp.retire_tms is null
and not st_intersects(ab.geometry, p.geometry)
order by
	sn.base_street_name,
	sn.street_type,
	ab.base_address_num;


-- unit level
-- count = 2668
SELECT distinct
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
	sn.street_type
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
and ab.retire_tms is null
and a.retire_tms is null
and axp.retire_tms is null
and not st_intersects(ab.geometry, p.geometry)
order by
	sn.base_street_name,
	sn.street_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;


-- parcel level
-- count = 3019
SELECT
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
	sn.street_type,
	p.blk_lot
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
and ab.retire_tms is null
and a.retire_tms is null
and axp.retire_tms is null
and p.date_map_drop is null
and not st_intersects(ab.geometry, p.geometry)
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;
