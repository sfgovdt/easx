
/*

select * 
from cleanup_address_unit_dupes 
--where flag = '0'
order by unq_unit_addr;

*/

-- select _eas_cleanup_dedupe_unit_addresses();


SELECT                  -- A flattened view of addresses.
	ab.address_base_id,
	a.address_base_flg,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
    p.parcel_id,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    '-----',
    caud.*
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
inner join cleanup_address_unit_dupes caud on caud.address_base_id = a.address_base_id and caud.address_id = a.address_id
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
order by
	ab.base_address_num,
	ss.st_name,
	ss.st_type,
	a.address_base_flg desc,
	ab.base_address_suffix,
	a.unit_num;


/* should be zero afterwards

select  count(*)
from addresses a, 
		(select
		address_base_id, 
		unit_num,
		address_base_id||coalesce(unit_num,'') as join1,
		count(*) as cnt_of_dups
		from addresses
		where retire_tms is null
		group by 
		address_base_id, 
		unit_num
		having count(*) > 1
		order by address_base_id) b
where
a.address_base_id||coalesce(a.unit_num,'') = join1

*/
