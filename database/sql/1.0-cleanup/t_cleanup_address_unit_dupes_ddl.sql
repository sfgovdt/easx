-- drop table cleanup_address_unit_dupes;
create table cleanup_address_unit_dupes as
select 
    a.address_id,
    a.address_base_id,
    a.unit_num,
    a.address_base_flg,
    b.join1 as unq_unit_addr,
    b.cnt_of_dups,
    (select count(*) from address_x_parcels where address_id=a.address_id) as axp_records_cnt,
    '0'::character varying (1) as flag
from addresses a, 
		(select
            address_base_id,
            unit_num,
            address_base_id||'.'||coalesce(unit_num,'') as join1,
            count(*) as cnt_of_dups
		from addresses
		where retire_tms is null
		group by 
            address_base_id,
            unit_num
		having count(*) > 1
		order by address_base_id) b
where
a.address_base_id||'.'||coalesce(a.unit_num,'') = join1
order by
    a.address_base_id,
    a.unit_num;
