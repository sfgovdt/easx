-- Table: cleanup_conflate_base_addresses

-- DROP TABLE cleanup_conflate_base_addresses;

CREATE TABLE cleanup_conflate_base_addresses
(
  address_base_id integer,
  base_address_prefix character varying(10),
  base_address_num integer,
  base_address_suffix character varying(10),
  geometry geometry,
  base_street_name character varying(60),
  street_type character varying(6),
  message character varying(256),
  processed_flg boolean default false
)
WITH (OIDS=FALSE);
ALTER TABLE cleanup_conflate_base_addresses OWNER TO postgres;
