-----
-- changes to zones table

-- Make some changes to zones so we can execute the ETL.
CREATE SEQUENCE zones_zone_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE zones_zone_id_seq OWNER TO eas_dbo;
GRANT ALL ON TABLE zones_zone_id_seq TO eas_dbo;
GRANT ALL ON TABLE zones_zone_id_seq TO django;


alter table zones
    ALTER COLUMN zone_id SET DEFAULT nextval('zones_zone_id_seq');


-----
-- new table zones staging
-- DROP TABLE zones_staging;

CREATE TABLE zones_staging
(
  zone_id serial NOT NULL,
  zipcode character varying(50),
  jurisdiction character varying(50),
  active_date timestamp without time zone,
  retire_date timestamp without time zone,
  geometry geometry,
  CONSTRAINT zones_staging_pk PRIMARY KEY (zone_id),
  CONSTRAINT enforce_dims_geometry CHECK (ndims(geometry) = 2),
  CONSTRAINT enforce_geotype_geometry CHECK (geometrytype(geometry) = 'MULTIPOLYGON'::text OR geometry IS NULL),
  CONSTRAINT enforce_srid_geometry CHECK (srid(geometry) = 2227)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE zones_staging OWNER TO eas_dbo;
GRANT ALL ON TABLE zones_staging TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE zones_staging TO django;

INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'zones_staging', 'geometry', 2, 2227, 'MULTIPOLYGON'
where not exists ( select 1 from geometry_columns where f_table_schema = 'public' and f_table_name = 'zones_staging');

-----
