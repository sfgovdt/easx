grant all on table django_migrations to django;



CREATE OR REPLACE VIEW vw_change_request AS
select 
    cr.change_request_id,
    cr.cr_change_request_id,
    cr.create_tms,
    crcr.requestor_user_id,
    crcr.requestor_comment,
    crcr.reviewer_user_id,
    crcr.reviewer_comment,
    au_req.first_name || ' ' ||  au_req.last_name as requestor_full_name,
    au_req.email requestor_email,
    au_rev.first_name || ' ' ||  au_rev.last_name as reviewer_full_name,
    au_rev.email reviewer_email
from change_requests cr
join cr_change_requests crcr on (cr.cr_change_request_id = crcr.change_request_id)
join auth_user au_req on (au_req.id = crcr.requestor_user_id)
join auth_user au_rev on (au_rev.id = crcr.reviewer_user_id)
order by cr.create_tms;
ALTER TABLE vw_change_request OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_change_request TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_change_request TO django;



CREATE OR REPLACE FUNCTION _eas_address_x_parcels_before()
  RETURNS trigger AS
$BODY$
DECLARE
    _address_number integer;
    _full_street_name character varying(64);
    _block_lot character varying(10);
    _block_num character varying(10);
    _block_nums character varying(64);
    _distance int;
    _message character varying(255);
BEGIN

    -- All the constraints in one place.

    IF (TG_OP = 'DELETE') THEN
        RAISE EXCEPTION 'delete rejected - address_x_parcels id=% - deletes are not allowed', OLD.id;
    ELSIF (TG_OP = 'UPDATE') THEN

        IF NEW.retire_tms IS NULL THEN
            RAISE EXCEPTION 'update rejected  - address_x_parcels id=% - retire_tms must not be null', NEW.id;
        END IF;
        IF NEW.retire_change_request_id IS NULL THEN
            RAISE EXCEPTION 'update rejected  - address_x_parcels id=% - retire_change_request_id must not be null', NEW.id;
        END IF;
        IF NEW.create_tms > NEW.retire_tms THEN
            RAISE EXCEPTION 'update rejected - address_x_parcels id=% - create_tms is greater than retire_tms', NEW.id;
        END IF;

    ELSE
        -- INSERT

        IF (NEW.retire_tms IS NOT NULL) THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - retire_tms must be null';
        end if;

        -- Do not allow multiple live rows.
        perform 1
        from address_x_parcels
        where address_id = NEW.address_id
        and parcel_id = NEW.parcel_id
        and retire_tms is null;

        IF FOUND THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - existing active row was found';
        END IF;

        -- Do not allow overlapping date ranges.
        perform 1
        from address_x_parcels
        where address_id = NEW.address_id
        and parcel_id = NEW.parcel_id
        and retire_tms > NEW.create_tms;

        IF FOUND THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - overlapping date range';
        END IF;

        -- Allow linking to a parcel only if the parcel is within 500 feet of the address point
        -- and the APN of the parcel is in the same block as the parcel(s) under the address point.

        -- Performance matters!

        -- This will be our path 9999 times out of 10000. It's also fastest.
        -- If the parcel contains the address point all other constraints should also be met.
        perform 1
        from
           address_base ab,
           addresses a,
           parcels p
        where a.address_id = NEW.address_id
        and a.address_base_id = ab.address_base_id
        and NEW.parcel_id = p.parcel_id
        and st_contains(p.geometry, ab.geometry);

        if FOUND then
            RETURN NEW;
        end if;
        --


        perform 1
        from
            address_base ab,
            addresses a,
            parcels p1
        where a.address_id = NEW.address_id
        and a.address_base_id = ab.address_base_id
        and NEW.parcel_id = p1.parcel_id
        and st_distance(p1.geometry, ab.geometry) <= 500.0
        and (p1.block_num in (
            select distinct p2.block_num
                from parcels p2
                where st_contains(p2.geometry, ab.geometry)) 
            or p1.block_num in (
                select p3.block_num from (
                    select (st_distance(geometry, ab.geometry)::int) as "distance", p2.block_num
                        from parcels p2
                        where ((ST_DWithin(p2.geometry, ab.geometry, 500)) and NOT (p2.block_num = '0000')) order by "distance" asc limit 1
                ) as p3
            )
        or p1.block_num = '0000');


        if not FOUND then
            ----- Gather common information for reporting to user.
            select ab.base_address_num, sn.full_street_name, blk_lot, block_num, st_distance(p1.geometry, ab.geometry)::int
            into _address_number, _full_street_name, _block_lot, _block_num, _distance
            from
                address_base ab,
                addresses a, 
                streetnames sn,
                parcels p1
            where a.address_id = NEW.address_id
            and a.address_base_id = ab.address_base_id
            and ab.street_segment_id = sn.street_segment_id
            and sn.category = 'MAP'
            and p1.parcel_id = NEW.parcel_id;
            -----


            ----- check block num
            perform 1
            from
                address_base ab,
                addresses a,
                parcels p1
            where a.address_id = NEW.address_id
            and a.address_base_id = ab.address_base_id
            and NEW.parcel_id = p1.parcel_id
            and (p1.block_num not in (
                select distinct p2.block_num
                    from parcels p2
                    where st_contains(p2.geometry, ab.geometry)) 
                or p1.block_num not in (
                    select p3.block_num from (
                        select (st_distance(geometry, ab.geometry)::int) as "distance", p2.block_num
                            from parcels p2
                            where ((ST_DWithin(p2.geometry, ab.geometry, 500)) and NOT (p2.block_num = '0000')) order by "distance" asc limit 1
                    ) as p3
                )
                or p1.block_num != '0000');

            -- It's less confusing to interpret the results of the exception report if this exception precedes distance exception.
            -- Why? It's mostly because addresses can be retired after they are loaded and we can't (yet) see retired addresses in the UI.
            IF FOUND THEN
                select string_agg(distinct p1.block_num, ', ') into _block_nums
                from
                    address_base ab,
                    addresses a,
                    parcels p1
                where a.address_id = NEW.address_id
                and a.address_base_id = ab.address_base_id
                and st_contains(p1.geometry, ab.geometry);

                _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot || ' BLOCK_NUMs: ' || _block_nums;
                RAISE EXCEPTION 'insert rejected - address_x_parcels - the parcel is not within the block number range: %', _message;
            END IF;
            -----

            IF _distance > 500.0 THEN
                _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot || ' DISTANCE: ' || _distance;
                RAISE EXCEPTION 'insert rejected - address_x_parcels - the parcel is not within within 500 feet of the address: %', _message;
            END IF;

            _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot;
            RAISE EXCEPTION 'insert rejected - address_x_parcels - dang - looks like there is a flaw in our validation process %', _message;

        END IF;

        
    END IF;


    RETURN NEW;

  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_address_x_parcels_before()
  OWNER TO eas_dbo;



CREATE OR REPLACE FUNCTION bulkloader.find_address_base(
        _street_number int,
        _street_number_suffix char(1),
        _street_name character varying(64),
        _street_type character varying(32),
        _bulkloader_id int
    ) RETURNS int [] AS $BODY$
DECLARE _address_base_ids int [];
_index int;
_category char(5);
_categories text [] = Array ['MAP','ALIAS']::Text [];
BEGIN -- patch for 
-- http://code.google.com/p/eas/issues/detail?id=349
-- Return a match of the street name matches and its in the right place based on the block lot.
if _street_name = 'UNKNOWN' then
select into _address_base_ids ARRAY(
        select distinct ab.address_base_id
        from public.address_base ab
            inner join public.streetnames sn on ab.street_segment_id = sn.street_segment_id
            inner join public.parcels p on st_contains(p.geometry, ab.geometry)
        where sn.category = 'MAP'
            and ab.base_address_num = _street_number
            and sn.base_street_name = _street_name
            and ab.retire_tms is null
            and p.blk_lot = (
                select blocklot
                from bulkloader.address_extract
                where id = _bulkloader_id
            )
    );
if array_upper(_address_base_ids, 1) > 1 then -- We found more than one - in this case (UNKNOWN streets) we'll want to create a separate new base address.
_address_base_ids := array []::integer [];
end if;
return _address_base_ids;
end if;
-- Try the MAP category first. This uses the official street name.
-- If that fails, try the alias.
-- 99/100 the MAP category will succeed so it should be first.
for _index in array_lower(_categories, 1)..array_upper(_categories, 1) loop _category = _categories [_index];
select into _address_base_ids ARRAY(
        select distinct ab.address_base_id
        from public.address_base ab
            inner join public.streetnames sn on ab.street_segment_id = sn.street_segment_id
            left outer join public.d_street_type st on sn.street_type = st.abbreviated
        where sn.category = _category
            and ab.base_address_num = _street_number
            and sn.base_street_name = _street_name
            and ab.retire_tms is null
            and (
                (
                    _street_type is null
                    and st.unabbreviated is null
                )
                or (
                    _street_type is not null
                    and coalesce(st.unabbreviated, '') = _street_type
                )
            )
            and (
                (
                    _street_number_suffix is null
                    and ab.base_address_suffix is null
                )
                or (
                    _street_number_suffix is not null
                    and coalesce(ab.base_address_suffix, '') = _street_number_suffix
                )
            )
    );
if array_upper(_address_base_ids, 1) is not null then return _address_base_ids;
end if;
end loop;
return _address_base_ids;
END;
$BODY$ LANGUAGE 'plpgsql' VOLATILE COST 100;
ALTER FUNCTION bulkloader.find_address_base(
    _street_number int,
    _street_number_suffix char(1),
    _street_name character varying(64),
    _street_type character varying(32),
    _bulkloader_id int
) OWNER TO postgres;

-- Function: _eas_address_x_parcels_before()

-- DROP FUNCTION _eas_address_x_parcels_before();

CREATE OR REPLACE FUNCTION _eas_address_x_parcels_before()
  RETURNS trigger AS
$BODY$
DECLARE
    _address_number integer;
    _full_street_name character varying(64);
    _block_lot character varying(10);
    _block_num character varying(10);
    _block_nums character varying(64);
    _distance int;
    _message character varying(255);
BEGIN

    -- All the constraints in one place.

    IF (TG_OP = 'DELETE') THEN
        RAISE EXCEPTION 'delete rejected - address_x_parcels id=% - deletes are not allowed', OLD.id;
    ELSIF (TG_OP = 'UPDATE') THEN

        IF NEW.retire_tms IS NULL THEN
            RAISE EXCEPTION 'update rejected  - address_x_parcels id=% - retire_tms must not be null', NEW.id;
        END IF;
        IF NEW.retire_change_request_id IS NULL THEN
            RAISE EXCEPTION 'update rejected  - address_x_parcels id=% - retire_change_request_id must not be null', NEW.id;
        END IF;
        IF NEW.create_tms > NEW.retire_tms THEN
            RAISE EXCEPTION 'update rejected - address_x_parcels id=% - create_tms is greater than retire_tms', NEW.id;
        END IF;

    ELSE
        -- INSERT

        IF (NEW.retire_tms IS NOT NULL) THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - retire_tms must be null';
        end if;

        -- Do not allow multiple live rows.
        perform 1
        from address_x_parcels
        where address_id = NEW.address_id
        and parcel_id = NEW.parcel_id
        and retire_tms is null;

        IF FOUND THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - existing active row was found';
        END IF;

        -- Do not allow overlapping date ranges.
        perform 1
        from address_x_parcels
        where address_id = NEW.address_id
        and parcel_id = NEW.parcel_id
        and retire_tms > NEW.create_tms;

        IF FOUND THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - overlapping date range';
        END IF;

        -- Allow linking to a parcel only if the parcel is within 500 feet of the address point
        -- and the APN of the parcel is in the same block as the parcel(s) under the address point.

        -- Performance matters!

        -- This will be our path 9999 times out of 10000. It's also fastest.
        -- If the parcel contains the address point all other constraints should also be met.
        perform 1
        from
           address_base ab,
           addresses a,
           parcels p
        where a.address_id = NEW.address_id
        and a.address_base_id = ab.address_base_id
        and NEW.parcel_id = p.parcel_id
        and st_contains(p.geometry, ab.geometry);

        if FOUND then
            RETURN NEW;
        end if;
        --


        perform 1
        from
            address_base ab,
            addresses a,
            parcels p1
        where a.address_id = NEW.address_id
        and a.address_base_id = ab.address_base_id
        and NEW.parcel_id = p1.parcel_id
        and st_distance(p1.geometry, ab.geometry) <= 500.0
        and (p1.block_num in (
            select distinct p2.block_num
                from parcels p2
                where st_contains(p2.geometry, ab.geometry)) 
            or p1.block_num in (
                select p3.block_num from (
                    select (st_distance(geometry, ab.geometry)::int) as "distance", p2.block_num
                        from parcels p2
                        where ((ST_DWithin(p2.geometry, ab.geometry, 500)) and NOT (p2.block_num = '0000')) order by "distance" asc limit 1
                ) as p3
            )
        or p1.block_num = '0000');


        if not FOUND then
            ----- Gather common information for reporting to user.
            select ab.base_address_num, sn.full_street_name, blk_lot, block_num, st_distance(p1.geometry, ab.geometry)::int
            into _address_number, _full_street_name, _block_lot, _block_num, _distance
            from
                address_base ab,
                addresses a, 
                streetnames sn,
                parcels p1
            where a.address_id = NEW.address_id
            and a.address_base_id = ab.address_base_id
            and ab.street_segment_id = sn.street_segment_id
            and sn.category = 'MAP'
            and p1.parcel_id = NEW.parcel_id;
            -----


            ----- check block num
            perform 1
            from
                address_base ab,
                addresses a,
                parcels p1
            where a.address_id = NEW.address_id
            and a.address_base_id = ab.address_base_id
            and NEW.parcel_id = p1.parcel_id
            and (p1.block_num not in (
                select distinct p2.block_num
                    from parcels p2
                    where st_contains(p2.geometry, ab.geometry)) 
                or p1.block_num not in (
                    select p3.block_num from (
                        select (st_distance(geometry, ab.geometry)::int) as "distance", p2.block_num
                            from parcels p2
                            where ((ST_DWithin(p2.geometry, ab.geometry, 500)) and NOT (p2.block_num = '0000')) order by "distance" asc limit 1
                    ) as p3
                )
                or p1.block_num != '0000');

            -- It's less confusing to interpret the results of the exception report if this exception precedes distance exception.
            -- Why? It's mostly because addresses can be retired after they are loaded and we can't (yet) see retired addresses in the UI.
            IF FOUND THEN
                select string_agg(distinct p1.block_num, ', ') into _block_nums
                from
                    address_base ab,
                    addresses a,
                    parcels p1
                where a.address_id = NEW.address_id
                and a.address_base_id = ab.address_base_id
                and st_contains(p1.geometry, ab.geometry);

                _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot || ' BLOCK_NUMs: ' || _block_nums;
                RAISE EXCEPTION 'insert rejected - address_x_parcels - the parcel is not within the block number range: %', _message;
            END IF;
            -----

            IF _distance > 500.0 THEN
                _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot || ' DISTANCE: ' || _distance;
                RAISE EXCEPTION 'insert rejected - address_x_parcels - the parcel is not within within 500 feet of the address: %', _message;
            END IF;

            _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot;
            RAISE EXCEPTION 'insert rejected - address_x_parcels - dang - looks like there is a flaw in our validation process %', _message;

        END IF;

        
    END IF;


    RETURN NEW;

  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_address_x_parcels_before()
  OWNER TO eas_dbo;


-- The following constraints were introduced during the conversion of PostGIS
-- from 1.5.2 to 2.5.2, along with the constraints "enforce_dims_geometry" and
-- "enforce_srid_geometry".  Since the constraints below were preventing us
-- from updating the corresponding tables, and because the geometries are
-- managed in an upstream database, we can safely drop the following:

ALTER TABLE public.parcels_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.parcels DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.street_segments_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.street_segments_work DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.street_segments DROP CONSTRAINT enforce_geotype_geometry;


-- END OF FILE
