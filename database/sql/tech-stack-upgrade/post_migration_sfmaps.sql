
-- The following constraints were introduced during the conversion of PostGIS
-- from 1.5.2 to 2.5.2, along with the constraints "enforce_dims_geometry" and
-- "enforce_srid_geometry".  Since the constraints below were preventing us
-- from updating the corresponding tables, and because the geometries are
-- managed in an upstream database, we can safely drop the following:

ALTER TABLE public.basemap_citylots_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_citylots_base_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_arterials_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_non_paper_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_other_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_ramp_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_city_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_highway_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_freeway_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_major_staging DROP CONSTRAINT enforce_geotype_geometry;
ALTER TABLE public.basemap_stclines_minor_staging DROP CONSTRAINT enforce_geotype_geometry;


-- END OF FILE
