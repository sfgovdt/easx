
DROP FUNCTION IF EXISTS avs.qa(
);

CREATE OR REPLACE FUNCTION avs.qa(
)
  RETURNS text AS
$BODY$
DECLARE

    _address_base_id int;
    _count int;

BEGIN


    truncate avs.qa;


    insert into avs.qa (description, pass)
    SELECT 
        'assert that addresses exist for: MISSION BAY BLVD NORTH & SOUTH',
        count(*) >= 56
    FROM address_base ab
    inner join addresses a on (ab.address_base_id = a.address_base_id)
    inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
    inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
    WHERE sn.category = 'ALIAS'
    and sn.base_street_name in ('MISSION BAY BLVD NORTH', 'MISSION BAY BLVD NORTH');


    insert into avs.qa (description, pass)
    SELECT 
        'assert that no addresses have number suffix of V',
        count(*) = 0
    FROM address_base ab
    inner join addresses a on (ab.address_base_id = a.address_base_id)
    inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
    inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
    left outer join address_x_parcels axp on (axp.address_id = a.address_id)
    left outer join parcels p on (axp.parcel_id = p.parcel_id)
    WHERE ab.base_address_suffix = 'V';


    insert into avs.qa (description, pass)
    SELECT 
        'assert that no addresses have a unit value of zero',
        count(*) = 0
    FROM address_base ab
    inner join addresses a on (ab.address_base_id = a.address_base_id)
    WHERE a.unit_num = '0';


    insert into avs.qa (description, pass)
    SELECT 
        'assert that there are at least 3 service addresses',
        count(*) >= 3
    FROM address_base ab
    inner join addresses a on (ab.address_base_id = a.address_base_id)
    inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
    inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
    left outer join address_x_parcels axp on (axp.address_id = a.address_id)
    left outer join parcels p on (axp.parcel_id = p.parcel_id)
    WHERE p.block_num = '0000';

    -- select * from avs.qa;

    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs.qa(
) OWNER TO postgres;
