
DROP FUNCTION IF EXISTS avs.load(
    _offset int,
    _limit int,
    _end_dated boolean
);

CREATE OR REPLACE FUNCTION avs.load(
    _offset int,
    _limit int,
    _end_dated boolean
)
  RETURNS text AS
$BODY$
DECLARE

    _avs_id                 int;
    -- In this load we choose an arbitrary create date
    _create_tms             timestamp without time zone = '01-01-1970';
    _current_tms            timestamp without time zone = now();
    _address_base_id        int;
    _address_id             int;
    _address_x_parcel_id    int;
    _exception_text         character varying(256);
    _sort_order             int;
    _success                boolean;
    _offset                 int = _offset;
    _limit                  int = _limit;
    _std_end_date_patch     date;

BEGIN

    -- disable the after triggers because we do not want the load audited or xmitted
    alter table address_base disable trigger _eas_address_base_after;
    alter table addresses disable trigger _eas_addresses_after;
    alter table address_x_parcels disable trigger _eas_address_x_parcels_after;


    if _end_dated then
        _offset = 0;
        _limit = 100000;
    end if;

    ---- avs
    -- Avoid joins here until we have a performance problem. The reason behind this is simplicity and readability.
    -- Process end-dated rows first, and run cascade-retire on them before processing non end-dated rows.
    -- When we run into duplicates the "first one in wins".
    -- The "order by" is important because
    --    we use address_type for priority in case of duplicates
    --    we use avsa.id to stabilize the sort so it can be used with offset and limit
    -- We use offset and limit on the cursor query to help reduce the size of the transaction and to help with progress indication.
    -- The use of offset is inefficient but I am not "feeling" that effect here - probably the rest of the processing takes so long.
    -- See discussion http://postgresql.1045698.n5.nabble.com/Indexing-problem-with-OFFSET-LIMIT-td1906950.html
    -- Total time to process about 350000 rows is 30 minutes.
    -- As long as we are loading end dated records, we REALLY DO want std_end_date to be order by asc, otherwise we'll get weird exceptions.
    declare cursorAvsAddress cursor for
        select
          avsa.id,
          avsa.exception_text,
          (case
            when avsa.address_type = 'ASSESSOR' then 1
            when avsa.address_type = 'DBI' then 2
            when avsa.address_type = 'MULTIUNIT' then 3
            when avsa.address_type = 'XREFERENCE' then 4
            when avsa.address_type = 'USER' then 5
            when avsa.address_type = 'THREER' then 6
            when avsa.address_type = 'UNKNOWN' then 7
            else 8
          end) as address_type_sort_order,
          (case
            when avsa.std_end_date is not null and _eas_is_date(avsa.std_end_date) then avsa.std_end_date::date
            else null
          end) as std_end_date_patch
        from avs.avs_addresses avsa
        where (
            (_end_dated and avsa.std_end_date is not null)
            or
            (not _end_dated and avsa.std_end_date is null)
        )
        order by 4 asc, address_type_sort_order, avsa.id
        offset _offset
        limit _limit;
    begin
        open cursorAvsAddress;
        loop
            -- We discard _sort_order.
            fetch cursorAvsAddress into _avs_id, _exception_text, _sort_order, _std_end_date_patch;

            if not found then
                exit;
            end if;


                -- Skip rows that have an exception.
                -- We do this here instead of in the select cursor to simplify QA and because we update exception_text during the cursor operation.
                if _exception_text is not null then
                    continue;
                end if;

                -- base address
                begin
                    select into _success avs.process_address_base(_avs_id, _create_tms);
                    if not _success then
                        continue;
                    end if;
                exception
                    when others then
                        update avs.avs_addresses set exception_text = substring(SQLERRM from 1 for 256) where id = _avs_id;
                        continue;
                end;


                -- unit address
                begin
                    perform avs.process_address_unit(_avs_id, _create_tms);
                exception
                    when others then
                        update avs.avs_addresses set exception_text = substring(SQLERRM from 1 for 256) where id = _avs_id;
                        continue;
                end;


                -- address parcel link
                begin
                    perform avs.process_address_parcel_link(_avs_id, _create_tms);
                exception
                    when others then
                        update avs.avs_addresses set exception_text = substring(SQLERRM from 1 for 256) where id = _avs_id;
                end;



        end loop;
        close cursorAvsAddress;
    end;
    ----


    if _end_dated then
        perform avs.cascade_retire(_current_tms);
    end if;


    -- enable the after triggers so the application itself works properly after the load is done
    alter table address_base enable trigger _eas_address_base_after;
    alter table addresses enable trigger _eas_addresses_after;
    alter table address_x_parcels enable trigger _eas_address_x_parcels_after;


    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs.load(
    _offset int,
    _limit int,
    _end_dated boolean
) OWNER TO postgres;
