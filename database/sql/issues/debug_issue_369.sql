
-- Use this to generate sql to send to DBI to correct some street name values.

-- http://code.google.com/p/eas/issues/detail?id=369

/*
select avs_street_name, avs_street_type, count(*)
from avs.avs_addresses
where exception_text like 'unable to find nearby street%'
group by avs_street_name, avs_street_type
order by count(*) desc;
*/


/*
select *, '', '', '', '', '', '', '', '', '', ''
from avs.avs_addresses
where exception_text like 'unable to find nearby street%'
and (avs_street_name, avs_street_type) in (
    select avs_street_name, avs_street_type
    from avs.avs_addresses
    where exception_text like 'unable to find nearby street%'
    group by avs_street_name, avs_street_type
    having count(*) >= 5
);
*/

/*
select *
from vw_streetnames_distinct
where base_street_name like 'EDGAR%';
*/


----- SQL updates begin here

-- LAKE MERCED HILL NORTH
select 
    id,
    avs_street_name ::varchar(64) as avs_street_name_existing,
    avs_street_sfx::varchar(32) as avs_street_sfx_existing,
    'LAKE MERCED HILL NORTH'::varchar(64) as avs_street_name_new,
    'ST'::varchar(32) as avs_street_sfx_new
from avs.avs_addresses
where avs_street_name = 'LAKE MERCED'
and avs_street_sfx = 'BL'
and exception_text like 'unable to find nearby street%'
and exception_text like '%LAKE MERCED HILL NORTH STREET%'

union all

-- EDGAR AVE
select 
    id,
    avs_street_name ::varchar(64) as avs_street_name_existing,
    avs_street_sfx::varchar(32) as avs_street_sfx_existing,
    'EDGAR'::varchar(64) as avs_street_name_new,
    'AV'::varchar(32) as avs_street_sfx_new
from avs.avs_addresses
where avs_street_name = 'EDGARDO'
and avs_street_sfx = 'PL'
and exception_text like 'unable to find nearby street%'
and exception_text like '%EDGAR AVENUE%';
