
    select string_agg(distinct block_num, ', ')
    from parcels p2
    where 1=1
    and st_contains(p2.geometry, ST_GeomFromEWKT('SRID=2227;POINT(6006839.81943328 2121446.42553871)'))



    select string_agg(block_num)
    from parcels p2
    where 1=1
    and st_contains(p2.geometry, ST_GeomFromEWKT('SRID=2227;POINT(6006839.81943328 2121446.42553871)'))

string_agg(distinct block_num, ',')


-- 2655 Hyde
select *
from parcels p1
where 1=1
--and blk_lot = '0026T305A' -- pass
--and blk_lot = '1234001' -- fail (is not in block)
--and blk_lot = '0256T373M' -- fail (distance > 500)
and st_distance(p1.geometry, ST_GeomFromEWKT('SRID=2227;POINT(6006839.81943328 2121446.42553871)')) <= 500
and block_num in (
    select generate_series(distinct block_num)
    from parcels p2
    where 1=1
    and st_contains(p2.geometry, ST_GeomFromEWKT('SRID=2227;POINT(6006839.81943328 2121446.42553871)'))
);






--
-- This highlights the records that have a different block but that are part of the same address.
-- 
select 
    block, 
    lot, 
    street_number, 
    avs_street_name, 
    avs_street_sfx, 
    unit, 
    unit_sfx,
    end_date,
    CASE 
        WHEN block = (select block from avs.avs_addresses where address_base_id = a1.address_base_id and address_x_parcel_id is not null limit 1) THEN ''
        ELSE '>>>'
    END, 
    exception_text
from avs.avs_addresses a1
where 1=1
and address_base_id in (
    select distinct address_base_id
    from avs.avs_addresses
    where exception_text like '%insert rejected - address_x_parcels - address geometry is not contained by the specified parcel%'
)
order by street_number, avs_street_name, avs_street_sfx, exception_text NULLS FIRST;

/*

select *
from avs.avs_addresses a1
where 1=1
and address_x_parcel_id is null
and address_base_id in (
    select distinct address_base_id
    from avs.avs_addresses
    where exception_text like '%insert rejected - address_x_parcels - address geometry is not contained by the specified parcel%'
)
and not exists(
    select 1
    from avs.avs_addresses a2
    where 1=1
    and block = a1.block
    and address_base_id = a1.address_base_id
    and address_x_parcel_id is not null
)
order by street_number, avs_street_name, avs_street_sfx, exception_text NULLS FIRST;

*/

            select blk_lot, block_num
            from parcels
            where parcel_id = 100000





----- AVS Load
address_base_id = 633051
address_id
axp_id

select * from addresses where address_base_id = 633051
address_id = 931171
axp = 685273
parcel_id = 100713

select * from address_x_parcels where parcel_id = 100713
select * from parcels where blk_lot = '2720006'

select * from avs.vw_load_summary;
select * from avs.avs_addresses where address_base_id is not null order by blocklot limit 1000
select *, 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo'
from avs.avs_addresses where exception_text is not null limit 1000

select *, 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo', 'foo'
from avs.avs_addresses avsa where exception_text like 'insert rejected%'
ORDER BY avsa.street_number, avsa.avs_street_name, avsa.std_street_type, avsa.street_number_sfx, avsa.unit, avsa.unit_sfx, avsa.blocklot;


select st_asewkt(geometry) from address_base where address_base_id = 633051

-- "2719B"
select * 
from parcels 
where st_contains(geometry, ST_GeomFromEWKT('SRID=2227;POINT(5998742.46533599 2104211.0586924)'))


        select *
        from address_base ab
        right join addresses a on a.address_base_id = ab.address_base_id
        cross join parcels p1 
        left join parcels p2 on st_contains(p2.geometry, ab.geometry) and p1.block_num = p2.block_num
        where a.address_id = 931171
        and p1.parcel_id = 100713
        and p2.parcel_id is null

            select *
            from address_base ab
            right join addresses a on a.address_base_id = ab.address_base_id
            cross join parcels p1
            left join parcels p2 on st_contains(p2.geometry, ab.geometry) and p1.block_num = p2.block_num
            where a.address_id = 851379
            and p1.parcel_id = 50
            and p2.parcel_id is null;



        select *
        from address_base ab
        right join addresses a on a.address_base_id = ab.address_base_id
        right join parcels p1 on st_distance(p1.geometry, ab.geometry) <= 500.0
        left join parcels p2 on st_contains(p2.geometry, ab.geometry)
        where a.address_id = 864858
        and p1.parcel_id = 23425
        and p1.block_num = p2.block_num
        limit 1

                select *
                from
                    address_base ab,
                    addresses a,
                    parcels p1,
                    parcels p2
                where a.address_id = 864858
                and a.address_base_id = ab.address_base_id
                and 23425 = p1.parcel_id
                and st_distance(p1.geometry, ab.geometry) <= 500.0
                and st_contains(p2.geometry, ab.geometry)
                and p1.block_num = p2.block_num
                limit 1;





SELECT ab.base_address_num,
	sn.base_street_name,
    sn.street_type,
    count(*)
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id) 
WHERE 1 = 1
and sn.category = 'MAP'
and st_distance(p.geometry, ab.geometry) <> 0
and ab.retire_tms is null
group by 
    ab.base_address_num,
	sn.base_street_name,
    sn.street_type
order by
    count(*) desc,
	ab.base_address_num,
	sn.base_street_name,
    sn.street_type
;
