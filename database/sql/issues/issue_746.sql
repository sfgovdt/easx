﻿
-- sfmaps
update basemap_stclines_city
set label_text = 'Mission Bay Blvd South'
where cnn in (
	15019101,
	15017101,
	15021101,
	15023101,
	15022101,
	15018101,
	15020101
);

update basemap_stclines_city
set label_text = 'Mission Bay Blvd North'
where cnn in (
	15019201,
	15020201,
	15023202,
	15017201,
	15023201,
	15021201,
	15022201,
	15018201
);

-- eas
/*
select seg_cnn
from streetnames
where full_street_name = 'MISSION BAY BLVD SOUTH'
and category = 'MAP';
*/

/*
select seg_cnn
from streetnames
where full_street_name = 'MISSION BAY BLVD NORTH'
and category = 'MAP';
*/

update streetnames sn1
set
	base_street_name = 'MISSION BAY BLVD SOUTH',
	street_type = null,
	full_street_name = 'MISSION BAY BLVD SOUTH'
where sn1.base_street_name = 'MISSION BAY'
and sn1.street_type = 'BLVD'
and sn1.category = 'MAP'
and exists (
	select 1
	from streetnames sn2
	where sn2.base_street_name = 'MISSION BAY BLVD SOUTH'
	and sn2.street_type is null
	and sn2.category = 'ALIAS'
	and sn2.seg_cnn = sn1.seg_cnn
);

update streetnames sn1
set
	base_street_name = 'MISSION BAY BLVD NORTH',
	street_type = null,
	full_street_name = 'MISSION BAY BLVD NORTH'
where sn1.base_street_name = 'MISSION BAY'
and sn1.street_type = 'BLVD'
and sn1.category = 'MAP'
and exists (
	select 1
	from streetnames sn2
	where sn2.base_street_name = 'MISSION BAY BLVD NORTH'
	and sn2.street_type is null
	and sn2.category = 'ALIAS'
	and sn2.seg_cnn = sn1.seg_cnn
);

