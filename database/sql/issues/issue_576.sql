
SELECT
	ab.address_base_id,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
    p.parcel_id,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    ab.zone_id,
    z.zipcode
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
inner join zones z on ab.zone_id = z.zone_id
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id) 
WHERE 1 = 1
and sn.category = 'MAP'
--and sn.base_street_name = ( 'HAWTHORNE')
--and ab.base_address_num = 20
and ab.address_base_id = 268370
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num
limit 1000;


-- look for duplicates
SELECT
	ab.base_address_num,
	coalesce(ab.base_address_suffix, ''),
	sn.base_street_name,
    coalesce(sn.street_type, ''),
    z.jurisdiction,
    count(*)
FROM address_base ab
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
inner join zones z on ab.zone_id = z.zone_id
WHERE 1 = 1
and sn.category = 'MAP'
and ab.retire_tms is null
and sn.base_street_name <> 'UNKNOWN'
group by
	ab.base_address_num,
	coalesce(ab.base_address_suffix, ''),
	sn.base_street_name,
    coalesce(sn.street_type, ''),
    z.jurisdiction
having count(*) > 1;



select * from zones


select *
from zones_staging
where active_date is not null
or retire_date is not null

select * from cr_address_base

select _eas_etl_zones();
