

select
  avsa.id,
  avsa.exception_text,
  (case
    when avsa.address_type = 'ASSESSOR' then 1
    when avsa.address_type = 'DBI' then 2
    when avsa.address_type = 'MULTIUNIT' then 3
    when avsa.address_type = 'XREFERENCE' then 4
    when avsa.address_type = 'USER' then 5
    when avsa.address_type = 'THREER' then 6
    when avsa.address_type = 'UNKNOWN' then 7
    else 8
  end) as address_type_sort_order
from avs.avs_addresses avsa
where (
    (true and avsa.std_end_date is not null)
    or
    (not true and avsa.std_end_date is null)
)
order by address_type_sort_order, avsa.id
offset 1000
limit 1000;


