
BEGIN;

LOCK TABLE address_x_parcels IN ACCESS EXCLUSIVE MODE NOWAIT;


-- disable triggers
alter table address_x_parcels disable trigger _eas_address_x_parcels_before;
alter table address_x_parcels disable trigger _eas_address_x_parcels_after;

--
--select *
update address_x_parcels
set retire_tms = null
where id = (
    SELECT
        axp.id
        --ab.address_base_id, a.address_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, a.unit_num, sn.base_street_name, sn.street_type, p.blk_lot, p.parcel_id,
        --ab.create_tms as ab_create_tms, a.create_tms as a_create_tms, axp.create_tms as axp_create_tms, ab.retire_tms as ab_retire_tms, a.retire_tms as a_retire_tms, axp.retire_tms as axp_retire_tms, ab.street_segment_id, ab.zone_id
    FROM address_base ab
    inner join addresses a on (ab.address_base_id = a.address_base_id)
    inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
    inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
    left outer join address_x_parcels axp on (axp.address_id = a.address_id)
    left outer join parcels p on (axp.parcel_id = p.parcel_id)
    WHERE 1 = 1
    and sn.category = 'MAP'
    and sn.base_street_name = ( 'KING')
    and ab.base_address_num = 200
    and p.blk_lot = '8702003'
    and ab.retire_tms is null
    and a.retire_tms is null
);

-- enable triggers
alter table address_x_parcels enable trigger _eas_address_x_parcels_before;
alter table address_x_parcels enable trigger _eas_address_x_parcels_after;

COMMIT;
