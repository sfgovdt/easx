select 
    seg_cnn,
    prefix_direction,
    prefix_type,
    base_street_name,
    street_type,
    suffix_direction,
    suffix_type,
    full_street_name,
    category,
    street_segment_id,
    create_tms,
    category,
    count(*)
from streetnames
where 1=1
--and category = 'MAP'
group by 
    seg_cnn,
    prefix_direction,
    prefix_type,
    base_street_name,
    street_type,
    suffix_direction,
    suffix_type,
    full_street_name,
    category,
    street_segment_id,
    create_tms,
    category
having count(*) > 1
order by street_segment_id


select * 
from streetnames
where street_segment_id = 7146
and category = 'MAP'

select distinct bsm_streetnameid, base_street_name, street_type, category
from streetnames_staging
order by base_street_name, street_type, category;


select distinct bsm_streetnameid, base_street_name, street_type, category
from streetnames_staging
order by base_street_name, street_type, category;

select *
from streetnames
where 1=1
limit 10

select *
from streetnames_staging


select *
from streetnames_staging
where 1=1
and bsm_streetnameid in (2938, 2937)
order by seg_cnn, bsm_streetnameid
and bsm_streetnameid is null
--and base_street_name like '%BUENA VISTA%'
and seg_cnn = 3368000

select sns.category, sn.category, sn.*
from
    streetnames sn,
    streetnames_staging sns
where sn.seg_cnn = sns.seg_cnn
and sn.base_street_name = sns.base_street_name 
and sn.street_type = sns.street_type
and sn.category <> sns.category;

update streetnames sn
set category = sns.category
from streetnames_staging sns
where sn.seg_cnn = sns.seg_cnn
and sn.base_street_name = sns.base_street_name 
and sn.street_type = sns.street_type
and sn.category <> sns.category;


SELECT ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.create_tms, st_transform(ab.geometry, 900913) AS geometry, sn.base_street_name AS street_name, sn.street_type, sn.category AS street_category, sn.prefix_direction AS street_prefix_direction, sn.suffix_direction AS street_suffix_direction, sn.full_street_name, z.zipcode, z.jurisdiction, '' AS summary_of_units, 0 AS count_of_units, min(ad.disposition_description::text) AS disposition_min, max(ad.disposition_description::text) AS disposition_max, 0 AS geocoding_score
FROM address_base ab, addresses a, streetnames sn, zones z, d_address_disposition ad
WHERE 1 = 1 AND ab.address_base_id = a.address_base_id AND ab.retire_tms IS NULL AND a.retire_tms IS NULL AND ab.zone_id = z.zone_id AND ab.street_segment_id = sn.street_segment_id AND a.disposition_code = ad.disposition_code
and ab.address_base_id = 298721
GROUP BY ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.create_tms, ab.geometry, sn.base_street_name, sn.street_type, sn.category, sn.prefix_direction, sn.suffix_direction, sn.full_street_name, z.zipcode, z.jurisdiction
HAVING count(*) = 1
UNION ALL 
SELECT ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.create_tms, st_transform(ab.geometry, 900913) AS geometry, sn.base_street_name AS street_name, sn.street_type, sn.category AS street_category, sn.prefix_direction AS street_prefix_direction, sn.suffix_direction AS street_suffix_direction, sn.full_street_name, z.zipcode, z.jurisdiction, _sfmad_get_summary_of_units(COALESCE(a.unit_num, ''::character varying)::text) AS summary_of_units, count(*) AS count_of_units, min(ad.disposition_description::text) AS disposition_min, max(ad.disposition_description::text) AS disposition_max, 0 AS geocoding_score
FROM address_base ab, addresses a, streetnames sn, zones z, d_address_disposition ad
WHERE 1 = 1 AND ab.address_base_id = a.address_base_id AND ab.retire_tms IS NULL AND a.retire_tms IS NULL AND ab.zone_id = z.zone_id AND ab.street_segment_id = sn.street_segment_id AND a.disposition_code = ad.disposition_code AND a.address_base_flg = false
and ab.address_base_id = 298721
GROUP BY ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.create_tms, ab.geometry, sn.base_street_name, sn.street_type, sn.category, sn.prefix_direction, sn.suffix_direction, sn.full_street_name, z.zipcode, z.jurisdiction
HAVING count(*) >= 1;




SELECT ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.create_tms, st_transform(ab.geometry, 900913) AS geometry, sn.base_street_name AS street_name, sn.street_type, sn.category AS street_category, sn.prefix_direction AS street_prefix_direction, sn.suffix_direction AS street_suffix_direction, sn.full_street_name, z.zipcode, z.jurisdiction, '' AS summary_of_units, 0 AS count_of_units, min(ad.disposition_description::text) AS disposition_min, max(ad.disposition_description::text) AS disposition_max, 0 AS geocoding_score, count(*)
FROM address_base ab, addresses a, streetnames sn, zones z, d_address_disposition ad
WHERE 1 = 1 AND ab.address_base_id = a.address_base_id AND ab.retire_tms IS NULL AND a.retire_tms IS NULL AND ab.zone_id = z.zone_id AND ab.street_segment_id = sn.street_segment_id AND a.disposition_code = ad.disposition_code
and ab.address_base_id = 298721
--and sn.category = 'MAP'
GROUP BY ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.create_tms, ab.geometry, sn.base_street_name, sn.street_type, sn.category, sn.prefix_direction, sn.suffix_direction, sn.full_street_name, z.zipcode, z.jurisdiction
--, sn.category 
HAVING count(*) = 1

UNION ALL 

SELECT ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.create_tms, st_transform(ab.geometry, 900913) AS geometry, sn.base_street_name AS street_name, sn.street_type, sn.category AS street_category, sn.prefix_direction AS street_prefix_direction, sn.suffix_direction AS street_suffix_direction, sn.full_street_name, z.zipcode, z.jurisdiction, _sfmad_get_summary_of_units(COALESCE(a.unit_num, ''::character varying)::text) AS summary_of_units, count(*) AS count_of_units, min(ad.disposition_description::text) AS disposition_min, max(ad.disposition_description::text) AS disposition_max, 0 AS geocoding_score
FROM address_base ab, addresses a, streetnames sn, zones z, d_address_disposition ad
WHERE 1 = 1 AND ab.address_base_id = a.address_base_id AND ab.retire_tms IS NULL AND a.retire_tms IS NULL AND ab.zone_id = z.zone_id AND ab.street_segment_id = sn.street_segment_id AND a.disposition_code = ad.disposition_code AND a.address_base_flg = false
and ab.address_base_id = 298721
and sn.category = 'MAP'
GROUP BY ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.create_tms, ab.geometry, sn.base_street_name, sn.street_type, sn.category, sn.prefix_direction, sn.suffix_direction, sn.full_street_name, z.zipcode, z.jurisdiction
HAVING count(*) >= 1;


