
-- DROP FUNCTION _eas_address_x_parcels_before() cascade;

CREATE FUNCTION _eas_address_x_parcels_before() RETURNS trigger AS $_eas_address_x_parcels_before$
DECLARE
    _address_number integer;
    _full_street_name character varying(64);
    _block_lot character varying(10);
    _block_num character varying(10);
    _block_nums character varying(64);
    _distance int;
    _message character varying(255);
BEGIN

    -- All the constraints in one place.

    IF (TG_OP = 'DELETE') THEN
        RAISE EXCEPTION 'delete rejected - address_x_parcels id=% - deletes are not allowed', OLD.id;
    ELSIF (TG_OP = 'UPDATE') THEN

        IF NEW.retire_tms IS NULL THEN
            RAISE EXCEPTION 'update rejected  - address_x_parcels id=% - retire_tms must not be null', NEW.id;
        END IF;
        IF NEW.retire_change_request_id IS NULL THEN
            RAISE EXCEPTION 'update rejected  - address_x_parcels id=% - retire_change_request_id must not be null', NEW.id;
        END IF;
        IF NEW.create_tms > NEW.retire_tms THEN
            RAISE EXCEPTION 'update rejected - address_x_parcels id=% - create_tms is greater than retire_tms', NEW.id;
        END IF;

    ELSE
        -- INSERT

        IF (NEW.retire_tms IS NOT NULL) THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - retire_tms must be null';
        end if;

        -- Do not allow multiple live rows.
        perform 1
        from address_x_parcels
        where address_id = NEW.address_id
        and parcel_id = NEW.parcel_id
        and retire_tms is null;

        IF FOUND THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - existing active row was found';
        END IF;

        -- Do not allow overlapping date ranges.
        perform 1
        from address_x_parcels
        where address_id = NEW.address_id
        and parcel_id = NEW.parcel_id
        and retire_tms > NEW.create_tms;

        IF FOUND THEN
            RAISE EXCEPTION 'insert rejected - address_x_parcels - overlapping date range';
        END IF;

        -- Allow linking to a parcel only if the parcel is within 500 feet of the address point
        -- and the APN of the parcel is in the same block as the parcel(s) under the address point.

        -- Performance matters!

        -- This will be our path 9999 times out of 10000. It's also fastest.
        -- If the parcel contains the address point all other constraints should also be met.
        perform 1
        from
           address_base ab,
           addresses a,
           parcels p
        where a.address_id = NEW.address_id
        and a.address_base_id = ab.address_base_id
        and NEW.parcel_id = p.parcel_id
        and st_contains(p.geometry, ab.geometry);

        if FOUND then
            RETURN NEW;
        end if;
        --


        perform 1
        from
            address_base ab,
            addresses a,
            parcels p1
        where a.address_id = NEW.address_id
        and a.address_base_id = ab.address_base_id
        and NEW.parcel_id = p1.parcel_id
        and st_distance(p1.geometry, ab.geometry) <= 500.0
        and p1.block_num in (
            select distinct p2.block_num
            from parcels p2
            where st_contains(p2.geometry, ab.geometry)
        );


        if not FOUND then
            ----- Gather common information for reporting to user.
            select ab.base_address_num, sn.full_street_name, blk_lot, block_num, st_distance(p1.geometry, ab.geometry)::int
            into _address_number, _full_street_name, _block_lot, _block_num, _distance
            from
                address_base ab,
                addresses a, 
                streetnames sn,
                parcels p1
            where a.address_id = NEW.address_id
            and a.address_base_id = ab.address_base_id
            and ab.street_segment_id = sn.street_segment_id
            and sn.category = 'MAP'
            and p1.parcel_id = NEW.parcel_id;
            -----


            ----- check block num
            perform 1
            from
                address_base ab,
                addresses a,
                parcels p1
            where a.address_id = NEW.address_id
            and a.address_base_id = ab.address_base_id
            and NEW.parcel_id = p1.parcel_id
            and p1.block_num not in (
                select distinct p2.block_num
                from parcels p2
                where st_contains(p2.geometry, ab.geometry)
            );

            -- It's less confusing to interpret the results of the exception report if this exception precedes distance exception.
            -- Why? It's mostly because addresses can be retired after they are loaded and we can't (yet) see retired addresses in the UI.
            IF FOUND THEN
                select string_agg(distinct p1.block_num, ', ') into _block_nums
                from
                    address_base ab,
                    addresses a,
                    parcels p1
                where a.address_id = NEW.address_id
                and a.address_base_id = ab.address_base_id
                and st_contains(p1.geometry, ab.geometry);

                _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot || ' BLOCK_NUMs: ' || _block_nums;
                RAISE EXCEPTION 'insert rejected - address_x_parcels - the parcel is not within the block number range: %', _message;
            END IF;
            -----

            IF _distance > 500.0 THEN
                _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot || ' DISTANCE: ' || _distance;
                RAISE EXCEPTION 'insert rejected - address_x_parcels - the parcel is not within within 500 feet of the address: %', _message;
            END IF;

            _message = 'ADDRESS: ' || _address_number::text || ' ' || _full_street_name || ' APN: ' || _block_lot;
            RAISE EXCEPTION 'insert rejected - address_x_parcels - dang - looks like there is a flaw in our validation process %', _message;

        END IF;

        
    END IF;


    RETURN NEW;

  END;
$_eas_address_x_parcels_before$ LANGUAGE plpgsql;

CREATE TRIGGER _eas_address_x_parcels_before BEFORE INSERT OR UPDATE OR DELETE ON address_x_parcels
    FOR EACH ROW EXECUTE PROCEDURE _eas_address_x_parcels_before();


/*

-- for testing

select *
from parcels
where 1=1
and date_map_drop is null
order by parcel_id
limit 10

select *
from addresses
where 1=1
and retire_tms is null
order by address_id
limit 10

select *
from address_x_parcels
where 1=1
-- and retire_tms is null
order by id
limit 10

76114;87156;80;;"2010-09-30 16:08:34.655225";"";"";4;

4, -- id
76114, -- parcel id 
87156, -- address id


-- fail - active record exists
INSERT INTO address_x_parcels(
	parcel_id,
	address_id,
    activate_change_request_id,
    retire_change_request_id,
    create_tms,
    last_change_tms,
    retire_tms
)
VALUES (
31526, -- parcel id 
706577, -- address id
    0,                          --activate_change_request_id
    null, 			              --retire_change_request_id,
    '2010-09-30 16:08:34.655225', --create_tms,
    '2010-09-30 16:08:34.655225', --last_change_tms,
    null 		                  --retire_tms,
);

-- fail - insert with null retire tms
INSERT INTO address_x_parcels(
	parcel_id,
	address_id,
    activate_change_request_id,
    retire_change_request_id,
    create_tms,
    last_change_tms,
    retire_tms
)
VALUES (
2, -- parcel id 
714070, -- address id
    null,                          --activate_change_request_id
    null, 			              --retire_change_request_id,
    '2010-09-30 16:08:34.655225', --create_tms,
    null, 	                      --last_change_tms,
    '2010-09-30 16:08:34.655225'  --retire_tms,
);


select * 
from address_x_parcels
where id = 538916

update address_x_parcels
set retire_tms = '2010-09-30 17:00:00.00',
retire_change_request_id = 0
where id = 538916

-- fail - overlapping date range
INSERT INTO address_x_parcels(
	parcel_id,
	address_id,
    activate_change_request_id,
    retire_change_request_id,
    create_tms
)
VALUES (
  52, -- parcel id 
  714070, -- address id
    null,                          --activate_change_request_id
    null, 			              --retire_change_request_id,
    '2010-09-30 16:00:00.00' --create_tms,
);


-- fail - create precedes retire
select *
from address_x_parcels
where id = 538916

update address_x_parcels
set retire_tms = '1969-01-01 00:00:00'
where id = 538916

-- fail - null retire not allowed 
update address_x_parcels
set retire_tms = null
where id = 538916


-- fail - delete not allowed
delete
from address_x_parcels
where id = 538916;

select *
from address_x_parcels
where parcel_id = 39845
and address_id = 197675;

select count(*)
from address_x_parcels
where address_id = 197675
and parcel_id = 39845
and retire_tms is not null;


----- find a parcel that is more than 500 ft away from 2655 hyde

select *
from parcels p1
where 1=1
and st_distance(p1.geometry, ST_GeomFromEWKT('SRID=2227;POINT(6006839.81943328 2121446.42553871)')) > 500
limit 1;


SELECT distinct a.address_id
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id) 
WHERE 1 = 1
and axp.retire_tms is null
and sn.category = 'MAP'
and sn.base_street_name = ( 'HYDE')
and ab.base_address_num = 2655


parcel_id = 112513
address_id = 981976


INSERT INTO address_x_parcels(
	parcel_id,
	address_id,
    activate_change_request_id,
    retire_change_request_id,
    create_tms
)
VALUES (
  112513, -- parcel id 
  981976, -- address id
    0,                          --activate_change_request_id
    null, 			              --retire_change_request_id,
    '2010-09-30 16:00:00.00' --create_tms,
);
-----


----- insert a parcel that is within 500 feet but not part of the same block
select distinct block_num, st_distance(p1.geometry, ST_GeomFromEWKT('SRID=2227;POINT(6006839.81943328 2121446.42553871)'))
from parcels p1
where 1=1
and st_distance(p1.geometry, ST_GeomFromEWKT('SRID=2227;POINT(6006839.81943328 2121446.42553871)')) > 500
order by st_distance(p1.geometry, ST_GeomFromEWKT('SRID=2227;POINT(6006839.81943328 2121446.42553871)'))
limit 10


select * 
from parcels where block_num = '0045'

parcel_id = 1924


INSERT INTO address_x_parcels(
	parcel_id,
	address_id,
    activate_change_request_id,
    retire_change_request_id,
    create_tms
)
VALUES (
  1924, -- parcel id 
  981976, -- address id
    0,                          --activate_change_request_id
    null, 			              --retire_change_request_id,
    '2010-09-30 16:00:00.00' --create_tms,
);






*/

