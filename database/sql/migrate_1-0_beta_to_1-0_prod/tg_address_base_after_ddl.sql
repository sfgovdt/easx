
-- DROP FUNCTION _eas_address_base_after() cascade;

CREATE FUNCTION _eas_address_base_after() RETURNS trigger AS $_eas_address_base_after$

    DECLARE

        _row_count integer = 0;
        _action character(10) = null;
        _message character varying(256);
        REVIEW_TYPE_STREET_RETIRED int = 5; -- street is retired

    BEGIN

        -- Some updates do not change data.
        -- http://sfgovdt.jira.com/browse/MAD-66

        _action = null;

        IF TG_OP = 'UPDATE' THEN
            IF NEW.retire_tms IS NOT NULL THEN
                _action = 'retire';

                UPDATE address_review
                SET resolution_tms = NEW.retire_tms,
                    last_update_tms = NEW.retire_tms,
                    last_update_user_id = 1,
                    concurrency_id = concurrency_id + 1
                WHERE resolution_tms is null
                and review_type_id = REVIEW_TYPE_STREET_RETIRED
                and address_id = (
                    select a.address_id
                    from addresses a
                    inner join address_base ab on ab.address_base_id = a.address_base_id
                    where a.address_base_flg = true
                    and a.address_base_id = NEW.address_base_id
                );

            ELSIF (NEW.geometry <-> OLD.geometry > 0.0001) THEN
                -- If the "distance between" is greater than the specified threshold, then this is an update.
                -- This use of a threshold is necessary because we re-project the geometry with each trip between client and server. 
                _action = 'update';
            ELSE
                _action = 'no change';
            END IF;
        ELSE
            _action = 'insert';
        END IF;


        INSERT INTO address_base_history(
            address_base_id,
            base_address_prefix,
            base_address_num,
            base_address_suffix,
            create_tms,
            retire_tms,
            zone_id,
            street_segment_id,
            distance_to_segment,
            geometry,
            unq_adds_id,
            last_change_tms,
            history_action
        )
        SELECT
            NEW.address_base_id,
            NEW.base_address_prefix,
            NEW.base_address_num,
            NEW.base_address_suffix,
            NEW.create_tms,
            NEW.retire_tms,
            NEW.zone_id,
            NEW.street_segment_id,
            NEW.distance_to_segment,
            NEW.geometry,
            NEW.unq_adds_id,
            NEW.last_change_tms,
            _action
        ;

        RETURN NEW;

    END;
$_eas_address_base_after$ LANGUAGE plpgsql;

CREATE TRIGGER _eas_address_base_after AFTER INSERT OR UPDATE ON address_base
    FOR EACH ROW EXECUTE PROCEDURE _eas_address_base_after();
