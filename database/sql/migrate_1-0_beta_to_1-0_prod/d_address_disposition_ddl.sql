
alter table d_address_disposition
	drop CONSTRAINT dad_pk;

alter table d_address_disposition
	add CONSTRAINT dad_pk PRIMARY KEY (disposition_code),
	ALTER COLUMN disposition_code SET NOT NULL;

alter table d_address_disposition
	drop column disposition_id;

INSERT INTO d_address_disposition(disposition_code, disposition_description)
VALUES (2, 'placeholder');
