
DROP TABLE address_source_data;

CREATE TABLE address_sources
(
    id serial NOT NULL,
    eas_id int,
    eas_table character varying(64),
    source_id int,
    source_system char(3),
    CONSTRAINT address_sources_pk PRIMARY KEY (id)
)
WITH (OIDS=FALSE);
ALTER TABLE address_sources OWNER TO postgres;
