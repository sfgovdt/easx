
ALTER TABLE addresses
   ADD COLUMN last_change_tms timestamp without time zone;

ALTER TABLE addresses ALTER column unit_num TYPE character varying(10);

ALTER TABLE addresses ALTER COLUMN unit_num SET DEFAULT NULL;

ALTER TABLE addresses DROP COLUMN unit_num_prefix;

ALTER TABLE addresses DROP COLUMN unit_num_suffix;

CREATE INDEX addresses_IDX_last_change_tms
  ON addresses
  USING btree (last_change_tms);

-- default date
update addresses set last_change_tms = '1970-01-01 00:00:00.000';

ALTER TABLE addresses add CONSTRAINT "addresses_NN_last_change_tms" CHECK (last_change_tms IS NOT NULL);

CREATE INDEX address_idx_unq_adds_id ON public.addresses USING btree (unq_adds_id);

