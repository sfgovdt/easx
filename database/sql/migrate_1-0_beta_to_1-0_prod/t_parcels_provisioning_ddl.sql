-- DROP TABLE parcels_provisioning cascade;

CREATE TABLE parcels_provisioning
(
  id serial NOT NULL,
  parcel_id int NOT NULL,
  create_user_id int NOT NULL,
  create_tms timestamp without time zone NOT NULL,
  provisioned_tms timestamp without time zone,
  CONSTRAINT "parcels_provisioning_PK" PRIMARY KEY (id),
  CONSTRAINT "parcels_provisioning_FK_parcels" FOREIGN KEY (parcel_id)
    REFERENCES parcels (parcel_id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "parcels_provisioning_FK_auth_user" FOREIGN KEY (create_user_id)
    REFERENCES auth_user (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);
ALTER TABLE parcels_provisioning OWNER TO postgres;

CREATE INDEX "parcels_provisioning_IDX_provisioned_tms"
  ON parcels_provisioning
  USING btree (provisioned_tms);

-- These are included here for convenience.
-- In the automated INIT these are generated and applied dynamically.
/*
ALTER TABLE parcels_provisioning OWNER TO eas_dbo;
GRANT ALL ON TABLE parcels_provisioning TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE parcels_provisioning TO django;
ALTER TABLE parcels_provisioning_id_seq OWNER TO eas_dbo;
GRANT ALL ON TABLE parcels_provisioning_id_seq TO eas_dbo;
GRANT ALL ON TABLE parcels_provisioning_id_seq TO django;
*/
