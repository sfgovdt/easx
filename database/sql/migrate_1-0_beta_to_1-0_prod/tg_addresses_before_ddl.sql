
-- DROP FUNCTION _eas_addresses_before() cascade;

CREATE FUNCTION _eas_addresses_before() RETURNS trigger AS $_eas_addresses_before$

    DECLARE

        _found          boolean;
        _address_number int;
        _street_name    character varying(60);
        _street_suffix  character varying(6);
        _message        character varying(256);

    BEGIN

        -- We do not use the _action variable in this trigger; its for docuemntation and consistency with other triggers.
        -- Some updates do not change data.
        -- http://sfgovdt.jira.com/browse/MAD-66

        IF TG_OP = 'DELETE' THEN
            RAISE EXCEPTION 'deleting addresses rows is not allowed';
        ELSIF TG_OP = 'UPDATE' THEN
            -- do nothing
        ELSE
            -- INSERT

            -- Do not allow duplicate unit addresses for the same base address.
            -- Do not allow a unit address with a null unit number.

            perform 1
            from addresses
            where unit_num is not distinct from NEW.unit_num
            and address_base_id = NEW.address_base_id
            and address_base_flg = false
            and retire_tms is null;

            _found = FOUND;

            IF (_found or (not NEW.address_base_flg and NEW.unit_num is null)) THEN
                select
                    ab.base_address_num, sn.base_street_name, sn.street_type into _address_number, _street_name, _street_suffix
                from address_base ab, streetnames sn
                where ab.address_base_id = NEW.address_base_id
                and ab.street_segment_id = sn.street_segment_id;

                _message = _address_number || ' ' || _street_name || ' ' || _street_suffix || ' unit ' || NEW.unit_num;
            end if;

            IF (_found) THEN
                RAISE EXCEPTION 'A duplicate unit was found (%). The unit value must be unique. | The insert into the addresses table was rejected by the trigger.', _message;
            END IF;

            IF (not NEW.address_base_flg and NEW.unit_num is null) THEN
                RAISE EXCEPTION 'An invalid unit was found (%). The unit value must be specified. | The insert into the addresses table was rejected by the trigger.', _message;
            END IF;

        END IF;

        RETURN NEW;

    END;
$_eas_addresses_before$ LANGUAGE plpgsql;

CREATE TRIGGER _eas_addresses_before BEFORE INSERT OR UPDATE OR DELETE ON addresses
    FOR EACH ROW EXECUTE PROCEDURE _eas_addresses_before();
