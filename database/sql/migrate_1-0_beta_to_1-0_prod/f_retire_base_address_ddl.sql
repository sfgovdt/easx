-- Function: _eas_retire_base_address()

DROP FUNCTION IF EXISTS _eas_retire_base_address(
    _address_base_id int,
    _current_tms timestamp without time zone
);

CREATE OR REPLACE FUNCTION _eas_retire_base_address(
    _address_base_id int,
    _current_tms timestamp without time zone
)
RETURNS void AS
$BODY$
DECLARE
BEGIN

    update address_x_parcels
    set
        retire_change_request_id = 0,
        last_change_tms = _current_tms,
        retire_tms = _current_tms
    where address_id in (
        select address_id
        from addresses
        where address_base_id = _address_base_id
    ) and retire_tms is null;

    update addresses
    set
        retire_tms = _current_tms,
        retire_change_request_id = 1,
        update_change_request_id = 1
    where address_base_id = _address_base_id
    and retire_tms is null;

    update address_base
    set retire_tms = _current_tms,
        last_change_tms = _current_tms
    where address_base_id = _address_base_id
    and retire_tms is null;


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION _eas_retire_base_address(
    _address_base_id int,
    _current_tms timestamp without time zone
)
OWNER TO postgres;
