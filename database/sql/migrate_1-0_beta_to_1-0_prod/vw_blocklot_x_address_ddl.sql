-- View: vw_blocklot_x_address

-- DROP VIEW vw_blocklot_x_address;

CREATE OR REPLACE VIEW vw_blocklot_x_address AS
 SELECT a.blk_lot, a.date_map_drop, b.parcel_id, b.address_id, c.address_base_id
   FROM parcels a, address_x_parcels b, addresses c
  WHERE a.parcel_id = b.parcel_id AND b.address_id = c.address_id;

ALTER TABLE vw_blocklot_x_address OWNER TO postgres;

