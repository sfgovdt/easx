
CREATE INDEX parcels_block_num_idx ON parcels
  USING btree (block_num);

ALTER TABLE parcels
    ADD CONSTRAINT blk_lot_UNQ
    UNIQUE (blk_lot);
