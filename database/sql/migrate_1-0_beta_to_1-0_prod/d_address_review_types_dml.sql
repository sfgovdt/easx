
delete 
from address_review
where review_type_id in (
	select review_type_id
	from d_address_review_types 
	where review_type_desc in ('parcel alteration', 'street alteration')
);

DELETE FROM d_address_review_types WHERE review_type_desc='street alteration';

DELETE FROM d_address_review_types WHERE review_type_desc='parcel alteration';

-- We need to widen a column; to do this, we need to drop and recreate a view that uses the column.
-- We will recreate the view later in a separate file.
ALTER TABLE d_address_review_types ALTER review_type_desc TYPE character varying(64);
UPDATE d_address_review_types SET review_type_desc = 'distance between address and parcel is greater than 500 feet' WHERE review_type_id = 4;
