# MacOS developer setup

## Install dependencies and IDE
- Install [Homebrew](https://brew.sh/)
- Install [Visual Studio Code](https://code.visualstudio.com/)
- Install application dependencies via Homebrew: 
```
brew install python3 gdal git
```

## Setup EAS
- Clone the EASX repo:
```
git clone git@bitbucket.org:sfgovdt/easx.git
```
- Rename the `easx` directory `eas`.
- Clone your config repo into `eas/web`:
```
cd eas/web
git clone git@{URL to your config repo}
```
- Rename the cloned folder `config`.
- Create a virtual environment and install python dependencies:
```
cd ../..
python3 -m venv env
source env/bin/activate
pip install -r eas/requirements.txt
```

## Run EAS
- Open your eas folder in Visual Studio Code
- Install the [Visual Studio Code Python Extension](https://marketplace.visualstudio.com/items?itemName=ms-python.python)
- From the command palette, choose "Python: Select Interpreter" and provide it with the path to your virtual environment when prompted
- Running EAS assumes access to PostgreSQL on localhost:5433 and Geoserver on localhost:8081, we normally create SSH tunnels to our development environment for this:
```
ssh -f -L localhost:8081:localhost:8080 -N {development geoserver host}
ssh -f -L localhost:5433:localhost:5432 -N {development database host}
```
- You should now be able to run EAS either via the VS Code debugger using the provided launch configuration, or from the integrated terminal like so:
```
cd web
python manage.py runserver
```
  