import urllib.request, urllib.parse, urllib.error
import http.client
import json

httpConnection = None
try:
    encodedParameters = urllib.parse.urlencode({'token': 'my_fme_auth_token'})
    httpConnection = http.client.HTTPConnection('my_host_name')
    httpConnection.request("GET", '/fmerest/repositories/Basemap_Streets.json?' + encodedParameters)
    response = httpConnection.getresponse()
    if response.status != 200:
        raise IOError('request failed - status: %s - message: %s' % (str (response.status), str(response.msg)))
    contentString = response.read()
    response = json.loads(contentString)
    workspaces = response['serviceResponse']['repository']['workspaces']['workspace']
    exists = False
    for workspace in workspaces:
        if (workspace['name'] == '%s.fmw' % 'streets_source_to_staging' ):
            exists = True
except Exception as e:
    raise
finally:
    if httpConnection:
        httpConnection.close()







