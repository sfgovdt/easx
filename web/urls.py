from django.conf.urls import include, url
from django.conf import settings
from MAD.views import views_reports, views_search, views_change_requests, views_authentication, views_domains, views
from django.views.static import serve

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^'+settings.VIRTUAL_DIR + r'admin/', admin.site.urls),

    # MAD APPLICATION:
    # url(r'^'+settings.VIRTUAL_DIR, include(('MAD.urls.urlpatterns')))
    #(r'^admin/(.*)', include(admin.site.urls)),
    url(r'^Media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    
    # main site page
    url(r'^'+settings.VIRTUAL_DIR + r'$', views.Index),
    url(r'^'+settings.VIRTUAL_DIR + r'debug', views.Index, {'mode': 'debug'}),
    url(r'^'+settings.VIRTUAL_DIR + r'build', views.Index, {'mode': 'build'}),
    url(r'^'+settings.VIRTUAL_DIR + r'js/(\d+)$', views.Js),
    
    # reports
    url(r'^'+settings.VIRTUAL_DIR + r'AddressReport/$', views_reports.AddressReport),
    url(r'^'+settings.VIRTUAL_DIR + r'Identifiers/$', views_reports.Identifiers),
    url(r'^'+settings.VIRTUAL_DIR + r'UnitsForAddressReport/$', views_reports.UnitsForAddressReport),
    url(r'^'+settings.VIRTUAL_DIR + r'ParcelsForAddressReport/$', views_reports.ParcelsForAddressReport),
    url(r'^'+settings.VIRTUAL_DIR + r'Aliases/$', views_reports.Aliases),
    url(r'^'+settings.VIRTUAL_DIR + r'History/$', views_reports.History),
    url(r'^'+settings.VIRTUAL_DIR + r'BaseGeometryForParcels/$', views_reports.BaseGeometryForParcels),

    # searches
    url(r'^'+settings.VIRTUAL_DIR + r'search/address$', views_search.AddressSearch),
    url(r'^'+settings.VIRTUAL_DIR + r'search/apn$', views_search.ApnSearch),
    url(r'^'+settings.VIRTUAL_DIR + r'search/addressbbox$', views_search.AddressBBoxWFS),
    url(r'^'+settings.VIRTUAL_DIR + r'search/virtualaddressbbox$', views_search.VirtualAddressBBoxWFS),
    url(r'^'+settings.VIRTUAL_DIR + r'search/parcelToAddresses$', views_search.parcelToAddresses),
    url(r'^'+settings.VIRTUAL_DIR + r'search/parcel/(?P<apn>\w+)/$', views_search.getParcelByApn),
    url(r'^'+settings.VIRTUAL_DIR + r'search/addressToParcels$', views_search.addressToParcels),
    url(r'^'+settings.VIRTUAL_DIR + r'search/pointToParcels$', views_search.pointToParcels),
    url(r'^'+settings.VIRTUAL_DIR + r'search/addressinfo/(\w+)$', views_search.AddressInfo),

    # change requests
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/mine/$', views_change_requests.getMyChangeRequests),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/available/$', views_change_requests.getAvailableChangeRequests),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/save/$', views_change_requests.saveCr, {'review_status_id': None}),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/edit/(?P<crId>\d+)/$', views_change_requests.getCr, {'review_status_id': 4}),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/submitforreview/$', views_change_requests.saveCr, {'review_status_id': 5}),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/approve/$', views_change_requests.saveCr, {'review_status_id': 2}),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/reject/$', views_change_requests.saveCr, {'review_status_id': 3}), 
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/acceptforreview/$', views_change_requests.saveCr, {'review_status_id': 1}),   
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/(?P<crId>\d+)/$', views_change_requests.getCr),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/delete/(?P<crId>\d+)/$', views_change_requests.deleteCr),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/report/(?P<changeRequestId>\d+)/$', views_change_requests.getChangeRequestReport),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/report/axp/(?P<axpId>\d+)/create/$', views_change_requests.getChangeRequestReportForAxpCreate),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/report/axp/(?P<axpId>\d+)/retire/$', views_change_requests.getChangeRequestReportForAxpRetire),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/report/address/(?P<baseAddrId>\d+)/$', views_change_requests.getChangeRequestReportForAddress),
    url(r'^'+settings.VIRTUAL_DIR + r'changeRequest/report/addresses/(?P<changeRequestId>\d+)/$', views_change_requests.getAddressesForChangeRequest),

    url(r'^'+settings.VIRTUAL_DIR + r'getbaseaddr/(?P<baseAddrId>\d+)/$', views_change_requests.getBaseAddr),

    # provision parcel
    # example values
    # block: ['1234', '1234A']
    # lot: ['001', '001T']
    # lon: -123.0001111 (we have hardcoded negative longitude)
    # lat: 42.0001111
    url(r'^'+settings.VIRTUAL_DIR + r'provision/parcel/block/(?P<block>\w+)/lot/(?P<lot>\w+)/x/(?P<x>-\d+\.\d+)/y/(?P<y>\d+\.\d+)/$', views.provisionParcel),

    # loosely based on OGC standard
    #     http://www.opengeospatial.org/standards/requests/89
    # e.g. usage /geocode/findAddressCandidates?f=json&Address=20 Sussex St&Zip=94131
    url(r'^'+settings.VIRTUAL_DIR + r'geocode/findAddressCandidates/$', views.findAddressCandidates),

    url(r'^'+settings.VIRTUAL_DIR + r'geocode/getAddress/$', views.get_address),

    # domains
    url(r'^'+settings.VIRTUAL_DIR + r'domain/changerequest/$', views_domains.getChangeRequestDomains),

    url(r'^'+settings.VIRTUAL_DIR + r'address/proposeAt/(?P<geom>.*)/$', views_change_requests.proposeAddress),

    # authentication
    url(r'^'+settings.VIRTUAL_DIR + r'login/$', views_authentication.login),
    url(r'^'+settings.VIRTUAL_DIR + r'logout/$', views_authentication.logout),
    url(r'^'+settings.VIRTUAL_DIR + r'user/$', views_authentication.getUser),
    url(r'^'+settings.VIRTUAL_DIR + r'user/resetpassword/$', views_authentication.resetPassword),
    url(r'^'+settings.VIRTUAL_DIR + r'user/changepassword/$', views_authentication.changePassword),

]
