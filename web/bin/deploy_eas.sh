#!/bin/bash


. ./set_eas_env.sh

HOME_DIR=`pwd`
CHANGE_NOTIF_SCRIPT=$EAS_HOME_LIVE/bin/xmit_change_notifications.bsh
DJANGO_SETTINGS_DEPLOY=$EAS_HOME_LIVE/settings_env/live/settings_deploy.py
PYTHON_EXE=/var/www/html/eas_venv/bin/python
PYTHON_HOME=/var/www/html/eas_venv/lib/python3.6
IFCONFIG_EXE=/sbin/ifconfig

stopChangeNotificationDaemon() {
    # todo - We should probably encapsulate this function into its own bash.
    if [ -f "$CHANGE_NOTIF_SCRIPT" ]; then
        echo making sure the change notification process is stopped
        chmod u+x $CHANGE_NOTIF_SCRIPT
        $CHANGE_NOTIF_SCRIPT stop
        sleep 3
        XMIT_DAEMON_PID=`ps -ef | grep xmit_daemon | grep -v grep | awk '{print $2}'`
        if [ -n "${XMIT_DAEMON_PID}" ]
        then
          kill $XMIT_DAEMON_PID
          rm $EAS_HOME_LIVE/eas_xmit_running
        fi
    fi
}


archiveExistingDeploy() {
    echo archiving existing deploy
    TARGET_FILE=./archive/eas_`date +%Y%m%d_%H%M%S`.tar
    SOURCE_DIR=$EAS_HOME_LIVE
    tar -cf $TARGET_FILE $SOURCE_DIR
}


deploySource() {

    # This is a little complicated for my bash skills - using python instead.

    TARGET_DIR=$EAS_HOME_LIVE
    BIN_DIR=$EAS_HOME_LIVE/bin
    rm -rf $TARGET_DIR
    mkdir $TARGET_DIR
    $PYTHON_EXE $HOME_DIR/deploy_eas_source.py $EAS_HOME_LIVE $DJANGO_SETTINGS_DEPLOY $EAS_COMMIT $CONFIG_COMMIT $BITBUCKET_USER $BITBUCKET_PASSWORD

    if [[ ! $? == 0 ]]
    then
        echo "deploy of source code failed"
        exit 1
    fi

    pushd $TARGET_DIR

    # echo converting linefeeds from dos to unix
    # # f:files, q:quietly
    # find . -name "*.bsh" -type f -print | xargs dos2unix -q
    # find . -name "*.sh" -type f -print | xargs dos2unix -q

    echo setting up link to admin media
    sudo $PYTHON_EXE $EAS_HOME_LIVE/manage.py collectstatic

    # todo - make sure permissions are set properly
    popd
    chown -R apache:apache $TARGET_DIR
    chmod -R g+w $TARGET_DIR
    chmod -R o-w $TARGET_DIR
    chmod -R ug+x $BIN_DIR
    find . -name "*.bsh" -type f -print | sudo xargs chmod gu+x
    find . -name "*.sh" -type f -print | sudo xargs chmod gu+x

}

startChangeNotificationDaemon() {
    IP=`$IFCONFIG_EXE | grep "inet addr" | head -1 | awk '{print $2'} | awk 'BEGIN { FS = ":"}; {print $2}'`
    if [ $IP = "10.250.60.155" ] ; then
        echo starting change notification process
        chmod u+x $CHANGE_NOTIF_SCRIPT
        $CHANGE_NOTIF_SCRIPT start
        XMIT_DAEMON_PID=`ps -ef | grep xmit_daemon | grep -v grep | awk '{print $2}'`
        if [ ! -n "${XMIT_DAEMON_PID}" ]
        then
          echo Warning: it appears as if the change notification daemon did not start!
        else
          echo change notification daemon started
        fi
    fi
}


usage() {
    echo "Usage: $0 <EAS_COMMIT> <CONFIG_COMMIT> <BITBUCKET_USER> <BITBUCKET_PASSWORD>"
    exit
}

if [[ ! $# == 4 ]]
then
   usage
fi
EAS_COMMIT=$1
CONFIG_COMMIT=$2
BITBUCKET_USER=$3
BITBUCKET_PASSWORD=$4


echo starting web deploy of eas

deploySource
./set_eas_mode.sh "LIVE"
startChangeNotificationDaemon
echo done!
