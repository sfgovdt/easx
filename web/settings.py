
# A good source for understanding these settings is "The Definitive Guide to Django"
# 

import os
import sys
import logging
import logging.handlers
from ctypes.util import find_library


##### application info
# This information is displayed in the UI; to see how this works, read the following:
#   releases/bin/deploy.sh
#   web/MAD/templates/index.html
#   web/views/views.py.index
#   Media/js/MAD/MAD-App.js
APPLICATION_NAME = 'Enterprise Addressing System'
APPLICATION_NAME_SHORT = 'eas'
APPLICATION_ENV = 'DESKTOP'
#####

ADMINS = (
    ('Paul McCullough - SFGOV', 'paul.mccullough@sfgov.org'),
    ('Jeff Johnson - SFGOV', 'jeffrey.johnson@sfgov.org'),
    ('Paul E. McCullough - GMAIL', 'p.e.mccullough@gmail.com'),
    ('Jeff A. Johnson - GMAIL', 'jeff.johnson.a@gmail.com'),
    ('Christophe Pettus - PGX', 'christophe.pettus@pgexperts.com'),
    ('Rob Gaston - FARGEO', 'rgaston@fargeo.com'),
)
MANAGERS = ADMINS

DEFAULT_FROM_EMAIL = 'noreply@sfgov.org'
DATABASE_ENGINE = "django.contrib.gis.db.backends.postgis"
TIME_ZONE = 'America/Los_Angeles'
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = False
GOOGLE_ANALYTICS_TRACKING_ID = None

STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

dev_database = {
    "default": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "eas_dev",
        "USER": "",
        "PASSWORD": "",
        "HOST": "localhost",
        "PORT": "5433",
        "POSTGIS_TEMPLATE": "template_postgis",
    }
}
if os.name == 'posix':
    GEOS_LIB_NAMES = ['geos_c', 'GEOS']
    try:
        KEY = 'EAS_REPOS_HOME'
        EAS_REPOS_HOME = os.environ[KEY]
        sys.path.append(os.path.join(EAS_REPOS_HOME, 'config', 'web'))
        MAD_ROOT = os.path.join(EAS_REPOS_HOME, 'eas', 'web')
        MEDIA_ROOT = os.path.join(EAS_REPOS_HOME, 'eas', 'Media')
        # Accept default for ADMIN_MEDIA_PREFIX
        HTCONF_FILENAMES = [MAD_ROOT + '/web/eas.htconf.live']
        DEBUG = True
        DATABASES = dev_database
        from config.web.settings_env.live.settings_deploy import * # needed for local DEV
    except Exception:
        # If EAS_REPOS_HOME variable is not present, we assume we are in a deployed environment
        MAD_ROOT = '/var/www/html/eas_live'
        MEDIA_ROOT = MAD_ROOT + '/Media/'
        STATIC_ROOT = MEDIA_ROOT + 'static/'
        STATIC_URL = '/Media/static/'
        ADMIN_MEDIA_PREFIX = '/Media/static/admin/'
        HTCONF_FILENAMES = [
            MAD_ROOT + '/eas.htconf.live',
            MAD_ROOT + '/eas.htconf.maint',
            MAD_ROOT + '/eas.htconf.standby_sf',
            MAD_ROOT + '/eas.htconf.standby_dr'
        ]
        from settings_env.live.settings_deploy import * # needed for QA
        DATABASES = {
            "default": {
                "ENGINE": DATABASE_ENGINE,
                "NAME": DATABASE_NAME,
                "USER": DATABASE_USER,
                "PASSWORD": DATABASE_PASSWORD,
                "HOST": DATABASE_HOST,
                "PORT": DATABASE_PORT,
                "POSTGIS_TEMPLATE": "template_postgis",
            }
        }
        pass
elif os.name == 'nt':
    try:
        KEY = 'EAS_REPOS_HOME'
        EAS_REPOS_HOME = os.environ[KEY]
    except Exception:
        sys.stdout.write('missing system variable: %s\n' % KEY)
        sys.exit(-1)
    sys.path.append(os.path.join(EAS_REPOS_HOME, 'config', 'web'))
    MAD_ROOT = os.path.join(EAS_REPOS_HOME, 'eas', 'web')
    MEDIA_ROOT = os.path.join(EAS_REPOS_HOME, 'eas', 'Media')
    # Accept default for ADMIN_MEDIA_PREFIX
    GEOS_LIB_NAMES = ['geos_c']
    HTCONF_FILENAMES = [MAD_ROOT + '/web/eas.htconf.live']
    DEBUG = True
    DATABASES = dev_database
    from config.web.settings_env.live.settings_deploy import * # needed for local DEV
else:
    raise ImportError('Unsupported OS "%s"' % os.name)

# location of this import is critical
# from settings_env.live.settings_deploy import * # needed for QA

APP_LOG_FILENAME            = MAD_ROOT + '/eas.log'

XMIT_LOG_FILENAME           = MAD_ROOT + '/eas_xmit.log'
XMIT_RUNNING_SEMAPHORE      = MAD_ROOT + '/eas_xmit_running'
XMIT_STOP_REQUEST_SEMAPHORE = MAD_ROOT + '/eas_xmit_stop_request'
XMIT_POLLING_INTERVAL = 10
XMIT_HOST = 'dbiweb02.sfgov.org'
XMIT_PORT = 80
# The following are in settings_deploy.py
# XMIT_PATH

GEOS_LIBRARY_PATH = None
# Taken from libgeos.py
for geos_lib_name in GEOS_LIB_NAMES:
    geos_library_path = find_library(geos_lib_name)
    if not geos_library_path is None:
        GEOS_LIBRARY_PATH = geos_library_path
        break

if GEOS_LIBRARY_PATH is None:
    raise EnvironmentError('geos c library not found')

MEDIA_URL = 'Media/'

SECRET_KEY = '$$_ubp&8ognlj%qs83f%&t!on9ed$!478z@m9(_z$%^+onpdz4'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': (MAD_ROOT + '/MAD/templates', MAD_ROOT + '/MAD/templates/xml'),
        'APP_DIRS': False,
        'OPTIONS': {
            'debug': True,
            'loaders': ['django.template.loaders.filesystem.Loader', 'django.template.loaders.app_directories.Loader'],
            'context_processors': [
                "django.contrib.auth.context_processors.auth",
                'django.contrib.messages.context_processors.messages'
            ]
        }
    }
]

MIDDLEWARE = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    # 'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

VIRTUAL_DIR = ''

ROOT_URLCONF = 'urls'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'django.contrib.gis',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'ebpub',
    'MAD',
)


# This line is necessary to use the ebpub lib (Every Block), which we use for address parsing.
# todo - maybe we just need to add this to our app list?
METRO_LIST = ()

LOGGING_MAX_BYTES = 10485760
LOGGING_BACKUP_COUNT = 2
LOGGING_TO_ADDRESSES = [] # minor abuse of "constant"

for tuple in ADMINS:
    LOGGING_TO_ADDRESSES.append(tuple[1])

LOGGING = {
    'version': 1,
    'formatters': {
        'simple': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
        }
    },

    'handlers': {
        'XMIT': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'INFO',
            'formatter': 'simple',
            'filename': XMIT_LOG_FILENAME,
            'backupCount': LOGGING_BACKUP_COUNT,
            'maxBytes': LOGGING_MAX_BYTES
        },
        'EAS': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'INFO',
            'formatter': 'simple',
            'filename': APP_LOG_FILENAME,
            'backupCount': LOGGING_BACKUP_COUNT,
            'maxBytes': LOGGING_MAX_BYTES
        }
    },
    'loggers': {
        'XMIT': {
            'handlers': ['XMIT'],
            'level': 'INFO'
        },
        'EAS': {
            'handlers': ['EAS'],
            'level': 'INFO'
        }
    }

}

DATETIME_FORMAT = 'm-d-Y H:i:s'
SFGIS_URL = 'http://gispub02.sfgov.org/website/sfparcel/showlotinfo.asp?LotIDValue='

TEST_RUNNER = "test_suites.EASTestRunner"

