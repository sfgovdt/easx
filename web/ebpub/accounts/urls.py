from django.conf.urls import include, url
from ebpub.savedplaces import views as savedplaces_views
from ebpub.preferences import views as preferences_views
from . import views # relative import

urlpatterns = [
    url(r'^dashboard/$', views.dashboard),
    url(r'^login/$', views.login),
    url(r'^logout/$', views.logout),
    url(r'^register/$', views.register),
    url(r'^password-change/$', views.request_password_change),
    url(r'^email-sent/$', 'django.views.generic.simple.direct_to_template', {'template': 'accounts/email_sent.html'}),
    url(r'^saved-places/add/$', savedplaces_views.ajax_save_place),
    url(r'^saved-places/delete/$', savedplaces_views.ajax_remove_place),
    url(r'^hidden-schemas/add/$', preferences_views.ajax_save_hidden_schema),
    url(r'^hidden-schemas/delete/$', preferences_views.ajax_remove_hidden_schema),
    url(r'^api/saved-places/$', savedplaces_views.json_saved_places),
    url(r'^c/$', views.confirm_email),
    url(r'^r/$', views.password_reset),
]
