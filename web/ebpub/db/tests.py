"""
Unit tests for db app.
"""

from django.test import TestCase
from ebpub.db.models import NewsItem, Attribute
import datetime

class ViewTestCase(TestCase):
    "Unit tests for views.py."
    fixtures = ('crimes',)

    def test_search(self):
        # response = self.client.get('')
        pass

    def test_newsitem_detail(self):
        # response = self.client.get('')
        pass

    def test_location_redirect(self):
        # redirect to neighborhoods by default
        response = self.client.get('/locations/')
        self.assertEqual(response.status_code, 301)
        self.assertEqual(response['Location'], 'http://testserver/locations/neighborhoods/')

    def test_location_type_detail(self):
        # response = self.client.get('')
        pass

    def test_location_detail(self):
        # response = self.client.get('')
        pass

    def test_schema_detail(self):
        response = self.client.get('/crime/')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/nonexistent/')
        self.assertEqual(response.status_code, 404)

    def test_schema_xy_detail(self):
        # response = self.client.get('')
        pass

    def test_filter_choices(self):
        # response = self.client.get('')
        pass

    def test_filter_detail(self):
        # response = self.client.get('')
        pass

    def test_filter_detail_month(self):
        # response = self.client.get('')
        pass

    def test_filter_detail_day(self):
        # response = self.client.get('')
        pass

class DatabaseExtensionsTestCase(TestCase):
    "Unit tests for the custom ORM stuff in models.py."
    fixtures = ('crimes',)

    def testAttributesLazilyLoaded(self):
        """
        Attributes are retrieved lazily the first time you access the
        `attributes` attribute.
        """
        # Turn DEBUG on and reset queries, so we can keep track of queries.
        # This is hackish.
        from django.conf import settings
        from django.db import connection
        connection.queries = []
        settings.DEBUG = True

        ni = NewsItem.objects.get(id=1)
        self.assertEqual(ni.attributes['case_number'], 'HM609859')
        self.assertEqual(ni.attributes['crime_date'], datetime.date(2006, 9, 19))
        self.assertEqual(ni.attributes['crime_time'], None)
        self.assertEqual(len(connection.queries), 3)

        connection.queries = []
        settings.DEBUG = False

    def testSetAllAttributesNonDict(self):
        """
        Setting `attributes` to something other than a dictionary will raise
        ValueError.
        """
        ni = NewsItem.objects.get(id=1)
        def setAttributeToNonDict():
            ni.attributes = 1
        self.assertRaises(ValueError, setAttributeToNonDict)

    def testSetAllAttributes1(self):
        """
        Attributes can be set by assigning a dictionary to the `attributes`
        attribute. As soon as `attributes` is assigned-to, the UPDATE query
        is executed in the database.
        """
        ni = NewsItem.objects.get(id=1)
        self.assertEqual(ni.attributes['case_number'], 'HM609859')
        ni.attributes = dict(ni.attributes, case_number='Hello')
        self.assertEqual(Attribute.objects.get(news_item__id=1).varchar01, 'Hello')

    def testSetAllAttributes2(self):
        """
        Setting attributes works even if you don't access them first.
        """
        ni = NewsItem.objects.get(id=1)
        ni.attributes = {
            'arrests': False,
            'beat_id': 214,
            'block_id': 25916,
            'case_number': 'Hello',
            'crime_date': datetime.date(2006, 9, 19),
            'crime_time': None,
            'domestic': False,
            'is_outdated': True,
            'location_id': 66,
            'police_id': None,
            'status': '',
            'type_id': 97
        }
        self.assertEqual(Attribute.objects.get(news_item__id=1).varchar01, 'Hello')

    def testSetAllAttributesNull(self):
        """
        If you assign to NewsItem.attributes and the dictionary doesn't include
        a value for every field, a None/NULL will be inserted for values that
        aren't represented in the dictionary.
        """
        ni = NewsItem.objects.get(id=1)
        ni.attributes = {'arrests': False}
        ni = NewsItem.objects.get(id=1)
        self.assertEqual(ni.attributes['arrests'], False)
        self.assertEqual(ni.attributes['beat_id'], None)
        self.assertEqual(ni.attributes['block_id'], None)
        self.assertEqual(ni.attributes['case_number'], None)
        self.assertEqual(ni.attributes['crime_date'], None)
        self.assertEqual(ni.attributes['crime_time'], None)
        self.assertEqual(ni.attributes['domestic'], None)
        self.assertEqual(ni.attributes['is_outdated'], None)
        self.assertEqual(ni.attributes['location_id'], None)
        self.assertEqual(ni.attributes['police_id'], None)
        self.assertEqual(ni.attributes['status'], None)
        self.assertEqual(ni.attributes['type_id'], None)

    def testSetSingleAttribute1(self):
        """
        Setting a single attribute will result in an immediate query setting
        just that attribute.
        """
        ni = NewsItem.objects.get(id=1)
        self.assertEqual(ni.attributes['case_number'], 'HM609859')
        ni.attributes['case_number'] = 'Hello'
        self.assertEqual(Attribute.objects.get(news_item__id=1).varchar01, 'Hello')

    def testSetSingleAttribute2(self):
        """
        Setting single attributes works even if you don't access them first.
        """
        ni = NewsItem.objects.get(id=1)
        ni.attributes['case_number'] = 'Hello'
        self.assertEqual(Attribute.objects.get(news_item__id=1).varchar01, 'Hello')

    def testSetSingleAttribute3(self):
        """
        Setting a single attribute will result in the value being cached.
        """
        ni = NewsItem.objects.get(id=1)
        self.assertEqual(ni.attributes['case_number'], 'HM609859')
        ni.attributes['case_number'] = 'Hello'
        self.assertEqual(ni.attributes['case_number'], 'Hello')

    def testSetSingleAttribute4(self):
        """
        Setting a single attribute will result in the value being cached, even
        if you don't access the attribute first.
        """
        ni = NewsItem.objects.get(id=1)
        ni.attributes['case_number'] = 'Hello'
        self.assertEqual(ni.attributes['case_number'], 'Hello')

    def testSetSingleAttributeNumQueries(self):
        """
        When setting an attribute, the system will only use a single query --
        i.e., it won't have to retrieve the attributes first simply because
        code accessed the NewsItem.attributes attribute.
        """
        # Turn DEBUG on and reset queries, so we can keep track of queries.
        # This is hackish.
        from django.conf import settings
        from django.db import connection
        connection.queries = []
        settings.DEBUG = True

        ni = NewsItem.objects.get(id=1)
        ni.attributes['case_number'] = 'Hello'
        self.assertEqual(len(connection.queries), 3)

        connection.queries = []
        settings.DEBUG = False

    def testBlankAttributes(self):
        """
        If a NewsItem has no attributes set, accessing NewsItem.attributes will
        return an empty dictionary.
        """
        Attribute.objects.filter(news_item__id=1).delete()
        ni = NewsItem.objects.get(id=1)
        self.assertEqual(ni.attributes, {})

    def testSetAttributesFromBlank(self):
        """
        When setting attributes on a NewsItem that doesn't have attributes yet,
        the underlying implementation will use an INSERT statement instead of
        an UPDATE.
        """
        Attribute.objects.filter(news_item__id=1).delete()
        ni = NewsItem.objects.get(id=1)
        ni.attributes = {
            'arrests': False,
            'beat_id': 214,
            'block_id': 25916,
            'case_number': 'Hello',
            'crime_date': datetime.date(2006, 9, 19),
            'crime_time': None,
            'domestic': False,
            'is_outdated': True,
            'location_id': 66,
            'police_id': None,
            'status': '',
            'type_id': 97
        }
        self.assertEqual(Attribute.objects.get(news_item__id=1).varchar01, 'Hello')

    def testSetSingleAttributeFromBlank(self):
        """
        When setting a single attribute on a NewsItem that doesn't have
        attributes yet, the underlying implementation will use an INSERT
        statement instead of an UPDATE.
        """
        Attribute.objects.filter(news_item__id=1).delete()
        ni = NewsItem.objects.get(id=1)
        ni.attributes['case_number'] = 'Hello'
        self.assertEqual(Attribute.objects.get(news_item__id=1).varchar01, 'Hello')

    def testAttributeFromBlankSanity(self):
        """
        Sanity check for munging attribute data from blank.
        """
        Attribute.objects.filter(news_item__id=1).delete()
        ni = NewsItem.objects.get(id=1)
        self.assertEqual(ni.attributes, {})
        ni.attributes['case_number'] = 'Hello'
        self.assertEqual(ni.attributes['case_number'], 'Hello')
        self.assertEqual(Attribute.objects.get(news_item__id=1).varchar01, 'Hello')
