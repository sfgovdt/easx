from django.conf import settings
from django.conf.urls import include, url
from ebpub.alerts import views as alert_views
from ebpub.db import feeds, views
from ebpub.db.constants import BLOCK_URL_REGEX
from ebpub.petitions import views as petition_views
from ebpub.utils.urlresolvers import metro_patterns

if settings.DEBUG:
    urlpatterns = [
        url(r'^(?P<path>(?:images|scripts|styles|openlayers).*)$', 'django.views.static.serve', {'document_root': settings.EB_MEDIA_ROOT}),
    ]
else:
    urlpatterns = []

urlpatterns.extend([
    url(r'^$', views.homepage),
    url(r'^search/$', views.search),
    url(r'^news/$', views.schema_list),
    url(r'^locations/$', 'django.views.generic.simple.redirect_to', {'url': '/locations/neighborhoods/'}),
    url(r'^locations/([-_a-z0-9]{1,32})/$', views.location_type_detail),
    url(r'^locations/([-_a-z0-9]{1,32})/([-_a-z0-9]{1,32})/$', views.place_detail, {'place_type': 'location', 'detail_page': True}),
    url(r'^locations/([-_a-z0-9]{1,32})/([-_a-z0-9]{1,32})/overview/$', views.place_detail, {'place_type': 'location'}),
    url(r'^locations/([-_a-z0-9]{1,32})/([-_a-z0-9]{1,32})/feeds/$', views.feed_signup, {'place_type': 'location'}),
    url(r'^locations/([-_a-z0-9]{1,32})/([-_a-z0-9]{1,32})/alerts/$', alert_views.signup, {'place_type': 'location'}),
    url(r'^rss/(.+)/$', feeds.feed_view),
    url(r'^maps/', include('ebgeo.maps.urls')),
    url(r'^accounts/', include('ebpub.accounts.urls')),
    url(r'^validate-address/$', views.validate_address),
    url(r'^alerts/unsubscribe/\d\d(\d{1,10})\d/$', alert_views.unsubscribe),
    url(r'^petitions/([-\w]{4,32})/$', petition_views.form_view, {'is_schema': False}),
    url(r'^petitions/([-\w]{4,32})/thanks/$', petition_views.form_thanks, {'is_schema': False}),
    url(r'^api/wkt/$', views.ajax_wkt),
    url(r'^api/map-popups/$', views.ajax_map_popups),
    url(r'^api/place-recent-items/$', views.ajax_place_newsitems),
    url(r'^api/place-lookup-chart/$', views.ajax_place_lookup_chart),
    url(r'^api/place-date-chart/$', views.ajax_place_date_chart),
    url(r'^api/map-browser/location-types/$', views.ajax_location_type_list),
    url(r'^api/map-browser/location-types/(\d{1,9})/$', views.ajax_location_list),
    url(r'^api/map-browser/locations/(\d{1,9})/$', views.ajax_location),
])

urlpatterns.extend(metro_patterns(
    multi=(
        (r'^streets/$', views.city_list),
        (r'^streets/([-a-z]{3,40})/$', views.street_list),
        (r'^streets/([-a-z]{3,40})/([-a-z0-9]{1,64})/$', views.block_list),
        (r'^streets/([-a-z]{3,40})/([-a-z0-9]{1,64})/%s/$' % BLOCK_URL_REGEX, views.place_detail, {'place_type': 'block', 'detail_page': True}),
        (r'^streets/([-a-z]{3,40})/([-a-z0-9]{1,64})/%s/overview/$' % BLOCK_URL_REGEX, views.place_detail, {'place_type': 'block'}),
        (r'^streets/([-a-z]{3,40})/([-a-z0-9]{1,64})/%s/feeds/$' % BLOCK_URL_REGEX, views.feed_signup, {'place_type': 'block'}),
        (r'^streets/([-a-z]{3,40})/([-a-z0-9]{1,64})/%s/alerts/$' % BLOCK_URL_REGEX, alert_views.signup, {'place_type': 'block'}),
    ),
    single=(
        (r'^streets/()$', views.street_list),
        (r'^streets/()([-a-z0-9]{1,64})/$', views.block_list),
        (r'^streets/()([-a-z0-9]{1,64})/%s/$' % BLOCK_URL_REGEX, views.place_detail, {'place_type': 'block', 'detail_page': True}),
        (r'^streets/()([-a-z0-9]{1,64})/%s/overview/$' % BLOCK_URL_REGEX, views.place_detail, {'place_type': 'block'}),
        (r'^streets/()([-a-z0-9]{1,64})/%s/feeds/$' % BLOCK_URL_REGEX, views.feed_signup, {'place_type': 'block'}),
        (r'^streets/()([-a-z0-9]{1,64})/%s/alerts/$' % BLOCK_URL_REGEX, alert_views.signup, {'place_type': 'block'}),
    )
))

urlpatterns.extend([
    url(r'^([-\w]{4,32})/$', views.schema_detail),
    url(r'^([-\w]{4,32})/about/$', views.schema_about),
    url(r'^([-\w]{4,32})/search/$', views.search),
    url(r'^([-\w]{4,32})/petition/$', petition_views.form_view, {'is_schema': True}),
    url(r'^([-\w]{4,32})/petition/thanks/$', petition_views.form_thanks, {'is_schema': True}),
    url(r'^([-\w]{4,32})/by-date/(\d{4})/(\d\d?)/(\d\d?)/(\d{1,8})/$', views.newsitem_detail),
    url(r'^([-\w]{4,32})/(?:filter/)?([^/].+/)?$', views.schema_filter),
])
