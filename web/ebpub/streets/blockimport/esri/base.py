from django.contrib.gis.gdal import DataSource

class EsriImporter(DataSource):
    def __init__(self, shapefile, model):
        self.shapefile = shapefile
        self.model = model