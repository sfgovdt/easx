
def application(environ, start_response):

    output = 'python version info:\n'
    import sys
    output += sys.version + '\n'

    output += '\ndjango version info:\n'
    import django
    output += str(django.VERSION) + '\n\n'

    output += 'psycopg2 version info:\n'
    import psycopg2
    output+= psycopg2.__version__ + '\n\n'

    output += 'mod_wsgi version info:\n'
    output += str(environ['mod_wsgi.version']) + '\n\n'

    output += 'gdal version info:\n'
    from django.contrib.gis import gdal
    output += str(gdal.GDAL_VERSION) + '\n\n'

    output += 'geos version info:\n'
    from django.contrib.gis import geos
    output += str(geos.geos_version_info()) + '\n\n'

    status = '200 OK'
    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)
    return [output]
