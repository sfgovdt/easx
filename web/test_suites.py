import os
# ebpub requires that the django settings be loaded
# This allows us to use plain old python unit testing - and avoid some django overhead.
# Still not sure what the right mix is here.

os.environ['DJANGO_SETTINGS_MODULE']='settings'

import unittest

from MAD.models.tests.parcels import ParcelTestCase
from MAD.models.tests.misc import AddressXParcelsTestCase, AddressBaseTestCase, CrAddressXParcelsTestCase, ClusteringTestCase
from MAD.models.tests.cr_change_requests import CrChangeRequestTestCase
from MAD.models.tests.ModelUtils import ModelUtilsTestCase
from MAD.views.tests.address_search import AddressSearchTests
from MAD.views.tests.views__change_request import ViewsChangeRequestsTestCase
from MAD.views.tests.views__find_address_candidates import ViewsFindAddressCandidatesTestCase
from MAD.views.tests.views__reports import ViewsReportsTestCase
from MAD.utils.geocoder.tests import InteractiveGeocodingTestCase

# To set up tests, lines such as this should be in settings_deploy.py
#     TEST_RUNNER = "test_suites.EASTestRunner"
# The tests are setup in
#     test_suites.py
# then use the standard django testing command line as follows:
#     > python manage.py test
# to provide a specific test class:
#     > python manage.py test AddressXParcelsTestCase

LABELS_TO_TEST_CASES = {
    klass.__name__: klass for klass in [
        ParcelTestCase,
        AddressXParcelsTestCase,
        AddressBaseTestCase,
        CrAddressXParcelsTestCase,
        ClusteringTestCase,
        CrChangeRequestTestCase,
        ModelUtilsTestCase,
        AddressSearchTests,
        ViewsChangeRequestsTestCase,
        ViewsFindAddressCandidatesTestCase,
        ViewsReportsTestCase,
        InteractiveGeocodingTestCase
    ]
}

class EASTestRunner:
    def __init__(self, verbosity=1, interactive=True, *args, **options):
        pass

    @staticmethod
    def runSuite(suiteClass):
        testSuite = unittest.TestLoader().loadTestsFromTestCase(suiteClass)
        unittest.TextTestRunner(verbosity=2).run(testSuite)

    def run_tests(self, test_labels, **kwargs):
        if not test_labels:
            test_labels = LABELS_TO_TEST_CASES
        for test_class in test_labels:
            self.runSuite(LABELS_TO_TEST_CASES[test_class])
