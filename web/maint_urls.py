from django.conf.urls import include, url
from django.conf import settings
from MAD.views import views
from django.views.static import serve

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r'^Media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^'+settings.VIRTUAL_DIR + r'$', views.MaintIndex),
]
