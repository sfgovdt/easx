import unittest
from MAD.utils.geocoder.base import Geocoder
from MAD.utils.geojson_encode import *
import time

class InteractiveGeocodingTestCase(unittest.TestCase):

    def setUp(self):
        return
        
    def tearDown(self):
        return

    def printAddresses(self, addresses):
        for a in addresses:
            print(a, a.score)

    def doGeocode(self, locationString):
        completeResultset = Geocoder.geocode(addressString=locationString, useType="INTERACTIVE")
        return completeResultset

    def doTestEmptyResults(self, locationString):
        resultSet = self.doGeocode(locationString)
        self.assertTrue(len(resultSet) == 0)

    def doTestSingle(self, locationString, expectedTuple):
        resultSet = self.doGeocode(locationString)
        self.assertEqual(len(resultSet), 1)
        streetNumber, streetName, streetType = expectedTuple
        self.assertEqual(resultSet[0].base_address_num, streetNumber)
        self.assertEqual(resultSet[0].street_name, streetName)
        self.assertEqual(resultSet[0].street_type, streetType)

    def doTestAmbiguous(self, locationString, expectedList):
        resultSet = self.doGeocode(locationString)
        self.assertTrue(len(expectedList) == len(resultSet))
        # cartesian compare
        for expectedTuple in expectedList:
            matchFound = False
            streetNumber, streetName, streetType = expectedTuple
            #print '***** expected *****'
            #print expectedTuple
            for result in resultSet:
                #print '***** actual *****'
                #print geojson_encode(result)
                if result.base_address_num == streetNumber and result.street_name == streetName and result.street_type == streetType:
                    matchFound = True
            self.assertEqual(matchFound, True)

    def doTestNoSuchNumberPerformance(self, locationString):
        # todo - figure out how to use timeit instead of time - as a newbie I had weird scoping issues
        begin = time.time()
        resultSet = self.doGeocode(locationString)
        end = time.time()
        # should take less than 1 second.
        #print "*****" + str(end-begin)
        # todo - improve performance and bring this number down
        self.assertLess(end - begin, 2)

    def doTestNoSuchNumber(self, locationString, number):
        resultSet = self.doGeocode(locationString)
        self.assertTrue(len(resultSet) > 0 )
        for result in resultSet:
            #print geojson_encode(result)
            self.assertNotEqual(result.base_address_num, number)
        

    def doTestAutocomplete(self, locationString, someOfTheExpected):
        resultSet = self.doGeocode(locationString)
        for expected in someOfTheExpected:
            #print '***** expected *****'
            #print expected
            streetNumber, streetName, streetType = expected
            matchFound = False
            for result in resultSet:
                #print '***** actual *****'
                #print result
                if result.base_address_num == streetNumber and result.street_name == streetName and result.street_type == streetType:
                    matchFound = True
            self.assertEqual(matchFound, True)

    def doTestStreetNameAlias(self, referenceLocation, compareLocations):
        # addresses will have aliases for the same address - eg street name changes
        referenceResultset = self.doGeocode(referenceLocation)
        for compareLocation in compareLocations:
            compareResultset = self.doGeocode(compareLocation)
            self.assertEqual(referenceResultset[0].address_base_id, compareResultset[0].address_base_id)


    ##### all above are reused in below



    ##### tests for geocoing without regard to scores - begin

    def testStreetNameAlias(self):
        self.doTestStreetNameAlias("100 02nd", ["100 2nd"])
        self.doTestStreetNameAlias("1100 army", ["1100 cesar chavez"])

    def testNoSuchNumber(self):
        # address number does not exist
        self.doTestNoSuchNumber("33 Sussex Street", [32, 31, 35, 36])
        # same but missing suffix
        self.doTestNoSuchNumber("33 Sussex", 33)
        self.doTestNoSuchNumber("15 Geary ST", 15)
        self.doTestNoSuchNumber("18000 Franklin", 1800)
        self.doTestNoSuchNumber("992 Sacramento", 992)


    def testNoSuchNumberPerformance(self):
        # address number does not exist
        self.doTestNoSuchNumberPerformance("33 Sussex Street")
        self.doTestNoSuchNumberPerformance("15 Geary ST")
        self.doTestNoSuchNumberPerformance("18000 Franklin")

    def testAutocomplete(self):
        self.doTestAutocomplete("33 Suss", [(32, "SUSSEX", "ST"), (31, "SUSSEX", "ST")])
        self.doTestAutocomplete("10 s", [
            (10, "SAN ANDREAS", "WAY"),
            (10, "SAN BENITO", "WAY"),
            (10, "SANCHEZ", "ST")
        ])

    def testSuffixMissing(self):
        self.doTestSingle("1 South Van Ness", (1, "SOUTH VAN NESS", "AVE"))

    def testSuffixAlias(self):
        self.doTestSingle("1 South Van Ness Ave", (1, "SOUTH VAN NESS", "AVE"))
        self.doTestSingle("1 South Van Ness Avn", (1, "SOUTH VAN NESS", "AVE"))
        self.doTestSingle("1 South Van Ness Aven", (1, "SOUTH VAN NESS", "AVE"))
        self.doTestSingle("1 South Van Ness Avenu", (1, "SOUTH VAN NESS", "AVE"))
        self.doTestSingle("1 South Van Ness Avenue", (1, "SOUTH VAN NESS", "AVE"))

    def testSuffixWrong(self):
        self.doTestSingle("20 Sussex Ave", (20, "SUSSEX", "ST"))

    def testAmbiguousResults(self):
        self.doTestAmbiguous("550 18th", [(550, "18TH", "AVE"), (550, "18TH", "ST")] )

    def testEmptyResults(self):
        self.doTestEmptyResults("1000")
        self.doTestEmptyResults("ABCD")

    #### tests for geocoing without regard to scores - end



    #### tests for scored geocoding - begin

    def doTestScoreAddressDoesNotExist(self, locationString, geocodingScore):
        baseAddresses = self.doGeocode(locationString)
        for baseAddress in baseAddresses:
            self.assertTrue(baseAddress.score <= geocodingScore + 10)
            self.assertTrue(baseAddress.score >= geocodingScore - 10)

    def testScoreAddressDoesNotExist(self):
        self.doTestScoreAddressDoesNotExist('992 Sacramento', 50)
        self.doTestScoreAddressDoesNotExist('33 Sussex', 50)


    def testScoreAddressExists(self):
            baseAddresses = self.doGeocode('44 Gough ST')
            for baseAddress in baseAddresses:
                self.assertTrue(baseAddress.score == 100)

    def testScoreAddressNotExist(self):
            baseAddresses = self.doGeocode('922 Sacramento ST')
            for baseAddress in baseAddresses:
                self.assertTrue(baseAddress.score <= 60)
                self.assertTrue(baseAddress.score >= 40)


    def testScoreAddressWithoutStreetType(self):
        baseAddresses = self.doGeocode('1 sussex')
        for baseAddress in baseAddresses:
            self.assertTrue(baseAddress.score == 96)

    def testScoreAddressStreetHasNoStreetType(self):
        # There is a "broadway st" and a "broadway" - the latter does not have suffix

        baseAddresses = self.doGeocode('100 broadway')
        self.assertTrue(len(baseAddresses) == 1)
        ba = baseAddresses[0]
        self.assertTrue(ba.score == 98)

        baseAddresses = self.doGeocode('100 broadway st')
        self.assertTrue(len(baseAddresses) == 1)
        self.assertTrue(baseAddresses[0].score == 98)

    def testScoreAddressWithTooManyMistakes(self):
        baseAddresses = self.doGeocode('1 south van ness south avenue drive')
        self.assertTrue(len(baseAddresses) == 0)

    def testStreetPostDirection(self):
        baseAddresses = self.doGeocode('515 buena vista ave west')
        self.assertTrue(len(baseAddresses) > 0)
        baseAddress = baseAddresses[0]
        self.assertTrue(baseAddress.street_post_direction == 'WEST')

