import types
from django.db import models
import json
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.gis.geos import GEOSGeometry
from decimal import Decimal
def geojson_encode(data):
    """
    The main issues with django's default json serializer is that properties that
    had been added to a object dynamically are being ignored (and it also has 
    problems with some models).
    """

    def _any(data):
        ret = None
        if isinstance(data, list):
            ret = _list(data)
        elif isinstance(data, dict):
            ret = _dict(data)
        elif isinstance(data, Decimal):
            # json.dumps() cant handle Decimal
            ret = str(data)
        elif isinstance(data, models.query.QuerySet):
            # Prefetch related fields that will be serialized.
            try:
                related_fields = data.__class__.related_fields_to_serialize()
            except AttributeError:
                pass
            else:
                data = data.prefetch_related(related_fields)
            ret = _list(data)
        elif isinstance(data, models.Model):
            ret = _model(data)
        elif isinstance(data, GEOSGeometry):
            ret = data.wkt
        elif _isClass(data):
            ret = _any(vars(data))
        else:
            ret = data
        return ret
    
    
    def _isClass(data):
        try:
            getattr(data, "__dict__")
            return True
        except:
            return False
    
    def _model(data):
        ret = {}
        # If we only have a model, we only want to encode the fields.
        for f in data._meta.fields:
            if hasattr(f, "attname"):
                ret[f.attname] = _any(getattr(data, f.attname))
            else:
                # Related manager
                related_manager = getattr(data, f)
                ret[f] = _list(related_manager.all())

        # Serialize related fields on an opt-in basis.
        try:
            related_fields = data.__class__.related_fields_to_serialize()
        except AttributeError:
            pass
        else:
            for related_field in related_fields:
                related_manager = getattr(data, related_field)
                ret[related_field] = _list(related_manager.all())
        # And additionally encode arbitrary properties that had been added.
        fields = dir(data.__class__) + list(ret.keys())
        add_ons = [k for k in dir(data) if k not in fields and k != "_state"]
        for k in add_ons:
            ret[k] = _any(getattr(data, k))
        return ret
    
    def _list(data):
        ret = []
        for v in data:
            ret.append(_any(v))
        return ret
    
    def _dict(data):
        ret = {}
        for k, v in list(data.items()):
            if k == "_state":
                continue
            ret[k] = _any(v)
        return ret
    
    ret = _any(data)
    
    return ret
