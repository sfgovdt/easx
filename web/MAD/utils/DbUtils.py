import logging

logger = logging.getLogger('EAS')

class DbUtils:
    #
    # The bulk operations will not commit unless you use manage the transaction manually or
    # unless some part of your save operation includes at least one model object save.
    #


    @staticmethod
    def getSequenceName(modelName):
        try:
            sequenceDictionary = {}
            sequenceDictionary['Addresses'] = 'addresses_address_id_seq'
            sequenceDictionary['CrAddresses'] = 'cr_addresses_cr_address_id_seq'
            sequenceDictionary['CrAddressXParcels'] = 'cr_address_x_parcels_id_seq'
            return sequenceDictionary[modelName]
        except:
            raise


    @staticmethod
    def getNextSequenceRange(connection, modelName, count=1):
        try:
            sequenceName = DbUtils.getSequenceName(modelName)
            sql = "select _sfmad_get_next_sequence_range('%s', %d);" % (sequenceName, count)
            with connection.cursor() as cursor:
                cursor.execute(sql)
                rowTuple = cursor.fetchone()
            minMaxString = rowTuple[0]
            # todo - make not so hacky
            left, right = minMaxString.split(',')
            min = int(left.lstrip('('))
            max = int(right.rstrip(')'))
            return (min, max)
        except:
            raise
