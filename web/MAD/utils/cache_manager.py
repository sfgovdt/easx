
# for now we do not really need anything as sophisticated as django caching
# from django.core.cache import cache

# todo - needs a complete rework - not cohesive etc etc. It was hacked together for a quick performance fix.

import logging
logger = logging.getLogger('EAS')

class CacheManager:
    cacheDict = {
        "DUnitType": {},
        "DAddressDisposition": {},
        "DFloors": {},
    }  # all instances share same cache

    def __init__(self):

        from MAD.models.models import DUnitType, DAddressDisposition, DFloors

        unitTypes = self.cacheDict["DUnitType"]
        if not unitTypes:
            for unitType in DUnitType.objects.all():
                unitTypes[str(unitType.unit_type_id)] = unitType

        dispositions = self.cacheDict["DAddressDisposition"]
        if not dispositions:
            for disposition in DAddressDisposition.objects.all():
                dispositions[str(disposition.disposition_code)] = disposition

        floors = self.cacheDict["DFloors"]
        if not floors:
            for floor in DFloors.objects.all():
                floors[str(floor.floor_id)] = floor


    def get(self, modelclassName, key):

        modelDict = None
        key = str(key)

        try:
            modelDict = self.cacheDict[modelclassName]
        except:
            # This will be a KeyError exception - we want to make that more intelligible.
            message = 'Oops! Looks like a programming error - we are not caching %s' % modelclassName
            logger.info(message)
            raise LookupError(message)

        try:
            returnValue = modelDict[key]
        except:
            # This will be a KeyError exception - we want to make that more intelligible.
            message = 'The value %s is not valid for %s.' % (str(key), modelclassName)
            logger.info(message)
            raise LookupError(message)

        return returnValue