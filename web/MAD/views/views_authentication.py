from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as djangoLogin, logout as djangoLogout
from django.views.decorators.cache import never_cache
from MAD.utils.GenericResponseObj import GenericResponseObj
import logging

logger = logging.getLogger('EAS')

#Authentication
@never_cache
def login(request):
    GR = GenericResponseObj('error in login in views_authentication.py')
    
    try:
        if request.method == 'POST':
            user = authenticate(username=request.POST.get("name"), password=request.POST.get("password"))
            
            GR.message = "There was a problem logging you in, please try again."
            if user is not None:
                if user.is_active:
                    djangoLogin(request, user)
                    GR.success = True
                    GR.status_code = 0
                    GR.message = ''
                    GR.returnObj["firstname"] = user.first_name
                    GR.returnObj["lastname"] = user.last_name
                    GR.returnObj["username"] = user.username
                    GR.returnObj['changePass'] = False
                    GR.returnObj['email'] = user.email
                    GR.returnObj['id'] = user.id
                    
                    GR.returnObj["roles"] = []
                    for group in user.groups.all():
                        GR.returnObj["roles"].append(group.name)                
                else:
                    GR.errors["name"] = "user disabled"
                    GR.status_code = 1
            else:
                message = "The username or password was invalid. Please try again."
                GR.errors["name"] = message
                GR.errors["password"] = message
                GR.message = message
                GR.status_code = 2
    
    except Exception as e:
        logger.critical(e)
    
    return JsonResponse(GR.toDict())


@never_cache
def logout(request):
    GR = GenericResponseObj("error in logout in views_authentication.py")
    try:
        djangoLogout(request)
        GR.status_code = 0
        GR.success = True
        GR.message = "Logout successfull"  
    except Exception as e:
        logger.critical(e)
    
    return JsonResponse(GR.toDict())


@never_cache
def resetPassword(request):
    GR = GenericResponseObj("error in resetPassword in views_authentication.py")
    user_name = request.POST.get('username', '')
    try:
        GR.message = "Invalid username."
        user = User.objects.get(username=user_name)
        if user:
            password_new = User.objects.make_random_password()
            user.set_password(password_new)
            user.save()
            from django.conf import settings as settings
            user.email_user('%s password' % settings.APPLICATION_NAME_SHORT, "Your password for %s has been reset to %s." % (settings.APPLICATION_NAME_SHORT, password_new))
            GR.status_code = 0
            GR.success = True 
            GR.message = "Your password has been reset, please check your email shortly for confirmation of your new password."
        else:
            GR.status_code = 1
            GR.errors["username"] = "User name not valid"
            GR.message = "Make sure all fields are entered and valid."
    
    except Exception as e:
        logger.critical(e)
    return JsonResponse(GR.toDict())
    

@never_cache
def changePassword(request):
    GR = GenericResponseObj("error in changePassword in views_authentication.py")
    password_old = request.POST.get('password_old', '')
    password_new = request.POST.get('password_new', '')
    password_confirm = request.POST.get('password_confirm', '')
    
    try:     
        if password_new != password_confirm:
            GR.errors["password_new"] = "Passwords don't match"
            GR.errors["password_confirm"] = "Passwords don't match"
            GR.message = "Please confirm that both new and confirmation passwords match"
            
        else:
            user = request.user          
            if user.is_authenticated and user.check_password(password_old):
                user.set_password(password_new)
                user.save()
                from django.conf import settings as settings
                user.email_user('%s - new password' % settings.APPLICATION_NAME_SHORT, "Your password for %s has been reset to %s." % (settings.APPLICATION_NAME_SHORT, password_new))
                GR.status_code = 0
                GR.success = True 
                GR.message = "Your password has been changed, please check your email shortly for confirmation of your new password."
            else:
                GR.status_code = 1
                GR.errors["password_old"] = "invalid username or password"
                GR.errors["password_new"] = "invalid username or password"
                GR.errors["password_confirm"] = "invalid username or password"
                GR.message = "There was problem updating your password, please try again."
    
    except Exception as e:
        logger.critical(e)
    
    return JsonResponse(GR.toDict())


@never_cache
def getUser(request):
    GR = GenericResponseObj("error in getUser in views_authentication.py")
    
    try:
        if request.user.is_authenticated:
            user = request.user
            GR.returnObj["firstname"] = user.first_name
            GR.returnObj["lastname"] = user.last_name
            GR.returnObj["username"] = user.username
            GR.returnObj['changePass'] = False
            GR.returnObj['email'] = user.email
            GR.returnObj['id'] = user.id
            GR.returnObj["roles"] = []
            for group in user.groups.all():
                GR.returnObj["roles"].append(group.name)
            
            GR.status_code = 0
            GR.success = True
            GR.message = ""

        else:
            GR.status_code = 1
            GR.message = "you don't have permission to access this information"
            
    except Exception as e:
        logger.critical(e)

    return JsonResponse(GR.toDict())
