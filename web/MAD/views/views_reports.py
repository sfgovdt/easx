import logging

from django.http import JsonResponse
from MAD.utils.geojson_encode import geojson_encode
from MAD.utils.ExtJSONResultsEncoder import ExtJSONResultsEncoder
from django.shortcuts import render
from MAD.models.models import AddressBase, Addresses, ManagerAliases, VwAddressesFlatNative, VwAddressSearch

from MAD.models.ModelUtils import ModelUtils
from django.contrib.gis.geos import *
from django.conf import settings as settings

logger = logging.getLogger('EAS')

def AddressReport(request, mode = ''):
    addrBaseId = request.GET.get('id')
    addressBaseObj = AddressBase.objects.get(address_base_id = addrBaseId)
    addressObj = Addresses.objects.filter(address_base = addrBaseId).get(address_base_flg = True)
    if addressBaseObj.base_address_suffix is None:
        addressBaseObj.base_address_suffix = ""
    return render(request, 'addressreport.html', {'addressBase': addressBaseObj, 'addressObj': addressObj, 'streetseg': addressBaseObj.street_segment, 'zone': addressBaseObj.zone})

def Identifiers(request, mode = ''):
    addrBaseId = request.GET.get('id')
    addressBaseObj = AddressBase.objects.get(address_base_id = addrBaseId)
    # TODO: this looks wrong, but is actually what we need due to mismatch
    # between gdal/postgis and django at 2.12. should fix this when upgraded
    # see: https://code.djangoproject.com/ticket/31611
    coords = {
        'easting' : round(addressBaseObj.geometry.y, 3),
        'northing' : round(addressBaseObj.geometry.x, 3),
        'long' : round(addressBaseObj.geometry.transform(4326, True).y, 5),
        'lat' : round(addressBaseObj.geometry.transform(4326, True).x, 5) }
    # want to call Aliases.objects.filter(address_base_id = addrBaseId), but this doesn't seem to work?? - anp
    aliases = ManagerAliases().filter(address_base_id = addrBaseId)
    return render(request, 'identifiers.html', {'addressBase': addressBaseObj, 'coordinates': coords, 'streetseg': addressBaseObj.street_segment, 'aliases': aliases})

def UnitsForAddressReport(request):
    logger.info('begin views_report.UnitsForAddressReport')
    addressBaseId = request.GET.get('id')
    unitAddresses = (
        Addresses.objects.filter(address_base__address_base_id=addressBaseId)
        .filter(unit_num__isnull=False, parent=None)
        .order_by("unit_num", "address_base_flg")
    )
    # Filtering in python instead of with .values() so that prefetch_related("children")
    # still works for children: was erroring "ManyToOneRel object has no attribute 'attname'"
    field_subset = [
        "address_base_flg",
        "unit_num",
        "floor_id",
        "unit_type_id",
        "disposition_code",
        "retire_tms",
        "create_tms",
        "activate_change_request_id",
        "retire_change_request_id",
        "children",
    ]
    filtered_result = [
        {k: v for k, v in obj.items() if k in field_subset}
        for obj in geojson_encode(unitAddresses)
    ]
    json = ExtJSONResultsEncoder(filtered_result, len(unitAddresses), request)
    json['children'] = json['results']
    del json['results']
    logger.info('end views_report.UnitsForAddressReport()')
    return JsonResponse(json)

def ParcelsForAddressReport(request):
    logger.info('begin views_report.ParcelsForAddressReport')
    addressBaseId = request.GET.get('id')
    parcelAddresses = VwAddressesFlatNative.objects.filter(address_base_id = addressBaseId)\
        .filter(address_x_parcel_id__isnull = False)\
        .order_by("blk_lot", "address_base_flg", "address_x_parcel_retire_tms")
    json = ExtJSONResultsEncoder(geojson_encode(parcelAddresses), len(parcelAddresses), request)
    logger.info('end views_report.ParcelsForAddressReport')
    return JsonResponse(json)

def Aliases(request, mode = ''):
    addrBaseId = request.GET.get('id')
    aliases = VwAddressSearch.objects.filter(address_base_id = addrBaseId).filter(unit_num = "-999").filter(category__iexact='alias')
    return render(request, 'aliases.html', {'aliases': aliases})

def History(request, mode = ''):
    return render(request, 'history.html', {})

def BaseGeometryForParcels(request):
    logger.info('BaseGeometryForParcels')
    try:
        apns = request.body
        #for apn in apns:
        #    logger.info(apn)
        geometry = ModelUtils.getBaseParcelGeometryWithApns(apns)
        jsonGeometry = ExtJSONResultsEncoder(geojson_encode(geometry), len(apns), request)
        #logger.info(jsonGeometry)
        return JsonResponse(jsonGeometry)

    except Exception as e:
        logger.warning(e)
