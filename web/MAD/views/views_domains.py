from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest, HttpResponseServerError, HttpResponseForbidden, JsonResponse
from MAD.models.models import DUnitType, DFloors, DAddressDisposition, DChangeRequestStatus, DAddressBaseNumberSuffix, DAddressBaseType
from MAD.utils.geojson_encode import geojson_encode
from MAD.utils.GenericResponseObj import GenericResponseObj
import json

def getChangeRequestDomains(request):
    
    GR = GenericResponseObj('error in getUnitTypeDomain in views_domains.py')

    if request.method == 'GET' and id != -1:
        GR.returnObj = { 'd_unit_type': DUnitType.objects.all(),
                         'd_address_disposition': DAddressDisposition.objects.all(),
                         'd_floors': DFloors.objects.all(),
                         'd_change_request_status': DChangeRequestStatus.objects.all(),
                         'd_address_base_number_suffix': DAddressBaseNumberSuffix.objects.all(),
                         'd_address_base_type': DAddressBaseType.objects.all()
        }
        GR.status_code = 0
        GR.success = True
        GR.message = ''
        
    return JsonResponse(geojson_encode(GR))

         

