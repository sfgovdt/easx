from django.http import JsonResponse
from django.contrib.gis.geos.geometry import GEOSGeometry
from django.db import transaction
from django.db.models import Prefetch, Q
from MAD.models.models import Addresses, CrJsonModel, CrChangeRequests, VwAddressesFlatNative, ChangeRequests, AddressXParcels, Streetnames
from MAD.utils.geojson_encode import geojson_encode
from MAD.utils.GenericResponseObj import GenericResponseObj
from MAD.exceptions import AddressIsRetiredWarning, ConcurrentUpdateWarning, ValidationWarning
from MAD.models.ModelUtils import ModelUtils
from django.forms import ValidationError
from django.core.exceptions import ObjectDoesNotExist
import logging

logger = logging.getLogger('EAS')


def getBaseAddr(request, baseAddrId = None):
    GR = GenericResponseObj('error in getBaseAddr in views_change_requests.py')
    
    try:
        user = request.user          
        if not user.is_authenticated:
            GR.message = 'Please login.'
        else:
            if baseAddrId is None:
                GR.message = 'base address id was invalid'
            else:
                crObj = CrJsonModel()
                baseAddrObj = crObj.getBaseAddrFromDb(baseAddrId)
                GR.returnObj = {'base_addresses': baseAddrObj}
                GR.status_code = 0
                GR.success = True
                GR.message = ''
    except AddressIsRetiredWarning as e:
        GR.returnObj = None
        GR.status_code = 0
        GR.success = False
        GR.message = e
    except Exception as e:
        logger.critical(e)

    geojson = geojson_encode(GR)
    #logger.info(json)
    return JsonResponse(geojson)


def getCr(request, crId = None, review_status_id = None):
    logger.info('getCr begin')
    GR = GenericResponseObj('error in getCr in views_change_requests.py')

    try:
        user = request.user          
        if not user.is_authenticated:
            GR.message = 'Please login.'
            raise GR
        else:
            if crId is None:
                GR.message = 'change request id was invalid'
                raise GR
            else:
                crObj = CrJsonModel(int(crId))
                if review_status_id is not None:
                    logger.info('review status id: %s ' % review_status_id)
                    crObj.review_status_id = review_status_id
                    GR = crObj.saveToDb(user)
                    crObj = CrJsonModel(int(crId))

                GR.returnObj = {'changeRequest': [crObj]}
                GR.status_code = 0
                GR.success = True
                GR.message = ''

    except Exception as e:
        GR.message = ModelUtils.getUserMessage(e)
        logger.critical(e)

    #logger.info(geojson_encode(GR))
    logger.info('getCr end')
    return JsonResponse(geojson_encode(GR))

@transaction.atomic
def saveCr(request, SRS = '3857', review_status_id = None):
    logger.info('views_change_requests - saveCr - BEGIN')
    GR = GenericResponseObj('error in saveCr in views_change_requests.py')
    
    try:
        user = request.user          
        if not user.is_authenticated:
            logger.info('User not authenticated - rolled back transaction')
            GR.message = 'Please login.'
            raise GR
        else:
            GR.message = 'error parsing POSTed json CR object'
            #crObj = CrJsonModel(request.POST['json'])
            # debug only
            #logger.info('the raw json from the request')
            #logger.info(request.body)
            crObj = CrJsonModel(request.body)
            if review_status_id is not None:
                crObj.review_status_id = review_status_id

            # debug only
            #logger.info('the populated CrJsonModel:')
            #logger.info(geojson_encode(crObj))
            
            GR.message = 'Error saving change request.'
            try:
                with transaction.atomic():
                    GR = crObj.saveToDb(user)
            except ValidationError as ve:
                logger.warning('ValidationError - rolled back transaction')
                GR.message = GR.message+', '+(str(ve))
            except ValidationWarning as vw:
                logger.warning('ValidationWarning - rolled back transaction')
                GR.message = GR.message+', '+(str(vw))
            except ConcurrentUpdateWarning as cw:
                logger.warning('ConcurrentUpdateWarning - rolled back transaction')
                GR.message = GR.message+', '+(str(cw))
            except Exception as e:
                logger.warning("Other Exception - rolled back transaction")
                GR.message = GR.message+', '+(str(e))
            else:
                if GR.success:
                    if review_status_id == 2:
                        # Refresh the CrJsonModel: otherwise, the cr_parent_id fields
                        # on the unit addresses will be empty, and no subaddresses will be created.
                        refreshedCr = CrJsonModel(int(crObj.change_request_id))
                        GR = refreshedCr.approve()
                else:
                    logger.warning('Unknown Exception - rolled back transaction')
                    raise GR

    except Exception as e:
        logger.warning('rolled back transaction outer')
        logger.warning(e)
        GR.success = False
        GR.message = ModelUtils.getUserMessage(e)

    logger.info('views_change_requests - saveCr - END')
    return JsonResponse(geojson_encode(GR))

@transaction.atomic
def deleteCr(request, crId):
    GR = GenericResponseObj('error in deleteCr in views_change_requests.py')
    
    try:
        with transaction.atomic():
            user = request.user          
            if not user.is_authenticated:
                GR.message = 'Please login.'
                raise GR
            else:
                cr = CrChangeRequests.objects.get(pk = crId)
                if cr.requestor_user.pk == user.pk:
                    crJsonModel = CrJsonModel()
                    GR = crJsonModel.deleteChangeRequest(crId)
                    GR.returnObj = { 'deleteCr': True }
                else:
                    GR.message = 'user can only delete their own change requests'
                    GR.status_code = 1
                    raise GR
                        
    except Exception as e:
        logger.critical(e)
        GR.status_code = 0
        GR.success = False
        GR.message = ModelUtils.getUserMessage(e)

    return JsonResponse(geojson_encode(GR))


def proposeAddress(request, geom = '', SRS = '3857'):
    response = GenericResponseObj('error in proposeAddress in views_change_requests.py')
    
    if geom == '':
        response.status_code = -1
        response.success = False
        response.message = 'need to submit a geometry location for the proposed address'
    else:
        newBaseAddress = CrJsonModel.BaseAddress()
        point = GEOSGeometry('SRID=' + SRS + ';' + geom)
        point.transform(2227)
        newBaseAddress.geometry = point
        newBaseAddress.proposeNew()

        point.transform(3857)
        newBaseAddress.geometry = point.wkt
        newBaseAddress.geometry_proposed = point.wkt
        response.returnObj = {'base_addresses': [newBaseAddress]}

        response.status_code = 0
        response.success = True
        response.message = ''
        
    return JsonResponse(geojson_encode(response))


def getAvailableChangeRequests(request, userId = None):
    response = GenericResponseObj('error in getAvailableChangeRequests in views_change_requests.py')

    crList = CrChangeRequests.objects.filter(review_status = 5)
    user = request.user          
    if user.is_authenticated:
        crList = CrChangeRequests.objects.filter(Q(review_status = 5) | (Q(review_status = 1) & Q(reviewer_user = user)) )
    
    crJsonList = crListToJson(crList)
        
    response.returnObj = {'changeRequests': crJsonList}

    response.status_code = 0
    response.success = True
    response.message = ''
    
    return JsonResponse(geojson_encode(response))


def getMyChangeRequests(request, userId = None):
    response = GenericResponseObj('error in getMyChangeRequests in views_change_requests.py')

    #user = User.objects.get(pk=userId)
    crList = []
    user = request.user          
    if user.is_authenticated:
        crList = CrChangeRequests.objects.filter((Q(review_status = 3) | Q(review_status = 4) | Q(review_status = 5)) & Q(requestor_user = user))
        crList = crList.select_related()
    crJsonList = crListToJson(crList)

    response.returnObj = {'changeRequests': crJsonList}

    response.status_code = 0
    response.success = True
    response.message = ''
    
    return JsonResponse(response.toDict())

def crListToJson(crList):
    crJsonList = []
    for cr in crList:
        if not cr.reviewer_last_update:
            reviewerLastUpdate = ""
        else:
            reviewerLastUpdate = cr.reviewer_last_update
            
        if not cr.requestor_last_update:
            requestorLastUpdate = ""
        else:
            requestorLastUpdate = cr.requestor_last_update

        if not cr.reviewer_user:
            reviewer = ""
        else:
            reviewer = cr.reviewer_user.first_name + " " + cr.reviewer_user.last_name
        
        crJsonList.append({ 
            "change_request_id": cr.change_request_id,
            "name": cr.name,
            "requestor_name": cr.requestor_user.first_name + " " + cr.requestor_user.last_name,
            "requestor_last_update": requestorLastUpdate,
            "reviewer_name": reviewer,
            "reviewer_last_update": reviewerLastUpdate,
            "status_description": cr.review_status.status_description
        })
    return crJsonList


def prepareChangeRequestForSerialization(changeRequest):
    # Just use the CrChangeRequest - it contains the information we need.
    # We should one day remove fields from ChangeRequest or refactor that code.
    changeRequest.cr_change_request.requestor_user.password = None
    changeRequest.cr_change_request.reviewer_user.password = None
    changeRequest.requestor = changeRequest.cr_change_request.requestor_user
    changeRequest.reviewer = changeRequest.cr_change_request.reviewer_user
    changeRequest.requestor_comment = changeRequest.cr_change_request.requestor_comment
    changeRequest.reviewer_comment = changeRequest.cr_change_request.reviewer_comment
    changeRequest.name = changeRequest.cr_change_request.name

    del(changeRequest._state)

    return changeRequest


def getChangeReport(changeRequestId):

    addressesFlat = []
    if changeRequestId == 1:
        # If we have change request number 1, we return an empty array because
        #  - change request 1 is the beginning of EAS time (feb 15 2012)
        #  - there is no lineage before this
        pass
    else:
        addressesFlat = VwAddressesFlatNative.objects.filter(
            Q(address_x_parcel_activate_change_request_id = changeRequestId)
            |
            Q(address_x_parcel_retire_change_request_id = changeRequestId)
        )

    changeRequest = ChangeRequests.objects.get(pk=changeRequestId)
    prepareChangeRequestForSerialization(changeRequest)

    changeReport = {
        'changeRequest': changeRequest,
        'addressesFlat': addressesFlat
    }
    return changeReport


def getChangeRequestReport(request, changeRequestId):
    change_report = getChangeReport(str(changeRequestId))
    return JsonResponse(geojson_encode(change_report))


def getChangeRequestReportForAxpCreate(request, axpId):
    changeReport = {}
    try:
        axp = AddressXParcels.objects.get(id=axpId)
        changeReport = getChangeReport(axp.activate_change_request.change_request_id)
    except ObjectDoesNotExist as e:
        # The web application should never put use here, but someone hacking the URL might.
        pass
    return JsonResponse(geojson_encode(changeReport))


def getChangeRequestReportForAxpRetire(request, axpId):
    changeReport = {}
    try:
        axp = AddressXParcels.objects.get(id=axpId)
        changeReport = getChangeReport(axp.retire_change_request.change_request_id)
    except ObjectDoesNotExist as e:
        # The web application should never put use here, but someone hacking the URL might.
        pass
    return JsonResponse(geojson_encode(changeReport))


def format_name(user):
    if user.pk == 1:
        return ""
    return user.first_name + " " + user.last_name


def getChangeRequestReportForAddress(request, baseAddrId):
    response = GenericResponseObj("error in getChangeRequestReportForAddress in views_change_requests.py")

    change_requests = (
        ChangeRequests.objects.exclude(
            # exclude(NOT ...) sounds a lot like plain old filter(),
            # however when traversing a reverse FK from change_request
            # to Addresses or AddressXParcels, they are not equivalent.
            # If you tried to filter(), you would get 3 LEFT JOINs that
            # hang up the app.
            # https://docs.djangoproject.com/en/stable/topics/db/queries/#spanning-multi-valued-relationships
            # STRATEGY 1: Check the Addresses instances on the ChangeRequest row, in case
            # CrAddressBase.address_base_id below is null (for creates).
            ~Q(activated_address__address_base=baseAddrId),
            # These next two (retires, updates) are probably duplicative, but
            # not costly to include for thoroughness.
            ~Q(updated_address__address_base=baseAddrId),
            ~Q(retired_address__address_base=baseAddrId),
            # STRATEGY 2: Traverse from CrAddress -> CrAddressBase, in case Addresses above
            # do not hold a reference to an earlier update change request.
            ~Q(cr_change_request__craddresses__cr_address_base__address_base_id=baseAddrId)
        )
        .distinct()
        .order_by("create_tms")
        .select_related("requestor_user", "reviewer_user")
        .prefetch_related("cr_change_request")
    )

    response.returnObj = [
        {
            "change_request_id": cr.pk,
            "name": cr.cr_change_request.name,
            "create_tms": cr.create_tms,
            "requestor_name": format_name(cr.requestor_user),
            "requestor_email": cr.requestor_user.email,
            "requestor_comment": cr.requestor_comment,
            "reviewer_name": format_name(cr.reviewer_user),
            "reviewer_email": cr.reviewer_user.email,
            "reviewer_comment": cr.reviewer_comment,
        }
        for cr in change_requests
    ]

    response.status_code = 200
    response.success = True
    response.message = ""

    return JsonResponse(response.toDict())


def getAddressesForChangeRequest(request, changeRequestId):
    response = GenericResponseObj("error in getAddressesForChangeRequest in views_change_requests.py")
    changeRequestId = int(changeRequestId)
    if changeRequestId == 1:
        response.status_code = 400
        response.success = False
        response.message = "Initial Load Change Request"

        return JsonResponse(response.toDict())

    change_request = (
        ChangeRequests.objects.filter(pk=changeRequestId)
        .select_related("cr_change_request")
        .get()
    )
    address_ids = {
        cr_address.address_id
        for cr_address in change_request.cr_change_request.craddresses_set.all()
    }

    addresses = (
        Addresses.objects.filter(
            # Get creates: same as "STRATEGY 1", above.
            Q(activate_change_request=change_request)
            # These next two are probably duplicative given strategy
            # 2 below, but not costly to include for thoroughness.
            | Q(update_change_request=change_request)
            | Q(retire_change_request=change_request)
            # Get updates: same as "STRATEGY 2", above.
            | Q(pk__in=address_ids)
        )
        .order_by("address_base")
        .select_related("address_base__street_segment")
        .prefetch_related(
            Prefetch(
                "address_base__street_segment__streetnames_set",
                to_attr="map_streetnames",
                queryset=Streetnames.objects.filter(category="MAP"),
            )
        )
    )

    def addressString(address_base):
        if address_base.address_base_type_id == 1:
            return address_base.complete_landmark_name

        a = address_base.base_address_prefix or ""
        b = str(address_base.base_address_num) or ""
        c = address_base.base_address_suffix or ""
        # Assumes map_streetnames has been prefetched
        d = address_base.street_segment.repr(streetName=address_base.street_segment.map_streetnames[0]) or ""

        return " ".join([x for x in [a, b, c, d] if x])

    def crType(address):
        nonlocal change_request
        if address.update_change_request_id == change_request.pk:
            return "Updated"
        if address.activate_change_request_id == change_request.pk:
            return "Created"
        if address.retire_change_request_id == change_request.pk:
            return "Retired"
        return "Updated"

    response.returnObj = [
        {
            "address_id": address.pk,
            "address_base_id": address.address_base.pk,
            "full_street_name": addressString(address.address_base),
            "unit_num": address.unit_num,
            "address_base_flg": address.address_base_flg,
            "change_type": crType(address),
            "geometry": geojson_encode(address.address_base.geometry),
        }
        for address in addresses
    ]

    response.status_code = 200
    response.success = True
    response.message = ""

    final = response.toDict()
    final["changeRequest"] = geojson_encode(prepareChangeRequestForSerialization(change_request))

    return JsonResponse(final)
