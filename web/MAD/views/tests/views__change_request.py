import json
import unittest

from MAD.models.models import Addresses, AddressXParcels, ChangeRequests
from MAD.views.views_change_requests import (
    getAddressesForChangeRequest,
    getChangeRequestReportForAddress,
)


class ViewsChangeRequestsTestCase(unittest.TestCase):
    def test_zeroth_change_request_included(self):
        response = getChangeRequestReportForAddress(request=None, baseAddrId=268370)
        content = json.loads(response.content)
        self.assertEqual(content["returnObj"][0]["change_request_id"], 1)

    def test_cr_included_from_activation(self):
        base_addr_id = 490440
        address = Addresses.objects.get(address_base=base_addr_id)

        assert not (
            address.activate_change_request.cr_change_request.craddresses_set.filter(
                cr_address_base__address_base_id__isnull=False
            ).exists()
        ), "Test case was supposed to have a null address_base_id on CrAddressBase"

        response = getChangeRequestReportForAddress(request=None, baseAddrId=base_addr_id)
        content = json.loads(response.content)
        returned_cr_ids = [cr["change_request_id"] for cr in content["returnObj"]]
        for cr in [address.activate_change_request, address.update_change_request]:
            self.assertIn(cr.pk, returned_cr_ids)

    def test_cr_included_from_activation_address_report(self):
        address_id = 739766
        change_request_id = 13103

        # This address (activation) does not have a CrAddresses row.
        assert not ChangeRequests.objects.filter(cr_change_request__craddresses=address_id).exists()

        response = getAddressesForChangeRequest(None, change_request_id)
        content = json.loads(response.content)
        returned_address_ids = [cr["address_id"] for cr in content["returnObj"]]
        self.assertEqual(returned_address_ids, [address_id])

    def test_cr_included_from_axp_multiple(self):
        activate_crs = AddressXParcels.objects.filter(
            address__address_base=505197
        ).values_list('activate_change_request', flat=True).distinct()
        # This address has multiple AddressXParcel rows with change requests.
        self.assertGreater(len(activate_crs), 1)

        response = getChangeRequestReportForAddress(request=None, baseAddrId=505197)
        content = json.loads(response.content)
        returned_cr_ids = [cr["change_request_id"] for cr in content["returnObj"]]
        for cr in activate_crs:
            self.assertIn(cr, returned_cr_ids)

    def test_address_included_from_earlier_update(self):
        # Prior to EAS-590, 542 BRANNAN ST was missing. CR 8760 is not the latest
        # change request to have updated 542 BRANNAN ST.
        base_addr_id = 454613
        change_request_id = 8760

        # Check test setup:
        qs = Addresses.objects.filter(address_base_id=base_addr_id, update_change_request_id=change_request_id)
        assert qs.first() is None, f"Unexpected setup: got update change request: {qs.first().update_change_request_id}"

        response = getAddressesForChangeRequest(request=None, changeRequestId=change_request_id)
        content = json.loads(response.content)
        street_names = {address["full_street_name"] for address in content["returnObj"]}

        self.assertIn("542 BRANNAN ST", street_names)
