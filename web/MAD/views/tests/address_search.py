import json
import unittest

from django.http import HttpRequest

from MAD.views.views_search import AddressSearch


class AddressSearchTests(unittest.TestCase):
    def testCompleteLandmarkNameNoDuplicate(self):
        # Numbered thoroughfare: 50 UNITED NATIONS PLZ
        # Complete landmark name: 50 United Nations Plaza Federal Office Building
        request = HttpRequest()
        request.GET.__setitem__("query", "50 United Nations")
        request.GET.__setitem__("start", 0)
        request.GET.__setitem__("limit", 10)
        response = AddressSearch(request)
        content = json.loads(response.content)
        self.assertEqual(int(content["count"]), 1)
