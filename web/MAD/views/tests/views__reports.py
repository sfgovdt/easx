import json
import unittest

from django.http import HttpRequest

from MAD.views.views_reports import UnitsForAddressReport


class ViewsReportsTestCase(unittest.TestCase):
    def test_units_report(self):
        dummy_request = HttpRequest()
        dummy_request.GET["id"] = 268370
        response = UnitsForAddressReport(dummy_request)
        content = json.loads(response.content)
        self.assertEqual(content["children"][0]["activate_change_request_id"], 1)
        self.assertEqual(content["children"][0]["retire_change_request_id"], 1)
