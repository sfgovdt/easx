
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from MAD.utils.jsbuild import build_scripts
from MAD.utils.DbUtils import DbUtils
from MAD.utils.GenericResponseObj import GenericResponseObj
from MAD.utils.geocoder.base import Geocoder
from MAD.models.ModelUtils import ModelUtils
from MAD.models.models import Parcels, ParcelsProvisioning, VwAddressesFlatNative, Addresses, VBaseAddressSearch
from django.db import transaction
from django.contrib.gis.geos.geometry import GEOSGeometry
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.conf import settings as settings
from json import JSONEncoder
import json
from django.db.models import F
from datetime import datetime
from django.views.decorators.cache import cache_control
from collections import OrderedDict
import logging
logger = logging.getLogger('EAS')

def Js(request):
    #Response.AppendHeader("Content-Encoding", "gzip");
    response = HttpResponse(mimetype='text/javascript')
    response['Content-Encoding'] = 'gzip'
    response['Content-Disposition'] = 'attachment; filename=somefilename.pdf'

    return response


@cache_control(no_cache=True)
def Index(request, mode = ''):
    debug = False
    if mode == 'debug':
        debug = True
    if mode == 'build':
        build_scripts()

    applicationInfo = {
        'name': settings.APPLICATION_NAME,
        'nameShort': settings.APPLICATION_NAME_SHORT,
        'env': settings.APPLICATION_ENV,
        'version': settings.APPLICATION_VERSION,
        'dataCenter': settings.APPLICATION_DATACENTER,
        'buildId': settings.BUILD_ID
    }

    return render(request,'index.html', {
        'debug': debug,
        'applicationInfo': applicationInfo,
        'google_analytics_tracking_id': settings.GOOGLE_ANALYTICS_TRACKING_ID
    })

def MaintIndex(request, page='maint_index.html'):
    return render(request, page)

def get_address_string(address, base_address=None):
    if base_address is None:
        base_address = VBaseAddressSearch.objects.get(address_base_id=address.address_base.pk)
    if base_address.address_type == 'Landmark Address':
        return base_address.complete_landmark_name
    else:
        ogc_address_strings = [
            str(base_address.base_address_num),
            base_address.base_address_suffix if base_address.base_address_suffix else "",
            base_address.street_name,
            base_address.street_type if base_address.street_type else "",
            base_address.street_post_direction if base_address.street_post_direction else "",
            address.unit_type.unit_type_description if address.unit_type.unit_type_description != "other" else "",
            address.unit_num if address.unit_num else "",
        ]
        ogc_address_string = " ".join(ogc_address_strings)
        # remove extra white spaces
        ogc_address_string = " ".join(ogc_address_string.split())
        return ogc_address_string


def findAddressCandidates(request, addressString=None, zipCodeString=None):

    # when we test test request is None and we pass in addressString and zipCodeString
    # follow OGC geocoding standard
    # RE http://www.opengeospatial.org/standards/requests/89


    if request:
        # TODO - if its not asking for JSON tell them "format unsupported" or something
        # json only
        #format = request.GET.get('f')
        addressString = request.GET.get('Address')
        zipCodeString = request.GET.get('Zip')


    def buildAddressDict(baseAddress=None, units=None, flatAddressParcels=None, address=None):
        pointGeometry = baseAddress.geometry.transform(4326, clone = True)
        ogcAddressString = get_address_string(address, baseAddress)
        parcelViews = []
        for flatAddressParcel in flatAddressParcels:
            od = OrderedDict([
                ('map_blk_lot', flatAddressParcel.map_blk_lot),
                ('blk_lot', flatAddressParcel.blk_lot),
                ('unit_num', flatAddressParcel.unit_num),
                ('date_map_add', flatAddressParcel.parcel_date_map_add),
                ('date_map_drop', flatAddressParcel.parcel_date_map_drop),
                ('address_base_flg', flatAddressParcel.address_base_flg)
            ])
            parcelViews.append(od)
        unitViews = []
        for unit in units:
            unitViews.append({
                'eas_address_id': unit.address_id,
                'unit_type': unit.unit_type.unit_type_description,
                'unit_num': unit.unit_num,
            })
        ogcAttributes = OrderedDict([
            ('eas_address_id', address.address_id),
            ('eas_base_address_id', baseAddress.address_base_id),
            ('base_address_num', baseAddress.base_address_num),
            ('base_address_suffix', baseAddress.base_address_suffix),
            ('street_name', baseAddress.street_name),
            ('street_type', baseAddress.street_type),
            ('street_post_direction', baseAddress.street_post_direction),
            ('zipcode', baseAddress.zipcode),
            ('parcels', parcelViews),
            ('units', unitViews)
        ])
        return OrderedDict([
            ('address', ogcAddressString),
            # TODO: this looks wrong, but is actually what we need due to mismatch
            # between gdal/postgis and django at 2.12. should fix this when upgraded
            # see: https://code.djangoproject.com/ticket/31611
            ('location', { "x" : pointGeometry.y, "y" : pointGeometry.x }),
            ('score', baseAddress.score),
            ('attributes', ogcAttributes),
        ])


    def buildResponseDict(addressTuples=()):
        candidates = []
        for addressTuple in addressTuples:
            baseAddress = addressTuple[0]
            units = addressTuple[1]
            flatAddressParcels = addressTuple[2]
            address = addressTuple[3]
            addressDict = buildAddressDict(baseAddress=baseAddress, units=units, flatAddressParcels=flatAddressParcels, address=address)
            candidates.append(addressDict)
        return OrderedDict([
            ('spatialReference', {'wkid' : 4326}),
            ('candidates', candidates)
        ])


    def getAddressTuple(baseAddress):
        units = Addresses.objects.filter(address_base__address_base_id = baseAddress.address_base_id)\
                                            .filter(unit_num__isnull = False)\
                                            .filter(retire_tms__isnull = True)\
                                            .order_by("unit_num")
        flatAddressParcels = VwAddressesFlatNative.objects.filter(address_base_id = baseAddress.address_base_id)\
                                                        .filter(address_x_parcel_id__isnull = False)\
                                                        .filter(address_x_parcel_retire_tms__isnull = True)
        address = Addresses.objects.get(address_base__address_base_id=baseAddress.address_base_id, address_base_flg=True)
        return baseAddress, units, flatAddressParcels, address


    def main(addressString, zipCodeString):

        if not addressString:
            return buildResponseDict()

        baseAddresses = Geocoder.geocode(addressString=addressString, zipCodeString=zipCodeString, useType="BATCH")

        # NOTE
        # Potential performance issue in this loop if we have lots of baseAddresses.
        # It is unlikely that any given call will result in more than a few baseAddresses.
        addressTuples = []
        for baseAddress in baseAddresses:
            addressTuple = getAddressTuple(baseAddress)
            addressTuples.append(addressTuple)

        return buildResponseDict(addressTuples=addressTuples)


    responseDict = main(addressString, zipCodeString)


    # todo - refactor - wrong smell
    # we do this odd thing to support unit testing
    if request is None:
        return responseDict
    
    return JsonResponse(responseDict)


def get_address(request):
    address_id = request.GET.get('eas_address_id')
    address = Addresses.objects.get(address_id=address_id)
    base_address = VBaseAddressSearch.objects.get(address_base_id=address.address_base.pk)
    address_string = get_address_string(address, base_address)
    point = base_address.geometry.transform(4326, clone = True)
    address_parcels = VwAddressesFlatNative.objects.filter(address_id=address_id)\
        .filter(address_x_parcel_id__isnull = False)
    parcel_info = []
    for address_parcel in address_parcels:
        parcel_info.append({
            'blk_lot': address_parcel.blk_lot,
            'address_parcel_linkage_id': address_parcel.address_x_parcel_id,
            'linked_date': address_parcel.address_x_parcel_create_tms,
            'unlinked_date': address_parcel.address_x_parcel_retire_tms,
        })
    response_dict = {
        'address': address_string,
        'eas_address_id': address.address_id,
        'eas_base_address_id': base_address.address_base_id,
        'is_base_address': address.address_base_flg,
        'base_address_num': base_address.base_address_num,
        'base_address_suffix': base_address.base_address_suffix,
        'street_name': base_address.street_name,
        'street_type': base_address.street_type,
        'street_post_direction': base_address.street_post_direction,
        'unit_type': address.unit_type.unit_type_description,
        'unit_num': address.unit_num,
        'zipcode': base_address.zipcode,
        # TODO: this looks wrong, but is actually what we need due to mismatch
        # between gdal/postgis and django at 2.12. should fix this when upgraded
        # see: https://code.djangoproject.com/ticket/31611
        'x': point.y,
        'y': point.x,
        'parcels': parcel_info
    }
    return JsonResponse(response_dict)


@transaction.atomic
def provisionParcel(request, block, lot, x, y):
    logger.info('provisionParcel(block=%s, lot=%s, lon=%s, lat=%s)' % (block, lot, x, y))
    response = GenericResponseObj('programming error')
    try:
        with transaction.atomic():
            user = request.user
            if not user.is_authenticated:
                message = 'cannot provision parcel because user is not logged in'
                logger.info(message)
                response.message = message
            else:

                # build point geometry
                wktPoint = 'SRID=3857;POINT(%s %s)'% (x, y)
                geosPoint = GEOSGeometry(wktPoint)
                geosPoint.transform(2227)

                # get base parcel at this location; exclude service parcel; exclude retired
                parcels = Parcels.objects.filter(geometry__intersects=geosPoint).filter(map_blk_lot=F('blk_lot')).exclude(blk_lot = '0000000').filter(date_map_drop__isnull = True)
                # If we have more than 1 we disallow provisioning here.
                if parcels.count() > 1:
                    raise ValidationError('Parcel provisiong is not allowed at this location because mulitiple base parcels exist at this location.  Please contact your system administrator if you believe you have received this message in error.')
                # If we have less than one, we are either in the water or in the service parcel.
                if parcels.count() < 1:
                    raise ObjectDoesNotExist('no base parcel exists at this location')

                # we know we have a single base parcel
                baseParcel = parcels[0]

                # parcel
                blockLot = block+lot
                newParcel = Parcels(blk_lot=blockLot, block_num=block, lot_num=lot, map_blk_lot=baseParcel.blk_lot, geometry=baseParcel.geometry, create_tms=datetime.now(), update_tms=datetime.now())
                newParcel.validate()
                if baseParcel.block_num != block:
                    raise ValidationError('block number must be %s' % baseParcel.block_num)
                newParcel.save()

                # parcel provisioning
                createUser = request.user
                provisionedParcel = ParcelsProvisioning(parcel=newParcel, create_user=createUser)
                provisionedParcel.save()

            response.status_code = 0
            response.success = True

    except ObjectDoesNotExist as e:
        logger.warning(e)
        response.success = False
        response.message = e
    except ValidationError as e:
        message = ', '.join(e)
        logger.warning(message)
        response.success = False
        response.message = message
    except Exception as e:
        logger.critical(e)
        response.message = 'an exception has occurred - please contact support'
        response.success = False

    return JsonResponse(response.toDict())
