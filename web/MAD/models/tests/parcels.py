import unittest
from MAD.models.models import Parcels
from django.core.exceptions import ValidationError

class ParcelTestCase(unittest.TestCase):

    def setUp(self):
        return

        
    def tearDown(self):
        return

    def testCompareAlpha(self):
        parcel = Parcels(parcel_id=0, block_num="1", lot_num="1")
        self.assertTrue(parcel._compareAlphaParts('', '') == 0)
        self.assertTrue(parcel._compareAlphaParts('', 'a') == -1)
        self.assertTrue(parcel._compareAlphaParts('a', 'a') == 0)
        self.assertTrue(parcel._compareAlphaParts('a', 'A') == 1)
        self.assertTrue(parcel._compareAlphaParts('A', 'a') == -1)
        self.assertTrue(parcel._compareAlphaParts('', 'A') == -1)
        self.assertTrue(parcel._compareAlphaParts('A', '') == -1)
        self.assertTrue(parcel._compareAlphaParts('a', 'b') == -1)
        self.assertTrue(parcel._compareAlphaParts('A', 'B') == -1)
        self.assertTrue(parcel._compareAlphaParts('B', 'A') == 1)

    def buildParcel(self, block, lot, id=0):
        parcel = Parcels(parcel_id=id, block_num=block, lot_num=lot)
        return parcel

    def test_equalTo(self):
        parcel_1 = self.buildParcel('100', '200')
        parcel_2 = self.buildParcel('100', '200')
        self.assertTrue(parcel_1 == parcel_2)

        parcel_1 = self.buildParcel('100A', '200A')
        parcel_2 = self.buildParcel('100A', '200A')
        self.assertTrue(parcel_1 == parcel_2)

    def test_lessThan_01(self):
        parcel_1 = self.buildParcel('100', '200')
        parcel_2 = self.buildParcel('1001', '2001')
        self.assertTrue(parcel_1 < parcel_2)

    def test_lessThan_02(self):
        parcel_1 = self.buildParcel('0012', '003A')
        parcel_2 = self.buildParcel('0012', '003B')
        self.assertTrue(parcel_1 < parcel_2)

    def test_lessThan_03(self):
        parcel_1 = self.buildParcel('0012', '003')
        parcel_2 = self.buildParcel('0012', '003A')
        self.assertTrue(parcel_1 < parcel_2)

    def test_lessThan_04(self):
        parcel_1 = self.buildParcel('3506', '001')
        parcel_2 = self.buildParcel('3638', '084')
        self.assertTrue(parcel_1 < parcel_2)

    def test_greaterThan(self):
        parcel_1 = self.buildParcel('3638', '084')
        parcel_2 = self.buildParcel('3506', '001')
        self.assertTrue(parcel_1 > parcel_2)

        parcel_1 = self.buildParcel('1001', '2002')
        parcel_2 = self.buildParcel('100', '200')
        self.assertTrue(parcel_1 > parcel_2)

    def test_lotIsAllAlpha_01(self):
        parcel_1 = self.buildParcel('7517', '001')
        parcel_2 = self.buildParcel('7517', 'COM')
        self.assertTrue(parcel_1 > parcel_2)

    def test_lotIsAllAlpha_02(self):
        parcel_1 = self.buildParcel('7517', 'ABC')
        parcel_2 = self.buildParcel('7517', 'DEF')
        self.assertTrue(parcel_1 < parcel_2)


    ##### block num tests

    def test_blockNum_01(self):
        parcel = self.buildParcel('1234', '000')
        try:
            parcel.validate_block_num()
            self.assertTrue(True)
        except ValidationError as e:
            # should not throw exception
            self.assertTrue(False)

    def test_blockNum_02(self):
        parcel = self.buildParcel('123A', '000')
        try:
            parcel.validate_block_num()
            self.assertTrue(False)
        except ValidationError as e:
            print('\n'+', '.join(e))
            self.assertTrue(True)

    def test_blockNum_03(self):
        parcel = self.buildParcel('1234A', '000')
        try:
            parcel.validate_block_num()
            self.assertTrue(True)
        except ValidationError as e:
            self.assertTrue(False)


    ##### lot num tests

    def test_lotNum_01(self):
        parcel = self.buildParcel('1234', '000')
        try:
            parcel.validate_lot_num()
            self.assertTrue(True)
        except ValidationError as e:
            self.assertTrue(False)

    def test_lotNum_02(self):
        parcel = self.buildParcel('1234', '0000')
        try:
            parcel.validate_lot_num()
            self.assertTrue(False)
        except ValidationError as e:
            print('\n'+', '.join(e))
            self.assertTrue(True)

    def test_lotNum_03(self):
        parcel = self.buildParcel('1234', '000A')
        try:
            parcel.validate_lot_num()
            self.assertTrue(True)
        except ValidationError as e:
            self.assertTrue(False)

    def test_lotNum_04(self):
        parcel = self.buildParcel('1234', 'AAA')
        try:
            parcel.validate_lot_num()
            self.assertTrue(False)
        except ValidationError as e:
            print('\n'+', '.join(e))
            self.assertTrue(True)

