from MAD.models.models import CrChangeRequests, CrAddresses, CrAddressBase, Streetnames, AddressBase, Zones, DAddressDisposition, Parcels, CrAddressXParcels

class ModelTestHelper:

    @staticmethod
    def createInMemoryCrChangeRequest(name='unit testing - test', comment='a comment'):
        from datetime import datetime
        crChangeRequest = CrChangeRequests()
        crChangeRequest.name = name
        crChangeRequest.requestor_user_id = 1
        crChangeRequest.requestor_comment = comment
        crChangeRequest.review_status_id = 4
        crChangeRequest.requestor_last_update = datetime.now()
        # add some stuff to help
        crChangeRequest.crBaseAddresses = []
        crChangeRequest.crAddresses = []
        return crChangeRequest

    @staticmethod
    def createCrAddressFromAddress(address=None, crAddressBase=None, crChangeRequest=None):
        crAddress = CrAddresses()
        crAddress.cr_address_base = crAddressBase
        crAddress.unit_num = address.unit_num
        crAddress.change_request = crChangeRequest
        crAddress.address_id = address.address_id
        crAddress.retire_tms = address.retire_tms
        crAddress.address_base_flg = address.address_base_flg
        return crAddress

    @staticmethod
    def createCrBaseAddressFromBaseAddress(baseAddress):
        crBaseAddress = CrAddressBase()
        crBaseAddress.base_address_num = baseAddress.base_address_num
        crBaseAddress.street_segment = baseAddress.street_segment
        crBaseAddress.zone = baseAddress.zone
        crBaseAddress.address_base_id = baseAddress.address_base_id
        crBaseAddress.geometry = baseAddress.geometry
        return crBaseAddress

    @staticmethod
    def getExistingBaseAddress(fullStreetName='', streetNumber=0, prefix=None, suffix=None):
        streetSegments = Streetnames.objects.filter(full_street_name = fullStreetName).values('street_segment')
        baseAddresses = AddressBase.objects.filter(base_address_num = streetNumber).filter(street_segment__in = streetSegments).filter(retire_tms__isnull = True)
        baseAddresses = AddressBase.filterForPrefix(baseAddresses, prefix)
        baseAddresses = AddressBase.filterForSuffix(baseAddresses, suffix)
        assert baseAddresses.count() == 1, 'expected a single base address but got ' + str(baseAddresses.count())
        return baseAddresses[0]

    @staticmethod
    def createInMemoryCrBaseAddress(streetNumber=None, baseAddressPrefix=None, fullStreetName=None, baseAddressSuffix=None, jurisdiction='SF MAIN', geom='POINT (0 0)'):
        crBaseAddress = CrAddressBase()
        crBaseAddress.base_address_prefix = baseAddressPrefix
        crBaseAddress.base_address_num = streetNumber
        crBaseAddress.base_address_suffix = baseAddressSuffix
        crBaseAddress.street_segment = Streetnames.objects.filter(full_street_name = fullStreetName)[0].street_segment
        crBaseAddress.zone = Zones.objects.filter(jurisdiction = jurisdiction)[0]
        crBaseAddress.geometry = geom
        crBaseAddress.geometry_proposed = geom
        return crBaseAddress

    @staticmethod
    def createInMemoryCrAddress(crBaseAddress=None, crChangeRequest=None, isBaseAddress=None):
        address = CrAddresses()
        address.cr_address_base = crBaseAddress
        address.change_request = crChangeRequest
        address.mailable_flg = False
        address.disposition_code = DAddressDisposition.objects.get(disposition_code = 1)
        address.retire_flg = False
        address.address_base_flg = isBaseAddress
        return address

    @staticmethod
    def createInMemoryCrAddressXParcel(crAddress=None, apn=None, link=False, unlink=False):
        if not crAddress or not apn:
            raise Exception('required arg(s) missing')

        parcel = Parcels.objects.filter(blk_lot = apn)[0]
        craxp = CrAddressXParcels(cr_address=crAddress, parcel=parcel, link=link, unlink=unlink)
        return craxp
