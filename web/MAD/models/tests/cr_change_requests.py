import json
import unittest

from MAD.models.models import CrAddresses, CrAddressBase, CrJsonModel, Parcels, CrAddressXParcels
from MAD.models.tests.utils import ModelTestHelper
from MAD.exceptions import ValidationWarning
from datetime import datetime
from django.db import transaction
from django.contrib.auth.models import Group
from django.contrib.gis.db.models.functions import Translate

# TODO: a good deal of refactoring can be done here - utility methods could be broken out into
# a separate class, seems that utility methods are not used in some of the older methods resulting
# in some redundant code
from MAD.models.models import Addresses

class CrChangeRequestTestCase(unittest.TestCase):

    def setUp(self):
        return
        
    def tearDown(self):
        return

    def doExpectPass_DuplicatesInQueue_ChangeRequest(self, crChangeRequest):
        for crBaseAddress in crChangeRequest.crBaseAddresses:
            self.doExpectPass_DuplicatesInQueue(crBaseAddress)

    def doExpectFail_DuplicatesInQueue_ChangeRequest(self, crChangeRequest):
        for crBaseAddress in crChangeRequest.crBaseAddresses:
            self.doExpectFail_DuplicatesInQueue(crBaseAddress)

    def addBaseAddressNew(self, crChangeRequest=None, streetNumber=0, fullStreetName=None):

        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=streetNumber, fullStreetName=fullStreetName)
        crBaseAddress.save()
        crChangeRequest.crBaseAddresses.append(crBaseAddress)

        crAddress = ModelTestHelper.createInMemoryCrAddress(crBaseAddress=crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        crAddress.save()
        crChangeRequest.crAddresses.append(crAddress)

        return crBaseAddress

    def addBaseAddressExisting(self, crChangeRequest=None, streetNumber=0, fullStreetName=None):

        baseAddress = ModelTestHelper.getExistingBaseAddress(fullStreetName=fullStreetName, streetNumber=streetNumber)
        crBaseAddress = ModelTestHelper.createCrBaseAddressFromBaseAddress(baseAddress)
        crBaseAddress.save()
        crChangeRequest.crBaseAddresses.append(crBaseAddress)

        addresses = Addresses.objects.filter(address_base = baseAddress)
        address = addresses.get(address_base_flg = True)
        crAddress = ModelTestHelper.createCrAddressFromAddress(address=address, crAddressBase=crBaseAddress, crChangeRequest=crChangeRequest)
        crAddress.save()
        crChangeRequest.crAddresses.append(crAddress)

        return crBaseAddress


    def doExpectFail_DuplicatesInQueue(self, crBaseAddress):
        duplicateBaseAddresses = crBaseAddress.findDuplicatesInQueue()
        self.assertTrue(duplicateBaseAddresses.count() > 0)
        validation = crBaseAddress.validateDuplicatesInQueue()
        self.assertTrue(validation.valid is False)
        duplicateBaseAddress = validation.object
        self.assertTrue(duplicateBaseAddress is not None)

    def doExpectPass_DuplicatesInQueue(self, crBaseAddress):
        duplicateBaseAddresses = crBaseAddress.findDuplicatesInQueue()
        self.assertTrue(duplicateBaseAddresses.count() > 0)
        validation = crBaseAddress.validateDuplicatesInQueue()
        self.assertTrue(validation.valid is True)
        duplicateBaseAddress = validation.object
        self.assertTrue(duplicateBaseAddress is None)

    def createChangeRequestCompleteExisting(self, baseAddressPrefix=None, streetNumber=0, baseAddressSuffix=None, fullStreetName=None, jurisdiction='SF MAIN', apnForParcelLinkChange=None, link=False, unlink=False, connection=None):

        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing - test', 'no comment')
        crChangeRequest.save()

        baseAddress = ModelTestHelper.getExistingBaseAddress(fullStreetName=fullStreetName, streetNumber=streetNumber)
        crBaseAddress = ModelTestHelper.createCrBaseAddressFromBaseAddress(baseAddress)
        crBaseAddress.save()

        # we just create what we need for the base address because that is all we are testing
        addresses = Addresses.objects.filter(address_base = baseAddress)
        addresses = addresses.filter(address_base_flg = True)
        address = addresses[0]
        crAddress = ModelTestHelper.createCrAddressFromAddress(address=address, crAddressBase=crBaseAddress, crChangeRequest=crChangeRequest)
        crAddress.save()

        if apnForParcelLinkChange:
            crAddressXParcel = ModelTestHelper.createInMemoryCrAddressXParcel(crAddress=crAddress, apn=apnForParcelLinkChange, link=link, unlink=unlink)
            crAddressXParcel.save()
            return crChangeRequest, crBaseAddress, crAddress, crAddressXParcel

        return crChangeRequest, crBaseAddress, crAddress


    def createChangeRequestComplete(self, baseAddressPrefix=None, streetNumber=0, baseAddressSuffix=None, fullStreetName=None, jurisdiction='SF MAIN', geom='POINT (0 0)', apnForParcelLinkChange=None, connection=None):
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing', 'a comment')
        crChangeRequest.validateForSave()
        crChangeRequest.save()

        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(baseAddressPrefix=baseAddressPrefix, streetNumber=streetNumber, baseAddressSuffix=baseAddressSuffix, fullStreetName=fullStreetName, jurisdiction=jurisdiction, geom=geom)
        crBaseAddress.validate()
        crBaseAddress.save()

        crAddress = ModelTestHelper.createInMemoryCrAddress(crBaseAddress=crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        crAddress.validate(requestor_is_subaddress_editor=False)
        crAddress.save()

        if apnForParcelLinkChange:
            # In the case of a new address we can either link or not link - we cannot unlink.
            crAddressXParcel = ModelTestHelper.createInMemoryCrAddressXParcel(crAddress=crAddress, apn=apnForParcelLinkChange, link=True, unlink=False)
            crAddressXParcel.save()
            return crChangeRequest, crBaseAddress, crAddress, crAddressXParcel

        return crChangeRequest, crBaseAddress, crAddress

    def cleanup(self, address, baseAddress, changeRequest):
        address.delete()
        baseAddress.delete()
        changeRequest.delete()

    def getCrAddressesForValidation(self, crChangeRequest):
        crAddresses = CrAddresses.objects.filter(change_request = crChangeRequest.change_request_id)
        crAddresses = crAddresses.filter(address_base_flg = True)
        
        return crAddresses


    def getCrBaseAddressesForValidation(self, crAddresses):
        crAddressBaseIds = []
        for crAddress in crAddresses:
            crAddressBaseIds.append(crAddress.cr_address_base.cr_address_base_id)
        crAddressBases = CrAddressBase.objects.filter(cr_address_base_id__in=crAddressBaseIds)

        return crAddressBases


    def flagAddressForRetire(self, crAddress):
        from datetime import datetime
        crAddress.retire_tms = datetime.now
        crAddress.retire_flg = True
        crAddress.save()
        
        return crAddress


    def runDuplicateInActiveChecks(self, crChangeRequest=None, crBaseAddress=None, expectValid=False):
        try:
            crAddresses = self.getCrAddressesForValidation(crChangeRequest)
            crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)

            activeBaseAddress = crBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
            if expectValid:
                self.assertTrue(activeBaseAddress is None)
            else:
                self.assertTrue(activeBaseAddress is not None)

            validation = crBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
            if expectValid:
                self.assertTrue(validation.valid)
            else:
                self.assertFalse(validation.valid)

            activeBaseAddress = validation.object
            if expectValid:
                self.assertTrue(activeBaseAddress is None)
            else:
                self.assertTrue(activeBaseAddress is not None)

        except:
            raise

    def runDuplicateInQueueChecks(self, crChangeRequest=None, crBaseAddress=None, expectValid=False):

        try:
            crBaseAddresses = crBaseAddress.findDuplicatesInQueue()
            if expectValid:
                self.assertTrue(crBaseAddresses.count() == 0)
            else:
                self.assertTrue(crBaseAddresses.count() > 0)

            validation = crBaseAddress.validateDuplicatesInQueue()
            if expectValid:
                self.assertTrue(validation.valid)
            else:
                self.assertFalse(validation.valid)

            changeRequest = validation.object
            if expectValid:
                self.assertTrue(changeRequest is None)
            else:
                self.assertTrue(changeRequest is not None)
        except:
            raise

    def getUnretiredCrAddresses(self, crChangeRequest):
        return (
            CrAddresses.objects
            .filter(change_request = crChangeRequest.change_request_id)
            .filter(address_base_flg = True)
            .filter(retire_flg=False)
            .select_related("cr_address_base")
        )

    def generatePostForCrChangeRequest(self, crChangeRequest, crBaseAddress, address):
        return {
            "change_request_id": crChangeRequest.pk,
            "name": "Test Request",
            "review_status_id": 4,
            "status_description": "editing",
            "requestor_name": crChangeRequest.requestor_user.username,
            "requestor_comment": "",
            "requestor_last_update": "November 17, 2023, 4:37 pm",
            "reviewer_name": "",
            "reviewer_comment": "",
            "reviewer_last_update": "",
            "concurrency_id": 0,
            "base_addresses": [
            {
                "address_base_id": crBaseAddress.address_base_id,
                "base_address_prefix": None,
                "base_address_num": 1511,
                "base_address_suffix": None,
                "complete_landmark_name": "",
                "landmark_aliases": [],
                "address_base_type": 0,
                "zone_id": 54,
                "street_segment": 5208,
                "geometry": "POINT (-13626353.16005156 4543907.029250734)",
                "geometry_proposed": "POINT (-13626353.16005156 4543907.029250734)",
                "address_id": address.pk,
                "floor": 105,
                "unit_type": 0,
                "unit_num": None,
                "unit_mailable_flg": False,
                "unit_mailable_flg_proposed": False,
                "unit_disposition": 1,
                "unit_disposition_proposed": 1,
                "retire_flg": False,
                "unit_addresses": [
                {
                    "address_id": None,
                    "address_base_flg": False,
                    "unit_type": 5,
                    "floor": 5,
                    "unit_num": "101",
                    "mailable_flg": True,
                    "mailable_flg_proposed": True,
                    "disposition": None,
                    "disposition_proposed": 1,
                    "retire_flg": False,
                    "isNew": True,  # <----------------------------- is new
                    "linked_parcels": [],
                    "parcel_link_changes": [],
                    "unit_addresses": [
                    {
                        "address_id": None,
                        "address_base_flg": False,
                        "unit_type": 1,
                        "floor": 5,
                        "unit_num": "101-sub",
                        "mailable_flg": True,
                        "mailable_flg_proposed": True,
                        "disposition": None,
                        "disposition_proposed": 1,
                        "retire_flg": False,
                        "isNew": True,        # <-------------------- subaddress
                        "linked_parcels": [],
                        "parcel_link_changes": [],
                        "unit_addresses": []
                    }
                    ]
                }
                ],
                "parcel_geometry": "",
                "linked_parcels": [
                {
                    "apn": "5513021",
                    "address_x_parcel_id": 552083
                }
                ],
                "parcel_link_changes": [],
                "isNew": False,
            }
            ]
        }

    ###################################################################################################################

    def testAddressNumberValidation(self):

        testDict = {
            -1: False,
            0: True,
            1: True,
            999999: True,
            "999999": True,
            "9,999": False,
            1000000: False,
            "000000": True,
            None: False,
            "": False,
            "1a": False,
            "a": False,
            "ab": False
        }
        for k, v in list(testDict.items()):
            crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=k, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            try:
                crBaseAddress.validateAddressNumber()
                try:
                    self.assertTrue(v)
                except:
                    print("\nfailed on %s, %s " % (k, v))
                    raise
            except ValidationWarning as e:
                try:
                    self.assertFalse(v)
                except:
                    print("\nfailed on %s, %s " % (k, v))
                    raise


    @transaction.atomic
    def testDuplicateInActive_00(self):
        sid = transaction.savepoint()

        # create 33 sussex in memory;
        # 33 susex is an address that does not exist
        # there is no equivalent in the business tables
        # this should result in passed validations because we will not be inserting a duplicate

        ##### setup
        # first we create a cr change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing - test_00', 'no comment')
        crChangeRequest.save()
        # create base address
        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=33, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.save()
        # create address
        address = ModelTestHelper.createInMemoryCrAddress(crBaseAddress = crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        address.save()
        crAddresses = self.getCrAddressesForValidation(crChangeRequest)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)
        #####

        activeBaseAddress = crBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(activeBaseAddress is None)
        validation = crBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(validation.valid is True)
        transaction.savepoint_rollback(sid)



    @transaction.atomic
    def testDuplicateInActive_01(self):
        sid = transaction.savepoint()
        # create a new 20 sussex - which is a duplicate of 20 sussex that is in active
        # this should result in failed validations - we would be inserting a materially duplicate address

        ##### setup
        # first we create a cr change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing - test_01', 'no comment')
        crChangeRequest.save()
        # create base address
        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=20, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.save()
        # create address
        address = ModelTestHelper.createInMemoryCrAddress(crBaseAddress = crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        address.save()
        crAddresses = self.getCrAddressesForValidation(crChangeRequest)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)
        #####

        activeBaseAddress = crBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(activeBaseAddress is not None)
        validation = crBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(validation.valid is False)
        activeBaseAddress = validation.object
        self.assertTrue(activeBaseAddress is not None)
        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testDuplicateInActive_02(self):
        sid = transaction.savepoint()
        # create a 20 sussex from active (with identity); there is an identical in active
        # this should result in passed validations
        # This is to allow update of existing rows.

        ##### setup
        # first we create a CrBaseAddress with an existing adress inside of it
        baseAddress = ModelTestHelper.getExistingBaseAddress(fullStreetName='SUSSEX ST', streetNumber=20)
        crBaseAddress = ModelTestHelper.createCrBaseAddressFromBaseAddress(baseAddress)
        # first we create a cr change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing - test_01', 'no comment')
        crChangeRequest.save()
        # save base address
        crBaseAddress.save()
        # create address
        address = ModelTestHelper.createInMemoryCrAddress(crBaseAddress = crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        address.save()
        crAddresses = self.getCrAddressesForValidation(crChangeRequest)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)
        #####

        activeBaseAddress = crBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(activeBaseAddress is None)
        validation = crBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(validation.valid is True)
        activeBaseAddress = validation.object
        self.assertTrue(activeBaseAddress is None)
        transaction.savepoint_rollback(sid)


    @transaction.atomic
    def testDuplicateInActiveWithPrefixAndSuffix(self):
        sid = transaction.savepoint()
        # Try to create a new 20 sussex - which will be a duplicate of 20 sussex that is in active, except the new 20 sussex has a different prefix or suffix.
        # This should result in passed validations - we would be inserting a materially distinct address.
        # This supports things like "20 1/2 sussex" where the "1/2" is the suffix on the base address.

        # prefix - no prefix
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(baseAddressPrefix='X', streetNumber=20, baseAddressSuffix=None, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        self.runDuplicateInActiveChecks(crChangeRequest=crChangeRequest, crBaseAddress=crBaseAddress, expectValid=True)
        transaction.savepoint_rollback(sid)

        sid = transaction.savepoint()
        # suffix - no suffix
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='A', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        self.runDuplicateInActiveChecks(crChangeRequest=crChangeRequest, crBaseAddress=crBaseAddress, expectValid=True)
        transaction.savepoint_rollback(sid)



    @transaction.atomic
    def testDuplicateInQueueWithPrefixAndSuffix(self):
        sid = transaction.savepoint()
        (crChangeRequest_A, crBaseAddress_A, address_A) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='C', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        (crChangeRequest_B, crBaseAddress_B, address_B) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='C', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        self.runDuplicateInQueueChecks(crChangeRequest=crChangeRequest_A, crBaseAddress=crBaseAddress_A, expectValid=False)
        self.runDuplicateInQueueChecks(crChangeRequest=crChangeRequest_B, crBaseAddress=crBaseAddress_B, expectValid=False)
        transaction.savepoint_rollback(sid)

        sid = transaction.savepoint()
        (crChangeRequest_A, crBaseAddress_A, address_A) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='B', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        (crChangeRequest_B, crBaseAddress_B, address_B) = self.createChangeRequestComplete(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix='C', fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        self.runDuplicateInQueueChecks(crChangeRequest=crChangeRequest_A, crBaseAddress=crBaseAddress_A, expectValid=True)
        self.runDuplicateInQueueChecks(crChangeRequest=crChangeRequest_B, crBaseAddress=crBaseAddress_B, expectValid=True)
        transaction.savepoint_rollback(sid)



    @transaction.atomic
    def testDuplicateInQueue_00(self):
        sid = transaction.savepoint()
        # an address that will have no Duplicate in queue

        ##### setup
        # first we create a cr change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest()
        crChangeRequest.save()
        # create base address
        crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.save()
        # create address
        address = ModelTestHelper.createInMemoryCrAddress(crBaseAddress = crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
        address.save()
        #####

        crBaseAddresses = crBaseAddress.findDuplicatesInQueue()
        self.assertTrue(crBaseAddresses.count() == 0)
        validation = crBaseAddress.validateDuplicatesInQueue()
        self.assertTrue(validation.valid is True)
        changeRequest = validation.object
        self.assertTrue(changeRequest is None)
        transaction.savepoint_rollback(sid)


    @transaction.atomic
    def testDuplicateInQueue_01(self):
        sid = transaction.savepoint()

        # a single cr base address in queue with no conflicts

        ##### setup
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')

        # report
        #        print ''
        #        print 'change_request_id: ' + str(crChangeRequest.change_request_id)
        #        print 'cr_address_base_id: ' + str(crBaseAddress.cr_address_base_id)
        #        print 'cr_address_id: ' + str(address.cr_address_id)


        # the test
        duplicateBaseAddresses = crBaseAddress.findDuplicatesInQueue()
        self.assertTrue(duplicateBaseAddresses.count() == 0)
        validation = crBaseAddress.validateDuplicatesInQueue()
        self.assertTrue(validation.valid is True)
        duplicateBaseAddress = validation.object
        self.assertTrue(duplicateBaseAddress is None)
        transaction.savepoint_rollback(sid)


    @transaction.atomic
    def testDuplicateInQueue_02(self):
        sid = transaction.savepoint()

        # a pair of duplicate new addresses in queue - should fail

        ##### setup
        # first we create a cr change request

        (crChangeRequest_1, crBaseAddress_1, address_1) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        (crChangeRequest_2, crBaseAddress_2, address_2) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')

        # report
        #        print ''
        #        print 'change_request_id: ' + str(crChangeRequest.change_request_id)
        #        print 'cr_address_base_id: ' + str(crBaseAddress.cr_address_base_id)
        #        print 'cr_address_id: ' + str(address.cr_address_id)

        ##### do the test
        duplicateBaseAddresses = crBaseAddress_2.findDuplicatesInQueue()
        self.assertTrue(duplicateBaseAddresses.count() > 0)
        validation = crBaseAddress_2.validateDuplicatesInQueue()
        self.assertTrue(validation.valid is False)
        duplicateBaseAddress = validation.object
        changeRequest = duplicateBaseAddress.getChangeRequest()
        self.assertTrue(changeRequest is not None)
        # should be symmetrical
        duplicateBaseAddresses = crBaseAddress_1.findDuplicatesInQueue()
        self.assertTrue(duplicateBaseAddresses.count() > 0)
        validation = crBaseAddress_1.validateDuplicatesInQueue()
        self.assertTrue(validation.valid is False)
        duplicateBaseAddress = validation.object
        changeRequest = duplicateBaseAddress.getChangeRequest()
        self.assertTrue(changeRequest is not None)

        transaction.savepoint_rollback(sid)




    @transaction.atomic
    def testDuplicateInQueue_03(self):
        sid = transaction.savepoint()

        # an existing address with no duplicate in the queue - should pass

        ##### setup
        # first we create a cr change request
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(streetNumber=20, fullStreetName='SUSSEX ST')

        duplicateBaseAddresses = crBaseAddress.findDuplicatesInQueue()
        self.assertTrue(duplicateBaseAddresses.count() == 0)
        validation = crBaseAddress.validateDuplicatesInQueue()
        self.assertTrue(validation.valid is True)
        duplicateBaseAddress = validation.object
        self.assertTrue(duplicateBaseAddress is None)
        transaction.savepoint_rollback(sid)


    @transaction.atomic
    def testDuplicateInQueue_04(self):
        sid = transaction.savepoint()

        # two copies of the same existing addresses - should fail - xxx

        # setup
        (crChangeRequest_1, crBaseAddress_1, address_1) = self.createChangeRequestCompleteExisting(streetNumber=20, fullStreetName='SUSSEX ST')
        (crChangeRequest_2, crBaseAddress_2, address_2) = self.createChangeRequestCompleteExisting(streetNumber=20, fullStreetName='SUSSEX ST')

        # the test
        # should pass symmetrically
        self.doExpectFail_DuplicatesInQueue(crBaseAddress_1)
        self.doExpectFail_DuplicatesInQueue(crBaseAddress_2)
        transaction.savepoint_rollback(sid)


    @transaction.atomic
    def testDuplicateInQueue_05(self):
        sid = transaction.savepoint()
        # an existing address in queue with duplicate new address
        # should fail because this is not a

        # setup
        (crChangeRequest_1, crBaseAddress_1, address_1) = self.createChangeRequestCompleteExisting(streetNumber=20, fullStreetName='SUSSEX ST')
        (crChangeRequest_2, crBaseAddress_2, address_2) = self.createChangeRequestComplete(streetNumber=20, fullStreetName='SUSSEX ST')

        # the test
        # should fail symmetrically
        self.doExpectFail_DuplicatesInQueue(crBaseAddress_1)
        self.doExpectFail_DuplicatesInQueue(crBaseAddress_2)

        # should fail symmetrically (looks like but is not a retire-replace)
        from datetime import datetime
        address_1.retire_tms = datetime.now
        address_1.save()
        self.doExpectFail_DuplicatesInQueue(crBaseAddress_1)
        self.doExpectFail_DuplicatesInQueue(crBaseAddress_2)
        transaction.savepoint_rollback(sid)


    @transaction.atomic
    def testRetireReplace_00(self):
        sid = transaction.savepoint()
        # a retire replace:
        # retire an existing address and add a new address that matches the retired one in the same CR
        # (should pass)

        # setup
        # create the change request
        crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest()
        crChangeRequest.save()
        # add the existing base address to the change request
        existingCrBaseAddress = self.addBaseAddressExisting(crChangeRequest=crChangeRequest, streetNumber=20, fullStreetName='SUSSEX ST')

        # flag the existing address as a retire
        self.flagAddressForRetire(crChangeRequest.crAddresses[0])

        # add the new base address to replace the one being retired
        newCrBaseAddress = self.addBaseAddressNew(crChangeRequest=crChangeRequest, streetNumber=20, fullStreetName='SUSSEX ST')
        # get the address and base address lists for validation
        crAddresses = self.getCrAddressesForValidation(crChangeRequest)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)

        # the test
        # test validation of the existing address
        existingActiveBaseAddress = existingCrBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(existingActiveBaseAddress is None)
        validation1 = existingCrBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(validation1.valid is True)
        # test validation of the new address
        newActiveBaseAddress = newCrBaseAddress.findDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(newActiveBaseAddress is None)
        validation2 = newCrBaseAddress.validateDuplicateInActive(crAddressBases, crAddresses)
        self.assertTrue(validation2.valid is True)
        transaction.savepoint_rollback(sid)


    @transaction.atomic
    def testRetireReplace_01(self):
        sid = transaction.savepoint()
        # a retire replace - unlikely case -
        # retire an existing address and add a new address that matches the retired one in the same CR
        # but a retire already exists for the existing address in another CR
        # (should fail)

        # setup
        # create the first change request
        # this change request will hold only a single retire
        crChangeRequestRetireOnly = ModelTestHelper.createInMemoryCrChangeRequest()
        crChangeRequestRetireOnly.save()
        # add the existing base address to the first change request
        existingCrBaseAddressForRetireOnly = self.addBaseAddressExisting(crChangeRequest=crChangeRequestRetireOnly, streetNumber=20, fullStreetName='SUSSEX ST')

        # flag the existing address as a retire
        self.flagAddressForRetire(crChangeRequestRetireOnly.crAddresses[0])

        # create the second change request
        # this change request will contain both a retire and a replace (new address matching retired one)
        crChangeRequestRetireReplace = ModelTestHelper.createInMemoryCrChangeRequest()
        crChangeRequestRetireReplace.save()
        # add the existing base address to the second change request
        existingCrBaseAddress = self.addBaseAddressExisting(crChangeRequest=crChangeRequestRetireReplace, streetNumber=20, fullStreetName='SUSSEX ST')

        # flag the existing address as a retire
        self.flagAddressForRetire(crChangeRequestRetireReplace.crAddresses[0])

        # add the new base address to replace the one being retired
        newCrBaseAddress = self.addBaseAddressNew(crChangeRequest=crChangeRequestRetireReplace, streetNumber=20, fullStreetName='SUSSEX ST')
        # get the address and base address lists for validation
        crAddresses = self.getCrAddressesForValidation(crChangeRequestRetireReplace)
        crAddressBases = self.getCrBaseAddressesForValidation(crAddresses)

        # the test
        # test for duplicates in the queue of change requests
        # should fail since the retire has been duplicated
        self.doExpectFail_DuplicatesInQueue_ChangeRequest(crChangeRequestRetireReplace)
        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testStreetSegmentValidation_00(self):
        sid = transaction.savepoint()
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.street_segment.date_dropped = None
        try:
            crBaseAddress.validateStreetSegment()
            self.assertTrue(True is True)
        except ValidationWarning as e:
            self.assertTrue(True is False)
        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testStreetSegmentValidation_01(self):
        sid = transaction.savepoint()
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        crBaseAddress.street_segment.date_dropped = datetime.now()
        try:
            crBaseAddress.validateStreetSegment()
            self.assertTrue(True is True)
        except Exception as e:
            self.assertTrue(True is False)
        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testParcelValidation_00(self):
        sid = transaction.savepoint()
        # a change request creating a new address, then an associated parcel is retired.
        # validation should return False

        # first we create a cr change request
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')

        # add a parcel
        parcel = Parcels.objects.get( blk_lot = '0026028' )
        crAddressXParcels = CrAddressXParcels(cr_address = address, parcel = parcel, unlink = False, link = True)
        crAddressXParcels.save()

        # flag the parcel as retired
        parcel.date_map_drop = datetime.now()
        parcel.save()

        ##### do the test
        unretiredAddresses = self.getUnretiredCrAddresses(crChangeRequest)
        try:
            validations = CrAddresses.validateForRetiredParcelLinks(unretiredAddresses)
        except Exception as e:
            print(e)
        self.assertTrue(len(validations) > 0)
        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testParcelValidation_01(self):
        sid = transaction.savepoint()

        # a change request creating a new address, the associated parcel is unretired.
        # validation should return True

        # first we create a cr change request
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')

        # add a parcel
        parcel = Parcels.objects.get( blk_lot = '0026028' )
        crAddressXParcels = CrAddressXParcels(cr_address = address, parcel = parcel)
        crAddressXParcels.save()

        # flag parcel as unretired
        parcel.date_map_drop = None
        parcel.save()

        ##### do the test
        unretiredAddresses = self.getUnretiredCrAddresses(crChangeRequest)
        validations = CrAddresses.validateForRetiredParcelLinks(unretiredAddresses)
        self.assertTrue(len(validations) == 0)
        transaction.savepoint_rollback(sid)


    @transaction.atomic
    def test_geom_reg__linking_to_moved_parcel(self):
        sid = transaction.savepoint()
        # create and save CR
        (crChangeRequest, crBaseAddress, address, crAddressXParcel) = self.createChangeRequestComplete(streetNumber=100000, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN', geom='SRID=3857;POINT(-13629182.3781258 4542210.64066713)', apnForParcelLinkChange='7548017')
        # We only run the validations against unretired parcels - get those now.
        unretiredCrAddresses = self.getUnretiredCrAddresses(crChangeRequest)
        # run the parcel geom validations - should pass
        failedValidations = crChangeRequest.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
        self.assertTrue(len(failedValidations) == 0)
        # now move the parcel geometry
        parcelQueryset = Parcels.objects.filter(blk_lot = '7548017')
        parcelQueryset.update(geometry=Translate('geometry', 1000, 1000))
        parcel = parcelQueryset[0]
        parcel.save()
        # run the validations again which should fail
        failedValidations = crChangeRequest.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
        self.assertTrue(len(failedValidations) > 0)
        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def test_geom_reg__linked_to_moved_parcel(self):
        sid = transaction.savepoint()
        # Existing address, already linked to a parcel; the parcel is then moved.
        # create and save CR
        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(baseAddressPrefix=None, streetNumber=20, baseAddressSuffix=None, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
        # We only run the validations against unretired parcels - get those now.
        unretiredCrAddresses = self.getUnretiredCrAddresses(crChangeRequest)
        # run the parcel geom validations - should pass
        failedValidations = crChangeRequest.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
        self.assertTrue(len(failedValidations) == 0)
        # now move the parcel geometry
        parcelQueryset = Parcels.objects.filter(blk_lot = '7548017')
        parcelQueryset.update(geometry=Translate('geometry', 1000, 1000))
        parcel = parcelQueryset[0]
        parcel.save()
        # Run the validations again which should not fail.
        # Even though the parcel is not spatially coincident it is not invalid
        failedValidations = crChangeRequest.validateGeometricRegistration(unretiredCrAddresses = unretiredCrAddresses)
        # warm fuzzies
        #            if failedValidations:
        #                print '\nfailed validations (%s):' % len(failedValidations)
        #                for v in failedValidations:
        #                    print v.message
        self.assertTrue(len(failedValidations) == 0)
        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testCreateCrSubaddress(self):
        sid = transaction.savepoint()

        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(
            streetNumber=1511, fullStreetName="YORK ST"
        )
        crChangeRequest.requestor_user_id = 1
        post_crObj = CrJsonModel(json.dumps(
            {"changeRequest": [
                self.generatePostForCrChangeRequest(crChangeRequest, crBaseAddress, address)
            ]}
        ))
        crChangeRequest.requestor_user.groups.add(Group.objects.get(name="subaddress_editor"))
        post_crObj.saveToDb(crChangeRequest.requestor_user)

        base_and_units = CrAddresses.objects.filter(change_request=crChangeRequest)
        self.assertEqual(base_and_units.count(), 3)
        subaddresses = base_and_units.filter(cr_parent__isnull=False)

        self.assertEqual(subaddresses.count(), 1)
        self.assertEqual(subaddresses.last().cr_parent.unit_num, '101')

        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testInvalidCrSubaddress_not_a_subaddress_editor(self):
        sid = transaction.savepoint()

        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(
            streetNumber=1511, fullStreetName="YORK ST"
        )
        post_crObj = CrJsonModel(json.dumps(
            {"changeRequest": [
                self.generatePostForCrChangeRequest(crChangeRequest, crBaseAddress, address)
            ]}
        ))
        crChangeRequest.requestor_user.groups.remove(Group.objects.get(name="subaddress_editor"))
        top_unit = post_crObj.base_addresses[0].unit_addresses[0]
        top_unit.unit_type = 0

        with self.assertRaisesRegex(
            ValidationWarning,
            "You do not have permission to create subaddresses.",
        ):
            post_crObj.saveToDb(crChangeRequest.requestor_user)

        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testInvalidCrSubaddress_invalid_unit_type_for_top_unit(self):
        sid = transaction.savepoint()

        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(
            streetNumber=1511, fullStreetName="YORK ST"
        )
        post_crObj = CrJsonModel(json.dumps(
            {"changeRequest": [
                self.generatePostForCrChangeRequest(crChangeRequest, crBaseAddress, address)
            ]}
        ))
        crChangeRequest.requestor_user.groups.remove(Group.objects.get(name="subaddress_editor"))
        top_unit = post_crObj.base_addresses[0].unit_addresses[0]
        top_unit.unit_type = 10  # penthouse - not allowed as top unit for this role.
        top_unit.unit_addresses = []
        with self.assertRaisesRegex(
            ValidationWarning,
            "can not be created by this requestor.",
        ):
            post_crObj.saveToDb(crChangeRequest.requestor_user)

        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testInvalidCrSubaddress_wrong_unit_type_level(self):
        sid = transaction.savepoint()

        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(
            streetNumber=1511, fullStreetName="YORK ST"
        )
        post_crObj = CrJsonModel(json.dumps(
            {"changeRequest": [
                self.generatePostForCrChangeRequest(crChangeRequest, crBaseAddress, address)
            ]}
        ))
        crChangeRequest.requestor_user.groups.add(Group.objects.get(name="subaddress_editor"))
        top_unit = post_crObj.base_addresses[0].unit_addresses[0]
        top_unit.unit_type = 5
        top_unit.unit_addresses[0].unit_type = 5
        with self.assertRaisesRegex(
            ValidationWarning,
            "must be lower than its parent's",
        ):
            post_crObj.saveToDb(crChangeRequest.requestor_user)

        transaction.savepoint_rollback(sid)

    @transaction.atomic
    def testInvalidCrSubaddress_active_child_of_retired_parent(self):
        sid = transaction.savepoint()

        (crChangeRequest, crBaseAddress, address) = self.createChangeRequestCompleteExisting(
            streetNumber=1511, fullStreetName="YORK ST"
        )
        post_crObj = CrJsonModel(json.dumps(
            {"changeRequest": [
                self.generatePostForCrChangeRequest(crChangeRequest, crBaseAddress, address)
            ]}
        ))
        crChangeRequest.requestor_user.groups.add(Group.objects.get(name="subaddress_editor"))
        top_unit = post_crObj.base_addresses[0].unit_addresses[0]
        top_unit.retire_flg = True
        with self.assertRaisesRegex(
            ValidationWarning,
            "Addresses cannot be retired while children are still active",
        ):
            post_crObj.saveToDb(crChangeRequest.requestor_user)

        transaction.savepoint_rollback(sid)
