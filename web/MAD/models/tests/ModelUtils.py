import unittest
from MAD.models.models import GEOSGeometry
from MAD.models.ModelUtils import ModelUtils

class ModelUtilsTestCase(unittest.TestCase):

    def setUp(self):
        self.modelUtils = ModelUtils()

    def tearDown(self):
        self.modelUtils = None

    def test_buildParcelPickList__1650_mission(self):
        point = GEOSGeometry('SRID=2227;POINT(6006835.09201982 2109174.53033911)')
        parcelPickList = self.modelUtils.buildParcelPickList(point)
        self.assertTrue(len(parcelPickList) == 6)

    def test_buildParcelPickList__691_chenery(self):
        point = GEOSGeometry('SRID=2227;POINT(6002444.94010647 2095171.22964628)')
        parcelPickList = self.modelUtils.buildParcelPickList(point)
        self.assertTrue(len(parcelPickList) == 3)

    def test_buildParcelPickList__2655_hyde(self):
        point = GEOSGeometry('SRID=2227;POINT(6006839.81943328 2121446.42553871)')
        parcelPickList = self.modelUtils.buildParcelPickList(point)
        self.assertTrue(len(parcelPickList) < 1500)
        self.assertTrue(len(parcelPickList) > 1200)

    def test_getBaseParcelGeometryWithApns(self):
        try:
            multiPolygon = ModelUtils.getBaseParcelGeometryWithApns(['1234001', '1234002'])
            print('\n', multiPolygon.ewkt)
            self.assertTrue(True)
        except Exception as e:
            print(e)
            self.assertTrue(False)
