import unittest
from django.contrib.gis.geos.geometry import GEOSGeometry
from MAD.models.models import CrJsonModel, AddressXParcels, CrAddressXParcels, Parcels, VwBaseAddresses
from MAD.models.tests.utils import ModelTestHelper
from MAD.utils.geojson_encode import *
from django.db import transaction


class AddressBaseTestCase(unittest.TestCase):

    def testNewAddress(self):
        newBaseAddress = CrJsonModel.BaseAddress()
        #wktPoint = 'POINT(-13627621.165471 4548075.3285316)'
        wktPoint = 'POINT(-13627625.345621 4548070.5512173)'
        srsId = '3857'
        point = GEOSGeometry('SRID=' + srsId + ';' + wktPoint)
        point.transform(2227)
        newBaseAddress.geometry = point
        newBaseAddress.proposeNew()
        #print geojson_encode(newBaseAddress)

class AddressXParcelsTestCase(unittest.TestCase):

    def test_01(self):
        q = AddressXParcels.unretiredObjects.filter(parcel__blk_lot = '3717022')
        #print q.query.as_sql()
        self.assertTrue(q.count() == 1)


class CrAddressXParcelsTestCase(unittest.TestCase):

    @transaction.atomic
    def test_save(self):
        sid = transaction.savepoint()
        try:

            crChangeRequest = ModelTestHelper.createInMemoryCrChangeRequest('unit testing', 'a comment')
            crChangeRequest.save()

            crBaseAddress = ModelTestHelper.createInMemoryCrBaseAddress(streetNumber=20, fullStreetName='SUSSEX ST', jurisdiction='SF MAIN')
            crBaseAddress.save()

            crAddress = ModelTestHelper.createInMemoryCrAddress(crBaseAddress=crBaseAddress, crChangeRequest=crChangeRequest, isBaseAddress=True)
            crAddress.save()

            parcel = Parcels.objects.get(blk_lot='7548017')
            caxp0 = CrAddressXParcels(cr_address = crAddress, parcel = parcel)
            caxp0.save()

            #print crChangeRequest
            #print crBaseAddress
            #print crAddress
            #print crAddressXParcel
            qs = CrAddressXParcels.objects.filter(id = caxp0.id)
            self.assertTrue(qs.count()==1)
            caxp1 = qs[0]

            self.assertTrue(caxp0.id == caxp1.id)
            self.assertTrue(caxp0.cr_address.cr_address_id == caxp1.cr_address_id)
            self.assertTrue(caxp0.parcel.parcel_id == caxp1.parcel.parcel_id)
            self.assertTrue(caxp0.address_x_parcel is None)
            self.assertTrue(caxp1.address_x_parcel is None)
            self.assertEqual(caxp0.link, None)
            self.assertEqual(caxp0.link, caxp1.link)
            self.assertEqual(caxp0.unlink, None)
            self.assertEqual(caxp0.unlink, caxp1.unlink)

        except Exception as e:
            print(e)
            raise
        transaction.savepoint_rollback(sid)

    def test_save_retrieve(self):
        pass

class ClusteringTestCase(unittest.TestCase):

    def setUp(self):
        #print "setting up"
        return
        
    def tearDown(self):
        #print "tearing down"
        return
    
    
    ##############################################
    # Test cases
        
    def testCluster(self):
        self.assertEqual(self.doCluster(), 1)
        
    
    ##############################################
    # Helper functions    
    
    def doCluster(self, apn='7140057', zoom=1):
        # apn 7140057 returns 45 addresses
        adds = []
        #print apn
        #print connection.queries
        try:
            p = Parcels.objects.get(blk_lot = apn)
            #print p
            if p:
                addresses = VwBaseAddresses.objects.filter(geometry__intersects = p.geometry)
        finally:
            clusterList = VwBaseAddresses.querysetToClusterList(addresses, zoom)
            return len(clusterList)
