
import logging

logger = logging.getLogger('EAS')

class ModelUtils:

    def iterateThis(self, anIterator):
        # convenience function to reduce verbosity of try-except
        x = None
        try:
            x = next(anIterator)
        except StopIteration:
            return (True, None)

        return (False, x)


    def assertKeysMontonicIncreasing(self, previousKey, nextKey):
        assert nextKey is not None, 'found a key with value of None'
        if previousKey:
            assert previousKey <= nextKey, 'key values non-monotonic'


    def mergeOneToMany(self, parents=None, children=None, mergeImpl=None):
        logger.info('mergeOneToMany')
        # handle emptiness
        if parents is None or len(parents) == 0:
            return parents
        if children is None or len(children) == 0:
            for parent in parents:
                mergeImpl.parentInitNode(parent)
            return parents

        childrenIterator = children.iterator()
        child = next(childrenIterator)
        doneWithChildren = False
        lastParentKey = None
        lastChildKey = None
        for parent in parents:

            mergeImpl.parentInitNode(parent)

            if mergeImpl.parentGetKey(parent) is None:
                continue

            while 1==1 and not doneWithChildren:
                if mergeImpl.childGetKey(child) is None:
                    (doneWithChildren, child) = self.iterateThis(childrenIterator)
                    if doneWithChildren: break
                    else: continue

                #logger.info('parentKey:' + str(mergeImpl.parentGetKey(parent)) + ' childKey: ' + str(mergeImpl.childGetKey(child)))

                self.assertKeysMontonicIncreasing(lastChildKey, mergeImpl.childGetKey(child))
                lastChildKey = mergeImpl.childGetKey(child)

                self.assertKeysMontonicIncreasing(lastParentKey, mergeImpl.parentGetKey(parent))
                lastParentKey = mergeImpl.parentGetKey(parent)

                if mergeImpl.parentGetKey(parent) == mergeImpl.childGetKey(child):
                    mergeImpl.parentAddChild(parent, mergeImpl.childGetValue(child))
                    (doneWithChildren, child) = self.iterateThis(childrenIterator)
                    if doneWithChildren: break
                    else: continue
                elif mergeImpl.parentGetKey(parent) < mergeImpl.childGetKey(child):
                    # do things like average, sum, etc
                    mergeImpl.parentSummarizeGroup()
                    break
                elif  mergeImpl.parentGetKey(parent) > mergeImpl.childGetKey(child):
                    (doneWithChildren, child) = self.iterateThis(childrenIterator)
                    if doneWithChildren: break
                    else: continue


        return parents


    def mergeOneToOne(self, parents=None, children=None, mergeImpl=None):
        logger.info('mergeOneToOne')
        # NOTE: assume parents and children are sorted by key

        # check for empty lists
        if parents is None or len(parents) == 0:
            return parents
        if children is None or len(children) == 0:
            for parent in parents:
                mergeImpl.parentSetValue(parent, None)
            return parents

        childrenIter = children.iterator()
        haveMoreChildren = True
        child = next(childrenIter)

        lastParentKey = None
        lastChildKey = None
        debugMatchCount=0
        for parent in parents:

            mergeImpl.parentSetValue(parent, None)

            # do not join on None
            if mergeImpl.parentGetKey(parent) is None:
                continue
            while mergeImpl.childGetKey(child) is None:
                try:
                    child = next(childrenIter)
                except StopIteration:
                    haveMoreChildren = False
                    break
            if not haveMoreChildren:
                break

            # logger.info('parentKey:' + str(mergeImpl.parentGetKey(parent)) + ' childKey: ' + str(mergeImpl.childGetKey(child)))

            self.assertKeysMontonicIncreasing(lastChildKey, mergeImpl.childGetKey(child))
            lastChildKey = mergeImpl.childGetKey(child)

            self.assertKeysMontonicIncreasing(lastParentKey, mergeImpl.parentGetKey(parent))
            lastParentKey = mergeImpl.parentGetKey(parent)


            if mergeImpl.parentGetKey(parent) < mergeImpl.childGetKey(child):
                continue

            while mergeImpl.parentGetKey(parent) > mergeImpl.childGetKey(child):
                try:
                    child = next(childrenIter)
                except StopIteration:
                    haveMoreChildren = False
                    break

            if mergeImpl.parentGetKey(parent) == mergeImpl.childGetKey(child):
                debugMatchCount+=1
                mergeImpl.parentSetValue(parent, mergeImpl.childGetValue(child))
                continue

        return parents

    def buildParcelPickListDict(self, apn=None, retireTms=None, distance=0):
        return {'apn': apn, 'retire_tms': retireTms, 'distance':distance}

    def buildParcelPickList(self, pointGeometry=None, includeParcels = [], includeRetired=False):
        # Include the following:
        # - unretired parcels at this point location
        # - the "includedParcels" regardless of any state
        # No duplicates.
        # Sort on APN.
        # Defer retrieval of geometry for performance - http://code.google.com/p/eas/issues/detail?id=459

        logger.info('buildApnPickList begin')
        logger.info('includeParcels count: ' + str(len(includeParcels)))

        from MAD.models.models import ParcelsAbstract, Parcels
        from django.db.models import Q

        # At this point location get the
        #  - unique block numbers
        #  - parcel ids
        blockNumSet = set()
        parcelIds = []
        parcelsAtPoint = Parcels.objects.filter(geometry__intersects=pointGeometry).defer('geometry')
        if not includeRetired:
            parcelsAtPoint = parcelsAtPoint.filter(date_map_drop__isnull = True)
        for p in parcelsAtPoint:
            p.distance = 0
            blockNumSet.add(p.block_num)
            parcelIds.append(p.parcel_id)
        logger.info('parcelsAtPoint count: ' + str(parcelsAtPoint.count()))

        #####
        # Build a set that contains the 'includeParcels' from the parameter
        # and
        # parcels with the same block number as the parcels from the point query above that are also within 500 feet.
        # Excludes the parcels from the point query above for performance (in the case of large geometries).
        # Filtering on block number alone is not sufficient (the data is not perfect).
        serviceParcelBlockNum = '0000'
        parcels = Parcels.objects.filter(
            Q(geometry__dwithin = (pointGeometry, 500))
            |
            Q(pk__in = [parcel.parcel_id for parcel in includeParcels])
        ).exclude(
            Q(parcel_id__in = parcelIds)
            |
            Q(block_num = serviceParcelBlockNum)
        ).extra(
            select={"distance" : "st_distance(geometry, st_geomfromtext('%s', %s))::int" % (pointGeometry.wkt, pointGeometry.srs.srid)}
        ).defer(
            'geometry'
        ).order_by(
            'distance'
        )

        if serviceParcelBlockNum in blockNumSet:
            p = parcels[:1]
            nearestParcel = list(parcels[:1])
            if nearestParcel:
                blockNumSet.add(nearestParcel[0].block_num)
        if includeRetired:
            parcels = parcels.filter(
                Q(block_num__in = blockNumSet)
                |
                Q(pk__in = [parcel.parcel_id for parcel in includeParcels])
            )
        else:
            parcels = parcels.filter(
                Q(block_num__in = blockNumSet) & Q(date_map_drop__isnull = True)
                |
                Q(pk__in = [parcel.parcel_id for parcel in includeParcels])
            )

        parcels = list(parcels)
        logger.info('parcels count (some): ' + str(len(parcels)))

        parcels.extend(parcelsAtPoint)

        includedServiceParcels = Parcels.objects.filter(
            Q(pk__in = [parcel.parcel_id for parcel in includeParcels]) & Q(block_num = serviceParcelBlockNum)
        ).exclude(
            Q(parcel_id__in = parcelIds)
        ).extra(
            select={"distance" : "'(service parcel)'"}
        ).defer(
            'geometry'
        )

        if includedServiceParcels.count() > 0:
            parcels.extend(includedServiceParcels)

        logger.info('parcels count (all): ' + str(len(parcels)))

        parcelPickList = []
        for parcel in parcels:
            dict = self.buildParcelPickListDict(apn=parcel.blk_lot, retireTms=parcel.date_map_drop, distance=parcel.distance)
            parcelPickList.append(dict)
        logger.info('parcelPickList count: ' + str(len(parcelPickList)))

        parcelPickList = sorted(parcelPickList, key=lambda x: x['apn'])
        logger.info('parcelPickList sorted')

        logger.info('buildApnPickList end')
        return parcelPickList

    @staticmethod
    def getBaseParcelGeometryWithApns(apns):
        # Given a list of APNs, return the distinct set of parcel polygons.
        logger.info('getBaseParcelGeometryWithApns begin')
        from MAD.models.models import Parcels
        parcelValues = Parcels.objects.filter(blk_lot__in = apns).filter(geometry__isnull = False).exclude(blk_lot = '0000000').values("geometry").distinct("geometry")
        from django.contrib.gis.geos import MultiPolygon
        polygons = []
        for parcelValue in parcelValues:
            for polygon in parcelValue['geometry']:
                polygons.append(polygon)
        if not polygons:
            return None
        multiPolygon = MultiPolygon(polygons)
        multiPolygon.srid = 2227
        logger.info('getBaseParcelGeometryWithApns end')
        return multiPolygon

    @staticmethod
    def getUserMessage(applicationMessage):
        # todo: add structure here
        # The idea here is to transform the message into a user friendly message.
        # For now let us proceed from this model: 'user message in english | detailed technical jargon'
        try:
            applicationMessage = applicationMessage.message
        except AttributeError as e:
            logger.info("\'applicationMessage\' raised is either a str or a builtin python Exception Object, not an custom Exception Object")
            # logger.warn(e)
        if isinstance(applicationMessage, str):
            if applicationMessage.find('|') > 0:
                return applicationMessage.partition('|')[0]
        else:
            applicationMessage = str(applicationMessage)
        
        return applicationMessage
