from django.conf.urls import include, url
from django.conf import settings
from MAD.views import views

from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    
    # admin functions
    #(r'^admin/(.*)', include(admin.site.urls)),
    url(r'^Media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    
    # main site page
    url(r'^$', views.Index),
    url(r'^debug', views.Index, {'mode': 'debug'}),
    url(r'^build', views.Index, {'mode': 'build'}),
    url(r'^js/(\d+)$', views.Js),
    
    # reports
    url(r'^AddressReport/$', views.AddressReport),
    url(r'^Identifiers/$', views.Identifiers),
    url(r'^UnitsForAddressReport/$', views.UnitsForAddressReport),
    url(r'^ParcelsForAddressReport/$', views.ParcelsForAddressReport),
    url(r'^Aliases/$', views.Aliases),
    url(r'^History/$', views.History),
    url(r'^BaseGeometryForParcels/$', views.BaseGeometryForParcels),

    # searches
    url(r'^search/address$', views.AddressSearch),
    url(r'^search/apn$', views.ApnSearch),
    url(r'^search/addressbbox$', views.AddressBBoxWFS),
    url(r'^search/parcelToAddresses$', views.parcelToAddresses),
    url(r'^search/parcel/(?P<apn>\w+)/$', views.getParcelByApn),
    url(r'^search/addressToParcels$', views.addressToParcels),
    url(r'^search/pointToParcels$', views.pointToParcels),
    url(r'^search/addressinfo/(\w+)$', views.AddressInfo),

    # change requests
    url(r'^changeRequest/mine/$', views.getMyChangeRequests),
    url(r'^changeRequest/available/$', views.getAvailableChangeRequests),
    url(r'^changeRequest/save/$', views.saveCr, {'review_status_id': None}),
    url(r'^changeRequest/edit/(?P<crId>\d+)/$', views.getCr, {'review_status_id': 4}),
    url(r'^changeRequest/submitforreview/$', views.saveCr, {'review_status_id': 5}),
    url(r'^changeRequest/approve/$', views.saveCr, {'review_status_id': 2}),
    url(r'^changeRequest/reject/$', views.saveCr, {'review_status_id': 3}), 
    url(r'^changeRequest/acceptforreview/$', views.saveCr, {'review_status_id': 1}),   
    url(r'^changeRequest/(?P<crId>\d+)/$', views.getCr),
    url(r'^changeRequest/delete/(?P<crId>\d+)/$', views.deleteCr),
    url(r'^changeRequest/report/(?P<changeRequestId>\d+)/$', views.getChangeRequestReport),
    url(r'^changeRequest/report/axp/(?P<axpId>\d+)/create/$', views.getChangeRequestReportForAxpCreate),
    url(r'^changeRequest/report/axp/(?P<axpId>\d+)/retire/$', views.getChangeRequestReportForAxpRetire),
    url(r'^changeRequest/report/address/(?P<baseAddrId>\d+)/$', views.getChangeRequestReportForAddress),
    url(r'^changeRequest/report/addresses/(?P<changeRequestId>\d+)/$', views.getAddressesForChangeRequest),

    url(r'^getbaseaddr/(?P<baseAddrId>\d+)/$', views.getBaseAddr),

    # provision parcel
    # example values
    # block: ['1234', '1234A']
    # lot: ['001', '001T']
    # lon: -123.0001111 (we have hardcoded negative longitude)
    # lat: 42.0001111
    url(r'^provision/parcel/block/(?P<block>\w+)/lot/(?P<lot>\w+)/x/(?P<x>-\d+\.\d+)/y/(?P<y>\d+\.\d+)/$', views.provisionParcel),

    # loosely based on OGC standard
    #     http://www.opengeospatial.org/standards/requests/89
    # e.g. usage /geocode/findAddressCandidates?f=json&Address=20 Sussex St&Zip=94131
    url(r'^geocode/findAddressCandidates/$', views.findAddressCandidates),

    (r'^geocode/getAddress/$', get_address),

    # domains
    url(r'^domain/changerequest/$', views.getChangeRequestDomains),

    url(r'^address/proposeAt/(?P<geom>.*)/$', views.proposeAddress),

    # authentication
    url(r'^login/$', views.login),
    url(r'^logout/$', views.logout),
    url(r'^user/$', views.getUser),
    url(r'^user/resetpassword/$', views.resetPassword),
    url(r'^user/changepassword/$', views.changePassword),

]
