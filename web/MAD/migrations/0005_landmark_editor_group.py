from django.db import migrations

def add_group(apps, schema_editor):
    Group = apps.get_model("auth", "Group")
    Group.objects.create(name='landmark_editor')


def remove_group(apps, schema_editor):
    Group = apps.get_model("auth", "Group")
    group = Group.objects.get(name='landmark_editor')
    group.delete()

class Migration(migrations.Migration):

    dependencies = [
        ('MAD', '0004_landmark_addresses'),
    ]

    operations = [
        migrations.RunPython(add_group, remove_group),
    ]

