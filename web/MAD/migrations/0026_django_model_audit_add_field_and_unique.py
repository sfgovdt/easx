# Generated by Django 2.2.12 on 2024-12-07 11:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MAD', '0025_django_model_audit_max_length_and_nullable'),
    ]

    operations = [
        migrations.SeparateDatabaseAndState(
            state_operations=[
                migrations.AddField(
                    model_name='streetnames',
                    name='update_tms',
                    field=models.DateTimeField(null=True),
                ),
                migrations.AddConstraint(
                    model_name='addressbase',
                    constraint=models.UniqueConstraint(fields=('base_address_prefix', 'base_address_num', 'base_address_suffix', 'zone', 'street_segment', 'retire_tms'), name='address_base_uk'),
                ),
                migrations.AddConstraint(
                    model_name='dstreettypealiases',
                    constraint=models.UniqueConstraint(fields=('street_type', 'alias'), name='d_street_type_aliases_UNQ1'),
                ),
                migrations.AddConstraint(
                    model_name='xmitqueue',
                    constraint=models.UniqueConstraint(fields=('address_base_history', 'address_history', 'address_x_parcel_history', 'last_change_tms'), name='xmit_queue_UNQ'),
                ),
                migrations.AddConstraint(
                    model_name='zones',
                    constraint=models.UniqueConstraint(fields=('jurisdiction', 'zipcode'), name='zone_uk'),
                ),
            ],
        ),
    ]
