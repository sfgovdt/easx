from django.db import migrations


forward_sql = """
DROP VIEW v_base_address_search;

CREATE OR REPLACE VIEW v_base_address_search AS
SELECT ab.address_base_id,
    ab.base_address_prefix,
    ab.base_address_num,
    ab.base_address_suffix,
    sn.base_street_name AS street_name,
    sn.street_type,
    coalesce(sn.category, 'MAP') AS street_category,
    sn.pre_direction AS street_pre_direction,
    sn.post_direction AS street_post_direction,
    sn.full_street_name,
    sn.street_segment_id,
    z.zipcode,
    z.jurisdiction,
    ad.disposition_description::text AS disposition,
    st_transform(ab.geometry, 3857) AS geometry,
    a.create_tms,
    a.activate_change_request_id,
    a.last_change_tms,
    a.update_change_request_id,
    a.retire_tms,
    a.retire_change_request_id,
    ab.complete_landmark_name,
    t.type_name as address_type,
    ab.landmark_aliases
FROM address_base ab
    JOIN addresses a ON ab.address_base_id = a.address_base_id
    JOIN zones z ON ab.zone_id = z.zone_id
    JOIN d_address_disposition ad ON a.disposition_code = ad.disposition_code
    JOIN d_address_base_type t ON ab.address_base_type_id = t.id
    LEFT JOIN streetnames sn ON ab.street_segment_id = sn.street_segment_id
WHERE 1 = 1
    AND a.address_base_flg = true;

DROP VIEW vw_base_addresses;
CREATE OR REPLACE VIEW vw_base_addresses AS
SELECT ab.address_base_id,
    a.address_id,
    ab.base_address_prefix,
    ab.base_address_num,
    ab.base_address_suffix,
    st_transform(ab.geometry, 3857) AS geometry,
    sn.full_street_name,
    z.zipcode,
    z.jurisdiction,
    ss.l_f_add,
    ss.l_t_add,
    ss.r_f_add,
    ss.r_t_add,
    ab.retire_tms,
    (
        SELECT count(*) AS count
        FROM invalid_addresses ia
        WHERE ia.address_id = a.address_id
    ) AS validation_warning_count,
    (
        CASE
            WHEN t.type_name = 'Landmark Address' THEN ab.complete_landmark_name::text
            ELSE (
                (
                    (
                        CASE
                            WHEN ab.base_address_prefix IS NULL THEN ''::text
                            ELSE ab.base_address_prefix::text || ' '::text
                        END || ab.base_address_num
                    ) || ' '::text
                ) || CASE
                    WHEN ab.base_address_suffix IS NULL THEN ''::text
                    ELSE ab.base_address_suffix::text || ' '::text
                END
            ) || sn.full_street_name::text
        END
    ) AS address,
    (
        (
            SELECT count(*) AS count
            FROM address_x_parcels axp
            WHERE axp.address_id = a.address_id
                AND axp.parcel_id = (
                    (
                        SELECT parcels.parcel_id
                        FROM parcels
                        WHERE parcels.blk_lot::text = '0000000'::text
                    )
                )
                AND axp.retire_tms IS NULL
        )
    ) > 0 AS service_address_flg,
    ab.complete_landmark_name,
    t.type_name as address_type,
    ab.landmark_aliases
FROM address_base ab
    JOIN addresses a ON ab.address_base_id = a.address_base_id
    JOIN zones z ON ab.zone_id = z.zone_id
    JOIN d_address_base_type t ON ab.address_base_type_id = t.id
    LEFT JOIN streetnames sn ON ab.street_segment_id = sn.street_segment_id
    LEFT JOIN street_segments ss ON ab.street_segment_id = ss.street_segment_id
WHERE (
        sn.category::text = 'MAP'::text
        OR sn.category is null
    )
    AND a.address_base_flg = true;

select UpdateGeometrySRID('virtual_addresses', 'geometry', 3857);
"""

reverse_sql = forward_sql.replace("3857", "900913")

class Migration(migrations.Migration):

    dependencies = [
        ("MAD", "0009_virtualaddresses_virtualaddressesstaging"),
    ]

    operations = [
        migrations.RunSQL(forward_sql, reverse_sql)
    ]
