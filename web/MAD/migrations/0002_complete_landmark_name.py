from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('MAD', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='addressbase',
            name='complete_landmark_name',
            field=models.CharField(max_length=150, null=True),
        ),
        migrations.AddField(
            model_name='craddressbase',
            name='complete_landmark_name',
            field=models.CharField(max_length=150, null=True),
        ),
    ]
