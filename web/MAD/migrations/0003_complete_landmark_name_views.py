from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('MAD', '0002_complete_landmark_name'),
    ]

    forward_sql = """
    CREATE OR REPLACE VIEW public.vw_address_search
        AS
        SELECT b.address_id,
            a.geometry,
            a.address_base_id,
            a.base_address_prefix AS base_prefix,
            a.base_address_num,
            a.base_address_suffix AS base_suffix,
            b.unit_num,
            c.full_street_name,
            c.category,
            d.zipcode,
            d.jurisdiction,
            a.complete_landmark_name
        FROM address_base a,
            addresses b,
            streetnames c,
            zones d
        WHERE b.retire_tms IS NULL AND a.address_base_id = b.address_base_id AND a.street_segment_id = c.street_segment_id AND a.zone_id = d.zone_id;

    CREATE OR REPLACE VIEW public.v_base_address_search
        AS
        SELECT ab.address_base_id,
            ab.base_address_prefix,
            ab.base_address_num,
            ab.base_address_suffix,
            sn.base_street_name AS street_name,
            sn.street_type,
            sn.category AS street_category,
            sn.pre_direction AS street_pre_direction,
            sn.post_direction AS street_post_direction,
            sn.full_street_name,
            sn.street_segment_id,
            z.zipcode,
            z.jurisdiction,
            ad.disposition_description::text AS disposition,
            st_transform(ab.geometry, 900913) AS geometry,
            a.create_tms,
            a.last_change_tms,
            a.retire_tms,
            ab.complete_landmark_name
        FROM address_base ab,
            addresses a,
            streetnames sn,
            zones z,
            d_address_disposition ad
        WHERE 1 = 1 AND ab.address_base_id = a.address_base_id AND a.address_base_flg = true AND ab.zone_id = z.zone_id AND ab.street_segment_id = sn.street_segment_id AND a.disposition_code = ad.disposition_code;

        
    CREATE OR REPLACE VIEW public.vw_invalid_addresses
        AS
        SELECT ia.invalid_address_id,
            ia.message,
            ab.address_base_id,
            ab.base_address_num,
            ab.base_address_suffix,
            ab.geometry,
            sn.full_street_name,
            a.unit_num,
            a.address_base_flg,
            diat.invalid_type_desc,
            ab.complete_landmark_name
        FROM invalid_addresses ia,
            addresses a,
            address_base ab,
            streetnames sn,
            d_invalid_address_types diat
        WHERE 1 = 1 AND ia.address_id = a.address_id AND a.address_base_id = ab.address_base_id AND sn.category::text = 'MAP'::text AND ab.street_segment_id = sn.street_segment_id AND diat.invalid_type_id = ia.invalid_type_id;

    CREATE OR REPLACE VIEW public.vw_addresses_flat_native
        AS
        SELECT (((ab.address_base_id::text || '-'::text) || a.address_id::text) || '-'::text) ||
                CASE
                    WHEN axp.id IS NULL THEN '0'::text
                    ELSE axp.id::text
                END AS id,
            ab.address_base_id,
            ab.base_address_num,
            ab.base_address_suffix,
            sn.base_street_name,
            sn.street_type,
            sn.post_direction,
            sn.full_street_name,
            ss.seg_cnn,
            ss.street_segment_id,
            ab.create_tms AS base_address_create_tms,
            ab.retire_tms AS base_address_retire_tms,
            a.address_id,
            a.address_base_flg,
            a.unit_num,
            a.unit_type_id,
            a.floor_id,
            a.disposition_code,
            dap.disposition_description,
            a.create_tms AS unit_address_create_tms,
            a.retire_tms AS unit_address_retire_tms,
            p.map_blk_lot,
            p.blk_lot,
            p.date_map_add AS parcel_date_map_add,
            p.date_map_drop AS parcel_date_map_drop,
            axp.id AS address_x_parcel_id,
            axp.create_tms AS address_x_parcel_create_tms,
            axp.retire_tms AS address_x_parcel_retire_tms,
            axp.activate_change_request_id AS address_x_parcel_activate_change_request_id,
            axp.retire_change_request_id AS address_x_parcel_retire_change_request_id,
            z.zipcode,
            ab.geometry,
            ab.complete_landmark_name
        FROM address_base ab
            JOIN addresses a ON ab.address_base_id = a.address_base_id
            JOIN streetnames sn ON ab.street_segment_id = sn.street_segment_id
            JOIN street_segments ss ON ab.street_segment_id = ss.street_segment_id
            JOIN zones z ON z.zone_id = ab.zone_id
            JOIN d_address_disposition dap ON a.disposition_code = dap.disposition_code
            LEFT JOIN address_x_parcels axp ON axp.address_id = a.address_id
            LEFT JOIN parcels p ON axp.parcel_id = p.parcel_id
        WHERE 1 = 1 AND sn.category::text = 'MAP'::text;

        ALTER TABLE vw_address_search OWNER TO eas_dbo;
        GRANT ALL ON TABLE vw_address_search TO eas_dbo;
        GRANT SELECT,
            UPDATE,
            INSERT,
            DELETE ON TABLE vw_address_search TO django;

        ALTER TABLE v_base_address_search OWNER TO eas_dbo;
        GRANT ALL ON TABLE v_base_address_search TO eas_dbo;
        GRANT SELECT,
            UPDATE,
            INSERT,
            DELETE ON TABLE v_base_address_search TO django;

        ALTER TABLE vw_invalid_addresses OWNER TO eas_dbo;
        GRANT ALL ON TABLE vw_invalid_addresses TO eas_dbo;
        GRANT SELECT,
            UPDATE,
            INSERT,
            DELETE ON TABLE vw_invalid_addresses TO django;

        ALTER TABLE vw_addresses_flat_native OWNER TO eas_dbo;
        GRANT ALL ON TABLE vw_addresses_flat_native TO eas_dbo;
        GRANT SELECT,
            UPDATE,
            INSERT,
            DELETE ON TABLE vw_addresses_flat_native TO django;
    """

    reverse_sql = """

    DROP VIEW public.vw_address_search;
    DROP VIEW public.v_base_address_search;
    DROP VIEW public.vw_invalid_addresses;
    DROP VIEW public.vw_addresses_flat_native;


    CREATE OR REPLACE VIEW public.vw_address_search
        AS
        SELECT b.address_id,
            a.geometry,
            a.address_base_id,
            a.base_address_prefix AS base_prefix,
            a.base_address_num,
            a.base_address_suffix AS base_suffix,
            b.unit_num,
            c.full_street_name,
            c.category,
            d.zipcode,
            d.jurisdiction
        FROM address_base a,
            addresses b,
            streetnames c,
            zones d
        WHERE b.retire_tms IS NULL AND a.address_base_id = b.address_base_id AND a.street_segment_id = c.street_segment_id AND a.zone_id = d.zone_id;


    CREATE OR REPLACE VIEW public.v_base_address_search
        AS
        SELECT ab.address_base_id,
            ab.base_address_prefix,
            ab.base_address_num,
            ab.base_address_suffix,
            sn.base_street_name AS street_name,
            sn.street_type,
            sn.category AS street_category,
            sn.pre_direction AS street_pre_direction,
            sn.post_direction AS street_post_direction,
            sn.full_street_name,
            sn.street_segment_id,
            z.zipcode,
            z.jurisdiction,
            ad.disposition_description::text AS disposition,
            st_transform(ab.geometry, 900913) AS geometry,
            a.create_tms,
            a.last_change_tms,
            a.retire_tms
        FROM address_base ab,
            addresses a,
            streetnames sn,
            zones z,
            d_address_disposition ad
        WHERE 1 = 1 AND ab.address_base_id = a.address_base_id AND a.address_base_flg = true AND ab.zone_id = z.zone_id AND ab.street_segment_id = sn.street_segment_id AND a.disposition_code = ad.disposition_code;


    CREATE OR REPLACE VIEW public.vw_invalid_addresses
        AS
        SELECT ia.invalid_address_id,
            ia.message,
            ab.address_base_id,
            ab.base_address_num,
            ab.base_address_suffix,
            ab.geometry,
            sn.full_street_name,
            a.unit_num,
            a.address_base_flg,
            diat.invalid_type_desc
        FROM invalid_addresses ia,
            addresses a,
            address_base ab,
            streetnames sn,
            d_invalid_address_types diat
        WHERE 1 = 1 AND ia.address_id = a.address_id AND a.address_base_id = ab.address_base_id AND sn.category::text = 'MAP'::text AND ab.street_segment_id = sn.street_segment_id AND diat.invalid_type_id = ia.invalid_type_id;

    CREATE OR REPLACE VIEW public.vw_addresses_flat_native
        AS
        SELECT (((ab.address_base_id::text || '-'::text) || a.address_id::text) || '-'::text) ||
                CASE
                    WHEN axp.id IS NULL THEN '0'::text
                    ELSE axp.id::text
                END AS id,
            ab.address_base_id,
            ab.base_address_num,
            ab.base_address_suffix,
            sn.base_street_name,
            sn.street_type,
            sn.post_direction,
            sn.full_street_name,
            ss.seg_cnn,
            ss.street_segment_id,
            ab.create_tms AS base_address_create_tms,
            ab.retire_tms AS base_address_retire_tms,
            a.address_id,
            a.address_base_flg,
            a.unit_num,
            a.unit_type_id,
            a.floor_id,
            a.disposition_code,
            dap.disposition_description,
            a.create_tms AS unit_address_create_tms,
            a.retire_tms AS unit_address_retire_tms,
            p.map_blk_lot,
            p.blk_lot,
            p.date_map_add AS parcel_date_map_add,
            p.date_map_drop AS parcel_date_map_drop,
            axp.id AS address_x_parcel_id,
            axp.create_tms AS address_x_parcel_create_tms,
            axp.retire_tms AS address_x_parcel_retire_tms,
            axp.activate_change_request_id AS address_x_parcel_activate_change_request_id,
            axp.retire_change_request_id AS address_x_parcel_retire_change_request_id,
            z.zipcode,
            ab.geometry
        FROM address_base ab
            JOIN addresses a ON ab.address_base_id = a.address_base_id
            JOIN streetnames sn ON ab.street_segment_id = sn.street_segment_id
            JOIN street_segments ss ON ab.street_segment_id = ss.street_segment_id
            JOIN zones z ON z.zone_id = ab.zone_id
            JOIN d_address_disposition dap ON a.disposition_code = dap.disposition_code
            LEFT JOIN address_x_parcels axp ON axp.address_id = a.address_id
            LEFT JOIN parcels p ON axp.parcel_id = p.parcel_id
        WHERE 1 = 1 AND sn.category::text = 'MAP'::text;
        
        ALTER TABLE vw_address_search OWNER TO eas_dbo;
        GRANT ALL ON TABLE vw_address_search TO eas_dbo;
        GRANT SELECT,
            UPDATE,
            INSERT,
            DELETE ON TABLE vw_address_search TO django;

        ALTER TABLE v_base_address_search OWNER TO eas_dbo;
        GRANT ALL ON TABLE v_base_address_search TO eas_dbo;
        GRANT SELECT,
            UPDATE,
            INSERT,
            DELETE ON TABLE v_base_address_search TO django;

        ALTER TABLE vw_invalid_addresses OWNER TO eas_dbo;
        GRANT ALL ON TABLE vw_invalid_addresses TO eas_dbo;
        GRANT SELECT,
            UPDATE,
            INSERT,
            DELETE ON TABLE vw_invalid_addresses TO django;

        ALTER TABLE vw_addresses_flat_native OWNER TO eas_dbo;
        GRANT ALL ON TABLE vw_addresses_flat_native TO eas_dbo;
        GRANT SELECT,
            UPDATE,
            INSERT,
            DELETE ON TABLE vw_addresses_flat_native TO django;
    """

    operations = [
        migrations.RunSQL(forward_sql, reverse_sql),
    ]
