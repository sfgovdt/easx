# A full on unix Daemon (pep 3143) is probably a bit more than we need.
# Also, we would like to run on windows or linux.
# Although this is an anemic implementation - but it is adequate at the moment.
#
# To use...
#
# on the command line:
#     python xmit_daemon.py start|stop|status|restart
#
# as a linux "daemon":
#    ./web/bin/xmit_change_notifications.bsh start|stop|status|restart
# "tail" the log file to see the stdout and stderr.
#
# as windows a service:
#    ?
#
# This has probably gotten a bit too complex and may benefit from a rework.
#
#

import sys, os
import inspect
import time
from datetime import datetime

import django
from django.template import Context
from django.template.loader import get_template
from django.template import TemplateDoesNotExist

# provide access to the django settings
KEY = 'MAD_HOME'
try:
    madHomeDir = os.environ[KEY]
except Exception as e:
    sys.stdout.write("%s\n" % e)
    sys.stdout.write('we might be missing a required system variable: %s\n' % KEY)
    sys.exit(-1)
sys.path.append(madHomeDir)
import settings
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()
import logging
logger = logging.getLogger('XMIT')

from MAD.utils.HttpUtils import HttpUtils


class Semaphore:

    # todo - needs better encapsulation

    @staticmethod
    def check(file):
        return os.path.exists(file)

    @staticmethod
    def create(file):
        try:
            outputPath = os.path.dirname(file)
            if not os.path.exists(outputPath):
                os.makedirs(outputPath)
            f = open(file, 'w')
            f.close()
        except Exception as e:
            if os.path.exists(file):
                os.remove(file)
            raise

    @staticmethod
    def clear(file):
        try:
            if os.path.exists(file):
                os.remove(file)
        except Exception as e:
            raise




class Runner:

    def throttleBackPolling(self, pollingInterval):
        return pollingInterval*2 if pollingInterval<3600 else 3600

    def sleep(self, seconds):
        for i in range(1, seconds):
            time.sleep(1)
            if Semaphore.check(settings.XMIT_STOP_REQUEST_SEMAPHORE):
                raise StopIteration()

    def postXml(self, xml):
        import urllib.request, urllib.parse, urllib.error, http.client
        httpConnection = http.client.HTTPConnection('%s:%s' % (settings.XMIT_HOST, settings.XMIT_PORT))
        try:
            logger.info('posting XML')
            logger.info('host:%s' % settings.XMIT_HOST)
            logger.info('port:%s' % settings.XMIT_PORT)
            logger.info('path:%s' % settings.XMIT_PATH)
            encodedParameters = urllib.parse.urlencode({'addressXML': str(xml).encode('utf-8')})
            headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
            httpConnection.request("POST", settings.XMIT_PATH, encodedParameters, headers)
            # If the server cannot send the response within 5 seconds, a socket.error will be raised (python 2.5).
            httpConnection.sock.settimeout(5.0)
            response = httpConnection.getresponse()
            textResponse = HttpUtils.responseAsText(response)
            logger.info(textResponse)
            if response.status != 200:
                raise IOError('request failed - status: %s - message: %s' % (str(response.status), str(response.msg)))
        except Exception as e:
            raise
        finally:
            httpConnection.close()


    def buildXml(self, context):
        try:
            template = get_template('DbiChangeNotification.xml')
            xml = template.render(context)
            return xml
        except Exception as e:
            raise

    def prepareContext(self, queuedItem):
        baseAddress = queuedItem.address_base_history
        baseAddress.geometry.transform(4326)

        # Use empty strings; do not use None.

        streetNameObject = baseAddress.street_segment.getStreetName()
        baseStreetName = streetNameObject.base_street_name.strip()

        streetTypeAbbreviated = streetNameObject.street_type if streetNameObject.street_type else ''
        streetTypeAbbreviated = streetTypeAbbreviated.strip()

        streetTypeUnabbreviated = streetNameObject.getStreetTypeUnabbreviated() if streetNameObject.getStreetTypeUnabbreviated() else ''
        streetTypeUnabbreviated = streetTypeUnabbreviated.strip()

        postDirection = streetNameObject.post_direction if streetNameObject.post_direction else ''
        postDirection = postDirection.strip()

        if len(postDirection) > 0:
            if len(streetTypeAbbreviated) > 0:
                # e.g. MISSION BAY BLVD NORTH
                streetNameForAvs = "%s %s %s" % (baseStreetName, streetTypeAbbreviated, postDirection)
            else:
                # e.g. BROADWAY NORTH (currently no known instances)
                streetNameForAvs = "%s %s" % (baseStreetName, postDirection)
        else:
            streetNameForAvs = baseStreetName

        jurisdiction = baseAddress.zone.jurisdiction if baseAddress.zone.jurisdiction else ""
        longitude = baseAddress.geometry.x
        latitude = baseAddress.geometry.y

        unitAddress = queuedItem.address_history
        addressXParcel = queuedItem.address_x_parcel_history

        context = {
            'address_base_id':              str(baseAddress.address_base_id)
            ,'base_address_prefix':         baseAddress.base_address_prefix if baseAddress.base_address_prefix else ''
            ,'base_address_num':            baseAddress.base_address_num
            ,'base_address_suffix':         baseAddress.base_address_suffix if baseAddress.base_address_suffix else ''
            ,'base_address_history_action': baseAddress.history_action.strip()
            ,'street_name_for_avs':         streetNameForAvs
            ,'street_type_abbreviated':     streetTypeAbbreviated
            ,'street_type_unabbreviated':   streetTypeUnabbreviated
            ,'jurisdiction':                jurisdiction
            ,'longitude':                   str(longitude)
            ,'latitude':                    str(latitude)
            ,'unit_address':                unitAddress
            ,'address_x_parcel':            addressXParcel
        }

        return context

    # I was unable to get @transaction.atomic to work here.
    def run(self):
        sys.stdout.write('polling is running\n')
        from MAD.models.models import XmitQueue
        from django.db import connection, transaction

        pollingInterval = settings.XMIT_POLLING_INTERVAL

        while True:
            try:
                with transaction.atomic():
                    cursor = connection.cursor()
                    XmitQueue.objects.enqueue(cursor)
                    cursor.close()

                with transaction.atomic():
                    queuedItems = XmitQueue.objects.getQueued()
                    if len(queuedItems) == 0:
                        # We want to use the default polling interval here in case polling_interval is set to a high value.
                        logger.info('next poll in %s seconds' % settings.XMIT_POLLING_INTERVAL)
                        connection.close()
                        self.sleep(settings.XMIT_POLLING_INTERVAL)
                        continue

                    # prepare the xml for all items - this will cause reads against the database
                    for queuedItem in queuedItems:
                        context = self.prepareContext(queuedItem)
                        xml = self.buildXml(context)
                        queuedItem.xml = xml

                    # Commit here so we do not get an idle transaction if we have problems with the web server connection below.

                with transaction.atomic():
                    # send the xml for all items
                    for queuedItem in queuedItems:
                        logger.info(queuedItem.xml)
                        self.postXml(queuedItem.xml)
                        queuedItem.xmit_tms = datetime.now()
                        queuedItem.save()

                # All is well; set polling interval to default.
                pollingInterval = settings.XMIT_POLLING_INTERVAL

            except StopIteration:
                connection.close()
                sys.stdout.write('stopping polling...\n')
                break
            except TemplateDoesNotExist as tdne:
                logger.critical(tdne)
                logger.critical('template not found')
                pollingInterval = self.throttleBackPolling(pollingInterval)
                self.sleep(pollingInterval)
            except Exception as e:
                logger.critical(e)
                pollingInterval = self.throttleBackPolling(pollingInterval)
                self.sleep(pollingInterval)
            finally:
                connection.close()

        sys.stdout.write('stopped polling\n')


class Daemon:

    # Implements a daemon interface but is not really a daemon in that it does no forking and remains connected to parent shell.

    def status(self):
        if Semaphore.check(settings.XMIT_RUNNING_SEMAPHORE):
            status = True
            message = 'daemon process is running'
        else:
            status = False
            message = 'daemon process is not running'
        sys.stdout.write(message+'\n')
        return status


    def start(self):
        if Semaphore.check(settings.XMIT_RUNNING_SEMAPHORE):
            sys.stdout.write('daemon process is already running\n')
            return False
        else:
            sys.stdout.write('starting daemon process...\n')
            try:
                Semaphore.create(settings.XMIT_RUNNING_SEMAPHORE)
                runner = Runner()
                runner.run()
                # I told you it's not a real daemon.
            except Exception as e:
                sys.stdout.write(e)
            finally:
                Semaphore.clear(settings.XMIT_RUNNING_SEMAPHORE)


    def stop(self):
        if not Semaphore.check(settings.XMIT_RUNNING_SEMAPHORE):
            sys.stdout.write('daemon process is not running\n')
            return True
        else:
            try:
                # process is running
                sys.stdout.write('stopping daemon process...\n')
                Semaphore.create(settings.XMIT_STOP_REQUEST_SEMAPHORE)
                seconds = settings.XMIT_POLLING_INTERVAL
                for i in range(1, seconds):
                    time.sleep(1)
                    if not Semaphore.check(settings.XMIT_RUNNING_SEMAPHORE):
                        sys.stdout.write('daemon process stopped\n')
                        return True
                sys.stdout.write('stop daemon process request failed\n')
                return False
            except Exception as e:
                sys.stdout.write(e)
            finally:
                Semaphore.clear(settings.XMIT_STOP_REQUEST_SEMAPHORE)


    def restart(self):
        sys.stdout.write('restarting daemon process...\n')
        if not self.stop():
            return False
        else:
            self.start()




def main():

    daemon = Daemon()

    methods = [key for key, value in inspect.getmembers(daemon) if inspect.ismethod(value)]
    usage = 'usage: %s %s\n' % (sys.argv[0], '|'.join(methods))

    if len(sys.argv) != 2:
        sys.stdout.write(usage)
        sys.exit(0)

    if sys.argv[1] not in methods:
        sys.stdout.write(usage)
        sys.exit(0)

    success = eval('daemon.' + sys.argv[1] + '()')


if __name__ == "__main__":
    main()
