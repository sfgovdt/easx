@echo off

set ACTION=%1
set JOB=%2
set ENV=%3

set PYTHON_EXE="C:\sfgis\program\python36\eas_venv\Scripts\python.exe"
set PYTHON_SCRIPT="C:\apps\eas_automation\automation\src\job.py"
set LOG_FILE="C:\apps\eas_automation\app_data\logs\%JOB%_%ACTION%.out"

rem This command line idiom may be better for scheduled tasks.

cmd.exe /C "%PYTHON_EXE% %PYTHON_SCRIPT% --env %ENV% --job %JOB% --action %ACTION% > %LOG_FILE% 2>&1"

rem END OF FILE