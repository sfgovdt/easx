<#

.SYNOPSIS
Restore (create or replace) EAS scheduled tasks that have been previously
exported and deployed to an EAS Automation Machine.

.DESCRIPTION
Please see the synopsis.

.PARAMETER Environment
The target EAS execution environment, which can also vary by data center. Legal
values are "SF_DEV", "SF_QA", "SF_PROD", "DR_DEV", "DR_PROD", and "SF_DEV" is
the default value.

.EXAMPLE
PS> cd C:\apps\eas_automation\automation\bin
PS> Get-Help .\Register-EASScheduledTasks.ps1 -full

Use this invocation to display all of the Help that is available for this
cmdlet.

.EXAMPLE
PS> cd C:\apps\eas_automation\automation\bin
PS> .\Register-EASScheduledTasks -Environment SF_DEV

Entering cmdlet Register-EASScheduledTasks.ps1.

Deleting the EAS scheduled task: run_job EXECUTE export_flat_file_to_ftp SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE export_flat_file_to_ftp SF_DEV

Deleting the EAS scheduled task: run_job EXECUTE eas_addresses_to_sfgis_stg SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE eas_addresses_to_sfgis_stg SF_DEV

Deleting the EAS scheduled task: run_job STAGE streets SF_DEV
Restoring the EAS scheduled task: run_job STAGE streets SF_DEV

Deleting the EAS scheduled task: run_job COMMIT streets SF_DEV
Restoring the EAS scheduled task: run_job COMMIT streets SF_DEV

Deleting the EAS scheduled task: run_job EXECUTE bulkloader_etl SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE bulkloader_etl SF_DEV

Deleting the EAS scheduled task: run_job STAGE parcels SF_DEV
Restoring the EAS scheduled task: run_job STAGE parcels SF_DEV

Deleting the EAS scheduled task: run_job COMMIT parcels SF_DEV
Restoring the EAS scheduled task: run_job COMMIT parcels SF_DEV

Deleting the EAS scheduled task: run_job EXECUTE map_cache_truncate SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE map_cache_truncate SF_DEV

Deleting the EAS scheduled task: run_job EXECUTE map_cache_reseed SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE map_cache_reseed SF_DEV

Deleting the EAS scheduled task: run_job EXECUTE address_change_report SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE address_change_report SF_DEV

Deleting the EAS scheduled task: run_job EXECUTE parcels_provisioning_report SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE parcels_provisioning_report SF_DEV

Deleting the EAS scheduled task: run_job EXECUTE address_change_notification SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE address_change_notification SF_DEV

Deleting the EAS scheduled task: run_job EXECUTE orphaned_address_report SF_DEV
Restoring the EAS scheduled task: run_job EXECUTE orphaned_address_report SF_DEV

All of the EAS scheduled tasks have been restored.
Exiting cmdlet Register-EASScheduledTasks.ps1.

Use this invocation to create or replace EAS scheduled tasks on the EAS
Automation Machine that is in the EAS execution environment that is denoted by
the moniker "SF_DEV".  In this case, the EAS scheduled tasks were replaced.

.EXAMPLE
PS> cd C:\apps\eas_automation\automation\bin
PS> .\Register-EASScheduledTasks -Environment DR_DEV

Entering cmdlet Register-EASScheduledTasks.ps1.

Restoring the EAS scheduled task: run_job EXECUTE export_flat_file_to_ftp DR_DEV

TaskPath                                       TaskName                          State     
--------                                       --------                          -----     
\                                              run_job EXECUTE export_flat_fi... Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job EXECUTE export_flat_file_to_ftp..., TaskPath = "\")

Restoring the EAS scheduled task: run_job EXECUTE eas_addresses_to_sfgis_stg DR_DEV
\                                              run_job EXECUTE eas_addresses_... Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job EXECUTE eas_addresses_to_sfgis_..., TaskPath = "\")

Restoring the EAS scheduled task: run_job STAGE streets DR_DEV
\                                              run_job STAGE streets DR_DEV      Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job STAGE streets DR_DEV", TaskPath = "\")

Restoring the EAS scheduled task: run_job COMMIT streets DR_DEV
\                                              run_job COMMIT streets DR_DEV     Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job COMMIT streets DR_DEV", TaskPath = "\")

Restoring the EAS scheduled task: run_job EXECUTE bulkloader_etl DR_DEV
\                                              run_job EXECUTE bulkloader_etl... Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job EXECUTE bulkloader_etl DR_DEV", TaskPath = "\")

Restoring the EAS scheduled task: run_job STAGE parcels DR_DEV
\                                              run_job STAGE parcels DR_DEV      Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job STAGE parcels DR_DEV", TaskPath = "\")

Restoring the EAS scheduled task: run_job COMMIT parcels DR_DEV
\                                              run_job COMMIT parcels DR_DEV     Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job COMMIT parcels DR_DEV", TaskPath = "\")

Restoring the EAS scheduled task: run_job EXECUTE map_cache_truncate DR_DEV

Restoring the EAS scheduled task: run_job EXECUTE map_cache_reseed DR_DEV

Restoring the EAS scheduled task: run_job EXECUTE address_change_report DR_DEV
\                                              run_job EXECUTE address_change... Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job EXECUTE address_change_report D..., TaskPath = "\")

Restoring the EAS scheduled task: run_job EXECUTE parcels_provisioning_report DR_DEV
\                                              run_job EXECUTE parcels_provis... Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job EXECUTE parcels_provisioning_re..., TaskPath = "\")

Restoring the EAS scheduled task: run_job EXECUTE address_change_notification DR_DEV
\                                              run_job EXECUTE address_change... Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job EXECUTE address_change_notifica..., TaskPath = "\")

Restoring the EAS scheduled task: run_job EXECUTE orphaned_address_report DR_DEV
\                                              run_job EXECUTE orphaned_addre... Disabled  
Disabled the EAS scheduled task: MSFT_ScheduledTask (TaskName = "run_job EXECUTE orphaned_address_report..., TaskPath = "\")

All of the EAS scheduled tasks have been restored.
Exiting cmdlet Register-EASScheduledTasks.ps1.

Use this invocation to create or replace EAS scheduled tasks on the EAS
Automation Machine that is in the EAS execution environment that is denoted by
the moniker "DR_DEV".  In this case, the EAS scheduled tasks were created, and
because this Automation Machine is hosted in the Disaster Recovery Data
Center ("DR_*"), most of the scheduled tasks were disabled.

.INPUTS
The inputs to this cmdlet are exported (as XML files) EAS scheduled tasks that
reside in a well-known directory on the target EAS Automation Machine:
C:\apps\eas_automation\automation\bin

.OUTPUTS
The outputs of this cmdlet are (created or replaced) EAS scheduled tasks that
reside on the target EAS Automation Machine.

.NOTES
This cmdlet must be run as an administrator on the target EAS Automation
Machine.

The value of the "Environment" parameter must match the environment of the
EAS Automation Machine where this cmdlet is being run.

.LINK
https://ccsfdt.atlassian.net/wiki/spaces/EAS/pages/235208761/Developers
https://bitbucket.org/sfgovdt/easx/src/tech-stack-upgrade/automation/bin/Register-EASScheduledTasks.ps1

#>

<# Approved Verbs for PowerShell Commands - Lifecycle Verbs
   https://docs.microsoft.com/en-us/powershell/scripting/developer/cmdlet/approved-verbs-for-windows-powershell-commands?view=powershell-5.1#lifecycle-verbs


   Only one static parameter, and its attributes.

   Static parameters
   https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_functions_advanced_parameters?view=powershell-5.1#static-parameters
#>
param(
  [Parameter(Mandatory)]
  [ValidateSet("SF_DEV", "SF_QA", "SF_PROD", "DR_DEV", "DR_PROD")]
  [string] $Environment = "SF_DEV"
)

<# Constants.
#>
New-Variable `
  -Name ENTRY_MESSAGE `
  -Value "Entering cmdlet Register-EASScheduledTasks.ps1." `
  -Option Constant -Scope Script 

New-Variable `
  -Name EXIT_MESSAGE `
  -Value "Exiting cmdlet Register-EASScheduledTasks.ps1." `
  -Option Constant -Scope Script 

New-Variable `
  -Name SF_DEV_ENVIRONMENT `
  -Value "SF_DEV" `
  -Option Constant -Scope Script 

New-Variable `
  -Name XML_ARGUMENTS_CLOSING_TAG `
  -Value "</Arguments>" `
  -Option Constant -Scope Script 

New-Variable `
  -Name TRUNCATE_MAP_CACHE -Value 7 -Option Constant -Scope Script 

New-Variable `
  -Name RESEED_MAP_CACHE -Value 8 -Option Constant -Scope Script 


<# Variables.
#>
[bool]     $disableThisScheduledTask = $false
[bool]     $drEnvironment = ($Environment.Substring(0,3) -eq "DR_")
[bool]     $targetEasScheduledTaskExists = $false
[psobject] $restoreScheduledTask
[psobject] $scheduledTask
[string]   $easScheduledTaskBaseName
[string[]] $easScheduledTaskBaseNames = `
           @(
            <# The following EAS scheduled tasks are in listed in the order
               in which they are executed, starting at 6:00 PM.
            #>
            "run_job EXECUTE export_flat_file_to_ftp",      #  0
            "run_job EXECUTE eas_addresses_to_sfgis_stg",
            "run_job STAGE streets",
            "run_job COMMIT streets",
            "run_job EXECUTE bulkloader_etl",
            "run_job STAGE parcels",
            "run_job COMMIT parcels",
            "run_job EXECUTE map_cache_truncate",           #  7
            "run_job EXECUTE map_cache_reseed",             #  8
            "run_job EXECUTE address_change_report",
            "run_job EXECUTE parcels_provisioning_report",
            "run_job EXECUTE address_change_notification",
            "run_job EXECUTE orphaned_address_report"       # 12
            )
[string]   $easScheduledTaskName
[string]   $easScheduledTaskPassword = "Automaton.01"
[string]   $easScheduledTaskUser
[string]   $exportedEasScheduledTaskFullPath
[string]   $exportedEasScheduledTaskName
[string]   $exportedScheduledTaskXml
[string]   $fullPathFormat = "C:\apps\eas_automation\automation\bin\{0}.xml"
[string]   $fullUserFormat = "{0}\eas_automation"
[string]   $replacedEnvironment
[string]   $replaceWithEnvironment
[string]   $targetEasScheduledTaskName
[string]   $thisHostName = [System.Net.Dns]::GetHostName()
[string]   $tmpStr


<# Set-StrictMode
   https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/set-strictmode?view=powershell-5.1
#>
Set-StrictMode -Version Latest

$ErrorActionPreference = "SilentlyContinue"
# $ErrorActionPreference = "Continue"

Write-Host
Write-Host $ENTRY_MESSAGE
Write-Host

$easScheduledTaskUser = $fullUserFormat -f $thisHostName
<# Use the next two lines if you are developing on a machine other than the
   target machine; the next two assignments are normally commented out.
#>
# $easScheduledTaskUser = "AD\jane.doe"
# $easScheduledTaskPassword = "xxx"

$replacedEnvironment = $SF_DEV_ENVIRONMENT + $XML_ARGUMENTS_CLOSING_TAG
$replaceWithEnvironment = $Environment + $XML_ARGUMENTS_CLOSING_TAG

foreach ($easScheduledTaskBaseName in $easScheduledTaskBaseNames)
{
  $targetEasScheduledTaskName = "$easScheduledTaskBaseName $Environment"

  <# Get-ScheduledTask
     https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/get-scheduledtask?view=win10-ps
  #>
  $scheduledTask = Get-ScheduledTask -TaskName $targetEasScheduledTaskName
  $targetEasScheduledTaskExists = -not($null -eq $scheduledTask)
  if ($targetEasScheduledTaskExists) {
    <# Delete it before restoring the latest one.

       Unregister-ScheduledTask
       https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/unregister-scheduledtask?view=win10-ps
    #>
    $tmpStr = `
      "Deleting the EAS scheduled task: " + $targetEasScheduledTaskName
    Write-Host $tmpStr
    Unregister-ScheduledTask `
      -TaskName $targetEasScheduledTaskName -Confirm:$false
  }

  $tmpStr = `
    "Restoring the EAS scheduled task: " + $targetEasScheduledTaskName
  Write-Host $tmpStr

  <# Use the next line if you are developing on a machine other than the
     target machine; the next assignment is normally commented out.
  #>
  # $fullPathFormat = "C:\SfGis\Bitbucket\easx\automation\bin\{0}.xml"

  <# The EAS' scheduled tasks were exported as XML from the Windows Task
     Scheduler GUI that is in the development environment that is in the
     San Francisco Data Center, so the last part of the scheduled task's
     name can only be one thing.
  #>
  $exportedEasScheduledTaskName = `
    "$easScheduledTaskBaseName " + $SF_DEV_ENVIRONMENT

  $exportedEasScheduledTaskFullPath = `
    $fullPathFormat -f $exportedEasScheduledTaskName

  $exportedScheduledTaskXml = `
    (Get-Content $exportedEasScheduledTaskFullPath | Out-String)

  <# The exported XML which came from the development environment that is in
     the San Francisco Data Center *also* contains an argument for that
     environment, so we need to check for that, and sometimes fix it.
  #>
  if ($Environment -ne $SF_DEV_ENVIRONMENT) {

    $exportedScheduledTaskXml = `
      $exportedScheduledTaskXml.Replace( `
        $replacedEnvironment, $replaceWithEnvironment)
  }

  $restoreScheduledTask = `
    Register-ScheduledTask `
      -Xml $exportedScheduledTaskXml `
      -TaskName $targetEasScheduledTaskName `
      -User $easScheduledTaskUser `
      -Password $easScheduledTaskPassword `
      -Force

  <# Most of these scheduled jobs will not be running in the disaster recovery
     environments.
  #>
  $disableThisScheduledTask = `
    ($drEnvironment `
    -and ($easScheduledTaskBaseName `
          -ne $easScheduledTaskBaseNames[$TRUNCATE_MAP_CACHE]) `
    -and ($easScheduledTaskBaseName `
          -ne $easScheduledTaskBaseNames[$RESEED_MAP_CACHE]))

  if ($disableThisScheduledTask) {

    <# Disable-ScheduledTask
      https://docs.microsoft.com/en-us/powershell/module/scheduledtasks/disable-scheduledtask?view=win10-ps
    #>
    Disable-ScheduledTask -TaskName $targetEasScheduledTaskName

    $tmpStr = "Disabled the EAS scheduled task: " + $restoreScheduledTask
    Write-Host $tmpStr
  }
  Write-Host

}

$tmpStr = "All of the EAS scheduled tasks have been restored."
Write-Host $tmpStr
Write-Host $EXIT_MESSAGE
Write-Host
Exit


# END OF CMDLET
