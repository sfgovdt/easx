import sys
import os
import subprocess
import copy
import shutil
import requests
import tarfile


def usage():
    print('usage: %s localTargetPath' % sys.argv[0])
    exit(1)


def initVersionControlParms(
        description, versionControlDict, versionControlDictOrder):
    import getpass
    message = '\nPlease enter values for %s' % description
    print(message)
    print('-' * len(message))
    for k in versionControlDictOrder:
        if k == 'PASSWORD':

            # Apparently, the following function prompts the user with the
            # string "Password", which some may find annoying because the
            # other prompts are all upper-case.

            capturedValue = getpass.getpass()
        else:
            sys.stdout.write(
                '%s (default = "%s"):' % (k, versionControlDict[k]))
            capturedValue = input()
        if capturedValue:
            versionControlDict[k] = capturedValue
    print('\n')


def bitbucketExport(targetPath, team, project, branch, user, password):

    # Create deployment workaround for TLS 1.2 compatibility problem
    # https://bitbucket.org/sfgovdt/eas/issues/261
    #
    # All of the "debugging print points" are commented out with three
    # comment characters (# ###), and the "original" code (before the EAS-261
    # workaround) is commented out with a single comment character (# ).  The
    # idea is that we may want to easily test, or back-out, the workarounds
    # later.

    # ###print "In the function bitbucketExport()"
    # ###print "  targetPath      = " + targetPath
    url = \
        'https://%s:%s@bitbucket.org/%s/%s/get/%s.tar.gz' \
        % (user, password, team, project, branch)  # pre-EAS-261
    # tarFileName = '%s-%s-%s.tar.gz' % (team, project, branch)  # post-EAS-261
    # tarFileName = '%s-%s-%s' % (team, project, branch)  # pre-EAS-261
    # ###print "  tarFileName     = " + tarFileName
    # tarFileFullPath = os.path.join(targetPath, tarFileName)
    # ###print "  tarFileFullPath = " + tarFileFullPath
    # ###print
    # ###sys.exit()
    tfile = tarfile.open(
        mode="r:gz", fileobj=requests.get(url, stream=True).raw)  # pre-EAS-261
    # tfile = tarfile.open(name=tarFileFullPath, mode="r:gz")  # post-EAS-261
    tfile.extractall(targetPath)
    tempSrcFolder = None
    for dirItemName in os.listdir(targetPath):
        # ###print "  dirItemName     = " + dirItemName
        expectedDirPrefix = '%s-%s' % (team, project)
        if expectedDirPrefix in dirItemName:

            # We only care about one item in this directory, and it has to
            # have the right name prefix, and ...

            dirItemPath = os.path.join(targetPath, dirItemName)
            # ###print "  dirItemPath     = " + dirItemPath
            dirItemIsDirectory = os.path.isdir(dirItemPath)

            if dirItemIsDirectory:
                # ... it has to be a directory.
                tempSrcFolder = os.path.join(targetPath, dirItemName)
                # ###print "  tempSrcFolder   = " + tempSrcFolder
                break

    if tempSrcFolder is None:
        raise Exception('Unable to locate an extracted repository archive.')
    else:
        return tempSrcFolder


def main():
    if len(sys.argv) < 2:
        usage()
    elif len(sys.argv) == 2:
        LOCAL_TARGET_PATH = sys.argv[1]
    else:
        usage()

    EAS_BRANCH = 'EAS_BRANCH'
    CONFIG_BRANCH = 'CONFIG_BRANCH'

    bitbucketParms = \
        {
            'USER': None,
            'PASSWORD': None,
            'TEAM': 'sfgovdt',
            EAS_BRANCH: 'master',
            CONFIG_BRANCH: 'master'
        }

    # Use this list to mimic an ordered dictionary as this was originally
    # implemented in Python 2.5 on the Automation Machine (SF * AUTO).
    bitbucketParmsOrder = \
        ['USER', 'PASSWORD', 'TEAM', EAS_BRANCH, CONFIG_BRANCH]
    initVersionControlParms('BITBUCKET', bitbucketParms, bitbucketParmsOrder)

    print("Extracting the " + EAS_BRANCH + " repository archive")
    easTempPath = \
        bitbucketExport(
            os.path.join(LOCAL_TARGET_PATH, 'temp'),
            bitbucketParms['TEAM'],
            'easx',
            bitbucketParms[EAS_BRANCH],
            bitbucketParms['USER'],
            bitbucketParms['PASSWORD']
        )

    # ###print "easTempPath    = " + easTempPath

    print("Extracting the " + CONFIG_BRANCH + " repository archive")
    configTempPath = \
        bitbucketExport(
            os.path.join(LOCAL_TARGET_PATH, 'temp'),
            bitbucketParms['TEAM'],
            'easconfigxsf',
            bitbucketParms[CONFIG_BRANCH],
            bitbucketParms['USER'],
            bitbucketParms['PASSWORD']
        )
    # ###print "configTempPath = " + configTempPath
    # ###sys.exit()

    print("Deploying code")
    shutil.move(
        os.path.join(easTempPath, 'database'),
        os.path.join(LOCAL_TARGET_PATH, 'database')
    )
    shutil.move(
        os.path.join(easTempPath, 'automation'),
        os.path.join(LOCAL_TARGET_PATH, 'automation')
    )
    shutil.move(
        os.path.join(configTempPath, 'automation/fme_workspaces/live'),
        os.path.join(LOCAL_TARGET_PATH, 'automation/fme_workspaces/live')
    )
    shutil.move(
        os.path.join(configTempPath, 'automation/src/config/live'),
        os.path.join(LOCAL_TARGET_PATH, 'automation/src/config/live')
    )
    shutil.move(
        os.path.join(configTempPath, 'automation/src/test_jobs.bat'),
        os.path.join(LOCAL_TARGET_PATH, 'automation/src/')
    )

    # To replace the EAS PuTTY sessions Registry file, if it exists from a
    # previous deployment.
    #
    #   11.10.1. Directory and files operations
    #   https://docs.python.org/3.6/library/shutil.html#directory-and-files-operations

    shutil.copyfile(
        os.path.join(configTempPath, 'automation/eas_all_autox.reg'),
        os.path.join(LOCAL_TARGET_PATH, 'app_data/ssh/eas_all_autox.reg')
    )

    print("Cleaning up")

    # TODO: -17 Dec 2020- The information at this URL may help resolve the
    #       intermittent issue of rmtree() not always doing its job.  This may
    #       also be resolved by running the Command Prompt as administrator on
    #       the target Automation Machine, but this needs to be confirmed.
    #
    # https://stackoverflow.com/questions/303200/how-do-i-remove-delete-a-folder-that-is-not-empty
    shutil.rmtree(os.path.join(LOCAL_TARGET_PATH, 'temp'))
    print("Done")


if __name__ == "__main__":
    main()


# END OF FILE
