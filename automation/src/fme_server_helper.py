
import http.client
import json
import time
import urllib.error
import urllib.parse
import urllib.request
from dateutil import parser


class FmeServerHelper:

    def __init__(self, host=None, user=None, password=None):
        self.host = host
        self.user = user
        self.password = password
        self.token = None
        parameters = {
            'user' : user,
            'password' : password,
            'expiration' : '10',
            'timeunit' : 'minute'
        }
        url = '/fmetoken/generate?'
        self.token = self.getResponse(url, parameters)

    @staticmethod
    def getJobId(job=None):
        if job is None:
            return None
        return job['id']

    @staticmethod
    def getJobStatus(job=None):
        if job is None:
            return None
        return job['status']

    def getResponse(self, url=None, parameters=None):
        if parameters is None:
            parameters = {}
        headers = {
            'Content-Type' : 'application/json',
            'Accept' : 'application/json',
        }
        if self.token:
            headers["Authorization"] = 'fmetoken token={0}'.format(self.token.decode("ascii"))
        contentString = ""
        https_connection = http.client.HTTPSConnection(self.host)
        try:
            encodedParameters = urllib.parse.urlencode(parameters)
            https_connection.request("GET", url + encodedParameters, headers=headers)
            response = https_connection.getresponse()
            if response.status != 200:
                raise IOError("Request failed - status: %s - message: %s" % \
                    (str(response.status), str(response.msg)))
            contentString = response.read()
        except Exception as e:
            raise e
        finally:
            https_connection.close()
        return contentString

    def doesJobExist(self, jobDict): 
        url = '/fmerest/v3/repositories/%s/items?' % ( jobDict['repository'] )
        response = json.loads(self.getResponse(url=url, parameters={"type": "WORKSPACE"}))
        workspaces = response['items']
        exists = False
        for workspace in workspaces:
            if (workspace['name'] == '%s.fmw' % ( jobDict['workspace'] )):
                exists = True
        return exists

    def isJobRunning(self, jobDict):
        url = '/fmerest/v3/transformations/jobs/running?'
        return len(self.getJobs(url, jobDict)) > 0

    def isJobQueued(self, jobDict):
        url = '/fmerest/v3/transformations/jobs/queued?'
        return len(self.getJobs(url, jobDict)) > 0

    def getLatestCompletedJob(self, jobDict):
        lastJob = None
        url = '/fmerest/v3/transformations/jobs/completed?'
        jobList = self.getJobs(url, jobDict)

        for job in jobList:
            jobDate = parser.parse(job['timeFinished'])
            if lastJob is None:
                lastJob = job
            else:
                lastJobDate = parser.parse(lastJob['timeFinished'])
                if jobDate > lastJobDate:
                    lastJob = job

        return lastJob

    def getJobs(self, url, jobDict):
        jobs = []
        parameters = {
            "limit": -1,
            "offset": -1,
        }
        response = json.loads(self.getResponse(url=url,parameters=parameters))
        allJobs = response['items']
        for job in allJobs:
            workspacePath = job['request']['workspacePath'][1:-1] # Remove the quotes.
            if workspacePath == '%s/%s/%s.fmw' % ( jobDict['repository'], jobDict['workspace'], jobDict['workspace'] ):
                jobs.append(job)
        return jobs


# END OF FILE
