"""This file can be run as python tests.py"""

import csv
import io
import os
import unittest
from pathlib import Path
from unittest.mock import patch

from config.live import SF_DEV_ENV
from config.live.config_jobs import JobOrganizer

TEST_DATA_DIR = Path(__file__).parent / "testdata" / "data"
TEST_SQL_DIR = Path(__file__).parent / "testdata" / "sql"


class AutomationTest(unittest.TestCase):
    @classmethod
    @patch("config.live.config_jobs.DATA_DIR", str(TEST_DATA_DIR))
    @patch("config.live.config_jobs.SQL_DIR", str(TEST_SQL_DIR))
    def setUpClass(cls):
        cls.organizer = JobOrganizer()


class StageBulkloadCsvTest(AutomationTest):
    command_name = "stage_bulkload_csv"

    @patch("commands.SqlFileCommand.execute", lambda noop: None)
    def test_sql_file_generated(self):
        job = self.organizer.getJob(self.command_name, env=SF_DEV_ENV)
        path_to_sql_file = TEST_DATA_DIR / "bulkload_data.sql"
        self.addCleanup(os.remove, path_to_sql_file)

        job.execute()

        count = 0
        with open(path_to_sql_file, newline="") as csvfile:
            for row in csv.reader(csvfile):
                count += 1

        self.assertEqual(count, 185)
        # Check last row.
        self.assertEqual(
            ",".join(row),
            "INSERT INTO bulkloader.address_extract (block,lot,unit,unit_sfx,street_number,street_number_sfx,street_name,street_sfx,street_type,load_source,geometry) VALUES ('','','907','','150','','LOMBARD','','ST','EAS-438',NULL);",
        )


if __name__ == "__main__":
    unittest.main(buffer=io.StringIO())
