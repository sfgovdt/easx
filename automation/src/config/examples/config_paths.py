
# Note:
# I found that if we try to execute plink, and the ppk specified in the putty config file is on a mapped drive,
# the execute can fail because the drive may not be mounted.  The error message may be misleading:
#        Is the server running on host "localhost" and accepting
#        TCP/IP connections on port 5433?
# You'd think that Plink would cause the drive to be mounted but it does not.
# There does not appear to be a simple, platform independent way, to coax the OS into mounting the drive.
# Therefore, we should probably not put things on mounted drives.

# todo - before we go into production we need:
#   - security review
#   - do not rely on mounted drives being available, especially for plink (see above)  

import os

def getPlinkExePath():
    return r'c:/program files/putty/plink.exe'

def getFmeExePath():
    return r'c:/program files/FME/fme.exe'

def getFmeWorkspacePath():
    return r'C:/Program Files/MAD/trunk/etl/fme_workspaces/secure/'

def getFmeOutputPath():
    return r'C:/Documents and Settings/mad_etl/Application Data/mad_etl/'

def getPpkPath():
    return r'C:/Documents and Settings/mad_etl/Application Data/ssh/'

def getPgDumpOutputPath():
    return r'/mnt/etl_data/'

def getDataFilePath():
    return r'C:/Documents and Settings/mad_etl/Application Data/mad_etl/data/'

def getSqlFilePath():
    return r'C:/Program Files/MAD/trunk/etl/sql/'
