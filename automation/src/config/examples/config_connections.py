import db
from db import *
from db_ssh import *
from ssh import *

class ConnectionConfigurations:
    # This class should contain only connection data and no business logic.

    def __init__(self, connectionOrganizer):

        ###### The single place to define ssh connections.
        # todo - review and improve design
        # In essence, because we are handing out instances, we are using the singleton pattern here.
        # But it's not very clean in that we sometimes ask the singleton to connect more than once, and to disconnect more than once.
        # At least we should make this more clear/explicit in the design.
        self.sshConnDefs = {}
        self.sshConnDefs['DEV'] = TunnelingConnectionPutty(sessionName = 'putty_session_name', port = "ssh_port")
        self.sshConnDefs['QA'] = TunnelingConnectionPutty(sessionName = 'putty_session_name', port = "ssh_port")
        self.sshConnDefs['PROD'] = TunnelingConnectionPutty(sessionName = 'putty_session_name', port = "ssh_port")

        self.sshConnNullDefs = {}
        self.sshConnNullDefs['DEV'] = TunnelingConnectionNull()
        self.sshConnNullDefs['QA'] = TunnelingConnectionNull()
        self.sshConnNullDefs['PROD'] = TunnelingConnectionNull()
        #####


        ##### The single place to define smtp connections.
        # We do not use the port number; it is included here for completeness.
        # Note that the host from which this code is running should be added to te smtp host's "relay list".
        self.smtpConnections = {}
        self.smtpConnections['DEV'] =  {'host' : 'host_name', 'port': 0}
        self.smtpConnections['QA'] = self.smtpConnections['DEV']
        self.smtpConnections['PROD'] = self.smtpConnections['DEV']
        connectionOrganizer.smtpConnections = self.smtpConnections
        #####


        #####
        # The single place to define geoserver connections
        # In case you are wondering about some inconsistencies here...
        # as I learn more about python I find myself using fewer java style constructs.
        # So here I use dictionaries rather than classes.
        geoserverConnections = {}
        geoserverParms = {'host' : 'host', 'port': 'port_number', 'user': 'user', 'password': 'password', 'seedPath': '/geoserver/gwc/rest/seed'}
        geoserverConnections['DEV'] = {'GEOSERVER': geoserverParms, 'SSH': self.sshConnDefs['DEV']}
        geoserverParms = geoserverParms.copy()
        geoserverParms['port']='port_number'
        geoserverConnections['QA'] = {'GEOSERVER': geoserverParms, 'SSH': self.sshConnDefs['QA']}
        geoserverParms = geoserverParms.copy()
        geoserverParms['port']='port_number'
        geoserverConnections['PROD'] = {'GEOSERVER': geoserverParms, 'SSH': self.sshConnDefs['PROD']}
        connectionOrganizer.geoserverConnections = geoserverConnections
        #####


        ##### The single place to define db connections

        # db2 sde example
        db2SdeExample = {}
        # CAUTION - next line is reused well below here.
        db2SdeExample['DEV'] = DbConnectionDefinition(type=db.TYPES.DB2, spatialType=db.SPATIAL_TYPES.SDE, host='host_name', port=None, sdePort='sde_port', db='SDE', user='user', password='password', version='SDE.DEFAULT')
        db2SdeExample['QA'] = db2SdeExample['DEV'].copy()
        db2SdeExample['PROD'] = db2SdeExample['DEV'].copy()
        connectionOrganizer.addConnectionGroup(groupName='DB2_SDE_EXAMPLE', dbConnGroup=db2SdeExample, sshConnDefs=self.sshConnNullDefs)

        # db2 example
        db2Example = {}
        # CAUTION - next line is reused well below here.
        db2Example['DEV'] = DbConnectionDefinition(type=db.TYPES.DB2, host='host', port='port', sdePort='port_number', db='db', user='user', password='password')
        db2Example['QA'] = db2Example['DEV'].copy()
        db2Example['PROD'] = db2Example['DEV'].copy()
        connectionOrganizer.addConnectionGroup(groupName='DB2_EXAMPLE', dbConnGroup=db2Example, sshConnDefs=self.sshConnNullDefs)

        # mssql sde example
        mssqlSdeExample = {}
        mssqlSdeExample['DEV'] = DbConnectionDefinition(type=db.TYPES.MSSQL, spatialType=db.SPATIAL_TYPES.SDE, host="host", port=None, sdePort='port', db="db", user="user", password="password", version="SDE.DEFAULT")
        mssqlSdeExample['QA'] = mssqlSdeExample['DEV'].copy()
        mssqlSdeExample['PROD'] = mssqlSdeExample['DEV'].copy()
        connectionOrganizer.addConnectionGroup(groupName='MSSQL_SDE_EXAMPLE', dbConnGroup=mssqlSdeExample, sshConnDefs=self.sshConnNullDefs)

        # mssql example
        mssqlExample = {}
        mssqlExample['DEV'] = DbConnectionDefinition(type=db.TYPES.MSSQL, spatialType=None, host="host", port='port', sdePort=None, db="db", user="user", password="password", version=None)
        mssqlExample['QA'] = mssqlExample['DEV'].copy()
        mssqlExample['PROD'] = mssqlExample['DEV'].copy()
        connectionOrganizer.addConnectionGroup(groupName='MSSQL_EXAMPLE', dbConnGroup=mssqlExample, sshConnDefs=self.sshConnNullDefs)

        # postgresql example
        pgsqlExample = {}
        pgsqlExample['DEV'] = DbConnectionDefinition(type=db.TYPES.POSTGRESQL, spatialType=db.SPATIAL_TYPES.POSTGIS, host="host", port="port", sdePort=None, db="db", user="user", password="user", version=None)
        pgsqlExample['QA'] = pgsqlExample['DEV'].copy(port="port")
        pgsqlExample['PROD'] = pgsqlExample['DEV'].copy(port="port")
        connectionOrganizer.addConnectionGroup(groupName='PGSQL_EXAMPLE', dbConnGroup=pgsqlExample, sshConnDefs=self.sshConnDefs)

        # oracle example
        oracleExample = {}
        oracleExample['DEV'] = DbConnectionDefinition(type=db.TYPES.ORACLE, spatialType=db.SPATIAL_TYPES.ORACLE, host="host", port="port", sdePort=None, db="db", user="user", password="password", version=None)
        oracleExample['QA'] = oracleExample['DEV'].copy()
        oracleExample['PROD'] = oracleExample['DEV'].copy()
        connectionOrganizer.addConnectionGroup(groupName='ORACLE_EXAMPLE', dbConnGroup=oracleExample, sshConnDefs=self.sshConnNullDefs)
        #####

