
class NotificationsOrganizer:

    def __init__(self):

        fromEmail = 'noreply@sfgov.org'

        paulEmail = 'paul.mccullough@sfgov.org'
        samEmail = 'sam.valdez@sfgov.org'
        richardEmail = 'richard.isen@sfgov.org'
        jeffEmail = 'jeffrey.johnson@sfgov.org'

        supportEmails = [
            paulEmail,
            jeffEmail
        ]

        self.notifications = {}
        self.notifications['DEV'] = { 'TO': (paulEmail), 'FROM' : fromEmail }
        self.notifications['QA'] = { 'TO': supportEmails, 'FROM' : fromEmail }
        self.notifications['PROD'] = { 'TO': supportEmails, 'FROM' : fromEmail }

    def getToEmailList(self, env):
        return self.notifications[env]['TO']

    def getFromEmail(self, env):
        return self.notifications[env]['FROM']

