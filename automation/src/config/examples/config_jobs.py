
from job import *
from connection_organizer import *
from subprocess import *
from sqlalchemy.sql import text
from .config_paths import *

DATA_DIR = getDataFilePath()
SQL_DIR = getSqlFilePath()


class JobOrganizer:

    # This class intended to be a configuration file that should contain little to no logic.

    # todo - improve encapsulation of tableMappings
    # todo - consider moving these commands into the command processor  
    #    StagingStartCommand
    #    StagingCompleteCommand
    #    CommitStartXaCommand
    #    CommitCompleteXaCommand

    # How to Test the STAGE/COMMIT
    # 1) truncate staging table
    # 2) truncate final target table if there are no constraints
    # 3) preview map layer with geoserver admin console; you should see no layer
    # 4) run STAGE; check rows count if appropriate
    # 5) run commit; check rows count if appropriate
    # 6) preview map layer with geoserver admin console; you should see the data now
    #
    # bay_area_cities
    # county_line
    # coast
    # ocean_bay_labels
    # poi
    # streets
    # parcels

    # todo - How to test EXECUTE

    def __init__(self):

        self.jobs = Jobs()
        connectionOrganizer = ConnectionOrganizer()
        self.connectionGroups = connectionOrganizer.connectionGroups

        # Rob's test job for work on issue #769
        job = Job(name = 'rob_test_job')
        job.commands = [
            RobTestCommand()
        ]
        self.jobs.addJob(job)


        ##### cache seeding
        # seed: do not seed 19 and 20 because it takes too long and because http://sfgovdt.jira.com/browse/MAD-60
        # etl reseed: we must reseed all levels
        # truncate: not implemented in our version of gwc

        # default args
        mapCacheCommandArgs = {'connectionGroup': self.connectionGroups['MAD'], 'geoserverConnections': connectionOrganizer.geoserverConnections, 'layerName': 'layer-group_map', 'mimeType': 'image/png', 'zoomStart': 12, 'zoomStop': 18, 'pollingIntervalInSeconds': None}

        # test args
        #mapCacheCommandArgs['zoomStart'] = 12
        #mapCacheCommandArgs['zoomStop'] = 13
        #mapCacheCommandArgs['pollingIntervalInSeconds'] = 15


        job = Job(name = 'map_cache_etl_reseed')
        mapCacheCommandArgs['job'] = job
        # The etl reseed uses bboxes from the etl_bbox table and must work on all levels
        job.commands = [
            MapCacheEtlReseedCommand(**mapCacheCommandArgs)
        ]
        self.jobs.addJob(job)


        # include just the city - no need to cache marin, or the bay, etc
        bbox = {'xmin': -13638689.0, 'xmax': -13620153.0, 'ymin': 4537935.0, 'ymax': 4553222.0}


        # reseed writes tiles existing or not
        job = Job(name = 'map_cache_reseed')
        mapCacheCommandArgs['job'] = job
        mapCacheCommandArgs['cacheCommandType'] = 'reseed'
        mapCacheCommandArgs['bbox'] = bbox
        job.commands = [
            MapCacheCommand(**mapCacheCommandArgs)
        ]
        self.jobs.addJob(job)


        # seed writes tiles that do not exist
        job = Job(name = 'map_cache_seed')
        mapCacheCommandArgs['job'] = job
        mapCacheCommandArgs['cacheCommandType'] = 'seed'
        mapCacheCommandArgs['bbox'] = bbox
        job.commands = [
            MapCacheCommand(**mapCacheCommandArgs)
        ]
        self.jobs.addJob(job)


        ## Truncate is not implemented in our version of gwc - http://sfgovdt.jira.com/browse/MAD-63
        ## Truncate invalidates tiles so that next request will regenerate. Presumably this acts only on the GWC H2 database.
        #job = Job(name = 'map_cache_truncate')
        #mapCacheCommandArgs['job'] = job
        #mapCacheCommandArgs['cacheCommandType'] = 'truncate'
        #mapCacheCommandArgs['bbox'] = bbox
        #job.commands = [
        #        MapCacheCommand(**mapCacheCommandArgs)
        #]
        #self.jobs.addJob(job)
        ######




        #
        job = Job(name = 'streets')
        job.setCommitConnGroups(connectionGroups = self.connectionGroups, connGroupKeys = ['SFMAPS', 'MAD'])
        tableMappings = {}
        tableMappings['MAD'] = TableMapping (
            connectionGroup = self.connectionGroups['MAD'],
            tableMappingItems = (
                TableMappingItem(stagingTable='streetnames_staging', targetTable='streetnames', copyOnCommit=False),
                TableMappingItem(stagingTable='street_segments_staging', targetTable='street_segments', copyOnCommit=False),
            )
        )
        tableMappings['SFMAPS'] = TableMapping (
            connectionGroup = self.connectionGroups['SFMAPS'],
            tableMappingItems = (
            TableMappingItem(stagingTable='basemap_stclines_arterials_staging', targetTable='basemap_stclines_arterials', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_stclines_city_staging', targetTable='basemap_stclines_city', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_stclines_freeway_staging', targetTable='basemap_stclines_freeway', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_stclines_highway_staging', targetTable='basemap_stclines_highway', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_stclines_major_staging', targetTable='basemap_stclines_major', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_stclines_minor_staging', targetTable='basemap_stclines_minor', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_stclines_non_paper_staging', targetTable='basemap_stclines_non_paper', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_stclines_other_staging', targetTable='basemap_stclines_other', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_stclines_ramp_staging', targetTable='basemap_stclines_ramp', copyOnCommit=True),
            )
        )
        job.initCommands = [InitDbCommand(job=job, tableMappings=tableMappings)]
        job.stagingCommands = [
            AreStreetUpdatesNeededCommand(job=job, connectionGroups=(self.connectionGroups['MAD'], self.connectionGroups['DPW'], )),
            StagingStartCommand(job=job, tableMappings=tableMappings),
            # origins to SFGIS staging
            FmeCommand(job=job, fmw="streets_source_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['DPW_SDE'], self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['DPW'],), useSdePorts=(True, True, False,)),
            # do work in SFGIS staging
            FmeCommand(job=job, fmw="streets_sfgisStg_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFGIS_STG_SDE'],), useSdePorts=(True, True,)),
            # copy to MAD staging
            PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['MAD'], tableNames=tableMappings['MAD'].getStagingTableNames()),
            PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['SFMAPS'], tableNames=tableMappings['SFMAPS'].getStagingTableNames()),
            FmeCommand(job=job, fmw="streets_sfgisStg_to_target.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['MAD'], self.connectionGroups['SFMAPS'],), useSdePorts=(True, False, False,)),
            StagingCompleteCommand(job=job, tableMappings=tableMappings)
        ]
        job.commitCommands = [
            GetJobIdXaCommand(job=job, tableMappings=tableMappings),
            BeginTransactionXaCommand(job=job),
            #AreDatabasesBackedUpXaCommand(job=job),
            CopyTablesXaCommand(job=job, tableMappings=tableMappings),
            ProcessStreetsXaCommand(job=job, connectionKey='MAD'),
            SetJobCommitedXaCommand(job=job, tableMappings=tableMappings),
            EndTransactionXaCommand(job=job),
            SendEmailXaCommand(job=job)
        ]
        self.jobs.addJob(job)




        #    PostgresPreFmeCommand(job=job, tableMappings=tableMappings),
        job = Job(name = 'parcels')
        job.setCommitConnGroups(connectionGroups = self.connectionGroups, connGroupKeys = ['SFMAPS', 'MAD'])
        tableMappings = {}
        tableMappings['MAD'] = TableMapping (
            connectionGroup = self.connectionGroups['MAD'],
            tableMappingItems = (
                TableMappingItem(stagingTable='parcels_staging', targetTable='parcels', copyOnCommit=False),
            )
        )
        tableMappings['SFMAPS'] = TableMapping (
            connectionGroup = self.connectionGroups['SFMAPS'],
            tableMappingItems = (
            TableMappingItem(stagingTable='basemap_citylots_staging', targetTable='basemap_citylots', copyOnCommit=True),
            TableMappingItem(stagingTable='basemap_citylots_base_staging', targetTable='basemap_citylots_base', copyOnCommit=True),
            )
        )
        job.initCommands = [InitDbCommand(job=job, tableMappings=tableMappings)]
        job.stagingCommands = [
                # AreParcelUpdatesNeededCommand(job=job, connectionGroups=(self.connectionGroups['MAD'], self.connectionGroups['DPW'], )),
                
                StagingStartCommand(job=job, tableMappings=tableMappings),
                # load data into SFGIS staging
                FmeCommand(job=job, fmw="parcels_source_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['DPW_SDE'], self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['ASSESSOR'], self.connectionGroups['DPW'],), useSdePorts=(True, True, False, False,)),
                # work within SFGIS staging
                FmeCommand(job=job, fmw="parcels_sfgisStg_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], ), useSdePorts=(True,)),
                # copy to MAD staging
                PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['MAD'], tableNames=tableMappings['MAD'].getStagingTableNames()),
                PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['SFMAPS'], tableNames=tableMappings['SFMAPS'].getStagingTableNames()),
                FmeCommand(job=job, fmw="parcels_sfgisStg_to_target.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['MAD'], self.connectionGroups['SFMAPS']), useSdePorts=(True, False, False,)),
                # copy to SFGIS system of record
                StagingCompleteCommand(job=job, tableMappings = tableMappings)
        ]
        job.commitCommands = [
            GetJobIdXaCommand(job=job, tableMappings=tableMappings),
            BeginTransactionXaCommand(job=job),
            #AreDatabasesBackedUpXaCommand(job=job),
            CopyTablesXaCommand(job=job, tableMappings=tableMappings),
            ProcessParcelsXaCommand(job=job, connectionKey='MAD'),
            SetJobCommitedXaCommand(job=job, tableMappings=tableMappings),
            EndTransactionXaCommand(job=job),
            SendEmailXaCommand(job=job)
        ]
        self.jobs.addJob(job)




        job = Job(name = 'poi')
        job.setCommitConnGroups(connectionGroups = self.connectionGroups, connGroupKeys = ['SFMAPS'])
        tableMappings = self.buildTableMappingsSingle(connGroupName='SFMAPS', stagingTable='basemap_poi_staging', targetTable='basemap_poi', copyOnCommit=True)
        job.initCommands = [InitDbCommand(job=job, tableMappings=tableMappings)]
        job.stagingCommands = [
            StagingStartCommand(job=job, tableMappings=tableMappings),
            #FmeCommand(job=job, fmw="poi_sfgisStg_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFGIS_STG_SDE'], ), useSdePorts=(True, True,)),
            #PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['SFMAPS'], tableNames=tableMappings['SFMAPS'].getStagingTableNames()),
            #FmeCommand(job=job, fmw="poi_sfgisStg_to_sfmaps.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFMAPS'], ), useSdePorts=(True, False,)),
            StagingCompleteCommand(job=job, tableMappings=tableMappings)
        ]
        job.commitCommands = self.buildCommitCommandsSingle(job=job, tableMappings=tableMappings)
        self.jobs.addJob(job)




        job = Job(name = 'ocean_bay_labels')
        job.setCommitConnGroups(connectionGroups = self.connectionGroups, connGroupKeys = ['SFMAPS'])
        tableMappings = self.buildTableMappingsSingle(connGroupName='SFMAPS', stagingTable='basemap_ocean_bay_labels_staging', targetTable='basemap_ocean_bay_labels', copyOnCommit=True)
        job.initCommands = [InitDbCommand(job=job, tableMappings=tableMappings)]
        job.stagingCommands = [
            StagingStartCommand(job=job, tableMappings=tableMappings),
            PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['SFMAPS'], tableNames=tableMappings['SFMAPS'].getStagingTableNames()),
            FmeCommand(job=job, fmw="ocean_bay_labels_sfgisStg_to_sfmaps.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFMAPS'], ), useSdePorts=(True, False,) ),
            StagingCompleteCommand(job=job, tableMappings=tableMappings)
        ]
        job.commitCommands = self.buildCommitCommandsSingle(job=job, tableMappings=tableMappings)
        self.jobs.addJob(job)




        job = Job(name = 'county_line')
        job.setCommitConnGroups(connectionGroups = self.connectionGroups, connGroupKeys = ['SFMAPS'])
        tableMappings = self.buildTableMappingsSingle(connGroupName='SFMAPS', stagingTable='basemap_county_line_staging', targetTable='basemap_county_line', copyOnCommit=True)
        job.initCommands = [InitDbCommand(job=job, tableMappings=tableMappings)]
        job.stagingCommands = [
            StagingStartCommand(job=job, tableMappings=tableMappings),
            PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['SFMAPS'], tableNames=tableMappings['SFMAPS'].getStagingTableNames()),
            FmeCommand(job=job, fmw="county_line_sfgisStg_to_sfmaps.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFMAPS'], ), useSdePorts=(True, False,) ),
            StagingCompleteCommand(job=job, tableMappings=tableMappings)
        ]
        job.commitCommands = self.buildCommitCommandsSingle(job=job, tableMappings=tableMappings)
        job.initCommands = [InitDbCommand(job=job, tableMappings=tableMappings)]
        self.jobs.addJob(job)




        job = Job(name = 'coast')
        job.setCommitConnGroups(connectionGroups = self.connectionGroups, connGroupKeys = ['SFMAPS'])
        tableMappings = self.buildTableMappingsSingle(connGroupName='SFMAPS', stagingTable='basemap_sfmaps_coast_staging', targetTable='basemap_sfmaps_coast', copyOnCommit=True)
        job.initCommands = [InitDbCommand(job=job, tableMappings=tableMappings)]
        job.stagingCommands = [
            StagingStartCommand(job=job, tableMappings=tableMappings),
            PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['SFMAPS'], tableNames=tableMappings['SFMAPS'].getStagingTableNames()),
            FmeCommand(job=job, fmw="coast_sfgisStg_to_sfmaps.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFMAPS'], ), useSdePorts=(True, False,)),
            StagingCompleteCommand(job=job, tableMappings=tableMappings)
        ]
        job.commitCommands = self.buildCommitCommandsSingle(job=job, tableMappings=tableMappings)
        self.jobs.addJob(job)




        job = Job(name = 'bay_area_cities')
        job.setCommitConnGroups(connectionGroups = self.connectionGroups, connGroupKeys = ['SFMAPS'])
        tableMappings = self.buildTableMappingsSingle(connGroupName='SFMAPS', stagingTable='basemap_bay_area_cities_staging', targetTable='basemap_bay_area_cities', copyOnCommit=True)
        job.initCommands = [InitDbCommand(job=job, tableMappings=tableMappings)]
        job.stagingCommands = [
            StagingStartCommand(job=job, tableMappings=tableMappings),
            PostgresPreFmeCommand(job=job, connectionGroup=self.connectionGroups['SFMAPS'], tableNames=tableMappings['SFMAPS'].getStagingTableNames()),
            FmeCommand(job=job, fmw="bay_area_cities_sfgisStg_to_sfmaps.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFMAPS'], ), useSdePorts=(True, False,)),
            StagingCompleteCommand(job=job, tableMappings=tableMappings)
        ]
        job.commitCommands = self.buildCommitCommandsSingle(job=job, tableMappings=tableMappings)
        self.jobs.addJob(job)




        job = Job(name = 'avs_redo')
        csvFile = os.path.join(DATA_DIR, "tblAVS.txt")
        sqlFileStep2 = os.path.join(SQL_DIR, "avs_step_2.sql")
        sqlFileStep3 = os.path.join(SQL_DIR, "avs_step_3.sql")
        sqlFileStep4 = os.path.join(SQL_DIR, "avs_step_4.sql")
        errorCodesToIgnore = (
            # this is commend out because I got the SQL to be idempotent
            #'ORA-00955',        # name is already used by an existing object
        )
        job.commands = [
            #FmeCommand(job=job, fmw="parcels_source_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['DPW_SDE'], self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['ASSESSOR'], self.connectionGroups['DPW'],), useSdePorts=(True,True,False,False,)),
            #FmeCommand(job=job, fmw="parcels_sfgisStg_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], ), useSdePorts=(True,)),
            FmeCommand(job=job, fmw="avs_reload/parcels_sfgisStg_to_AddrProcessing.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFGIS_SOR_SDE'], self.connectionGroups['SFGIS70'],), useSdePorts=(True, True, False,)),
            #FmeCommand(job=job, fmw="streets_source_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['DPW_SDE'], self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['DPW'],), useSdePorts=(True, True,False,)),
            #FmeCommand(job=job, fmw="streets_sfgisStg_to_sfgisStg.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFGIS_STG_SDE'],), useSdePorts=(True, True,)),
            FmeCommand(job=job, fmw="avs_reload/streets_sfgisStg_to_AddrProcessing.fmw", connectionGroups=(self.connectionGroups['SFGIS_STG_SDE'], self.connectionGroups['SFGIS70'],), useSdePorts=(True, False,)),
            FmeCommand(job=job, fmw="avs_reload/DBIAVS2_to_AddrProcessing.fmw", connectionGroups=(self.connectionGroups['SFGIS70'],), useSdePorts=(False,), files=(csvFile,)),
            SqlStatementsCommand(job=job, connectionGroup=self.connectionGroups['SFGIS70'], sqlFile=sqlFileStep2, errorCodesToIgnore=errorCodesToIgnore),
            SqlStatementsCommand(job=job, connectionGroup=self.connectionGroups['SFGIS70'], sqlFile=sqlFileStep3, errorCodesToIgnore=errorCodesToIgnore),
            SqlStatementsCommand(job=job, connectionGroup=self.connectionGroups['SFGIS70'], sqlFile=sqlFileStep4, errorCodesToIgnore=errorCodesToIgnore),
        ]
        self.jobs.addJob(job)




    # todo - move these functions to the framework - does not belong in this file which is intended to be just a config file

    def getJob(self, jobName=None, env=None, verbose=False,
               *, title="Bulk load change request", comment="This change request was bulk-loaded."):
        job = self.jobs.getJob(jobName)
        job.env = env
        job.verbose = verbose
        
        if jobName == "bulkload":
            assert len(title) <= 100, "Title exceeded 100 chars."
            assert len(comment) <= 256, "Comment exceeded 256 chars."
            job.title = self.partialSanitize(title)
            job.comment = self.partialSanitize(comment)
            job.commands = (
                SqlCommand(job=job, sqlStatement="delete from bulkloader.metadata where key in ('cr_name', 'cr_comment');", connectionGroup=self.connectionGroups['MAD_ETL']),
                SqlCommand(job=job, sqlStatement=f"insert into bulkloader.metadata (key, value) values ('cr_name', '{job.title}');", connectionGroup=self.connectionGroups['MAD_ETL']),
                SqlCommand(job=job, sqlStatement=f"insert into bulkloader.metadata (key, value) values ('cr_comment', '{job.comment}');", connectionGroup=self.connectionGroups['MAD_ETL']),
            ) + job.commands

        return job

    @staticmethod
    def partialSanitize(val):
        """This is the minimum we can do; it should not be considered fully sanitized."""
        return val.replace("%", "%%").replace("'", "''")

    def buildCommitCommandsSingle(self, job=None, tableMappings=None):
            return [
                GetJobIdXaCommand(job=job, tableMappings=tableMappings),
                BeginTransactionXaCommand(job=job),
                CopyTablesXaCommand(job=job, tableMappings=tableMappings),
                SetJobCommitedXaCommand(job=job, tableMappings=tableMappings),
                EndTransactionXaCommand(job=job),
                SendEmailXaCommand(job=job)
            ]
            

    def buildTableMappingsSingle(self, connGroupName=None, stagingTable=None, targetTable=None, copyOnCommit=False):
        tableMappings = {}
        tableMappings[connGroupName] = TableMapping (
            connectionGroup = self.connectionGroups[connGroupName],
            tableMappingItems = (
                TableMappingItem(stagingTable=stagingTable, targetTable=targetTable, copyOnCommit=copyOnCommit),
            )
        )
        return tableMappings
