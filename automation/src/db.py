
import config.live.config_paths as config_paths
import os.path
import ssh
import sys

# If this gets more complicated consider creating a separate class for each
# vendor.

# TODO: -Legacy- Decide whether to do away with the "useSdePort" approach and
#       instead, define separate connections?  Seems like a wart.


class Types:
    POSTGRESQL = 'POSTGRESQL'

    # TODO: -17 Dec 2020- I suspect that we can just remove all references
    #       to the following databases, but let us just comment them out for
    #       now.
    # DB2 = 'DB2'
    # ORACLE = 'ORACLE'
    # MSSQL = 'MSSQL'


class SpatialTypes:
    POSTGIS = 'POSTGIS'
    SDE = 'SDE'

    # TODO: -17 Dec 2020- I suspect that we can just remove all references
    #       to the following spatial database, but let us just comment them
    #       out for now.
    # ORACLE = 'ORACLE'


TYPES = Types()
SPATIAL_TYPES = SpatialTypes()

# TODO: -08 Jan 2021- Consider creating a base class and deriving the classes
#       ArcSDEConnectionFileDefinition and DbConnectionDefinition from it.

class ArcSDEConnectionFileDefinition:
    def __init__(
        self,
        i_file_name=None
    ):

        # At the time of the EAS technology stack upgrade (late 2020), ArcGIS
        # enterprise geodatabases are "accessed" by using an ArcSDE connection
        # file.  These files have already been created, so all we are going to
        # do here is provide the path to the file, which in turn, will be
        # passed to an FME workspace.
        #
        #   Create ArcSDE Connection File
        #   https://desktop.arcgis.com/en/arcmap/latest/tools/data-management-toolbox/create-arcsde-connection-file.htm

        file_path = config_paths.get_arcsde_connection_file_path()
        self._full_path = os.path.join(file_path, i_file_name)
        return None

    def toFmeParms(self, infix=None):

        # The argument of "infix" may be any arbitrary string that matches
        # what we need for the FME workspace parameters, for example "c1" for
        # "Connection_1 parameter", "c2" for "Connection_2 parameter", etc.
        r_fme_parm = ""
        r_fme_parm += " --%s_arcsde_conn_file %s" % (infix, self._full_path)
        return r_fme_parm.strip()


class DbConnectionDefinition:
    def __init__(
        self,
        type=None,
        spatialType=None,
        host=None,
        port=None,
        sdePort=None,
        db=None,
        user=None,
        password=None,
        version=None,
        useSdePort=False
    ):

        # TODO: -13 Oct 2020- Does this next comment even make sense?  I could
        #       not find any other reference to a "host type" in this file.

        # "host_type" is ([Arc]SDE, PostgreSQL, Oracle, etc.)
        self.type = type
        self.spatialType = spatialType
        self.host = host
        self.port = port
        self.sdePort = sdePort
        self.db = db
        self.user = user
        self.password = password
        self.version = version

        # Set as needed; this is a bit of a "wart".
        self.useSdePort = useSdePort

    def __str__(self):
        return 'host: %s port: %s db: %s user: %s password: %s version: %s sdePort: %s' % (self.host, self.port, self.db, self.user, self.password, self.version, self.sdePort)  # noqa: E501

    def toStringforLogging(self):
        return 'host: %s port: %s db: %s ' % (self.host, self.port, self.db)

    def __hash__(self):
        h = hash(self.type)
        h += hash(self.spatialType)
        h += hash(self.host)
        h += hash(self.port)
        h += hash(self.sdePort)
        h += hash(self.db)
        h += hash(self.user)
        h += hash(self.password)
        h += hash(self.version)
        return h

    def __eq__(self, other):
        if self.type != other.type:
            return False
        if self.spatialType != other.spatialType:
            return False
        if self.host != other.host:
            return False
        if self.port != other.port:
            return False
        if self.sdePort != other.sdePort:
            return False
        if self.db != other.db:
            return False
        if self.user != other.user:
            return False
        if self.password != other.password:
            return False
        if self.version != other.version:
            return False
        if self.useSdePort != other.useSdePort:
            return False

        return True

    def copy(self, **kwargs):
        newInstance = \
            DbConnectionDefinition(
                type=self.type,
                spatialType=self.spatialType,
                host=self.host,
                port=self.port,
                sdePort=self.sdePort,
                db=self.db,
                user=self.user,
                password=self.password,
                version=self.version,
                useSdePort=self.useSdePort)
        for (k, v) in kwargs.items():
            setattr(newInstance, k, v)
        return newInstance

    def toPsyopg2(self):
        return "host='%s' port='%s' dbname='%s' user='%s' password='%s'" % (self.host, self.port, self.db, self.user, self.password)  # noqa: E501

    def toSqlAlchemy(self):

        connectionString = None

        if self.type == TYPES.POSTGRESQL:
            # 'postgresql://user:password@localhost:5432/sfmaps'
            connectionString = "postgresql://%s:%s@%s:%s/%s" % (self.user, self.password, self.host, self.port, self.db)  # noqa: E501
        else:
            tmp_str = \
                'Only PostgreSQL SQLAlchemy connections are implemented'
            raise Exception(tmp_str)

        return connectionString

    def toFmeParms(self, infix=None):

        # The argument of "infix" may be any arbitrary string that matches
        # what we need for the FME workspace parameters.
        returnString = ""
        returnString += " --%s_host %s" % (infix, self.host)
        returnString += " --%s_db %s" % (infix, self.db)
        returnString += " --%s_user %s" % (infix, self.user)
        returnString += " --%s_password %s" % (infix, self.password)

        if self.useSdePort and not self.spatialType == SPATIAL_TYPES.SDE:
            tmp_str = \
                'There is a misconfiguration in the file ' \
                + 'config_connections.py.  "useSdePort" was TRUE but the ' \
                + 'spatial type was not SDE.'
            raise Exception(tmp_str)

        # TODO: -17 Dec 2020- I suspect that we can just remove all references
        #       to DB2 but let us just comment them out for now.
        #
        # if self.type == TYPES.DB2:
        #     if self.useSdePort and self.spatialType == SPATIAL_TYPES.SDE:
        #         returnString += \
        #             " --%s_instance port:%s" % (infix, self.sdePort)
        #         returnString += " --%s_version %s" % (infix, self.version)
        #     else:
        #         returnString += " --%s_instance port:%s" % (infix, self.port)

        if self.type == TYPES.POSTGRESQL:
            if self.useSdePort and self.spatialType == SPATIAL_TYPES.SDE:
                returnString += \
                    " --%s_instance port:%s" % (infix, self.sdePort)
                returnString += " --%s_version %s" % (infix, self.version)
            else:
                returnString += " --" + infix + "_port %s" % self.port

        # TODO: -17 Dec 2020- I suspect that we can just remove all references
        #       to MSSQL but let us just comment them out for now.
        #
        # elif self.type == TYPES.MSSQL:
        #     pass

        # TODO: -17 Dec 2020- I suspect that we can just remove all references
        #       to ORACLE but let us just comment them out for now.
        #
        # elif self.type == TYPES.ORACLE:
        #     #  e.g., username/password@//10.250.60.31:1521/sfgis70
        #     returnString = "--%s_oracle_connection %s/%s@//%s:%s/%s" % (infix, self.user, self.password, self.host, self.port, self.db)  # noqa: E501

        elif self.type is None:
            returnString += " --" + infix + "_port %s" % self.port
        else:
            tmp_str = 'Unsupported database connection type: '
            raise Exception(tmp_str + self.type)

        return returnString.strip()

    def getConnection(self):

        connection = None

        if self.type == TYPES.POSTGRESQL:
            connection_string = self.toPsyopg2()
            # print 'connection_string: ' + str(connection_string)
            import psycopg2
            try:
                connection = psycopg2.connect(connection_string)
            except Exception as e:
                print("Unable to connect to " + self.toStringforLogging())
                raise e

        # TODO: -13 Oct 2020- I suspect that we can just remove all references
        #       to DB2 but let us just comment them out for now.
        #
        # elif self.type == TYPES.DB2:
        #     import DB2
        #     try:
        #         dnsString = 'Hostname=%s;Database=%s;Port=%s;' % (self.host, self.db, self.port)  # noqa: E501
        #         connection = DB2.connect(dsn=dnsString, uid=self.user, pwd=self.password, autoCommit=False)  # noqa: E501
        #     except Exception as e:
        #         print("unable to connect to " + self.toStringforLogging())
        #         print(e)
        #         raise

        # TODO: -13 Oct 2020- I suspect that we can just remove all references
        #       to SQL Server but let us just comment them out for now.
        #
        # elif self.type == TYPES.MSSQL:
        #     # we use pymssql
        #     import pymssql
        #     try:
        #         connection = pymssql.connect(host=self.host, user=self.user, password=self.password, database=self.db)  # noqa: E501
        #     except Exception as e:
        #        print("unable to connect to " + self.toStringforLogging())
        #        print(e)
        #        raise

        # TODO: -13 Oct 2020- I suspect that we can just remove all references
        #       to Oracle but let us just comment them out for now.
        #
        # elif self.type == TYPES.ORACLE:
        #     import cx_Oracle
        #     try:
        #         #cx_Oracle.connect("user", "password", "TNS")
        #         # scott/tiger@server:1521/orcl
        #         connectionString = '%s/%s@%s:%s/%s' % (self.user, self.password, self.host, self.port, self.db)  # noqa: E501
        #         connection = cx_Oracle.connect(connectionString)
        #     except Exception as e:
        #        print("unable to connect to " + self.toStringforLogging())
        #        print(e)
        #        raise

        else:
            message = "Unsupported connection type: %s" % self.type
            print(message)
            raise Exception(message)

        return connection

    def executeSqlStatements(self, sqlStatements):

        # We pass in a tuple of SQL statements because some drivers, such as
        # DB2, do not handle mulitple statements in one block as does
        # psycopg2.

        connection = self.getConnection()

        try:
            cursor = connection.cursor()
            print("Executing SQL")
            for sql in sqlStatements:
                # For debugging only.
                # print sql
                cursor.execute(sql)

            cursor.close()
            connection.commit()
        except Exception as e:
            print('Error while executing SQL')
            print(e)
            raise
        finally:
            connection.close()

    def executeSql(self, sql):

        connection = self.getConnection()

        try:
            cursor = connection.cursor()
            print("Executing SQL")
            cursor.execute(sql)
            cursor.close()
            connection.commit()
        except Exception as e:
            print('Error while executing SQL')
            print(e)
            raise
        finally:
            connection.close()

    def executeSqlStatement(self, cursor, sqlStatement):
        try:
            cursor.execute(sqlStatement)
        except Exception as e:
            raise e
        finally:
            pass

    def standardizeException(self, exceptionImpl):
        # TODO: -Legacy- Make this more of a real factory kind of pattern (get
        #       away from using all these IF statements).
        try:
            if self.type == TYPES.POSTGRESQL:
                return dict(
                    code=exceptionImpl.pgcode,
                    message=exceptionImpl.pgerror)

            # TODO: -17 Dec 2020- I suspect that we can just remove all
            #       references to the following databases but let us just
            #       comment them out for now.
            # elif self.type == TYPES.DB2:
            #     tmp_str = 'Standardized exception is not supported for DB2'
            #     raise Exception(tmp_str)
            # elif self.type == TYPES.MSSQL:
            #     tmp_str = 'Standardized exception is not supported for MSSQL'
            #     raise Exception(tmp_str)
            # elif self.type == TYPES.ORACLE:
            #     return dict(
            #         code=exceptionImpl.message.code,
            #         message=exceptionImpl.message.message)
            else:
                tmp_str = 'Standardized exception is not supported for '
                raise Exception(tmp_str + self.type)
        except Exception as e:
            print(e)
            raise


class DbUtils:

    @staticmethod
    def ResultIter(cursor, arraysize=1000):
        while True:
            rows = cursor.fetchmany(arraysize)
            if not rows:
                break
            for row in rows:
                yield row


# END OF FILE
