
from config.live import config_jobs
from config.live import config_paths
import db
import os
import sys
import subprocess
import time
from optparse import OptionParser
import sqlalchemy
import uuid


class Job:

    # Command pattern
    # https://en.wikipedia.org/wiki/Command_pattern

    def __init__(self, name=None):
        self.id = uuid.uuid4()
        self.name = name
        self.commitConnGroups = []
        self.commitResources = {}
        self.verbose = False
        self.env = None
        self.message = ''

    def init(self, verbose=False):
        self.executeCommands(self.initCommands)

    def stage(self, verbose=False):
        self.executeCommands(self.stagingCommands)

    # Supports two-phase commit.
    def commit(self, verbose=False):
        try:
            self.initTransactionalConnections()
            for index, command in enumerate(self.commitCommands):
                self.logDescription(index, command)
                command.execute()
        except StopJob as e:
            print(e)
        except Exception as e:
            print(e)
            self.rollbackTransaction()
            raise
        finally:
            self.closeTransactionalConnections()

    def execute(self, verbose=False):
        self.executeCommands(self.commands)

    # Use for staging; auto-commit is TRUE.
    def executeCommands(self, commands):
        if not commands:
            return
        try:
            for i, command in enumerate(commands):
                self.logDescription(i, command)
                command.execute()
        except StopJob as e:
            print(e)
        except Exception as e:
            print(e)
            raise
        finally:
            pass

    def rollbackTransaction(self):

        # TODO: -Legacy- Do not rollback a transaction that does not exist.

        print('rolling back')
        for resourceDict in list(self.commitResources.values()):
            transaction = resourceDict['TRANSACTION']
            transaction.rollback()

    def endTransaction(self):

        for resourceDict in list(self.commitResources.values()):
            transaction = resourceDict['TRANSACTION']
            transaction.prepare()

        for resourceDict in list(self.commitResources.values()):
            transaction = resourceDict['TRANSACTION']
            transaction.commit()

    def beginTransaction(self):

        # pylint: disable=unused-variable
        for key, resourceDict in self.commitResources.items():
            dbConn = resourceDict['DB_CONNECTION']
            transaction = dbConn.begin_twophase()
            resourceDict['TRANSACTION'] = transaction

    def initTransactionalConnections(self):

        self.commitResources = {}

        for connectionGroup in self.commitConnGroups:
            key = connectionGroup[self.env].name
            sshDbPair = connectionGroup[self.env].sshDbPair
            sshClient = sshDbPair.getSshClient()
            dbConnDef = sshDbPair.getDbConnDef()

            # We use SQLAlchemy because it supports two-phase commit.
            connectionString = dbConnDef.toSqlAlchemy()

            sshClient.connect()
            engine = sqlalchemy.create_engine(connectionString)
            conn = engine.connect()

            self.commitResources[key] = {}
            self.commitResources[key]['DB_CONNECTION'] = conn
            self.commitResources[key]['SSH_CLIENT'] = sshClient

            # We will create the transaction when we begin it.
            self.commitResources[key]['TRANSACTION'] = None

    def closeTransactionalConnections(self):

        try:
            for resourceDict in list(self.commitResources.values()):
                resourceDict['DB_CONNECTION'].close()
        except Exception as e:  # pylint: disable=unused-variable
            pass

        try:
            for resourceDict in list(self.commitResources.values()):
                resourceDict['SSH_CLIENT'].disconnect()
        except Exception as e:  # pylint: disable=unused-variable
            pass

    def logDescription(self, i, command):
        print('\nCommand number: ' + str(i+1))
        # TODO: -Legacy- Move away from the use of the "type" field because it
        #       increases the maintenance burden.
        try:
            print('Command: %s' % command.type)
        except AttributeError:
            print('Command: %s' % command.__class__.__name__)

    def setCommitConnGroups(self, connectionGroups=None, connGroupKeys=None):
        self.commitConnGroups = []
        for key in connGroupKeys:
            self.commitConnGroups.append(connectionGroups[key])


class StopJob(Exception):

    def __init__(self, message):
        self.message = message

    def __str__(self):
        return repr(self.message)


class Jobs:

    def addJob(self, job):
        self.jobDict[job.name] = job

    def getJob(self, jobName):
        return self.jobDict[jobName]

    def getJobList(self):
        # pylint: disable=unused-variable
        jobList = []
        for key, value in self.jobDict.items():
            jobList.append(value)
        return jobList

    def __init__(self):
        self.jobDict = {}


class TableMapping:

    def __init__(self, connectionGroup=None, tableMappingItems=None):
        self.connectionGroup = connectionGroup
        self.tableMappingItems = tableMappingItems

    def getStagingTableNames(self):
        stagingTableNames = []
        for tableMappingItem in self.tableMappingItems:
            stagingTableNames.append(tableMappingItem.stagingTable)
        return stagingTableNames

    def getTargetTableNames(self):
        targetTableNames = []
        for tableMappingItem in self.tableMappingItems:
            targetTableNames.append(tableMappingItem.targetTable)
        return targetTableNames

    def getTableTuples(self):
        tableTupleList = []
        for tableMappingItem in self.tableMappingItems:
            tableTupleList.append(
                (
                    tableMappingItem.stagingTable,
                    tableMappingItem.targetTable,
                )
            )
        return tableTupleList


class TableMappingItem:

    def __init__(
        self,
        stagingTable=None,
        targetTable=None,
        copyOnCommit=False
    ):
        self.stagingTable = stagingTable
        self.targetTable = targetTable
        self.copyOnCommit = copyOnCommit


def main():

    # TODO: -Legacy- Make this smarter by passing in a job organizer instance.

    jobOrganizer = config_jobs.JobOrganizer()
    jobKeys = sorted(jobOrganizer.jobs.jobDict.keys())
    environments = jobOrganizer.connectionOrganizer.environments

    usage = "\npython %prog --env environment --job jobname --action action "
    usage += "--v verbose\n"
    usage += '\nenvironments:'
    for env in environments:
        usage += '\n\t%s' % env
    usage += '\njobs:\n'

    leftJustLength = len(max(jobKeys, key=len)) + 1
    for jobKey in jobKeys:
        usage += '\t' + jobKey.ljust(leftJustLength, ' ') + 'actions: '
        job = jobOrganizer.jobs.jobDict[jobKey]

        actions = []

        try:
            x = job.initCommands  # pylint: disable=unused-variable
            actions.append('INIT')
        except Exception as e:  # pylint: disable=unused-variable
            pass

        try:
            x = job.stagingCommands
            actions.append('STAGE')
        except Exception as e:  # pylint: disable=unused-variable
            pass

        try:
            x = job.commitCommands
            actions.append('COMMIT')
        except Exception as e:  # pylint: disable=unused-variable
            pass

        try:
            x = job.commands
            actions.append('EXECUTE')
        except Exception as e:
            pass

        usage += ' | '.join(actions)
        usage += '\n'

    usage += """
Description


An INIT action may be run exactly one time.
EXECUTE, STAGE, and COMMIT actions may be run multiple times.

DO NOT run an INIT action a second time without a database refresh.
If a job has an INIT action, then you must run INIT before using any other
job action.

However, the first time you run a STAGE-COMMIT job, you must run through this
sequence:

    STAGE
    INIT
    COMMIT

INIT will initialize the table structure in the relational database.
When you change a staging table definition in an FME workspace, you must run
through the same sequence to reinitialize the table structure.  Thereafter,
you simply run:

    STAGE
    COMMIT

See the source code README for details.

"""

    parser = OptionParser(usage)
    parser.add_option("--job", dest="job", help="Specify the job")
    parser.add_option("--env", dest="env", help="Specify the environment")
    parser.add_option("--action", dest="action", help="Specify the action")
    parser.add_option(
        "--title",
        dest="title",
        help="Specify a title for the change request",
        default="Bulk load change request",
    )
    parser.add_option(
        "--comment",
        dest="comment",
        help="Specify a comment for the change request",
        default="This change request was bulk-loaded.",
    )
    parser.add_option(
        "--verbose",
        action="store_true",
        dest="verbose",
        default=False,
        help="Print lots of output to stdout"
    )

    (options, args) = parser.parse_args()  # pylint: disable=unused-variable

    if options.job is None:
        parser.error("Please specify a job.")
    if options.job not in jobKeys:
        parser.error("No such job: " + options.job)
    if options.env is None:
        parser.error("Please specify an environment.")
    if options.action is None:
        parser.error("Please specify an action.")
    if options.env not in environments:
        parser.error("No such environment: " + options.env)
    if options.action not in ('INIT', 'STAGE', 'COMMIT', 'EXECUTE'):
        parser.error("No such action: " + options.action)

    job = jobOrganizer.getJob(options.job, options.env, options.verbose, title=options.title, comment=options.comment)

    print("running job: %s" % job.name)
    print("env: %s" % options.env)
    print("action: %s" % options.action)
    if job.name == "bulkload":
        print("title: %s" % options.title)
        print("comment: %s" % options.comment)
    print("verbose: %s" % str(job.verbose))

    if (options.action == 'INIT'):
        job.init()
    elif (options.action == 'STAGE'):
        job.stage()
    elif (options.action == 'COMMIT'):
        job.commit()
    elif (options.action == 'EXECUTE'):
        job.execute()
    else:
        parser.error("No such action: " + options.action)


if __name__ == "__main__":
    main()


# END OF FILE
