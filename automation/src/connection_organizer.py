
import db
import db_ssh
from config.live.config_connections import ConnectionConfigurations


# To organize the database connections, we use a "connection group", which is
# a group of database connections that typically includes three environments:
#
#   DEV, QA, PROD
#
# For the SSH connections, we may use the:
#
#   Null object pattern
#   https://en.wikipedia.org/wiki/Null_object_pattern

class ConnectionOrganizer:

    def __init__(self, type=None, arg=None, connectionKeys=()):

        # These are all populated by ConnectionConfigurations() below.
        self.connectionGroups = {}
        self.environments = ()
        self.ftpConnections = {}
        self.geoserverConnections = {}
        self.smtpConnections = {}
        self.webConnections = {}

        ConnectionConfigurations(self)


    def addConnectionGroup(
        self,
        groupName=None,
        dbConnGroup=None,
        sshConnDefs=None
    ):
        for env in self.environments:

            ssh_db_pair = \
                db_ssh.SshDbConnectionPair(
                    dbConnDef=dbConnGroup[env], sshConnDef=sshConnDefs[env])

            connectionGroup = \
                db_ssh.ConnectionGroup(
                    name=groupName, env=env, sshDbPair=ssh_db_pair)

            if not (groupName in self.connectionGroups):
                self.connectionGroups[connectionGroup.name] = {}

            cg_env = connectionGroup.env
            self.connectionGroups[groupName][cg_env] = connectionGroup


    def buildEnvDict(self, values):
        if len(values) != len(self.environments):
            raise Exception( 'error in connection configuration - missing environment or value \nEnvironments%s\nValues:%s' % (str(self.environments), str(values)) )
        d = dict()
        for key, value in zip(self.environments, values):
            if key is None:
                raise Exception('error in connection configuration - missing key')
            if value is None:
                raise Exception('error in connection configuration - missing value')
            d[key] = value
        return d


# END OF FILE
