# A lightweight automation of the installation of vmware tools.
# Hardcoded paths used to keep this useful for a wide audience (have debate here).
# Before you run this script you must:
# 1) insure that the VM OS is known to the VM host
# 2) todo - describe the menu pick we do on the VMWare console to get access to /dev/cdrom.

# only root
if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

mkdir /mnt/cdrom
mount /dev/cdrom /mnt/cdrom

pushd /tmp
tar zxvf /mnt/cdrom/VMwareTools-8.3.7-381511.tar.gz .
pushd vmware-tools-distrib
./vmware-install.pl --default
popd
popd

umount /mnt/cdrom
rmdir /mnt/cdrom
