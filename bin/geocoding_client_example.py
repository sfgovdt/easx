# This is an example of how to use python to access the eas street geocoding service.
# The wiki page for this service is here
# https://sfgovdt.jira.com/wiki/display/MAD/Street+Address+Validation

# make sure you have json - http://pypi.python.org/pypi/json/
import json
import urllib.request, urllib.parse, urllib.error, http.client

httpConnection = http.client.HTTPConnection('10.250.60.189:80')  # EAS DEV instance
try:
    encodedParameters = urllib.parse.urlencode({
        'f':'json',
        'Address':'1650 Mission St',
        'Zip': '95103'
    })
    httpConnection.request("GET", '/geocode/streetNetwork/findAddressCandidates/?'+encodedParameters)
    response = httpConnection.getresponse()

    # If you need to pick apart the response you can use this code.
    # In most cases you probably don't need to do this.
    #    values = list()
    #    values.append('status: %s' % response.status)
    #    values.append('reason: %s' % response.reason)
    #    values.append('version: %s' % response.version)
    #    values.append('headers')
    #    for key, value in response.getheaders():
    #        values.append('\t%s: %s' % (key, value))
    #        content = response.read()
    #        values.append('content: %s' % content)
    #    responseAsText = '\n'.join(values)

    contentString = response.read()
    #    print 'content in string form - format for readability not guaranteed'
    #    print contentString
    #    print ''

    contentDict = json.loads(contentString)
    #    print 'content in dictionary form - you can now access the data easily'
    #    print contentDict
    #    print ''

    contentStringPretty = json.dumps(contentDict, indent=4)
    print('content in formatted string form - format for readability is now guaranteed')
    print(contentStringPretty)
    print('')

    if response.status != 200:
        raise IOError('request failed - status: %s - message: %s' % (str(response.status), str(response.msg)))

except Exception as e:
    raise

finally:
    httpConnection.close()
