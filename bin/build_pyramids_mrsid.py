
# http://docs.geoserver.org/stable/en/user/tutorials/imagepyramid/imagepyramid.html

import os, glob, sys
from sys import argv
import subprocess
import osgeo.gdal
import osgeo.osr

GDAL_HOME = None
key = 'GDAL_HOME'
try:
    GDAL_HOME = os.environ[key]
except Exception:
    sys.stdout.write('missing system variable: %s\n' % key)
    sys.exit(-1)


def usage():
    print("""python %s
""" % argv[0])
    sys.exit()



def retile(inputFilePath=None, pyramidOutputPath=None, levels=None, epsg=None):
    import shutil
    if os.path.exists(pyramidOutputPath):
        shutil.rmtree(pyramidOutputPath)
    os.makedirs(pyramidOutputPath)

    # e.g. gdal_retile.py -v -r bilinear -levels 4 -ps 2048 2048 -co "TILED=YES" -co "COMPRESS=JPEG" -targetDir bmpyramid --optfile 'foo.txt'
    command = []
    command.append('python2.5')
    command.append(os.path.join(GDAL_HOME, 'gdal_retile.py'))
    command.append('-v')
    command.append('-r')
    command.append('bilinear')
    command.append('-levels')
    command.append(str(levels))
    command.append('-ps')
    command.append('2048')
    command.append('2048')
    command.append('-s_srs')
    command.append('EPSG:%s' % epsg)
    command.append('-co')
    command.append('TILED=YES')
    command.append('-co')
    command.append('BLOCKXSIZE=256')
    command.append('-co')
    command.append('BLOCKYSIZE=256')
    command.append('-co')
    command.append('WORLDFILE=YES')
    command.append('-targetDir')
    command.append(pyramidOutputPath)
    command.append(inputFilePath)
    print("executing command line: %s\n" % ' '.join(command))

    process = subprocess.Popen(command, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()
    print("command return code: " + str(process.returncode))
    if process.returncode != 0:
        sys.exit(-1)
    else:
        #print "command success"
        pass


def reproject(inputFilePath=None, outputFilePath=None, outputImageType=None, inputEpsg=None, outputEpsg=None):

    outputDir = os.path.dirname(outputFilePath)
    import shutil
    if os.path.exists(outputDir):
        shutil.rmtree(outputDir)
    os.makedirs(outputDir)

    # e.g. gdalwarp -s_srs EPSG:2227 -t_srs \
    # "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs" \
    #  -co "TILED=YES" -co "COMPRESS=JPEG" %s %s
    command = []
    command.append('gdalwarp')
    command.append('-s_srs')
    command.append('EPSG:%s' % inputEpsg)
    command.append('-t_srs')
    command.append('EPSG:%s' % outputEpsg)
    if outputImageType == 'jpg':
        command.append('-co')
        command.append('COMPRESS=JPEG')
    command.append('-dstalpha')
    command.append(inputFilePath)
    command.append(outputFilePath)
    print("executing command line: %s\n" % ' '.join(command))
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()
    if process.returncode != 0:
        print("***** command failure *****")
        sys.exit(-1)
    else:
        #print "command success"
        pass


def createGeoFiles(filePath):
    # mostly from http://gis.stackexchange.com/questions/9421/how-to-create-tfw-and-prj-files-for-a-bunch-of-geotiffs
    source = osgeo.gdal.Open(filePath)
    sourceTransform = source.GetGeoTransform()

    source_srs = osgeo.osr.SpatialReference()
    source_srs.ImportFromWkt(source.GetProjection())
    source_srs.MorphToESRI()
    source_wkt = source_srs.ExportToWkt()

    prjFilename = os.path.splitext(filePath)[0] + '.prj'
    prjFile = open(prjFilename, 'wt')
    prjFile.write(source_wkt)
    prjFile.close()

    source = None

    twfFilename = os.path.splitext(filePath)[0] + '.tfw'
    tfwFile = open(twfFilename, 'wt')
    tfwFile.write("%0.8f\n" % sourceTransform[1])
    tfwFile.write("%0.8f\n" % sourceTransform[2])
    tfwFile.write("%0.8f\n" % sourceTransform[4])
    tfwFile.write("%0.8f\n" % sourceTransform[5])
    tfwFile.write("%0.8f\n" % sourceTransform[0])
    tfwFile.write("%0.8f\n" % sourceTransform[3])
    tfwFile.close()


def downloadData(sftpAccess=None, downloadDir=None):
    pass
    # wget the shapefile
    # for each record in the shape file
    #   if fully within bbox
    #       wget the image
    # wget notes
    # make this section optional (parm or interrogate user)
    # do not hard code these SFTP credentials (parm or interrogate user)
    # for user intterogation see web/bin/deploy_source.py
    # tell user that the credentials are here - http://10.250.60.17/citypedia/index.php/SFGIS_FTP_Resource
    # here is the command line
    #  wget -North.* -r --no-directories --user=SFGISRO --password=YhmDA4xS https://sfftp.sfgov.org/Data/Imagery/Mosaic_2012/CASANF12-SID-6INCH
    # note that command line idiom is OK but this may be more understandable (depending)
    # commandList = " ".split("wget -North.* -r --no-directories --user=SFGISRO --password=YhmDA4xS https://sfftp.sfgov.org/Data/Imagery/Mosaic_2012/CASANF12-SID-6INCH")


def createRetileListFile(sourceDir, outputFilePath, fileSuffix):
    if os.path.exists(outputFilePath):
        os.remove(outputFilePath)
    inputFilePaths = glob.glob(sourceDir + "/*." + fileSuffix)
    outputFile = open(outputFilePath, 'wt')
    for inputFilePath in inputFilePaths:
        outputFile.write("%s\n" % inputFilePath)
    outputFile.close()


def convertMrSidToTif(inputImageFile, outputImageFile, subsetOffset, subsetSize):
    print("\nconverting mrsid to tif\nfrom %s \nto %s \nsubset offset %s \nsubset size %s" % (inputImageFile, outputImageFile, subsetOffset, subsetSize))

    # e.g. command line
    # mrsidgeodecode -i C:\temp\eas_imagery\mrsid\CASANF11-SID-6INCH-North.sid -o C:\temp\eas_imagery\mrsid\CASANF11-SID-6INCH-North_subset.tif -ulxy 50000 50000 -wh 1000 1000 -wf
    command = []
    command.append('mrsidgeodecode')
    command.append('-i')
    command.append(inputImageFile)
    command.append('-o')
    command.append(outputImageFile)
    command.append('-wf')
    if subsetOffset > 0:
        command.append('-ulxy')
        subsetOffset = str(subsetOffset)
        command.append(subsetOffset)
        command.append(subsetOffset)
    if subsetSize > 0:
        command.append('-wh')
        subsetSize = str(subsetSize)
        command.append(subsetSize)
        command.append(subsetSize)

    print("executing command line: %s\n" % ' '.join(command))

    process = None
    try:
        process = subprocess.Popen(command, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    except OSError as e:
        print("unable to exeute command")
        print(" ".join(command))
        print("make sure %s is installed" % command[0])
        sys.exit(-1)

    process.wait()
    if process.returncode != 0:
        print("***** command failure *****")
        sys.exit(-1)
    else:
        #print "command success"
        pass


def createGeoFilesForPyramid(pyramidOutputPath, levels):

    def processDir(dir):
        inputFilePaths = glob.glob(dir + "/*.tif")
        for inputFilePath in inputFilePaths:
            createGeoFiles(inputFilePath)

    processDir(pyramidOutputPath)

    for level in range(1, levels+1):
        dir = os.path.join(pyramidOutputPath, str(level))
        processDir(dir)


def go(mrsidSourcePath=None, subsetOriginOffset=None, subsetSize=None, subsetDir = None,
       reprojectDir=None, inputEpsg=2227, outputEpsg=2227,
       pyramidOutputDir=None, outputImageType='tif', levels=7, ):

    # house keeping
    if not os.path.exists(mrsidSourcePath):
        raise OSError('source directory not found: %s' % mrsidSourcePath)
    import shutil
    if os.path.exists(subsetDir):
        shutil.rmtree(subsetDir)
    os.mkdir(subsetDir)

    # extract and optionally subset from mrsid
    inputDirectoryName, inputFilename = os.path.split(mrsidSourcePath)
    prePart, postPart = os.path.splitext(inputFilename)
    outputFilePath = os.path.join(subsetDir, prePart+".tif")
    if mrsidSourcePath:
        convertMrSidToTif(mrsidSourcePath, outputFilePath, subsetOriginOffset, subsetSize)

    # project
    inputFilePath = outputFilePath
    dirName, fileName= os.path.split(inputFilePath)
    outputFilePath = os.path.join(reprojectDir, os.path.splitext(fileName)[0]+".tif")
    if inputEpsg != outputEpsg:
        reproject(inputFilePath=inputFilePath, outputFilePath=outputFilePath, outputImageType=outputImageType, inputEpsg=inputEpsg, outputEpsg=outputEpsg)
        createGeoFiles(outputFilePath)

    # retile
    inputFilePath = outputFilePath
    retile(inputFilePath=inputFilePath, pyramidOutputPath=pyramidOutputDir, levels=levels, epsg=outputEpsg)
    createGeoFilesForPyramid(pyramidOutputDir, levels)

    print('done!')


def main():

    go(
            mrsidSourcePath='/home/dev/ip_source/North.sid',
            subsetOriginOffset=50000,
            subsetSize=10000,
            subsetDir = '/home/dev/ip_subset',

            reprojectDir='/home/dev/ip_project',
            inputEpsg=2227,
            outputEpsg=900913,

            outputImageType='tif',
            levels=3,
            pyramidOutputDir='/home/dev/ip_pyramid',

    )


if __name__ == "__main__":
    main()

