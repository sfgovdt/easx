Ext.namespace('MAD', 'MAD.config');

MAD.config.Paths = {
    Proj4jsLibPath : "Media/js/proj4js/lib/"
};

MAD.config.Emails = {
    ParcelsProvisioning : "DT-EAS-DPW-Basemap-Data@sfgov.org"
};

MAD.config.Images = {
    TaskIcon: 'Media/images/task-icon.gif'
    ,RedPin: 'Media/images/red.png'
    ,AquaPin: 'Media/images/aqua.png'
    ,BluePin: 'Media/images/blue.png'
    ,UserEdit: 'Media/images/silk/icons/user_edit.png'
    ,UserGo: 'Media/images/silk/icons/user_go.png'
    ,UserRed: 'Media/images/silk/icons/user_red.png'
    ,Disk: 'Media/images/silk/icons/disk.png'
    ,Add: 'Media/images/silk/icons/add.png'
    ,Delete: 'Media/images/silk/icons/delete.png'
    ,Zoom: 'Media/images/silk/icons/zoom.png'
    ,House: 'Media/images/silk/icons/house.png'
    ,ArrowOut: 'Media/images/silk/icons/arrow_out.png'
    ,ShapeSquareAdd: 'Media/images/silk/icons/shape_square_add.png'
    ,ShapeSquare: 'Media/images/silk/icons/shape_square.png'
    ,Error: '/Media/images/silk/icons/error.png'
    ,ServiceIcon: '/Media/images/cloud.png'
};

MAD.config.Urls = {

    // Account
    Login: 'login/',
    Logout: 'logout/',
    GetUser: 'user/',
    ChangePassword: 'user/changepassword/',
    ResetPassword: 'user/resetpassword/',
    FirstLogin: '',

    // Searches
    AddressSearch: 'search/address',
    ParcelSearch: 'search/apn',
    AddressToParcels: 'search/addressToParcels',
    PointToParcels: 'search/pointToParcels',
    ParcelToAddresses: 'search/parcelToAddresses',
    AddressInfo: 'search/addressinfo/',
    LineageByChangeRequest: 'changeRequest/report/',
    Lineage: 'changeRequest/report/axp/',
    History: 'changeRequest/report/address/',
    ChangeRequestAddresses: 'changeRequest/report/addresses/',

    //Reports
    AddressReportHeader: 'AddressReport/?id=',
    AddressReportIdentifiers: 'Identifiers/?id=',
    UnitsForAddressReport: 'UnitsForAddressReport/?id=',
    ParcelsForAddressReport: 'ParcelsForAddressReport/?id=',
    AddressReportAliases: 'Aliases/?id=',
    AddressReportHistory: 'History/?id=',
    BaseGeometryForParcels: 'BaseGeometryForParcels/',

    AddressProposeAt: 'address/proposeAt/',

    //Change Requests
    GetChangeRequest: 'changeRequest/{0}/',
    EditChangeRequest: 'changeRequest/edit/{0}/',
    SaveChangeRequest: 'changeRequest/save/',
    SubmitChangeRequest: 'changeRequest/submitforreview/',
    AcceptChangeRequestForReview: 'changeRequest/acceptforreview/',
    RejectChangeRequest: 'changeRequest/reject/',
    ApproveChangeRequest: 'changeRequest/approve/',
    DeleteChangeRequest: 'changeRequest/delete/',
    GetBaseAddrForChangeRequest: 'getbaseaddr/',
    GetMyChangeRequests: 'changeRequest/mine/',
    GetChangeRequestsForReview: 'changeRequest/available/',

    // create a new parcel record from the base parcel at specific lon-lat
    ProvisionParcel: 'provision/parcel/block/{0}/lot/{1}/x/{2}/y/{3}/',

    //Domains
    ChangeRequestDomains: 'domain/changerequest/',

    getGeoWebCacheUrl: function() {
        if (MAD.app.applicationInfo.env.indexOf('DESKTOP') > -1)
            return 'http://localhost:8081/geoserver/gwc/service/wms/';
        else
            return '/geoserver/gwc/service/wms/';
    },

    getGeoserverWmsUrl: function() {
        if (MAD.app.applicationInfo.env.indexOf('DESKTOP') > -1)
            return 'http://localhost:8081/geoserver/wms?';
        else
            return '/geoserver/wms?';
    },

    getUserGuidesUrl: function() {
        return 'https://ccsfdt.atlassian.net/wiki/spaces/EAS/pages/235208705/User+Guides';
    },

    getHelpDocsUrl: function() {
        return 'https://ccsfdt.atlassian.net/wiki/spaces/EAS/pages/182255617/Help';
    },

    getWebSiteUrl: function() {
        return 'https://ccsfdt.atlassian.net/wiki/spaces/EAS/overview';
    },

    getParcelResearchUrl: function() {
        return '/ParcelResearch/';
    },

    getParcelResearchUrlTarget: function() {
        return 'easParcelResearch';
    },

    getGoogleStreetViewUrl: function () {
        // http://mapki.com/wiki/Google_Map_Parameters#Street_View
        // http://stackoverflow.com/questions/387942/google-street-view-url-question
        // e.g. http://maps.google.com/maps?q=&layer=c&cbll=31.335198,-89.287304&cbp=12,0,0,0,0
        return 'http://maps.google.com/maps?q=&layer=c&cbll={0},{1}&cbp=12,0,0,0,0';
    },

    getPimResourceObject: function (blockLot) {
        return {
            'url': 'https://sfplanninggis.org/pim/?tab=Property&search=' + blockLot,
            'target': 'easPim'
        };
    },

    getRecordedMapsResourceObject: function(block) {
        var suffix = '';
        if (block) {
            suffix = 'recordedBottom.asp?block=' + block + '&lot=&streetname=&map=&book=&page=&box=&project=&projectid=';
        }
        return {
            'url': 'http://bsm.sfdpw.org/subdivision/recorded/' + suffix,
            'target': 'easRecordedMaps'
        }
    },

    getRecorderResourceObject: function() {
        return {
            'url': 'https://recorder.sfgov.org/#!/disclaimer',
            'target': 'easRecorderSearch'
        }
    }

};
