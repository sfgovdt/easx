Ext.define('MAD.widgets.Results', {
    extend: 'Ext.panel.Panel',
    layout: 'card',
    title: 'Results',
    floatable: false,
    region: 'west',
    minWidth: 200,
    maxWidth: 900,
    width: 300,
    margins: '0 5 0 5',
    border: true,
    resizable: true,
    resizeHandles: 'e',

    pageSize: 10,
    highlightedRow: null,
    user: null,

    initComponent: function () {

        this.user = MAD.model.User;

        this.addEvents('itemmouseenter','itemmouseleave','loadstore');
        this.addEvents({
            'showaddressclicked': true,
        });

        this.renderers = {
            unitType: function(value) {
                var index = MAD.app.unitTypeStore.find('unit_type_id', value);
                if (index > -1) {
                    return MAD.app.unitTypeStore.getAt(index).get('unit_type_description');
                } else {
                    return value;
                }
    
            },
        };

        var gridDefaults = {
            pageSize: this.pageSize,
            hideHeaders: true,
            disableSelection: true,
            border: false,
            viewConfig: {
                stripeRows: false
            },
            listeners: {
                'itemmouseenter': function(gridview, record, element, index, event, eOpts) {
                    this.highlightRecord(record, false);
                    this.fireEvent('itemmouseenter', gridview, record, element, index, event, eOpts);
                },
                'itemmouseleave': function (gridview, record, element, index, event, eOpts) {
                    this.fadeHighlight();
                    this.fireEvent('itemmouseleave', gridview, record, element, index, event, eOpts);
                },
                scope: this
            }
        };

        var proxyDefaults = {
            type: 'ajax',
            method: 'GET',
            reader: {
                type: 'json',
                    root: 'results',
                    totalProperty: 'count'
            }
        };

        var storeDefaults = {
            autoLoad: false,
            pageSize: this.pageSize,
            listeners: {
                'load': function(store, records, success, eOpts) {
                    this.fireEvent('loadstore', store, records, success, eOpts);
                },
                scope: this
            }
        };

        var toolbarDefaults = {
            xtype: 'pagingtoolbar',
            dock: 'bottom',
            displayInfo: true
        };

        var addressStore = new Ext.data.Store(Ext.apply({
            storeId: 'AddressSearchResultsStore',
            proxy: Ext.apply({
                url: MAD.config.Urls.AddressSearch
            }, proxyDefaults),
            model: 'MAD.model.AddressSearch'
        }, storeDefaults));

        var parcelStore = new Ext.data.Store(Ext.apply({
            storeId: 'ParcelSearchResultsStore',
            proxy: Ext.apply({
                url: MAD.config.Urls.ParcelSearch
            }, proxyDefaults),
            model: 'MAD.model.LandParcelRecord'
        }, storeDefaults));

        this.reportContainer = Ext.create('Ext.container.Container', {
            layout: 'fit'
        });

        this.lineageHeader = Ext.create('Ext.panel.Panel', {
            tpl: MAD.config.Tpls.lineageReportHeader,
            bodyStyle: 'padding:7px;',
            border: false
        });

        var updateGridToolbar = function (grid) {
            var toolbars = grid.query('toolbar');
            if (toolbars.length > 0) {
                toolbars[0].update({ count: grid.store.count() });
            }
        };

        this.crAddressesGrid = Ext.create('Ext.grid.Panel', {
            id: 'CRAddressesGrid',
            title: 'Addresses',
            border: false,
            layout: 'fit',
            disableSelection: true,
            viewConfig: { trackOver: false },
            emptyText: 'No records found. Lineage is not supported prior to February 15, 2012.',
            dockedItems: [{
                xtype: 'toolbar',
                style: 'font-size:10px;',
                tpl: '{count} row(s)',
                layout: 'fit',
                height: 18,
                dock: 'bottom'
            }],
            store: Ext.create('Ext.data.Store', {
                model: 'MAD.model.AddressesFlat',
                proxy: {
                    type: 'ajax',
                    method: 'GET',
                    reader: {
                        type: 'json',
                        root: 'returnObj',
                    },
                    url: MAD.config.Urls.ChangeRequestAddresses,
                },
                listeners: {
                    'load': function (store, records, success, operation) {
                        if (!store.proxy.reader.rawData) {
                            store.load();
                        }
                        var changeRequest = store.proxy.reader.rawData.changeRequest;
                        this.lineageHeader.update(changeRequest);
                        Ext.each(records, function(record) {
                            record.set('report_change_request_id', changeRequest.change_request_id);
                        }, this);
                    },
                    'datachanged': function () {
                        updateGridToolbar(this.crAddressesGrid);
                    },
                    scope: this,
                }
            }),
            columns: [
                { text: 'Address',  dataIndex: 'full_street_name', flex: 4, 
                    renderer: function(value, metaData, record) {
                        var address;
                        if (record.get('address_type') !== 'Landmark Address') {
                            address = [
                                record.get('base_address_num'),
                                record.get('base_address_suffix'),
                                value,
                            ].filter(part => !!part).join(' ');
                        } else {
                            address = record.get('complete_landmark_name');
                        }
                        return '<a class="address-information-link" href="#">' + address + '</a>';
                    } 
                },
                { text: "Type", dataIndex: 'unit_type_id', flex: 1, renderer: this.renderers.unitType },
                { text: "Unit #", dataIndex: 'unit_num', flex: 2,
                    renderer: function(value) {
                        var num = value ? value : "";
                        return Ext.util.Format.trim(num);
                    } 
                },
                { text: 'Base/Unit', dataIndex: 'address_base_flg', flex: 1, 
                    renderer: function(value) {
                        if (value) {
                            return 'base';
                        } else {
                            return 'unit';
                        }
                    }
                },
                { text: 'Change Type', dataIndex: 'change_type', flex: 2 },
            ],
            listeners: {
                'itemclick': function (view, record, item, index, e) {
                    var className = e.getTarget().className;
                    if (className === 'address-information-link') {
                        this.fireEvent('addresslinkclicked', record);
                    }
                },
                scope: this,
            },
        });

        this.lineageGrid = Ext.create('Ext.grid.Panel', {
            id: 'LineageGrid',
            title: 'Parcel Links',
            border: false,
            layout: 'fit',
            disableSelection: true,
            viewConfig: { trackOver: false },
            dockedItems: [{
                xtype: 'toolbar',
                style: 'font-size:10px;',
                tpl: '{count} row(s)',
                layout: 'fit',
                height: 18,
                dock: 'bottom'
            }],
            emptyText: 'No parcel links were modified in this change request.',
            store: Ext.create('Ext.data.Store', {
                model: 'MAD.model.AddressesFlat',
                proxy: {
                    type: 'ajax',
                    method: 'GET',
                    reader: {
                        type: 'json',
                        root: 'addressesFlat'
                    },
                    url: MAD.config.Urls.Lineage
                },
                listeners: {
                    'load': function (store, records, success, operation) {
                        var changeRequest = store.proxy.reader.rawData.changeRequest;
                        this.lineageHeader.update(changeRequest);
                        Ext.each(records, function(record) {
                            record.set('report_change_request_id', changeRequest.change_request_id);
                        }, this);
                    },
                    'datachanged': function () {
                        updateGridToolbar(this.lineageGrid);
                    },
                    scope: this,
                }
            }),
            columns: [
                { text: 'APN',  dataIndex: 'blk_lot', flex: 3,
                    renderer: function (value) {
                        return '<a class="property-information-link" href="#">' + value + '</a>';
                    }
                },
                { text: 'Address',  dataIndex: 'full_street_name', flex: 4, 
                    renderer: function(value, metaData, record) {
                        var address;
                        if (record.get('address_type') !== 'Landmark Address') {
                            address = [
                                record.get('base_address_num'),
                                record.get('base_address_suffix'),
                                value,
                            ].filter(part => !!part).join(' ');
                        } else {
                            address = record.get('complete_landmark_name');
                        }
                        return '<a class="address-information-link" href="#">' + address + '</a>';
                    } 
                },
                { text: 'Base/Unit', dataIndex: 'address_base_flg', flex: 2, 
                    renderer: function(value) {
                        if (value) {
                            return 'base';
                        } else {
                            return 'unit';
                        }
                    }
                },
                { text: "Type", dataIndex: 'unit_type_id', flex: 1, renderer: this.renderers.unitType, hidden: true },
                { text: "Unit #", dataIndex: 'unit_num', flex: 2, renderer: function(value) {
                        var num = value ? value : "";
                        return Ext.util.Format.trim(num);
                    } 
                },
                { header: "Linked", dataIndex: 'address_x_parcel_create_tms', flex: 5,
                    renderer: function (rawValue, metaData, record) {
                        var value = rawValue ? Ext.util.Format.formatDateDefault(rawValue) : rawValue;
                        var displayValue = '<span style="color:green;">' + value + '</span>';
                        if (!MAD.utils.isPlausibleTime(rawValue)) {
                            displayValue = 'initial load';
                        } else if (record.get('report_change_request_id') !== record.get('address_x_parcel_activate_change_request_id')) {
                            displayValue = '<a class="lineage-create-link" href="#">' + value + '</a>';
                        }
                        return displayValue;
                    }
                },
                { header: "Unlinked", dataIndex: 'address_x_parcel_retire_tms', flex: 5,
                    renderer: function (rawValue, metaData, record) {
                        var value = rawValue ? Ext.util.Format.formatDateDefault(rawValue) : rawValue;
                        var displayValue = '<span style="color:green;">' + value + '</span>';
                        if (!rawValue) {
                            displayValue = '';
                        } else if (record.get('report_change_request_id') !== record.get('address_x_parcel_retire_change_request_id')) {
                            displayValue = '<a class="lineage-retire-link" href="#">' + value + '</a>';
                        }
                        return displayValue;
                    }
                }
            ],
            listeners: {
                'itemclick': function (view, record, item, index, e) {
                    var className = e.getTarget().className;
                    if (className === "property-information-link") {
                        // include retired
                        this.showSearchResults("ParcelSearch", record.get('blk_lot'), true, true);
                    } else if (className === 'lineage-create-link' || className === 'lineage-retire-link') {
                        this.showLineage(record.get('address_x_parcel_id'), className==='lineage-create-link'?'create':'retire');
                    } else if (className === 'address-information-link') {
                        this.fireEvent('addresslinkclicked', record);
                    }
                },
                scope: this,
            },
        });

        this.changeReportTabs = Ext.create('Ext.tab.Panel', {
            border: false,
            layout: 'fit',
            items: [
                this.crAddressesGrid,
                this.lineageGrid,
            ],
            listeners: {
                'click': {
                    element: 'el',
                    fn: function(el) {
                        switch (el.target.innerText) {
                            case 'Addresses':
                                this.showCrAddresses(this.lineageHeader.data.change_request_id);
                                break;
                            case 'Parcel Links':
                                this.showLineageByCr(this.lineageHeader.data.change_request_id);
                                break;
                        }
                    },
                },
                scope: this,
            },
        });

        this.items = [
            Ext.create('Ext.grid.GridPanel', Ext.apply({
                id: 'AddressSearchResultsGrid',
                columns: [{
                    flex: 1,
                    renderer: Ext.bind(function (value, p, record) {
                        return MAD.config.Tpls.addressDetailsSearchResults.apply(Ext.apply({
                                isRequestor: this.user.isRequestor(),
                                isSearchResult: true
                            }, record.data));
                    }, this)
                }],
                store: addressStore,
                dockedItems: [Ext.apply({
                    store: addressStore
                }, toolbarDefaults)]
            }, gridDefaults)),
            Ext.create('Ext.grid.GridPanel', Ext.apply({
                id: 'ParcelSearchResultsGrid',
                columns: [{
                    flex: 1,
                    renderer: function (value, p, record) {
                        return MAD.config.Tpls.parcelSearchResults.apply(record.data);
                    }
                }],
                store: parcelStore,
                dockedItems: [Ext.apply({
                    store: parcelStore
                }, toolbarDefaults)]
            }, gridDefaults)),
            Ext.create('Ext.panel.Panel', {
                id: 'ChangeReport',
                dockedItems: [this.lineageHeader],
                items: [this.changeReportTabs],
                layout: 'fit',
            }),
            this.reportContainer
        ];

        this.callParent(arguments);
    },

    switchResultsTypeTo: function (resultsType) {
        var grid,
            activeItem,
            title,
            url,
            width = 300;

        switch (resultsType) {
            case 'AddressSearch':
                url = MAD.config.Urls.AddressSearch;
                grid = this.query('#AddressSearchResultsGrid')[0];
                title = 'Addresses';
                break;
            case 'ParcelToAddressesSearch':
                url = MAD.config.Urls.ParcelToAddresses;
                grid = this.query('#AddressSearchResultsGrid')[0];
                title = 'Addresses';
                break;
            case 'ParcelSearch':
                url = MAD.config.Urls.ParcelSearch;
                grid = this.query('#ParcelSearchResultsGrid')[0];
                title = 'Parcels';
                break;
            case 'AddressToParcelsSearch':
                url = MAD.config.Urls.AddressToParcels;
                grid = this.query('#ParcelSearchResultsGrid')[0];
                title = 'Parcels';
                break;
            case 'PointToParcelsSearch':
                url = MAD.config.Urls.PointToParcels;
                grid = this.query('#ParcelSearchResultsGrid')[0];
                title = 'Parcels';
                break;
            case 'CRAddresses':
                url = MAD.config.Urls.ChangeRequestAddresses;
                grid = this.crAddressesGrid,
                title = 'Change Report';
                width = 560;
                activeItem = this.query("#ChangeReport")[0];
                this.changeReportTabs.setActiveTab(this.crAddressesGrid);
                break;
            case 'Lineage':
                url = MAD.config.Urls.Lineage;
                grid = this.lineageGrid,
                title = 'Change Report';
                width = 560;
                activeItem = this.query("#ChangeReport")[0];
                this.changeReportTabs.setActiveTab(this.lineageGrid);
                break;
        }
        this.setWidth(width);
        grid.store.proxy.url = url;
        this.reportContainer.removeAll();
        this.layout.setActiveItem(activeItem ?? grid);
        this.setTitle(title);
        return grid;
    },

    setUser: function(user) {
        this.user = user;
        if (this.layout.getActiveItem().getView) {
            this.layout.getActiveItem().getView().refresh();
        }
    },

    showSearchResults: function(resultsType, queryValue, includeRetired, exact) {
        var grid = this.switchResultsTypeTo(resultsType);
        Ext.apply(grid.store.proxy.extraParams, {
            query: queryValue,
            includeRetired: includeRetired,
            exact: exact
        });
        grid.store.loadPage(1);
    },

    showCrAddresses: function (crId) {
        const previousUrl = this.crAddressesGrid.store.proxy.url;
        const grid = this.switchResultsTypeTo('CRAddresses');
        grid.store.proxy.url += crId;
        if (grid.store.proxy.url !== previousUrl) {
            grid.store.load();
        }
    },

    showLineage: function (axpId, type) {
        const previousUrl = this.lineageGrid.store.proxy.url;
        const grid = this.switchResultsTypeTo('Lineage');
        grid.store.proxy.url = MAD.config.Urls.Lineage + axpId + '/' + type + '/';
        if (grid.store.proxy.url !== previousUrl) {
            grid.store.load();
        }
    },

    showLineageByCr: function (changeRequestId) {
        const previousUrl = this.lineageGrid.store.proxy.url;
        const grid = this.switchResultsTypeTo('Lineage');
        grid.store.proxy.url = MAD.config.Urls.LineageByChangeRequest + changeRequestId + '/';
        if (grid.store.proxy.url !== previousUrl) {
            grid.store.load();
        }
    },

    showReport: function (report) {
        this.reportContainer.removeAll();
        this.reportContainer.add(report);
        this.setWidth(480);
        this.layout.setActiveItem(this.reportContainer);
        this.setTitle('Address Details');
    },

    crLinkClicked: function(successMessageId) {
        const baseAddrId = (
            this.crAddressesGrid.store.data.items.length
            ? this.crAddressesGrid.store.data.items[0].data.address_base_id
            : this.lineageGrid.store.data.items[0].data.address_base_id
        );
        this.fireEvent(
            'showaddressclicked',
            baseAddrId,
            this.lineageHeader.data.change_request_id,
            successMessageId,
        );
    },

    addressLinkClicked: function (successMessageId) {
        this.fireEvent(
            'showaddressclicked',
            this.reportContainer.getComponent(0).addressId,
            null,
            successMessageId,
        );
    },

    highlightRecord: function (record, autoFade) {
        var view = this.layout.getActiveItem().getView(),
            node = view.getNode(record)
            rowForHighlight = Ext.get(node).first();

        if (this.highlightedRow) {
            this.highlightedRow.stopAnimation();
        }
        rowForHighlight.setStyle('backgroundColor', '#EFEFEF');
        this.highlightedRow = rowForHighlight;
        if (autoFade) {
            this.fadeHighlight();
        }
    },

    fadeHighlight: function () {
        if (this.highlightedRow) {
            this.highlightedRow.animate({
                duration: 1500,
                to: {
                    backgroundColor: '#FFFFFF'
                },
                listeners: {
                    'afteranimate': function () {
                        if (this.highlightedRow) {
                            this.highlightedRow.setStyle('backgroundColor', '#FFFFFF');
                            this.highlightedRow = null;
                        }
                    },
                    scope: this
                }
            });
        }
    }
});
