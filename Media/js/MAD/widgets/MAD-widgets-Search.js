Ext.define('MAD.widgets.Search', {
    extend: 'Ext.panel.Panel',

    layout: {
        type: 'hbox',
        align: 'top'
    },
    // This widget contain a combobox and a checkbox... all these parms in one handy location.
    height: 40,
    width: 550,
    minWidth: 425,
    maxWidth: 550,
    baseCls: '',
    bodyStyle: 'padding-top:9px; padding-right:10px;',
    searchType: 'ParcelSearch',
    includeRetired: false,

    initComponent: function () {
        var comboBoxMinWidth = 300,
            comboBoxMaxWidth = 500,
            checkBoxWidth = 125,
            searchString = '',
            that = this;

        this.addEvents({
            'searchbuttonclicked': true,
            'addressselected': true,
            'parcelselected': true,
            'includeretiredchanged': true
        });

        this.proxies = {
            'AddressSearch': {
                type: 'ajax',
                url: MAD.config.Urls.AddressSearch,
                method: 'GET',
                reader: {
                    type: 'json',
                    root: 'results',
                    totalProperty: 'count'
                },
                model: 'MAD.model.AddressSearch'
            },
            'ParcelSearch': {
                type: 'ajax',
                url: MAD.config.Urls.ParcelSearch,
                method: 'GET',
                reader: {
                    type: 'json',
                    root: 'results',
                    totalProperty: 'count'
                },
                model: 'MAD.model.LandParcelRecord'
            }
        };

        this.comboBox = Ext.create('Ext.form.field.ComboBox', {
            store: Ext.create('Ext.data.Store', {
                proxy: this.proxies[this.searchType],
                pageSize: 10
            }),
            name: 'Search',
            emptyText: 'APN or Address or Landmark Name',
            minWidth: comboBoxMinWidth,
            maxWidth: comboBoxMaxWidth,
            listConfig: {
                loadingText: 'Searching...',
                emptyText: 'No matches found.',
                itemTpl: MAD.config.Tpls.searchComboBox
            },
            fieldLabel: 'Find',
            labelAlign: 'right',
            labelWidth: 50,
            labelStyle: 'font-weight:bold;color: rgb(21, 66, 139);font-size:14px;',
            typeAhead: false,
            anchor: '100%',
            queryParam: 'query',
            queryCaching: false,
            triggerCls: 'x-form-search-trigger',
            pageSize: 10,
            onTriggerClick: function () {
                that.fireEvent('searchbuttonclicked', that.searchType, this.getValue(), that.includeRetired);
            },
            listeners: {
                'beforeselect': function (combo) {
                    searchString = combo.getValue();
                },
                'select': function (combo, records, index) {
                    var eventName,
                        searchType = this.searchType;
                    combo.setValue(searchString);
                    if (searchType === 'AddressSearch') {
                        eventName = 'addressselected';
                    } else if (searchType === 'ParcelSearch') {
                        eventName = 'parcelselected';
                    }
                    this.fireEvent(eventName, records[0], that.includeRetired);
                },
                'beforequery': function (queryEvent) {
                    // If the query string starts with numbers
                    // and does not contain a space
                    // then assume parcel search
                    if (
                        MAD.utils.isParcelSearch(queryEvent.query)
                    ) {
                        this.searchType = 'ParcelSearch';
                    } else {
                        this.searchType = 'AddressSearch';
                    }
                    
                    var proxy = this.proxies[this.searchType];
                    proxy.extraParams = { includeRetired: that.includeRetired };
                    queryEvent.combo.store.setProxy(proxy);
                    return true;
                },
                'afterrender': function(combo, eopts) {
                    // Should the use click on the combo-box, we want to display the drop down.
                    combo.inputEl.on('click', function(){
                        combo.doQuery(combo.getValue());
                    })
                },
                scope: this
            }
        });

        this.checkBox = new Ext.form.field.Checkbox({
            checked: this.includeRetired,
            boxLabel: 'include historical',
            style: 'color:rgb(21, 66, 139);',
            padding: '2 0 0 10',
            width: checkBoxWidth,
            name: 'includeRetiredCheckbox',
            listeners: {
                change: function (checkbox, value) {
                    this.includeRetired = value;
                    this.fireEvent('includeretiredchanged', value);
                    this.comboBox.doQuery(this.comboBox.getValue());
                },
                scope: this
            }
        });

        this.items = [
            {
                xtype: 'container',
                layout: 'anchor',
                flex: 1,
                minWidth: this.comboBox.minWidth,
                items: [this.comboBox]
            },
            this.checkBox
        ];

        this.callParent(arguments);
    }
});
