Ext.define('MAD.widgets.ClusterReport', {
    extend: 'Ext.panel.Panel',

    layout: 'fit',
    data: [],

    initComponent: function() {
        this.addEvents({
            'viewreportclicked': true
        });

        // create the data store
        var store = new Ext.data.Store({
            model: 'MAD.model.ClusterReport',
            proxy: {
                type: 'ajax'
            }
        });

        // manually load local data
        store.loadData(this.data);
        // Server side clustering causes order to be unsorted - so we sort here.
        store.sort([{
            property : 'address',
            direction: 'ASC'
        }]);

        var addressRenderer = function(value, p, record, rowindex) {
            var r = '<span style="line-height: 16px;font-weight:normal;';
            if (record.get('validation_warning_count')) {
                r += 'background-image: url(' + MAD.config.Images.Error + ');background-repeat: no-repeat;background-position: right;padding-right: 24px;'
            }
            r += '" class="search-result-title';
            if (record.get('retire_tms')) {
                r += ' retired';
            }
            r += '">';
            return r + value + " - <a href='#' class='view-report-link'>Details</a></span>";
        };

        var grid = new Ext.grid.GridPanel({
            store: store,
            hideHeaders: true,
            columns: [{ header: 'Address', sortable: true, dataIndex: 'address', width: 250, renderer: addressRenderer, fixed: true }],
            forceFit: true,
            autoScroll: true,
            cls: 'cluster-report',
            listeners: {
                'itemmousedown': function(grid, record, el, index, e, eOpts) {            
                    if (e.target.className == 'view-report-link') {
                        this.fireEvent("viewreportclicked", this, index, record);
                    }
                },
                scope: this
            }
        });

        Ext.apply(this, {
            height: 268,
            width: 243,
            autoScroll: true,
            layout: 'fit',
            border: false,
            items: [
                grid
            ]
        });

        this.callParent(arguments);
    }
});