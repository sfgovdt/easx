Ext.define('MAD.widgets.AddressReport', {
    extend: 'Ext.panel.Panel',
    requires: [
        'Ext.ux.grid.FiltersFeature',
        'Ext.ux.RowExpander'
    ],

    layout: 'card',
    addressId: '',
    addressJson: '',
    user: null,
    showHistoricalData: false,
    renderers: {
        disposition: function(value, p, record, rowIndex) {
            var index = MAD.app.dispositionStore.find('disposition_code', value);
            if (index > -1) {
                return MAD.app.dispositionStore.getAt(index).get('disposition_description');
            } else {
                return value;
            }

        },
        unitType: function(value, p, record, rowIndex) {
            var index = MAD.app.unitTypeStore.find('unit_type_id', value);
            if (index > -1) {
                return MAD.app.unitTypeStore.getAt(index).get('unit_type_description');
            } else {
                return value;
            }

        },
        isBase: function(value, p, record, rowIndex) {
            if (value) {
                return 'base';
            } else {
                return 'unit';
            }

        },
        unitInfo: function(value, p, record, rowIndex) {
            var num = record.get("unit_num") !== null ? record.get("unit_num") : "";
            return Ext.util.Format.trim(num);
        }
    },

    initComponent: function() {
        if (!this.user) {
            this.user = MAD.model.User;
        }

        this.user.on('logout', this.updateHeaderContent, this);
        this.user.on('login', this.updateHeaderContent, this);

        this.on('beforedestroy', function () {
            this.user.un('logout', this.updateHeaderContent, this);
            this.user.un('login', this.updateHeaderContent, this);
        }, this);

        this.addEvents({
            'addressReportPopulated': true,
            'editButtonClicked': true,
            'showmapdatatoggled': true,
            'showaddressclicked': true
        });
    
        var disableControls = true;
        if (this.user.isRequestor()) {
            disableControls = false;
        }

        var updateGridToolbar = function (grid) {
            var toolbars = grid.query('toolbar');
            if (toolbars.length > 0) {
                var count = (
                    grid.store.count ?
                    grid.store.count() :
                    grid.store.tree.root.childNodes.length
                );
                toolbars[0].update({ count });
            }
        };

        this.parcelStore = Ext.create('Ext.data.Store', {
            proxy: {
                type: 'ajax',
                url: MAD.config.Urls.ParcelsForAddressReport + this.addressId,
                method: 'GET',
                reader: {
                    type: 'json',
                    root: 'results'
                }
            },
            listeners: {
                'beforeload': function () {
                    this.parcelGridPanel.setLoading(true);
                },
                'load': function () {
                    this.parcelGridPanel.setLoading(false);
                    if (!this.showHistoricalData) {
                        this.parcelStore.historicalData = [];
                        this.cacheHistoricalData(this.parcelStore, 'address_x_parcel_retire_tms');
                    }
                },
                'datachanged': function () {
                    updateGridToolbar(this.parcelGridPanel);
                },
                scope: this
            },
            model: 'MAD.model.UnitAddressReportRecord'
        });

        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true //only filter locally
        };

        var gridToolbarCfg = {
            xtype: 'toolbar',
            style: 'font-size:10px;',
            tpl: '{count} row(s)',
            layout: 'fit',
            height: 18,
            dock: 'bottom'
        };

        this.parcelGridPanel = new Ext.grid.GridPanel({
            features : [filtersCfg],
            border: true,
            loadMask: true,
            store: this.parcelStore,
            columns: {
                defaults: { filterable: true },
                items: [
                    { header: "APN", dataIndex: 'blk_lot', flex: 5,
                        renderer: function (value) {
                            return '<a class="property-information-link" href="#">' + value + '</a>';
                        }
                    },
                    { header: "Base/Unit", dataIndex: 'address_base_flg', flex: 4, renderer: this.renderers.isBase },
                    { header: "Type", dataIndex: 'unit_type_id', flex: 4, renderer: this.renderers.unitType, hidden: true },
                    { header: "Unit #", dataIndex: 'unit_num', flex: 4, renderer: this.renderers.unitInfo },
                    { header: "Status", dataIndex: 'disposition_code', flex: 4, renderer: this.renderers.disposition, hidden: true },
                    { header: "Linked", dataIndex: 'address_x_parcel_create_tms', flex: 7,
                        renderer: function (value) {
                            var displayValue;
                            if (MAD.utils.isPlausibleTime(value)) {
                                value = Ext.util.Format.formatDateDefault(value);
                                return '<a class="lineage-create-link" href="#">' + value + '</a>';
                            } else {
                                return displayValue = 'initial load';
                            }
                        }
                    },
                    { header: "Unlinked", dataIndex: 'address_x_parcel_retire_tms', flex: 7,
                        renderer: function (value) {
                            if (!value) {
                                return '';
                            }
                            if (MAD.utils.isPlausibleTime(value)) {
                                value = Ext.util.Format.formatDateDefault(value);
                                return '<a class="lineage-retire-link" href="#">' + value + '</a>';
                            } else {
                                return 'initial load';
                            }
                        }
                    }
                ]
            },
            title: 'Parcels',
            forceFit: true,
            viewConfig: {
                emptyText: '<div class="x-grid-empty">This address is not linked to any parcels.</div>',
                deferEmptyText: true
            },
            dockedItems: [gridToolbarCfg],
            listeners: {
                'itemclick': function (view, record, item, index, e) {
                    var className = e.getTarget().className;
                    if (className === "property-information-link") {
                        // include retired
                        this.fireEvent("searchitemselected", "ParcelSearch", record.get('blk_lot'), true);
                    } else if (className === 'cr-addresses-link') {
                        this.fireEvent("craddresseslinkclicked", record.get('change_request_id'));
                    } else if (className === 'lineage-create-link' || className === 'lineage-retire-link') {
                        this.fireEvent("lineagelinkclicked", record.get('address_x_parcel_id'), className==='lineage-create-link'?'create':'retire');
                    }
                },
                scope: this
            }
        });

        this.unitAddressStore = Ext.create('Ext.data.TreeStore', {
            proxy: {
                type: 'ajax',
                url: MAD.config.Urls.UnitsForAddressReport + this.addressId,
                method: 'GET',
                reader: {
                    type: 'json',
                    root: 'children',
                }
            },
            listeners: {
                'beforeload': function () {
                    if (this.unitsGridPanel) {
                        this.unitsGridPanel.setLoading(true);
                    }
                },
                'load': function () {
                    this.unitsGridPanel.setLoading(false);
                    if (!this.showHistoricalData) {
                        this.unitAddressStore.historicalData = [];
                        this.cacheHistoricalData(this.unitAddressStore, 'retire_tms');
                    }
                    this.unitsGridPanel.getRootNode().expandChildren(true);
                },
                'datachanged': function () {
                    if (this.unitsGridPanel) {
                        updateGridToolbar(this.unitsGridPanel);
                    }
                },
                scope: this
            },
            root: {
                expanded: true,
            },
            model: 'MAD.model.UnitAddressReportRecord'
        });

        this.unitsGridPanel = new Ext.tree.Panel({
            // features : [filtersCfg],
            border: true,
            loadMask: true,
            store: this.unitAddressStore,
            columns: {
                defaults: { filterable: true },
                items: [
                    { header: "Base/Unit", dataIndex: 'address_base_flg', flex: 4, renderer: this.renderers.isBase, xtype: 'treecolumn' },
                    { header: "Type", dataIndex: 'unit_type_id', flex: 4, renderer: this.renderers.unitType },
                    { header: "Unit #", dataIndex: 'unit_num', flex: 4, renderer: this.renderers.unitInfo },
                    { header: "Status", dataIndex: 'disposition_code', flex: 4, renderer: this.renderers.disposition, hidden: true },
                    { header: "Created", dataIndex: 'create_tms', flex: 5,
                        renderer: function (value) {
                            const formatted = Ext.util.Format.jsonDateToDisplayDate(value);
                            if (!formatted || formatted === 'initial load') {
                                return formatted;
                            }
                            return `<a class="cr-addresses-created-link" href="#" role="button">${formatted}</a>`;
                        },
                    },
                    { header: "Retired", dataIndex: 'retire_tms', flex: 5,
                        renderer: function (value) {
                            const formatted = Ext.util.Format.jsonDateToDisplayDate(value);
                            if (!formatted || formatted === 'initial load') {
                                return formatted;
                            }
                            return `<a class="cr-addresses-retired-link" href="#" role="button">${formatted}</a>`;
                        },
                    },
                ]
            },
            title: 'Units',
            forceFit: true,
            viewConfig: {
                emptyText: '<div class="x-grid-empty">This address does not have any units.</div>',
                deferEmptyText: true,
                rootVisible: false,
            },
            listeners: {
                'itemclick': function (view, record, item, index, e) {
                    var className = e.getTarget().className;
                    if (className === 'cr-addresses-created-link') {
                        this.fireEvent(
                            'craddresseslinkclicked',
                            record.get('activate_change_request_id'),
                        );
                    } else if (className === 'cr-addresses-retired-link') {
                        this.fireEvent(
                            'craddresseslinkclicked',
                            record.get('retire_change_request_id'),
                        );
                    }
                },
                scope: this,
            },
            dockedItems: [gridToolbarCfg]
        });

        this.historyStore = Ext.create('MAD.model.AbstractStore', {
            proxy: {
                type: 'ajax',
                url: MAD.config.Urls.History + this.addressId,
                method: 'GET',
                reader: {
                    type: 'json',
                    root: 'returnObj',
                }
            },
            listeners: {
                'beforeload': function () {
                    this.historyGridPanel.setLoading(true);
                },
                'load': function () {
                    this.historyGridPanel.setLoading(false);
                },
                'datachanged': function () {
                    updateGridToolbar(this.historyGridPanel);
                },
                scope: this
            },
            fields: [
                { name: "change_request_id", defaultValue: null },
                { name: "name", defaultValue: "" },
                { name: "create_tms", defaultValue: null },
                { name: "requestor_name", defaultValue: "" },
                { name: "requestor_email", defaultValue: "" },
                { name: "requestor_comment", defaultValue: "" },
                { name: "reviewer_name", defaultValue: "" },
                { name: "reviewer_email", defaultValue: "" },
                { name: "reviewer_comment", defaultValue: "" },
            ],
        });

        this.historyGridPanel = new Ext.grid.GridPanel({
            border: true,
            loadMask: true,
            store: this.historyStore,
            plugins: [{
                ptype: 'rowexpander',
                rowBodyTpl: [
                    '<div style="padding: 10px 20px;">',
                        '<div style="float:left;color:#777777;">Requestor:</div>',
                        '<div style="color:#15428B;padding-left:5px">&nbsp;{requestor_name} ({requestor_email})</div>',
                        '<div style="clear:both;"></div>',
                        '<div style="float:left;color:#777777;">Comments:</div>',
                        '<div style="color:#15428B;padding-left:5px;white-space:pre-wrap;">&nbsp;{requestor_comment:clearNull()}</div>',
                        '<div style="clear:both;margin-bottom:12px;"></div>',
                        '<div style="float:left;color:#777777;">Reviewer:</div>',
                        '<div style="color:#15428B;padding-left:5px;">&nbsp;{reviewer_name} ({reviewer_email})</div>',
                        '<div style="clear:both;"></div>',
                        '<div style="float:left;color:#777777;">Comments:</div>',
                        '<div style="color:#15428B;padding-left:5px;white-space:pre-wrap;">&nbsp;{reviewer_comment:clearNull()}</div>',
                        '<div style="clear:both;"></div>',
                    '</div>'
                ]
            }],
            columns: {
                defaults: { filterable: true },
                items: [
                    { header: 'Title', dataIndex: 'name' },
                    { header: 'Created', dataIndex: 'create_tms',
                        renderer: function (value, metadata, record) {
                            const formatted = Ext.util.Format.formatDateDefault(value);
                            if (!record.data.requestor_name) {
                                return formatted;
                            }
                            return formatted ? '<a class="cr-addresses-link" href="#" role="button">' + formatted + '</a>' : '';
                        },
                    }
                ]
            },
            title: 'Changes',
            forceFit: true,
            viewConfig: {
                emptyText: '<div class="x-grid-empty">This address is not included in any change request.</div>',
                deferEmptyText: true,
            },
            dockedItems: [gridToolbarCfg],
            listeners: {
                'itemclick': function (view, record, item, index, e) {
                    var className = e.getTarget().className;
                    if (className === 'cr-addresses-link') {
                        this.fireEvent(
                            'craddresseslinkclicked',
                            record.get('change_request_id'),
                        );
                    }
                },
                scope: this,
            },
        });

        this.summary = new Ext.Panel({
            title: 'More',
            layout: 'fit',
            border: false,
            id: 'addr-report-summary',
            autoScroll: true,
            html: ''
        });

        this.headerContent = Ext.create('Ext.container.Container', {
            layout: 'fit',
            flex: 1,
            html: ''
        });

        this.linkButton = new Ext.Panel({
            html: (
                '<div class="tooltip" style="height: 30px;">'
                    + '<span class="tooltip-text">Copy hyperlink</span>'
                    + '<i role="button" class="link-icon" '
                    +    'onclick="MAD.app.results.addressLinkClicked(\'address-report-copy-success-message\')">'
                    + '</i>'
                    + '<span aria-hidden="true" class="copy-success-message"'
                    +       'id="address-report-copy-success-message">Copied to clipboard'
                    + '</span>'
                + '</div>'
            ),
            border: false,
        });

        this.showHistoryCheckbox = Ext.create('Ext.form.field.Checkbox', {
            checked: this.showHistoricalData,
            boxLabel: 'historical',
            style: 'margin-left: 8px;',
            name: 'historicdata',
            listeners: {
                change: function (checkbox, value) {
                    if (value !== this.showHistoricalData) {
                        this.toggleHistoricalData(value);
                    }
                },
                scope: this
            }
        });

        this.showMapData = Ext.create('Ext.form.field.Checkbox', {
            checked: true,
            boxLabel: 'on map',
            style: 'margin-left: 8px;',
            name: 'mapdata',
            listeners: {
                change: function (checkbox, value) {
                    this.fireEvent('showmapdatatoggled', value);
                },
                scope: this
            }
        });

        this.contentPanel = new Ext.Panel({
            layout: 'fit',
            border: false,
            dockedItems: [
                new Ext.Panel({
                    dock: 'top',
                    bodyStyle: 'padding:5px;',
                    border: false,
                    layout: {
                        type: 'hbox',
                        align: 'bottom'
                    },
                    items: [
                        this.headerContent, 
                        Ext.create('Ext.container.Container', {
                            layout: {
                                type: 'vbox',
                                align: 'right'
                            },
                            items: [
                                this.linkButton,
                                {
                                    xtype:'fieldset',
                                    title: 'Show',
                                    style: 'padding-top:3px;padding-left:3px;',
                                    layout: {
                                        type: 'vbox',
                                        align: 'left'
                                    },
                                    items :[
                                        this.showMapData,
                                        this.showHistoryCheckbox,
                                    ]
                                }
                            ]
                        })
                    ]
                })
            ],
            items: [new Ext.tab.Panel({
                deferredRender: true,
                border: false,
                items: [
                    this.parcelGridPanel,
                    this.unitsGridPanel,
                    this.historyGridPanel,
                    this.summary
                ],
                listeners: {
                    // forcing an update of the grid stores after tab change ensures that
                    // grid contents (including empty message) are visible when shown
                    // (see EAS-519)
                    'tabchange': {
                        fn: function () {
                            this.toggleHistoricalData(this.showHistoricalData);
                        },
                        scope: this
                    }
                }
            })]
        });

        this.messagePanel = new Ext.Panel({
            html: '<div class="address-report-loading">Loading</div>',
            layout: 'fit',
            border: false
        });

        Ext.apply(this, {
            cls: 'address-report',
            border: false,
            items: [
                this.messagePanel,
                this.contentPanel
            ]
        });

        Ext.Ajax.request({
            url: MAD.config.Urls.AddressInfo + this.addressId,
            disableCaching: true,
            success: this.populateAddressReport,
            scope: this
        });

        this.callParent(arguments);
    },

    updateHeaderContent: function() {
        this.addressJson.isRequestor = this.user.isRequestor();
        this.headerContent.update(MAD.config.Tpls.addressDetailsSearchResults.apply(this.addressJson));
    },

    populateAddressReport: function(result) {
        if (!this.isDestroyed) {
            var response = Ext.JSON.decode(result.responseText);

            const addressJson = {
                ...response.returnObj,
                // Wrap each item of array in an object for templating purposes.
                landmark_aliases: response.returnObj.landmark_aliases.map(
                    a => { return { landmark_alias: a } }
                ),
            };

            this.addressJson = addressJson;

            if (this.isRetired()) {
                this.showHistoricalData = true;
                this.showHistoryCheckbox.setValue(true);
            }

            this.summary.update(MAD.config.Tpls.addressReportSummary.apply(addressJson));

            this.updateHeaderContent();

            this.layout.setActiveItem(this.contentPanel);

            this.parcelStore.load();
            this.historyStore.load();
            // Don't reload unitAddressStore: would generate new IDs in the tree,
            // breaking the pointers in the historical data cache.

            this.fireEvent('populateAddressReport', [addressJson.streetSegmentGeometry, addressJson.parcelFootprintGeometry]);
        }
    },

    toggleHistoricalData: function(showData) {
        var grids = [
            { grid: this.parcelGridPanel, retireField: 'address_x_parcel_retire_tms' },
            { grid: this.unitsGridPanel, retireField: 'retire_tms' }
        ];
        if (typeof showData === "undefined") {
            showData = !this.showHistoricalData;
        }

        Ext.each(grids, function(gridObj) {
            // Ugly code to ensure that filters stay in sync with content
            // simply removes all filters from the store and unchecks all
            // FiltersFeature menu items... please refactor
            if (gridObj.grid.filters) {
                gridObj.grid.store.clearFilter();
                gridObj.grid.filters.clearFilters();
            }

            const store = gridObj.grid.store;
            if (store.historicalData === undefined) {
                store.historicalData = [];
            }
            if (showData) {
                this.restoreHistoricalData(store);
            } else {
                this.cacheHistoricalData(store, gridObj.retireField);
            }
        }, this);

        this.showHistoricalData = showData;

        // ensure checkbox is in sync with data
        if (this.showHistoryCheckbox.getValue() !== showData) {
            this.showHistoryCheckbox.setValue(showData);
        }
    },

    restoreHistoricalData: function(store) {
        if (store.loading) {
            return;
        }
        if (store.tree) {
            store.historicalData.forEach(
                ({ parent, historicalChild }) => parent.appendChild(historicalChild)
            );
        } else {
            if (store.historicalData.length) {
                store.add(store.historicalData);
            }
            store.historicalData = [];
        }
        store.sort([{
            property: 'unit_num',
            direction: 'ASC',
        }]);
    },

    cacheHistoricalData: function (store, retireField) {
        if (store.tree) {
            if (store.historicalData.length) {
                store.historicalData.forEach(
                    ({ parent, historicalChild }) => parent.removeChild(historicalChild)
                );
                return;
            }
            store.historicalData = [];  // { parent, historicalChild }[]
            const root = store.getRootNode();
            var parentId = root.internalId;

            function traverse(record) {
                const parent = record.parentNode ?? root;

                if (record.get(retireField)) {
                    if (record.parentNode) {
                        parentId = record.parentNode?.internalId;
                    }
                    store.historicalData.push({ parent, historicalChild: record });
                }
                record.eachChild((child) => traverse(child));
            }

            traverse(store.getRootNode());
            store.historicalData.forEach(
                ({ parent, historicalChild }) => parent.removeChild(historicalChild)
            );
        } else {
            if (store.historicalData.length) {
                return;
            }
            var activeData = [];
            store.historicalData = [];

            store.each(function (record) {
                if (record.get(retireField)) {
                    store.historicalData.push(record);
                } else {
                    activeData.push(record);
                }
            }, this);

            // Here we remove all then readd the active records because
            // removing the historical data is very slow when there are 1000+
            // records, while removeAll is always fast.  Adding records in 
            // this volume is not nearly as slow
            store.removeAll();
            store.add(activeData);
        }
    },

    // one more of these and we should push into a proper model
    isRetired: function () {
        if (this.addressJson.retire_tms) {
            return true;
        }
        return false;
    }

});