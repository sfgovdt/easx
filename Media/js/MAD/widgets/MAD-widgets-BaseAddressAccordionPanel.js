Ext.define('MAD.widgets.BaseAddressAccordionPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.baseAddressAccordionPanel',
    autoScroll: true,
    baseAddressStore: null,

    initComponent: function () {

        Ext.apply(this, {
            region: 'center',
            border: true,
            bodyStyle: 'background-color: #DFE8F6;',
            layout: {
                type: 'accordion',
                titleCollapse: false,
                fill: false
            }
        });

        this.callParent(arguments);

    },

    collapseAll: function () {
        Ext.each(this.items.items, function (item, index, allItems) {
            item.collapse();
        }, this);
    },

    allCollapsed: function () {
        var allCollapsed = true;

        Ext.each(this.items.items, function (item, index, allItems) {
            if (!item.collapsed) {
                allCollapsed = false;
            }
        }, this);

        return allCollapsed;
    },

    unselectAll: function () {
        Ext.each(this.items.items, function (item, index, allItems) {
            item.unselect();
        }, this);
    },

    getFirst: function () {
        return this.items.items[0];
    }

});