Ext.define('MAD.widgets.AddressDetailPanel', {
    extend: 'Ext.TabPanel',
    alias: 'widget.addressDetailPanel',

    detailFormPanel: null,
    detailGridPanel: null,
    hidden: true,

    resetDetailPanel: function (newDetailFormPanel, newDetailGridPanel, newLandmarkAliasFormPanel, record) {
        this.removeAll();

        this.detailFormPanel = newDetailFormPanel;
        this.detailGridPanel = newDetailGridPanel;
        this.landmarkAliasFormPanel = newLandmarkAliasFormPanel;

        if (this.detailFormPanel !== null) {
            this.add(this.detailFormPanel);
            if (this.detailGridPanel !== null) {
                this.add(this.detailGridPanel);
            }
            this.show();
            if (record !== null) {
                if (!record.isValid()) {
                    this.setActiveTab(this.detailFormPanel);
                } else {
                    this.setActiveTab(this.detailGridPanel);
                }
            }
        } else {
            this.hide();
        }

        if (this.detailGridPanel !== null) {
            this.detailGridPanel.on('activate', function() {
                if (!this.detailGridPanel.checkColumnsEnabled) {
                    this.detailGridPanel.disableCheckColumns();
                }
            }, this);
        }

        if (
            this.landmarkAliasFormPanel !== null
            && MAD.model.User.isLandmarkEditor()
        ) {
            this.add(this.landmarkAliasFormPanel);
        }
    },

    initComponent: function () {

        Ext.apply(this, {
            border: false,
            collapsible: false
        });

        this.callParent(arguments);

    }
});
