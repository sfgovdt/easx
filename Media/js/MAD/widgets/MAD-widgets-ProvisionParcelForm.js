Ext.define('MAD.widgets.ProvisionParcelPanel', {
    extend: 'Ext.FormPanel',
    alias: 'widget.provisionParcelPanel',

    lonlat: null,
    border: false,
    frame: true,
    title: 'Provision Parcel',
    header: false,

    initComponent: function () {

        this.blockField = new Ext.form.TextField({
            name: 'block',
            fieldLabel: 'Block',
            allowBlank: false,
            vtype: 'BlockNumber'
        });

        this.lotField = new Ext.form.TextField({
            name: 'lot',
            fieldLabel: 'Lot',
            allowBlank: false,
            vtype: 'LotNumber'
        });

        var html =  Ext.String.format('To provision a parcel, you <b>must</b> send recorded documentation such as a recorded map or a declaration of covenants, conditions, and restrictions to <b>{0}</b> at the Department of Public Works.', MAD.config.Emails.ParcelsProvisioning);
        this.reminderPanel = new Ext.Panel({ html: html, baseCls: 'x-form-reminder-msg' });

        var config = {
            defaultType: 'textfield',
            defaults: { anchor: '-24' },
            fieldDefaults: {
                labelWidth: 80
            },
            items: [this.blockField, this.lotField, this.reminderPanel],
            buttons: [{
                text: 'Save',
                scope: this,
                handler: this.save
            },
                {
                    text: 'Cancel',
                    scope: this,
                    handler: this.cancel
                }]
        };

        Ext.apply(this, Ext.apply(this.initialConfig, config));

        this.on('afterlayout', function () {
            this.blockField.focus(false, 300);
        }, this, { single: true });

        this.callParent(arguments);
    },

    save: function () {
        this.fireEvent('provisionParcelExecute', this.blockField.getValue(), this.lotField.getValue(), this.lonlat);
    },

    cancel: function () {
        this.fireEvent('provisionParcelCancel');
    }

});