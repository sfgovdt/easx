Ext.define('MAD.widgets.LandmarkAliasFormPanel', {
    extend: 'Ext.FormPanel',
    alias: 'widget.landmarkAliasFormPanel',

    record: undefined,
    frame: false,
    title: 'Landmark Names',
    bodyStyle: 'background-color: #DFE8F6; padding: 5px;',
    autoScroll: true,
    layout: 'anchor',
    border: false,

    commitRecord: function () {
        this.getForm().updateRecord(this.record);
        this.record.validate();
        this.record.commit();
    },

    initComponent: function () {
        Ext.apply(this, {
            items: [
                this.landmarkTextField = new Ext.form.TextField({
                    anchor: '100%',
                    fieldLabel: 'Complete Landmark Name',
                    name: 'complete_landmark_name',
                    disabled: (status === 'retire'),
                    maxLength: 150,
                    blankText: 'Assigned by the EAS administrator',
                }),
                this.aliases = Ext.create('Ext.ux.form.MultiSelect', {
                    title: 'Landmark Names',
                    ddReorder: true,
                    store: this.record.data.landmark_aliases,
                    fieldLabel: 'Landmark Names',
                }),
                this.newAliasField = new Ext.form.TextField({
                    anchor: '100%',
                    maxLength: 150,
                    emptyText: 'Enter a new landmark name alias',
                    fieldStyle: { 'margin-left': "105px", 'width': "calc(100% - 105px)" },
                }),
                this.createAlias = new Ext.Button({
                    xtype: 'tbbutton',
                    icon: MAD.config.Images.Add,
                    tooltip: { text: 'Add a landmark name' },
                    handler: function (button, event) {
                        const newValue = this.newAliasField.getValue();
                        if (newValue) {
                            this.aliases.store.add({'field1': newValue});
                            this.newAliasField.setValue('');
                            // Trigger same update that watches reorders,
                            // otherwise this record is only patched in to the "memory store",
                            // not the real store.
                            this.aliases.fireEvent('change');
                        }
                    },
                    style: { 'margin-left': '105px' },
                    scope: this,
                }),
                this.removeAliases = new Ext.Button({
                    xtype: 'tbbutton',
                    icon: MAD.config.Images.Delete,
                    tooltip: { text: 'Remove the selected landmark name(s)' },
                    handler: function (button, event) {
                        var name;
                        this.aliases.value.forEach(deletedName => {
                            name = deletedName;
                            this.aliases.store.remove(
                                this.aliases.store.findRecord('field1', deletedName)
                            );
                        });
                        this.newAliasField.setValue(name);
                        this.aliases.clearValue();
                    },
                    scope: this,
                }),
            ],
        });

        this.landmarkTextField.on('afterrender', function () {
            this.loadData();
        }, this, { single: true });

        this.callParent(arguments);
    },

    loadData: function () {
        this.getForm().loadRecord(this.record);

        Ext.each(this.query('textfield'), function (item, index, allItems) {
            item.on('change', function () {
                this.commitRecord();
            }, this);
        }, this);
    },

    enableAll: function () {
        this.landmarkTextField.enable();
        this.aliases.enable();
        this.newAliasField.enable();
        this.createAlias.enable();
        this.removeAliases.enable();
    },

    disableAll: function () {
        this.landmarkTextField.disable();
        this.aliases.disable();
        this.newAliasField.disable();
        this.createAlias.disable();
        this.removeAliases.disable();
    },

    reconfigureStatus: function (status) {
        switch (status) {
            case 'edit':
                this.enableAll();
                this.record.set('retire_flg', false);
                this.commitRecord();
                break;
            case 'retire':
                this.disableAll();
                this.record.set('retire_flg', true);
                this.commitRecord();
                break;
        }
    },
});
