Ext.define('MAD.widgets.ChangeRequestBrowser', {
    extend: 'Ext.window.Window',

    myChangeRequests: false,

    title: 'Change Request Browser...',

    initComponent: function () {
        this.addEvents('changeRequestSelected');

        this.url = '';

        var openButtonText = '';

        if (this.myChangeRequests) {
            this.url = MAD.config.Urls.GetMyChangeRequests;
            openButtonText = 'Edit';
        } else {
            this.url = MAD.config.Urls.GetChangeRequestsForReview;
            openButtonText = 'Review';
        }

        this.store = Ext.create('Ext.data.Store', {
            proxy: {
                type: 'ajax',
                url: this.url,
                method: 'GET',
                reader: {
                    type: 'json',
                    root: 'returnObj.changeRequests'
                }
            },
            model: 'MAD.model.ChangeRequestRecord',
            autoLoad: true,
            pageSize: this.pageSize
        });

        // create the grid
        this.grid = new Ext.grid.GridPanel({
            store: this.store,
            columns: [
                { header: "Name", width: 250, dataIndex: 'name', sortable: true },
                { header: "Status", width: 75, dataIndex: 'status_description', sortable: true },
                { header: "Requestor Name", width: 100, dataIndex: 'requestor_name', sortable: true },
                { header: "Requestor Last Update", width: 175, dataIndex: 'requestor_last_update', sortable: true, renderer: Ext.util.Format.jsonDateToDisplayDate },
                { header: "Reviewer", width: 100, dataIndex: 'reviewer_name', sortable: true },
                { header: "Reviewer Last Update", width: 175, dataIndex: 'reviewer_last_update', sortable: true, renderer: Ext.util.Format.jsonDateToDisplayDate }
            ],
            singleSelect: true,
            tbar: [
                {
                    text: openButtonText,
                    iconCls: 'silk-table-edit',
                    id: 'cr-browser-open-btn',
                    disabled: true,
                    handler: function() {
                        this.openChangeRequest(this.grid.getSelectionModel().getSelection()[0]);
                    },
                    scope: this
                }
            ]
        });

        this.grid.getSelectionModel().on('selectionchange', function () {
            Ext.getCmp('cr-browser-open-btn').enable();
        }, this);

        this.grid.getView().on('itemdblclick', function (view, record, htmlElement, index, eventObject, options) {
            this.openChangeRequest(record);
        }, this);

        Ext.apply(this, {
            modal: true,
            layout: 'fit',
            height: Ext.getBody().getHeight() - 200,
            width: Ext.getBody().getWidth() - 200,
            items: [this.grid]
        });

        this.callParent(arguments);
    },

    openChangeRequest: function(record) {
        var reclaim = false;
        var revStatus = record.get('status_description');
        if ((revStatus == 'submitted' || revStatus == 'rejected') && this.myChangeRequests) {
            reclaim = true;
        }
        if (revStatus == 'submitted' && this.myChangeRequests) {
            Ext.MessageBox.show({
                msg: 'This change request has already been submitted for approval. Are you sure you would like to reopen this change request for editing? (You will have to resubmit it to make it available for review.)',
                fn: function (buttonId, text, opt) {
                    if (buttonId == 'ok') {
                        this.fireEvent('changeRequestSelected', record.get('change_request_id'), reclaim);
                    }
                },
                scope: this,
                buttons: Ext.Msg.OKCANCEL,
                animEl: 'elId',
                icon: Ext.MessageBox.WARNING
            });
        } else {
            this.fireEvent('changeRequestSelected', record.get('change_request_id'), reclaim);
        }
    }
});
