Ext.define('MAD.widgets.UnitAddressGridPanel', {
    extend: 'Ext.tree.Panel',
    alias: 'widget.unitAddressGridPanel',
    cls: 'unit-address-panel',

    // todo - add conditional behavior and style based on row state (new, existing)

    initComponent: function () {
        var cols = [
            {
                xtype: 'actioncolumn',
                header: 'Add/Remove',
                width: 75,
                sortable: false,
                dataIndex: 0,
                fixed: true,
                align: 'center',
                menuDisabled: true,
                items: [
                    ...(MAD.model.User.isSubaddressEditor() ? [
                        new Ext.Button({
                            xtype: 'tbbutton',
                            icon: MAD.config.Images.Add,
                            tooltip: 'add a subaddress to this unit address',
                            handler: function (grid, rowIndex, colIndex) {
                                const store = grid.getStore();
                                const record = store.getAt(rowIndex);

                                const subaddress = record.addSubaddress();
                                store.treeStore.initRecords(store, record);
                                record.expand();
                                this.fireEvent('subaddressAdded', this, subaddress);
                                this.fireEvent('unitAddressSelected', this, record);
                            },
                            scope: this,
                        }),
                    ] : []),
                    new Ext.Button({
                        xtype: 'tbbutton',
                        icon: MAD.config.Images.Remove,
                        tooltip: 'remove this unit address',
                        getClass: function(v, meta, record) {
                            if (record.getStatusString() == 'new') {
                                return 'silk-delete action-icon';
                            } else {
                                return '';
                            } 
                        },
                        handler: function (grid, rowIndex, colIndex) {
                            var store = grid.getStore();
                            var record = store.getAt(rowIndex);
                            var selection = this.getSelectionModel().getSelection()[0];
                            if (record.getStatusString() == 'new') {
                                this.fireEvent('unitAddressRemoved', grid, record, selection);
                                const parent = record.parentNode;
                                parent.removeChild(record);
                                if (parent.isRoot()) {
                                    grid.getStore().remove(record);
                                } else {
                                    if (parent.childNodes.length === 0) {
                                        parent.collapse();
                                    }
                                }
                            }
                        },
                        scope: this
                    }),
                ],
            },
            { 
                xtype: 'treecolumn',
                header: "Number", 
                sortable: true, 
                dataIndex: 'address_num',
                menuDisabled: true
            },
            {
                header: "Edit/Retire/New",
                sortable: true,
                dataIndex: 'retire_flg',
                menuDisabled: true,
                renderer: function (v, p, record, rowIndex) {
                    return record.getStatusString();
                }
            },
            {
                header: "Validation",
                sortable: true,
                dataIndex: 'validation_message',
                tdCls: 'validation-column',
                menuDisabled: true,
                renderer: function (value, p, record) {
                    return Ext.String.format('<div class="{0}">{1}</div>', record.data.exception_css_class, record.data.exception_message);
                }
            }
        ];

        Ext.apply(this, {
            name: 'unit_address_grid',
            height: 250,
            border: false,
            autoScroll: true,
            columns: cols,
            selModel: {
                mode: 'SINGLE',
                // It is sad to disable key nav, but...
                // we so because the UI can't keep up with the updates on larger addresses like 1000 pine.
                // One day we will completely change the UI so this will not be a problem
                enableKeyNav: false
            },
            viewConfig: {
                rootVisible: false,
            },
            forceFit: true
        });

        this.addEvents('unitAddressSelected');
        this.addEvents('unitAddressRemoved');

        this.on({
            "selectionchange": function (selectionModel, records) {
                if (records.length >0) {
                    this.fireEvent('unitAddressSelected', this, records[0]);
                }
            },
            scope: this
        });

        this.callParent(arguments);
    }
});