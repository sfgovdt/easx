Ext.define('MAD.widgets.BaseAddressPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.baseAddressPanel',

    dataRecord: {},
    isSelected: false,
    childSelected: false,

    getTitle: function () {
        var titleData = this.dataRecord.getTitleData();
        var dividerChar = titleData.validation_message ? '|' : '';
        if (this.isSelected) {
            return Ext.String.format('<span style="color:green;">{0} | {1} {2} <span class="validation-msg {3}">{4}</span></span>', titleData.address, titleData.status, dividerChar, titleData.exception_css_class, titleData.exception_message)
        } else {
            return Ext.String.format('{0} | {1} {2} <span class="validation-msg {3}">{4}</span>', titleData.address, titleData.status, dividerChar, titleData.exception_css_class, titleData.exception_message)
        }
    },

    updateTitle: function () {
        this.setTitle(this.getTitle());
    },

    select: function () {
        this.fireEvent('baseAddressSelected', this, this.dataRecord);
        this.isSelected = true;
        this.updateTitle();
    },

    unselect: function () {
        this.isSelected = false;
        this.childSelected = false;
        this.updateTitle();
    },

    initComponent: function () {

        this.addEvents('baseAddressSelected');
        this.addEvents('baseAddressZoom');
        this.addEvents('baseAddressCollapsed');
        this.addEvents('newUnitAddressAdded');

        this.headerButtons = [
                this.addUnitButton = new Ext.Button({
                    xtype: 'tbbutton',
                    icon: MAD.config.Images.Add,
                    tooltip: { text: 'add a unit address to this base address' },
                    handler: function (button, event) {
                        // prevent accordion container from responding
                        event.stopPropagation();
                        this.fireEvent('newUnitAddressAdded', this, this.dataRecord.addNewUnitAddress());
                    },
                    scope: this
                }),
                {
                    xtype: 'container',
                    width: 3,
                    html: ''
                },
                this.removeBaseButton = new Ext.Button({
                    xtype: 'tbbutton',
                    icon: MAD.config.Images.Delete,
                    tooltip: { text: 'remove this base address from the change request' },
                    handler: function (button, event) {
                        // prevent accordion container from responding
                        event.stopPropagation();
                        this.dataRecord.store.remove(this.dataRecord);
                    },
                    scope: this
                }),
                {
                    xtype: 'container',
                    width: 3,
                    html: ''
                }
            ];

        Ext.apply(this, {
            title: this.getTitle()
            , border: false
            , collapsible: true
            , collapsed: true
            , animate: true
            , frame: false
            , layout: 'fit'
            , header: {
                items: this.headerButtons
            }
        });

        this.on('render',
            function () {
                // no duplicates
                // todo  - there must be a more elegant way
                this.header.removeListener('click', this.select, this);
                this.header.addListener('click', this.select, this);
            }
        );

        this.on('collapse', function () {
            if (this.isSelected || this.childSelected) {
                this.unselect();
                this.fireEvent('baseAddressCollapsed', this, this.dataRecord);
            }
        }, this);

        this.on('expand', function () {
            if (!this.isSelected && !this.childSelected) {
                this.select();
            }
        }, this);

        this.unitAddressStore = this.dataRecord.get('unit_addresses');

        this.callParent(arguments);
    }
});