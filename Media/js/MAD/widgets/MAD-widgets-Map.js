Ext.define('MAD.widgets.Map', {
    extend: 'FGI.widgets.MapPanel',
    alias: 'widgets.mad-widgets-map',

    border: true,
    displaySystem: 'english',
    includeRetired: false,

    initComponent: function () {
        this.map = new OpenLayers.Map('map', {
            restrictedExtent: new OpenLayers.Bounds(-13672208, 4513285, -13588432, 4578637),
            maxExtent: new OpenLayers.Bounds(-20037508, -20037508, 20037508, 20037508.34),
            sfMapsExtent: new OpenLayers.Bounds(-13650208.0, 4530285.0, -13610432.0, 4561637.0),
            numZoomLevels: 9,
            maxResolution: 38.21851414,
            controls: [
                new OpenLayers.Control.MousePosition(),
                new OpenLayers.Control.Navigation({ handleRightClicks: true }),
                new OpenLayers.Control.PanZoomBar({panIcons: false})
            ],
            units: "m",
            // for completeness sake use projection, although this is inferred by the use of google layers
            projection: new OpenLayers.Projection("EPSG:900913"),
            // displayProjection is used for controls like OpenLayers.Control.MousePosition only
            displayProjection: new OpenLayers.Projection("EPSG:4326")
            //displayProjection: new OpenLayers.Projection("EPSG:900913")
        });
        
        this.callParent(arguments);

        var contextZoomButton = new Ext.menu.Item({
            text: 'Zoom To',
            icon: MAD.config.Images.Zoom,
            handler: function () {
                var point = MAD.utils.getPointFromViewPortPixel(this.map, this.contextMenu.event.xy);
                this.map.zoomToGeom(point, 6);
                this.contextMenu.hide();
            },
            scope: this
        });

        var googleStreetViewButton = new Ext.menu.Item({
            text: 'Google Street View',
            icon: MAD.config.Images.House,
            handler: function () {
                var point = MAD.utils.getPointFromViewPortPixel(this.map, this.contextMenu.event.xy);
                this.contextMenu.hide();
                this.contextMenu.fireEvent('googleStreetViewButtonClicked', point);
            },
            scope: this,
            disabled: false
        });

        var contextAddButton = new Ext.menu.Item({
            text: 'Add Address',
            icon: MAD.config.Images.Add,
            handler: function () {
                var point = MAD.utils.getPointFromViewPortPixel(this.map, this.contextMenu.event.xy);
                this.contextMenu.hide();
                this.contextMenu.fireEvent('addNewAddressButtonClicked', point);
            },
            scope: this,
            disabled: true
        });

        var contextMoveAddressButton = new Ext.menu.Item({
            text: 'Move Address',
            icon: MAD.config.Images.ArrowOut,
            handler: function () {
                var point = MAD.utils.getPointFromViewPortPixel(this.map, this.contextMenu.event.xy);
                this.contextMenu.hide();
                this.contextMenu.fireEvent('moveAddressButtonClicked', point);
            },
            scope: this,
            disabled: true
        });

        var contextProvisionParcelButton = new Ext.menu.Item({
            text: 'Provision Parcel',
            icon: MAD.config.Images.ShapeSquareAdd,
            handler: function () {
                var point = MAD.utils.getPointFromViewPortPixel(this.map, this.contextMenu.event.xy);
                this.contextMenu.hide();
                this.contextMenu.fireEvent('provisionParcelButtonClicked', point);
            },
            scope: this,
            disabled: true
        });

        this.contextMenu = new Ext.menu.Menu({
            width: 170,
            items: [
                contextZoomButton,
                googleStreetViewButton,
                contextAddButton,
                contextMoveAddressButton,
                Ext.create('Ext.menu.Item', {
                    text: 'View Parcels Here',
                    icon: MAD.config.Images.ShapeSquare,
                    handler: function () {
                        var point = MAD.utils.getPointFromViewPortPixel(this.map, this.contextMenu.event.xy);
                        this.fireEvent('viewparcelsclicked', point);
                    },
                    scope: this
                }),
                contextProvisionParcelButton
            ]
        });

        this.contextMenu.zoomButton = contextZoomButton;
        this.contextMenu.addAddressButton = contextAddButton;
        this.contextMenu.moveAddressButton = contextMoveAddressButton;
        this.contextMenu.contextProvisionParcelButton = contextProvisionParcelButton;

        this.contextMenu.setUser = function (user) {
            this.addAddressButton.disable();
            this.contextProvisionParcelButton.disable();
            if (user.isRequestor()) {
                this.addAddressButton.enable();
            }
            if (user.isApprover()) {
                this.contextProvisionParcelButton.enable();
            }
        };

        this.contextMenu.addEvents('googleStreetViewButtonClicked');
        this.contextMenu.addEvents('addNewAddressButtonClicked');
        this.contextMenu.addEvents('moveAddressButtonClicked');
        this.contextMenu.addEvents('provisionParcelButtonClicked');

        // Provide access to the context menu from the map.
        var navControl = this.map.getControlsByClass('OpenLayers.Control.Navigation')[0];
        navControl.handlers.click.callbacks.rightclick = Ext.bind(this.showContextMenu, this);

        var hideMenus = function () {
            Ext.menu.Manager.hideAll();
        };
        this.map.events.on({
            // Hiding menus on map click to mimic behavior of Ext widgets
            'click': hideMenus,
            // hide menus on move as well to handle zoom (eg via mousewheel)
            'move': hideMenus
        });

        // OL Event for clicking on a feature in the map
        this.map.events.addEventType('featureclicked');
        this.map.events.addEventType('featurerightclicked');
        this.map.events.addEventType('popupclosed');

        var streetsBaseLayer = new OpenLayers.Layer.WMS('Streets',
            MAD.config.Urls.getGeoWebCacheUrl(),
            {
                layers: 'layer-group_map',
                FORMAT: 'image/png'
            },
            {
                visibility: true,
                'isBaseLayer': true,
                'primaryDataType': 'VECTOR'
            }
        );
        var resolutions = [
            38.21851414,
            19.10925707,
            9.554628535,
            4.7773142675,
            2.38865713375,
            1.194328566875,
            0.5971642834375,
            0.29858214171875,
            0.149291070859375,
            0.0746455354296875,
            0.03732276771484375
        ];
        var imageryBaseLayer = new OpenLayers.Layer.WMS('Imagery',
            MAD.config.Urls.getGeoWebCacheUrl(),
            {
                layers: 'easimagerylatest',
                FORMAT: 'image/png'
            },
            {
                visibility: true,
                'isBaseLayer': true,
                'primaryDataType': 'IMAGE',
                numZoomLevels: 11,
                resolutions: resolutions,
                serverResolutions: resolutions.slice(0, 9)
            }
        );

        queryString = Ext.urlDecode(window.location.search.substring(1))
        this.baseLayer = queryString.basemap === 'Imagery' ? imageryBaseLayer : streetsBaseLayer;


        this.baseLayerControl = Ext.create('Ext.button.Button', {
            text: '',
            tooltip: 'Click to toggle base map',
            handler: function () {
                this.setBaseLayer((this.baseLayer === streetsBaseLayer) ? imageryBaseLayer : streetsBaseLayer);
            },
            scope: this,
            setTextForBaseLayer: function() {
                var text = 'Map';
                if (this.scope.baseLayer === streetsBaseLayer) {
                    text = imageryBaseLayer.name;
                }
                this.setText(text);
            }
        });

        this.baseLayerControlContainer = Ext.create('Ext.container.Container', {
            floating: true,
            width: 80,
            height: 30,
            layout: 'fit',
            items: [this.baseLayerControl]
        });

        this.virtualAddressControl = Ext.create('Ext.container.Container', {
            floating: true,
            layout: 'fit',
            style: {
                backgroundColor: 'rgb(223, 232, 246)',
                padding: '6px'
            },
            items: [new Ext.form.field.Checkbox({
                checked: false,
                boxLabel: 'Show 311 virtual-addresses',
                style: 'color:rgb(21, 66, 139);',
                name: 'showVirtualCheckbox',
                listeners: {
                    change: function (checkbox, value) {
                        this.showVirtual(value);
                    },
                    scope: this
                }
            })]
        });

        

        // this listener ensures that the floating button remains aligned with the map
        this.on('afterlayout', function () {
            this.baseLayerControlContainer.alignTo(this, 'tr-tr', [-10, 10]);
            this.virtualAddressControl.alignTo(this, 'tr-tr', [-100, 10]);
        }, this);
        this.baseLayerControlContainer.show();
        this.virtualAddressControl.show();

        // Add parcel labels separately so we can tile cache generic for possible/easier reuse.
        var parcelLabelLayer = new OpenLayers.Layer.WMS(
            'ParcelLabelLayer',
            MAD.config.Urls.getGeoserverWmsUrl(), {
                layers: 'sfmaps:basemap_citylots_base'
                , format: 'image/png'
                , transparent: true
            }, {
                visibility: true
                , isBaseLayer: false
                , singleTile: true
                , setStyleForBaseLayer: function() {
                    var style = 'map_citylots_labels_highcontrast';
                    if (this.map.baseLayer === streetsBaseLayer) {
                        style = 'map_citylots_labels';
                    }
                    this.mergeNewParams({styles: style});
                }
            }
        );


        // todo - standardize they way we are adding layers.
        this.map.addLayers([
            this.baseLayer
        ]);

        // center on the country of San Francisco
        //this.map.setCenter(new OpenLayers.LonLat(-13630000, 4538000), 12);
        this.map.zoomToExtent(this.map.sfMapsExtent);

        var fadeLayerDefaults = {
            // NOTE - "fade" is not exactly the right word; in the case of a marker layer, we "deflate" because we (I) cannot set the opacity successfully in IE8.
            // TODO - refactor this when we get a patch for // http://trac.osgeo.org/openlayers/ticket/3540
            intervalId: 0,
            _removeFeatures: function() {
                if (this.CLASS_NAME === 'OpenLayers.Layer.Markers') {
                    Ext.each(this.markers, function (marker) {
                        this.removeMarker(marker);
                    }, this);
                } else if (this.CLASS_NAME === 'OpenLayers.Layer.Vector') {
                    this.destroyFeatures();
                }
            },
            _setOpacity: function(opacity) {
                if (this.CLASS_NAME === 'OpenLayers.Layer.Markers') {
                    Ext.each(this.markers, function (marker) {
                        marker.inflate(opacity);
                    }, this);
                } else if (this.CLASS_NAME === 'OpenLayers.Layer.Vector') {
                    this.styleMap.styles['default'].defaultStyle.strokeOpacity = opacity;
                }
                this.setOpacity(opacity);
            },
            _fade: function() {
                if (this.opacity < 0) {
                    this._removeFeatures();
                    clearInterval(this.intervalId);
                    return;
                }
                var opacity = this.opacity - 0.1;
                opacity = Math.max(0, opacity);
                this._setOpacity(opacity);
                this.redraw();
            },
            addFadeFeature: function(feature, runFade) {
                this.stopFade();
                if (this.CLASS_NAME === 'OpenLayers.Layer.Markers') {
                    this.addMarker(feature);
                } else if (this.CLASS_NAME === 'OpenLayers.Layer.Vector') {
                    this.addFeatures([feature]);
                }
                if (runFade) {
                    this.runFade();
                }
            },
            runFade: function() {
                this._setOpacity(1.0);
                clearInterval(this.intervalId);
                this.intervalId = window.setInterval(Ext.bind(this._fade, this), 150);
            },
            stopFade: function() {
                clearInterval(this.intervalId);
                this._setOpacity(1.0);
            }
        };

        // vector layer for highlighting a parcel on selection from the parcel search
        var highlighted_parcel_layer = new OpenLayers.Layer.Vector(
            "highlighted_parcel_layer",
            Ext.apply(
                {
                    projection: new OpenLayers.Projection("EPSG:900913"),
                    styleMap: new OpenLayers.StyleMap({
                        "default": new OpenLayers.Style({
                            fillOpacity: 0,
                            strokeColor: "#146FE4",
                            strokeWidth: 2
                        }),
                        'imageCore': new OpenLayers.Style({
                            fillOpacity: 0,
                            strokeColor: "#146FE4",
                            strokeWidth: 2
                        }),
                        'imageHalo': new OpenLayers.Style({
                            fillOpacity: 0,
                            strokeColor: "#000000",
                            strokeWidth: 3
                        })
                    }),
                    eventListeners: {
                        'featuresadded' : this.resetFeatureStyles,
                        scope: this
                    }
                },
                fadeLayerDefaults)
        );

        ///// address_report_layer shows the "netted parcel geometry" when an address report is displayed
        var address_report_layer = new OpenLayers.Layer.Vector(
            "address_report_layer",
            Ext.apply({
                projection: new OpenLayers.Projection("EPSG:900913"),
                styleMap: new OpenLayers.StyleMap({
                    "default": new OpenLayers.Style({
                        fillOpacity: 0,
                        strokeColor: "#167BFD",
                        strokeWidth: 2,
                        pointRadius: 8
                    }),
                    'imageCore': new OpenLayers.Style({
                        fillOpacity: 0,
                        strokeColor: "#167BFD",
                        strokeWidth: 2,
                        pointRadius: 8
                    }),
                    'imageHalo': new OpenLayers.Style({
                        fillOpacity: 0,
                        strokeColor: "#000000",
                        strokeWidth: 3,
                        pointRadius: 8
                    })
                }),
                eventListeners: {
                    'featuresadded' : this.resetFeatureStyles,
                    scope: this
                }
            }, fadeLayerDefaults)
        );

        /////



        /******************************************************************************
        *
        *           Address Points
        *
        ******************************************************************************/

        // Create a styleMap to style your features for two different
        // render intents.  The style for the 'default' render intent will
        // be applied when the feature is first drawn.  The style for the
        // 'select' render intent will be applied when the feature is selected.
        var checkForRetiredMembers = function (cluster) {
            var retiredCount = 0,
                members = [];
            if (cluster.data.clusteredItems) {
                members = Ext.Array.clone(cluster.data.clusteredItems);
            }
            members.push(cluster.data);
            Ext.each(members, function (member) {
                if (member.retire_tms) {
                    retiredCount += 1;
                }
            }, this);
            return retiredCount;
        }
        var scaleObj = {
            context: {
                scaledSize: function (feature) {
                    return feature.data.count > 1 ? "10" : "6";
                },
                getFillColor: function (feature) {
                    var color = '#AF2300',
                        retiredCount = checkForRetiredMembers(feature);
                    if (retiredCount) {
                        if (retiredCount == feature.data.count) {
                            color = 'white';
                        } else {
                            color = '#E1AD9F';
                        }
                    }
                    return color;
                },
                getStrokeColor: function (feature) {
                    return checkForRetiredMembers(feature) ? 'black': '#880000';
                }
            }
        };
        var addressesStyleImage = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: "${scaledSize}",
                fillColor: "${getFillColor}",
                fillOpacity: .4,
                strokeColor: "${getStrokeColor}",
                strokeOpacity: .8,
                strokeWidth: 2,
                cursor: 'pointer'
            }, OpenLayers.Feature.Vector.style["default"]), scaleObj),
            "select": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: "${scaledSize}",
                fillOpacity: .4,
                fillColor: "#E5E522",
                strokeOpacity: .8,
                strokeColor: "${getStrokeColor}",
                strokeWidth: 2,
                cursor: 'pointer'
            }, OpenLayers.Feature.Vector.style["select"]), scaleObj),
            "temporary": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: "${scaledSize}",
                fillOpacity: .2,
                fillColor: "#E5E522",
                strokeOpacity: .8,
                strokeColor: "${getStrokeColor}",
                strokeWidth: 2,
                cursor: 'pointer'
            }, OpenLayers.Feature.Vector.style["temporary"]), scaleObj)
        });

        var virtualAddressesStyleVector = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: 5,
                fillColor: "blue",
                fillOpacity: .6,
                strokeColor: "white",
                strokeOpacity: 1,
                strokeWidth: 3,
                cursor: 'pointer',
                label: "${base_address_num}",
                fontColor: "blue",
                fontSize: "10px",
                fontWeight: "bold",
                labelAlign: 'lt',
                labelXOffset: 13,
                labelYOffset: 6
            }, OpenLayers.Feature.Vector.style["default"]), scaleObj),
            "select": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: 5,
                fillColor: "blue",
                fillOpacity: .6,
                strokeColor: "white",
                strokeOpacity: 1,
                strokeWidth: 3,
                cursor: 'pointer',
                label: "${base_address_num}",
                fontColor: "blue",
                fontSize: "10px",
                fontWeight: "bold",
                labelAlign: 'lt',
                labelXOffset: 13,
                labelYOffset: 6
            }, OpenLayers.Feature.Vector.style["default"]), scaleObj),
            "temporary": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: 5,
                fillColor: "blue",
                fillOpacity: .6,
                strokeColor: "white",
                strokeOpacity: 1,
                strokeWidth: 3,
                cursor: 'pointer',
                label: "${base_address_num}",
                fontColor: "blue",
                fontSize: "10px",
                fontWeight: "bold",
                labelAlign: 'lt',
                labelXOffset: 13,
                labelYOffset: 6
            }, OpenLayers.Feature.Vector.style["default"]), scaleObj)
        });

        var addressesStyleVector = new OpenLayers.StyleMap({
            "default": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: "${scaledSize}",
                fillColor: "${getFillColor}",
                fillOpacity: .2,
                strokeColor: "${getStrokeColor}",
                strokeOpacity: .2,
                strokeWidth: 2,
                cursor: 'pointer'
            }, OpenLayers.Feature.Vector.style["default"]), scaleObj),
            "select": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: "${scaledSize}",
                fillOpacity: .1,
                fillColor: "#E5E522",
                strokeOpacity: .1,
                strokeColor: "${getStrokeColor}",
                strokeWidth: 2,
                cursor: 'pointer'
            }, OpenLayers.Feature.Vector.style["select"]), scaleObj),
            "temporary": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                pointRadius: "${scaledSize}",
                fillOpacity: .3,
                fillColor: "#E5E522",
                strokeOpacity: .1,
                strokeColor: "${getStrokeColor}",
                strokeWidth: 2,
                cursor: 'pointer'
            }, OpenLayers.Feature.Vector.style["temporary"]), scaleObj)
        });

        var invalidAddressesLayer = new OpenLayers.Layer.Vector("InvalidAddresses", {
            projection: new OpenLayers.Projection("EPSG:900913"),
            visibility: true,
            minResolution: 0.01,
            maxResolution: 0.85,
            styleMap: new OpenLayers.StyleMap({
                "default": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
                    externalGraphic: MAD.config.Images.Error,
                    graphicWidth: 16,
                    graphicHeight: 16,
                    graphicXOffset: 0,
                    graphicYOffset: -16,
                    graphicOpacity: 1,
                    display: '${showInvalidIcon}'
                }, OpenLayers.Feature.Vector.style["temporary"]), {
                    context: {
                        showInvalidIcon: function (feature) {
                            var r = 'none';
                            var validationWarningCount = feature.data.validation_warning_count;
                            if (feature.data.clusteredItems) {
                                Ext.each(feature.data.clusteredItems, function (item) {
                                    validationWarningCount += item.validation_warning_count;
                                }, this);
                            }
                            if (validationWarningCount) {
                                r = '';
                            }
                            return r;
                        }
                    }
                })
            })
        });

        // var serviceAddressesLayer = new OpenLayers.Layer.Vector("ServiceAddresses", {
        //     projection: new OpenLayers.Projection("EPSG:900913"),
        //     visibility: true,
        //     minResolution: 0.01,
        //     maxResolution: 0.85,
        //     styleMap: new OpenLayers.StyleMap({
        //         "default": new OpenLayers.Style(OpenLayers.Util.applyDefaults({
        //             fill: false, 
        //             stroke: false,
        //             display: '${showServiceIcon}',
        //             label: "\uf27d",//'v'
        //             fontSize: "12px",
        //             fontFamily: "FontAwesome",
        //             fontColor: '#15498B',
        //             labelXOffset: -7,
        //             labelYOffset: 7
        //         }, OpenLayers.Feature.Vector.style["temporary"]), {
        //             context: {
        //                 showServiceIcon: function (feature) {
        //                     var r = 'none';
        //                     var isServiceAddress = feature.data.service_address_flg;
        //                     if (feature.data.clusteredItems) {
        //                         Ext.each(feature.data.clusteredItems, function (item) {
        //                             if (item.service_address_flg) {
        //                                 isServiceAddress = item.service_address_flg;
        //                             }
        //                         }, this);
        //                     }
        //                     if (isServiceAddress) {
        //                         r = '';
        //                     }
        //                     return r;
        //                 }
        //             }
        //         })
        //     })
        // });

        var bbox = new OpenLayers.Strategy.BBOX();
        // Here we use the min and max resolution values to make this layer to be visible only at zoom levels 1-3.
        var addresses_layer = new OpenLayers.Layer.Vector("Addresses", {
            strategies: [bbox],
            protocol: new OpenLayers.Protocol.HTTP({
                url: "search/addressbbox",
                params: {
                    includeRetired: this.includeRetired,
                    SRS: "EPSG:900913",
                    zoomLevel: this.map.getZoom()
                },
                format: new OpenLayers.Format.SimpleJSONFormat()
            }),
            projection: new OpenLayers.Projection("EPSG:900913"),
            visibility: true,
            minResolution: 0.01,
            maxResolution: 0.85,
            styleMap: addressesStyleVector
        });

        this.virtualAddressesLayer = new OpenLayers.Layer.Vector("VirtualAddresses", {
            strategies: [new OpenLayers.Strategy.BBOX()],
            protocol: new OpenLayers.Protocol.HTTP({
                url: "search/virtualaddressbbox",
                params: {
                    includeRetired: this.includeRetired,
                    SRS: "EPSG:900913",
                    zoomLevel: this.map.getZoom()
                },
                format: new OpenLayers.Format.SimpleJSONFormat()
            }),
            projection: new OpenLayers.Projection("EPSG:900913"),
            visibility: true,
            minResolution: 0.01,
            maxResolution: 0.85,
            styleMap: virtualAddressesStyleVector
        });

        this.loadingWindow = Ext.create('Ext.panel.Panel', {
            floating: true,
            shadow: false,
            bodyStyle: 'background: transparent;',
            frame: false,
            border: false,
            width: 42,
            height: 42,
            html: '<img src="/Media/images/large-loading.gif" style="padding-left:10px;padding-bottom:10px;width:42px;height:42px;"/>'
        });

        // Using defers here when hiding loadingWindow to prevent corrupting the layout when events fire in succession too quickly
        addresses_layer.events.on({
            'loadstart': function(e) {
                this.loadingWindow.show();
                this.loadingWindow.alignTo(this, 'bl-bl');
            },
            'loadend': function(e) {
                Ext.defer(this.loadingWindow.hide, 100, this.loadingWindow);
            },
            'loadcancel': function(e) {
                Ext.defer(this.loadingWindow.hide, 100, this.loadingWindow);
            },
            'featuresadded': function (e) {
                invalidAddressesLayer.removeAllFeatures();
                //serviceAddressesLayer.removeAllFeatures();
                Ext.each(e.features, function(feature) {
                    invalidAddressesLayer.addFeatures([feature.clone()]);
                    //serviceAddressesLayer.addFeatures([feature.clone()]);
                }, this);
            },
            scope: this
        });

        ///// CHANGE REQUEST LAYERS - BEGIN
        // todo - We may want to encapsulate this further in the change request work flow.
        //  Setup an draggable point layer to support the "proposed" address location.
        var addressDragLayer = new OpenLayers.Layer.Vector(
            'addressDragLayer',
            {
                styleMap: new OpenLayers.StyleMap({
                    'default': new OpenLayers.Style({
                        pointRadius: '10',
                        fillOpacity: 0.0,
                        strokeColor: "#00AD00",
                        strokeWidth: 2,
                        fontColor: "#00AD00",
                        fillColor: "#00AD00",
                        label: "${label}",
                        fontSize: "10px",
                        fontWeight: "bold",
                        labelAlign: 'lt',
                        labelXOffset: 12,
                        labelYOffset: 5
                    }),
                    'imageCore': new OpenLayers.Style({
                        pointRadius: '10',
                        fillOpacity: 0.0,
                        strokeColor: "#00AD00",
                        strokeWidth: 2,
                        fontColor: "#00AD00",
                        fillColor: "#00AD00",
                        label: "${label}",
                        fontSize: "10px",
                        fontWeight: "bold",
                        labelAlign: 'lt',
                        labelXOffset: 12,
                        labelYOffset: 5
                    }),
                    'imageHalo': new OpenLayers.Style({
                        pointRadius: '10',
                        fillOpacity: 0.0,
                        strokeColor: "#000000",
                        strokeWidth: 3,
                        fontColor: "#000000",
                        fillColor: "#00AD00",
                        label: "${label}",
                        fontSize: "10px",
                        fontWeight: "bold",
                        labelAlign: 'lt',
                        labelXOffset: 13,
                        labelYOffset: 6
                    })
                }),
                eventListeners: {
                    'featuresadded' : this.resetFeatureStyles,
                    scope: this
                }
            }
        );
        this.map.addLayer(addressDragLayer);
        var addressDragControl = new OpenLayers.Control.DragFeature(addressDragLayer, {
            name: 'addressDragControl',
            onComplete: function () {
                // implementation is in controllers
            }
        });
        this.map.addControl(addressDragControl);


        // Setup a street segment vector layer; uses to display street segment that goes with an address.
        var streetSegmentLayer = new OpenLayers.Layer.Vector(
            'streetSegmentLayer', {
                styleMap: new OpenLayers.StyleMap({
                    'default': new OpenLayers.Style({
                        strokeColor: "#00AD00",
                        strokeWidth: 2
                    }),
                    'imageCore': new OpenLayers.Style({
                        strokeColor: "#00AD00",
                        strokeWidth: 2
                    }),
                    'imageHalo': new OpenLayers.Style({
                        strokeColor: "#000000",
                        strokeWidth: 3
                    })
                }),
                eventListeners: {
                    'featuresadded' : this.resetFeatureStyles,
                    scope: this
                }
            }
        );
        this.map.addLayer(streetSegmentLayer);

        // Setup a parcel polygon vector layer; use to display parcel polygon that goes with an address.
        var parcelLayer = new OpenLayers.Layer.Vector(
            'parcelLayer', {
                styleMap: new OpenLayers.StyleMap({
                    'default': new OpenLayers.Style({
                        strokeColor: "#00AD00"
                        , strokeWidth: 2
                        , fillOpacity: 0.0
                    }),
                    'imageCore': new OpenLayers.Style({
                        fillOpacity: 0,
                        strokeColor: "#00AD00",
                        strokeWidth: 2
                    }),
                    'imageHalo': new OpenLayers.Style({
                        fillOpacity: 0,
                        strokeColor: "#000000",
                        strokeWidth: 3
                    })
                }),
                eventListeners: {
                    'featuresadded' : this.resetFeatureStyles,
                    scope: this
                }
            }
        );
        this.map.addLayer(parcelLayer);
        ///// CHANGE REQUEST LAYERS - END



        /******************************************************************************
        *
        *
        *
        *           Address Search Markers
        *
        *
        *
        ******************************************************************************/
        var search_layer = new OpenLayers.Layer.Markers(
            "SearchedAddresses", Ext.apply({
                // Ext.Tip object for showing feature name, etc...
                hoverTip: new Ext.tip.Tip({ closable: false,
                    renderTo: 'map'
                })
            }, fadeLayerDefaults)
        );


        this.map.addLayers([
            address_report_layer,
            highlighted_parcel_layer,
            addresses_layer,
            invalidAddressesLayer,
            //serviceAddressesLayer,
            search_layer,
            parcelLabelLayer
        ]);


        var addressSelector = new FGI.openlayers.ClickAndHighlightFeature(addresses_layer, {
            name: 'selectControl',
            showTip: true,
            hoverTipFieldName: 'address',
            zoomOnClick: false,
            featureIdField: 'address_base_id'
        });
        this.map.addControl(addressSelector);
        addressSelector.activate();


        // Toggle the visibility of the layers and set the appropriate active layer
        this.map.events.register('zoomend', this, function (e) {
            // updating the zoomLevel param on the addresses layer as it is used by clustering code
            addresses_layer.protocol.params.zoomLevel = this.map.getZoom();
            addresses_layer.refresh({force: true});
        });

        // zooms the map to a given WKT and optionally limits the max zoom level the map zooms to
        this.map.zoomToWKT = function (wkt, zoomLimit) {
            var geom = new OpenLayers.Format.WKT().read(wkt).geometry;
            this.zoomToGeom(geom, zoomLimit);
        };

        // zooms the map to a given geometry and optionally limits the max zoom level the map zooms to
        this.map.zoomToGeom = function (geom, zoomLimit) {
            var bounds = geom.getBounds();
            if (zoomLimit) {
                // get the zoom level at which to view the feature
                var zoom = this.getZoomForExtent(bounds, false);

                // zoom to the feature but not closer then level 17
                this.setCenter(bounds.getCenterLonLat(), Math.min(zoom, zoomLimit));
            } else {
                this.zoomToExtent(bounds);
            }
        };

        this.map.zoomToGeometry = function (geometry, project) {
            var feature = new OpenLayers.Format.WKT().read(geometry);
            if (project) {
                feature.geometry.transform(new OpenLayers.Projection("EPSG:2227"), new OpenLayers.Projection("EPSG:900913"));
            }
            this.zoomToGeom(feature.geometry, 17);
        };

        this.map.zoomToClusterBounds = function (clusterBounds) {
            var se = new OpenLayers.Format.WKT().read(clusterBounds.SE);
            var nw = new OpenLayers.Format.WKT().read(clusterBounds.NW);
            var bounds = new OpenLayers.Bounds();
            bounds.extend(se.geometry);
            bounds.extend(nw.geometry);
            this.zoomToExtent(bounds, true);
        };

        this.map.events.register('changebaselayer', this, function (event, element) {
            Ext.each([address_report_layer, address_report_layer, parcelLayer, streetSegmentLayer, addressDragLayer], function(layer) {
                this.resetLayerStyles(layer);
            }, this);

            if (this.map.baseLayer.primaryDataType === 'IMAGE') {
                addresses_layer.styleMap = addressesStyleImage;
            } else if (this.map.baseLayer.primaryDataType === 'VECTOR')  {
                addresses_layer.styleMap = addressesStyleVector;
            }
            addresses_layer.refresh();

        });

        this.baseLayerControl.setTextForBaseLayer();
        parcelLabelLayer.setStyleForBaseLayer();
    },
    ///// end initComponent

    setBaseLayer: function(layer) {
        if (this.baseLayer) {
            this.map.removeLayer(this.baseLayer);
        }
        this.map.addLayer(layer);
        this.map.setBaseLayer(layer);
        this.baseLayer = layer;
        this.baseLayerControl.setTextForBaseLayer();
        this.map.getLayersByName('ParcelLabelLayer')[0].setStyleForBaseLayer();
    },

    showContextMenu: function (e) {

        this.contextMenu.hide();

        // Note - we infer from the lack of a draggable address point that the user is not a requestor and not an approver.
        // In other words, a draggable address point will only show up here if there is an active change request workflow.
        // This kind of inference is not ideal - we'll add structure when it becomes more clear how to do it properly.
        var moveAddressMenuItem = this.contextMenu.items.items[3]; // todo - this is brittle - use another approach
        moveAddressMenuItem.disable();
        var addressDragLayer = this.map.getLayersByName('addressDragLayer')[0];
        if (addressDragLayer) {
            var feature = addressDragLayer.features[0];
            if (feature) {
                var featureLonLat = new OpenLayers.LonLat(feature.geometry.x, feature.geometry.y);
                var featurePixel = this.map.getPixelFromLonLat(featureLonLat);
                var p1 = new OpenLayers.Geometry.Point(featurePixel.x, featurePixel.y);
                var p2 = new OpenLayers.Geometry.Point(e.xy.x, e.xy.y);
                var TOLERANCE = 12;
                if (p1.distanceTo(p2) <= TOLERANCE) {
                    moveAddressMenuItem.enable();
                }
            }
        }

        this.contextMenu.showAt(e.clientX, e.clientY);
        this.contextMenu.event = e;
    },

    zoomToExtentConditional : function(bounds, minZoomLevel) {
        // If the map extent does not contain the bounds, set the map extent to the bounds.
        if (!this.map.getExtent().containsBounds(bounds)) {
            this.map.zoomToExtent(bounds);
            return;
        }
        // The map bounds contains the bounds.
        // If the zoom level is equal to or greater than minZoomLevel, do nothing.
        if (this.map.getZoom() >= minZoomLevel) {
            return;
        }
        // Otherwise set the extent to the bounds.
        this.map.zoomToExtent(bounds);
    },

    setIncludeRetired: function(includeRetired) {
        var addressLayer = this.map.getLayersByName('Addresses')[0];
        Ext.apply(addressLayer.protocol.params, {
            includeRetired: includeRetired
        });
        addressLayer.refresh({force: true});
    },

    showVirtual: function(show) {
        if (show) this.map.addLayer(this.virtualAddressesLayer);
        else this.map.removeLayer(this.virtualAddressesLayer);
    },

    zoomToConditional : function(geometryWkt, project, minZoomLevel) {
        var feature = new OpenLayers.Format.WKT().read(geometryWkt);
        if (project) {
            feature.geometry.transform(new OpenLayers.Projection("EPSG:2227"), new OpenLayers.Projection("EPSG:900913"));
        }
        this.setZoomLevelConditional(minZoomLevel);
        var lonLat = feature.geometry.getBounds().getCenterLonLat();
        var wktPoint = MAD.utils.lonLatToWktPoint(lonLat);
        this.setCenterConditional(wktPoint);
    },

    setZoomLevelConditional: function (minZoomLevel) {
        var zoomLevel = this.map.getZoom();
        if (zoomLevel < minZoomLevel) {
            this.map.zoomTo(minZoomLevel);
        }
    },

    setCenterConditional: function (wktPoint) {
        var lonLat = MAD.utils.wktPointToLonLat(wktPoint);
        if (!this.map.getExtent().containsLonLat(lonLat)) {
            this.map.setCenter(lonLat);
        }
    },

    resetFeatureStyles: function(event) {
        //  vectorLayer must have styles named imageHalo and imageCore
        //  base layers must have "primaryDataType" member value with domain (IMAGE, VECTOR)
        var vectorLayer = event.features[0].layer;
        this.resetLayerStyles(vectorLayer);
    },

    resetLayerStyles: function(vectorLayer) {
        vectorLayer.events.unregister("featuresadded", this, this.resetFeatureStyles);
        var features = vectorLayer.features;
        vectorLayer.removeFeatures(features);
        Ext.each(features, function(feature) {
            var featuresToAdd = [];
            if (feature.renderIntent === 'imageHalo') {
                // exclude halo features lest they accrete
                return true;
            }
            if (this.map.baseLayer.primaryDataType === 'IMAGE') {
                var haloFeature = feature.clone();
                haloFeature.renderIntent = 'imageHalo';
                feature.renderIntent = 'imageCore';
                featuresToAdd.push(haloFeature, feature)
            } else if (this.map.baseLayer.primaryDataType === 'VECTOR')  {
                feature.renderIntent = 'default';
                featuresToAdd.push(feature)
            }
            vectorLayer.addFeatures(featuresToAdd);
        }, this);
        vectorLayer.events.register("featuresadded", this, this.resetFeatureStyles);
    }

});
