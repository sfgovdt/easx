Ext.namespace('MAD', 'MAD.model', 'MAD.model.exception', 'MAD.model.exception.Record');

MAD.model.landmarkAddressID = 1;

Ext.define('MAD.model.SerializationMixin', {
    writeAllFields: true,

    // The serialize field is used to mark a datastore for inclusion or exclusion during a model serialization.
    // http://code.google.com/p/eas/issues/detail?id=260
    serialize: true,

    /**
    * Converts a Store to a simple JSON object representing only the data in the store
    */
    write: function () {
        var theRecords = this.getRange();
        var obj = {};
        var data = [];

        // todo - check performance with large datasets
        // RE http://www.extjs.com/forum/showthread.php?t=62611

        Ext.each(theRecords, function (item, index, allItems) {
            var hash = this.toHash(item);
            //var json = Ext.encode(hash);
            data.push(hash);
        }, this);

        var root = this.getProxy().getReader().root;
        if (!root) {
            root = this.reader.root;
        }
        obj[root] = data;
        return obj;
    },

    /**
    * Converts a Record to a hash (code partially derived from Ext 3.0)
    * @param {Record}
    * @private
    */
    toHash: function (rec) {
        var map = rec.fields.map,
            data = {},
            obj = (this.writeAllFields === false && rec.phantom === false) ? rec.getChanges() : rec.data,
            m;

        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                if ((m = map[prop])) {
                    var name = m.mapping ? m.mapping : m.name;
                    var value = obj[prop];
                    if (value instanceof Ext.data.AbstractStore) {
                        // Do not (value.serialize === true) because field may be null if store was not inherited from AbstractStore.
                        if (value.serialize) {
                            data[name] = value.write()[name];
                        }
                    } else {
                        data[name] = value;
                    }
                }
            }
        }
        return data;
    },

    compare: function (thatStore, fieldName) {

        // check counts
        if (this.getCount() !== thatStore.getCount()) {
            return false;
        }

        // compare contents
        this.sort(fieldName, "ASC");
        thatStore.sort(fieldName, "ASC");
        var areStoresEquivelent = true;
        Ext.each(this.getRange(), function (thisRecord, index, allItems) {
            var thatRecord = thatStore.getAt(index);
            if (thisRecord.get(fieldName) !== thatRecord.get(fieldName)) {
                areStoresEquivelent = false;
                return false;
            }
        });

        return areStoresEquivelent;
    },

});

Ext.define('MAD.model.AbstractStore', {
    extend: 'Ext.data.Store',
    mixins: ['MAD.model.SerializationMixin'],

    constructor: function (config) {
        this.addEvents({
            'beforeadd': true
        });

        this.callParent(arguments);

        if (this.changeListener) {
            this.changeListener.add(this);
        }

    },

    addDefaultRecord: function (index) {
        var record = Ext.create(this.model);
        this.add(record);
        return record;
    },

    insertDefaultRecord: function () {
        var record = Ext.create(this.model);
        this.insert(0, record);
        return record;
    },

    insert: function(index, records) {
        this.fireEvent('beforeadd', this, records);
        this.callParent(arguments);
    }
});

MAD.model.ChangeListener = function(config) {

    this.notify = function(store, record, stringOrIndex) {
        // update : 3rd arg is string (Ext.data.Record.EDIT ||  Ext.data.Record.REJECT ||  Ext.data.Record.COMMIT)
        // add: 3rd arg is index
        // remove: 3rd arg is index
        alert('model has changed - client should override this function');
    };

    this.active = false;

    Ext.apply(this, config);

    this.stores = [];

    this.add = function(store) {
        if (this.active) {
            this.activateStore(store);
        }
        this.stores.push(store);
    };

    this.activateStore = function(store){
        store.on('add', this.notify, this);
        store.on('remove', this.notify, this);
        store.on('update', this.notify, this);
    };

    this.deactivateStore = function(store){
        store.un('add', this.notify);
        store.un('remove', this.notify);
        store.un('update', this.notify);
    };

    this.removeInvalid = function() {
        // store may be removed from the model so we insure they get removed from here as well
        var invalidStores = [];
        Ext.each(this.stores, function(store, index, allStores) {
            if (!store) {
                invalidStores.push(store);
            }
        }, this);
        Ext.each(invalidStores, function(invalidStore, index, allInvalidStores){
            Ext.Array.remove(this.stores, invalidStore);
        }, this);
    };

    this.activate = function() {

        this.removeInvalid();
        var INVALID_STORES_REMOVED = true;
        // protect against inadvertant extra listeners
        this.deactivate(INVALID_STORES_REMOVED);

        Ext.each(this.stores, function(store, index, allItems) {
            this.activateStore(store);
        }, this);
        this.active = true;
    };

    this.deactivate = function(INVALID_STORES_REMOVED) {

        if (! INVALID_STORES_REMOVED) {
            this.removeInvalid();
        }

        Ext.each(this.stores, function(store, index, allItems) {
            this.deactivateStore(store);
        }, this);
        this.active = false;
    };


};

Ext.define('MAD.model.ExceptionRecord', {
    extend: 'Ext.data.Model',
    fields: [
        {name:'level'},
        {name:'key'},
        {name:'css_class'},
        {name:'message'}
    ]
});

///// record level exception/validation - BEGIN
MAD.model.exception.getDataStore = function() {
    return new Ext.data.Store({
        model: 'MAD.model.ExceptionRecord',
        data: [
            [0, "NONE", "validation-none-msg", ""],
            [1, "INFORMATION", "validation-information-msg", ""],
            [2, "WARNING", "validation-warning-msg", ""],
            [3, "CRITICAL", "validation-critical-msg", ""]
        ]
    });
};

MAD.model.exception.getRow = function(key, message) {
    if (message === undefined) {
        message = "";
    }
    var dataStore = MAD.app.exceptionDataStore;
    var index = dataStore.findBy(function(record, id) {
        return record.get('key') === key;
    });
    if (index < 0) {
        throw new Error("no matching exception row");
    }
    var record = dataStore.getAt(index);
    var recordCopy = record.copy();
    recordCopy.set("message", message);
    return recordCopy;
};

MAD.model.exception.copyExceptionValues = function(sourceRow, targetRow) {
    targetRow.beginEdit();
    targetRow.set("exception_level", sourceRow.get("level"));
    targetRow.set("exception_key", sourceRow.get("key"));
    targetRow.set("exception_css_class", sourceRow.get("css_class"));
    targetRow.set("exception_message", sourceRow.get("message"));
    targetRow.endEdit();
};

MAD.model.exception.Record.isValid = function() {
    return this.get("exception_level") <= 2;
};
///// record level exception/validation - END


// todo - improve javascript fu!
MAD.model.AbstractParcelRecordGetBlock = function() {
    var apn = this.get('apn');
    var character5 = apn.substring(4, 5);
    var block;
    if (isNaN(parseInt(character5))) {
        block = apn.substring(0, 5)
    } else {
        block = apn.substring(0, 4);
    }
    return block;
};


///// parcel_link_changes
Ext.define('MAD.model.ParcelLinkChangeRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "apn", defaultValue: 0 },
        { name: "retire_tms", defaultValue: '', type: "date", dateFormat: "Y-m-d" },
        { name: "address_x_parcel_id", defaultValue: 0 },
        { name: "link", defaultValue: false },
        { name: "unlink", defaultValue: false },
        { name: "distance", defaultValue: 0 }
    ]
});
Ext.define('MAD.model.ParcelLinkChangeStore', {
    extend: 'MAD.model.AbstractStore',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'parcel_link_changes'
        }
    },
    model: 'MAD.model.ParcelLinkChangeRecord'
});


///// parcel_pick_list
Ext.define('MAD.model.ParcelRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "apn", defaultValue: '' },
        { name: "retire_tms", defaultValue: '' },
        { name: "distance", defaultValue: 0 }
    ]
});

MAD.model.ParcelRecord.prototype.getBlock = MAD.model.AbstractParcelRecordGetBlock;

Ext.define('MAD.model.ParcelPickStore', {
    extend: 'MAD.model.AbstractStore',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'parcel_pick_list'
        }
    },
    constructor: function () {
        this.callParent(arguments);
        this.on({
            'load': this.init
        });
    },
    hasRetired: false,
    init: function() {
        this.each(function(record){
            if (record.get('retire_tms') != null) {
                this.hasRetired = true;
                return false;
            }
        }, this);
    },
    model: 'MAD.model.ParcelRecord',
    sorters: [
        {
            property : 'distance',
            direction: 'ASC'
        }
    ],
    getNearestNonServiceParcel: function(){
        var nearestParcel
        this.sort()
        this.each(function(record){
            if (record.get('apn') != '0000000') {
                nearestParcel = record;
                return false;
            }
        })
        return nearestParcel;
    }

});

MAD.model.ParcelPickStore.prototype.serialize = false;

///// linked_parcels
Ext.define('MAD.model.LinkedParcelRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "apn", defaultValue: '' },
        { name: "address_x_parcel_id", defaultValue: 0 }
    ]
});
Ext.define('MAD.model.LinkedParcelStore', {
    extend: MAD.model.AbstractStore,
    model: 'MAD.model.LinkedParcelRecord',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'linked_parcels'
        }
    }
});


///// This is the "land parcel record" - as it is maintained by dept of public works.
Ext.define('MAD.model.LandParcelRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "parcel_id", defaultValue: '' },
        { name: "map_blk_lot", defaultValue: '' },
        { name: "blk_lot", defaultValue: '' },
        { name: "block_num", defaultValue: '' },
        { name: "lot_num", defaultValue: '' },
        { name: "date_map_add", defaultValue: '' },
        { name: "date_map_drop", defaultValue: '' },
        { name: "geometry", defaultValue: '' },
        { name: "provisioned_tms", defaultValue: '' },
        { name: "suspended_tms", defaultValue: '' },
        { name: "suspended_comment", defaultValue: '' },
        { name: "provisioning_create_tms", defaultValue: '' }
    ]
});
Ext.define('MAD.model.LandParcelStore', {
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'parcel'
        }
    },
    model: 'MAD.model.LandParcelRecord'
});
/////


// todo - more structure
MAD.model.AbstractAddressRecordValidateParcels = function() {

    // todo - needs simplification

    // The actual execution of the validation code is tricky.
    // This is because with a single change in the parcelLinkChangeStore,
    // the validation may run 3 times
    //  - once from a change to to the parcelLinkChangeStore,
    //  - once possibly from change on the unit address store (caused by the validation)
    //  - once possibly from change on the base address store (caused by the validation)
    // If there is a better way - code it up - I am not an extjs guru.

    // A unit address row is valid if
    //  - it is not linked to retired parcels
    //  - if is being unlinked from retired parcels

    var valid = true;

    var parcelPickStore = this.get('parcel_pick_list');
    if (!parcelPickStore.hasRetired) {
        return true;
    }

    var parcelLinkChangeStore = this.get('parcel_link_changes');
    var linkedParcelStore = this.get('linked_parcels');

    // Collect the retired APNs from the parcelPickStore.
    // This includes all retired APNS - whether they are linked or not.
    parcelPickStore.filterBy(function(record, id){
        return (record.get('retire_tms') != null);
    });
    var retiredApns = parcelPickStore.collect('apn');
    parcelPickStore.clearFilter();

    // We should suspect only the retired APNs that are linked.
    var suspectApns = new Array();
    var linkedApns = linkedParcelStore.collect('apn');
    Ext.each(retiredApns, function(retiredApn, index, allItems) {
        if ( Ext.Array.indexOf(linkedApns, retiredApn) != -1 ) {
            suspectApns.push(retiredApn);
        }
    });

    // Here we exclude APNs that have been "corrected".
    // Corrected means that its linked to a suspect APN but it has also been unlinked.
    parcelLinkChangeStore.each(function(parcelLinkChangeRecord) {
        if (parcelLinkChangeRecord) {
            var apn = parcelLinkChangeRecord.get('apn')
            var linked = parcelLinkChangeRecord.get('link');
            var unlinked = parcelLinkChangeRecord.get('unlink');
            if (Ext.Array.indexOf(suspectApns, apn) > -1) {
                if (!linked) {
                    Ext.Array.remove(suspectApns, apn);
                } else if (unlinked) {
                    Ext.Array.remove(suspectApns, apn);
                }
            }
        }
    }, this);

    Ext.each(suspectApns, function(suspectApn, index, allItems) {
        var index2 = linkedParcelStore.find('apn', suspectApn);
        if (index2 > -1) {
            valid = false;
            return false;
        }
    }, this);

    return valid;

};


// UnitAddress - record and store
Ext.define('MAD.model.UnitAddressRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "address_id", defaultValue: null },
        { name: "base_address_flg", defaultValue: false, mapping: "address_base_flg" },
        { name: "unit_type", defaultValue: null },
        { name: "floor_id", defaultValue: null, mapping: "floor" },
        { name: "address_num", defaultValue: "", mapping: "unit_num" },
        { name: "mailable_flg", defaultValue: true },
        { name: "mailable_flg_proposed", defaultValue: true },
        { name: "disposition", defaultValue: null},
        { name: "disposition_proposed", defaultValue: 1}, // default: official
        { name: "retire_flg", defaultValue: false },
        { name: "isNew", defaultValue: true },
        { name: "parcel_pick_list", defaultValue: [] },
        { name: "linked_parcels", defaultValue: [] },
        { name: "parcel_link_changes", defaultValue: [] },
        { name: "unit_addresses", defaultValue: [] },  // children (sub-addresses)
        { name: "exception_level"},
        { name: "exception_key"},
        { name: "exception_css_class"},
        { name: "exception_message"}
    ]
});

MAD.model.UnitAddressRecord.prototype.validateParcels = MAD.model.AbstractAddressRecordValidateParcels;

MAD.model.UnitAddressRecord.prototype.validate = function() {

    // least expensive validations first - exit on first validation failure
    // Do not return true or false from here.
    // Why? We call this function on a data store event - returning false would halt the processing of other events.

    if (this.get('retire_flg')) {
        MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("NONE", ""), this);
        return;
    }

    if (
        this.parentNode
        && this.parentNode.data.unit_type !== null
        && this.data.unit_type !== null
        && this.data.unit_type.level >= this.parentNode.data.unit_type.level
    ) {
        MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow(
            "CRITICAL",
            `Parent unit type level ${this.parentNode.data.unit_type} `
            + `must be greater than child unit type level (${this.data.unit_type})`
        ), this);
        return;
    }

    // todo - check for missing fields
    if (! this.get('address_num')) {
        MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("CRITICAL", "missing unit number"), this);
        return;
    }

    if (! this.validateParcels()) {
        MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("WARNING", "retired parcel(s)"), this);
        return;
    }

    // If we get to here everything is valid.
    MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("NONE", ""), this);

};


MAD.model.UnitAddressRecord.prototype.addSubaddress = function() {
    const newSubaddress = Ext.create(this.store.treeStore.model);
    newSubaddress.set('isNew', true);
    newSubaddress.validate();
    this.appendChild(newSubaddress);

    return newSubaddress;
};

MAD.model.UnitAddressRecord.prototype.isValid = MAD.model.exception.Record.isValid;


MAD.model.UnitAddressRecord.prototype.getStatusString = function() {
    // returns a single status string for a unit record based on retire_flg and isNew
    var status = 'edit';
    if (this.get('retire_flg') === true) {
        status = 'retire';
    }
    if (this.get('isNew') === true) {
        status = 'new';
    }
    return status;
};


MAD.model.UnitAddressRecord.prototype.isProposedStatusOfficial = function() {
    return this.get('disposition_proposed') === 1;
};

MAD.model.UnitAddressRecord.prototype.initialize = function (changeListener, parcelPickStore) {
    this.set('parcel_pick_list', parcelPickStore);

    var linkedParcelStore = new MAD.model.LinkedParcelStore();
    linkedParcelStore.loadRawData(this.data, false);
    this.set('linked_parcels', null);
    this.set('linked_parcels', linkedParcelStore);

    var parcelLinkChangeStore = new MAD.model.ParcelLinkChangeStore({changeListener: changeListener});
    parcelLinkChangeStore.loadRawData(this.data, false);
    this.set('parcel_link_changes', null);
    this.set('parcel_link_changes', parcelLinkChangeStore);

    // When there is any change in the parcelLinkChangeStore, run the validate.
    parcelLinkChangeStore.on('add', this.validate, this);
    parcelLinkChangeStore.on('remove', this.validate, this);
    parcelLinkChangeStore.on('update', this.validate, this);

    this.validate();
    // use commit to make the red tick go away
    this.commit();
};

Ext.define('MAD.model.UnitAddressStore', {
    extend: 'Ext.data.TreeStore',
    mixins: ['MAD.model.SerializationMixin'],
    model: 'MAD.model.UnitAddressRecord',

    root: {
        expanded: true,
    },
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'unit_addresses'
        }
    },
    parcelPickStore: null,
    constructor: function () {
        this.callParent(arguments);
        this.on({
            'beforeadd': this.initRecords, 
            'load': this.initRecords, 
            scope: this
        });
    },

    // Avoid error in ext-all-debug.js line 56059:
    // if (me.store.indexOf(lastFocused) !== -1) {
    indexOf: () => {},

    initRecords: function(store, records, index, eOpts) {
        function traverse(node, changeListener, parcelPickStore) {
            // Skip root node.
            // getRootNode might be missing when init'ing subaddresses
            if (!store.getRootNode || store.getRootNode() !== node) {
                Ext.each(node, function(child) {
                    child.initialize(changeListener, parcelPickStore);
                }, this);
            }
            node.childNodes.forEach(child => traverse(child, changeListener, parcelPickStore));
        }
        traverse(records, this.changeListener, this.parcelPickStore);
    },

    insertDefaultRecord: function () {
        var record = Ext.create(this.model);
        this.fireEvent('beforeadd', this, record);
        this.getRootNode().appendChild(record);
        return record;
    },

    write: function () {
        function traverse(node, acc, hashFn, isUnitAddress) {
            let nextAccumulator = acc;
            if (isUnitAddress) {
                // The root node is a dummy for the sake of the tree store.
                const json = hashFn(node);
                json.unit_addresses = [];
                acc.push(json);
                nextAccumulator = json.unit_addresses;
            }
            node.childNodes.forEach(child => traverse(child, nextAccumulator, hashFn, true));
            return { unit_addresses: acc };
        };

        return traverse(this.getRootNode(), [], this.toHash, false);
    },
});

Ext.define('MAD.model.UnitAddressReportRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "address_base_flg", defaultValue: false },
        { name: "unit_type_id", defaultValue: null },
        { name: "floor_id", defaultValue: null },
        { name: "unit_num", defaultValue: null },
        { name: "disposition_code", defaultValue: null },
        { name: "blk_lot", defaultValue: null },
        { name: "create_tms", defaultValue: null },
        { name: "retire_tms", defaultValue: null },
        { name: "activate_change_request_id", defaultValue: null },
        { name: "retire_change_request_id", defaultValue: null },
        { name: "address_x_parcel_create_tms", defaultValue: null, type: "string" }, // use type string and default null to support sorting
        { name: "address_x_parcel_retire_tms", defaultValue: null, type: "string" }, // use type string and default null to support sorting
        { name: "address_x_parcel_id", defaultValue: null },
    ]
});

// BaseAddress - record and store
Ext.define('MAD.model.BaseAddressRecord', {
    extend: 'Ext.data.Model',
    fields: [
    // The following are in CrBaseAddresses
        {name: "address_base_id", defaultValue: null },
        { name: "prefix", defaultValue: "", mapping: "base_address_prefix" },
        { name: "num", defaultValue: "", mapping: "base_address_num" },
        { name: "base_address_suffix", mapping: "base_address_suffix", defaultValue: null },
        { name: "complete_landmark_name", mapping: "complete_landmark_name"},
        { name: "address_base_type", mapping: "address_base_type" },
        { name: "landmark_aliases", mapping: "landmark_aliases" },
        { name: "zone_id", defaultValue: "", mapping: "zone_id" },
        { name: "street_segment_id", defaultValue: "", mapping: "street_segment" },
        { name: "geometry", defaultValue: "" },
        { name: "geometry_proposed", defaultValue: "" },


    // The following are in CrAddresses
        {name: "address_id", defaultValue: null },
        { name: "floor_id", defaultValue: null, mapping: "floor" },
        { name: "unit_type", defaultValue: null },
        { name: "unit_num", defaultValue: null },
        { name: "unit_mailable_flg", defaultValue: "" },
        { name: "unit_mailable_flg_proposed", defaultValue: "" },
        { name: "unit_disposition", defaultValue: null },
        { name: "unit_disposition_proposed", defaultValue: 1 },  // default: official
        {name: "retire_flg", defaultValue: false },
        { name: "available_streets", defaultValue: [] },
        { name: "zones", defaultValue: [] },
        { name: "unit_addresses", defaultValue: [] },
        { name: "parcel_geometry", defaultValue: "" },
        { name: "parcel_pick_list", defaultValue: [] },
        { name: "linked_parcels", defaultValue: [] },
        { name: "parcel_link_changes", defaultValue: [] },

    // Potentially unused or non-table affiliated fields
    //{ name: "street_segment_name", defaultValue: "" },
    //{ name: "unit_flg", defaultValue: "" },
        {name: "isNew", defaultValue: false },
        { name: "exception_level" },
        { name: "exception_key" },
        { name: "exception_css_class" },
        { name: "exception_message" }
    ]
});

MAD.model.BaseAddressRecord.prototype.validateParcels = MAD.model.AbstractAddressRecordValidateParcels;

MAD.model.BaseAddressRecord.prototype.validate = function() {

    // We do not return true or false because this is called on datastore events.
    // If we return a false, it stops the event processing which is not what we want.
    // todo - more OO

    var exceptionLevel;

    if (this.get('retire_flg')) {
        MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("NONE", ""), this);
        return;
    }

    var typeId = this.get('address_base_type');
    if (typeId === MAD.model.landmarkAddressID || this.get('landmark_aliases').length) {
        if (!this.get('complete_landmark_name')) {
            MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("CRITICAL", "please enter a landmark name"), this);
            return;
        }
        if (this.get('landmark_aliases').some(alias => alias.length > 150)) {
            MAD.model.exception.copyExceptionValues(
                MAD.model.exception.getRow("CRITICAL", "Landmark names must be under 150 characters."), this
            );
            return;
        }
    } else {
        if ( ! Ext.form.VTypes.AddressNumber(this.get('num')) ) {
            MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("CRITICAL", "invalid street number"), this);
            return;
        }

        if ( ! this.get('street_segment_id') ) {
            MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("CRITICAL", "please select street"), this);
            return;
        }
    }

    var valid = true;
    var highestExceptionLevel = MAD.model.exception.getRow("NONE", "").get("level");
    var unitAddresses = this.get('unit_addresses');

    function visitChildNodes (record) {
        exceptionLevel = record.get('exception_level');
        if (exceptionLevel > highestExceptionLevel) {
            valid = false;
            highestExceptionLevel = exceptionLevel;
        }
        record.childNodes.forEach(child => visitChildNodes(child));
    }
    visitChildNodes(unitAddresses.getRootNode());

    if (! valid) {
        if (highestExceptionLevel >= MAD.model.exception.getRow("CRITICAL").get("level")) {
            MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("CRITICAL", "unit exceptions"), this);
        } else {
            MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("WARNING", "unit warnings"), this);
        }
        return;
    }

    if (! this.validateParcels()) {
        MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("WARNING", "retired parcel(s)"), this);
        return;
    }

    if (typeId !== MAD.model.landmarkAddressID) {
        var streetSegmentRecord = this.getStreetSegmentRecord();

        if (streetSegmentRecord) {
            if (streetSegmentRecord.get('date_dropped')) {
                MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("WARNING", "retired street"), this);
                return;
            }

            var addressNumber = this.get('num');
            var streetRangeMin = streetSegmentRecord.getRangeMin();
            var streetRangeMax = streetSegmentRecord.getRangeMax();
            if ( !(addressNumber >= streetRangeMin) || !(addressNumber <= streetRangeMax) ) {
                    MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("WARNING", "number not in street range"), this);
                    return;
            }
        }
    }


    // If we get to here, everything is valid.
    MAD.model.exception.copyExceptionValues(MAD.model.exception.getRow("NONE", ""), this);

};


MAD.model.BaseAddressRecord.prototype.isValid = MAD.model.exception.Record.isValid;


MAD.model.BaseAddressRecord.prototype.getStreetSegmentDescription = function() {
    var streetSegmentDescription = '';
    var streetSegmentRecord = this.getStreetSegmentRecord();
    if (streetSegmentRecord) {
        streetSegmentDescription = streetSegmentRecord.get('description');
    }
    return streetSegmentDescription;
};

MAD.model.BaseAddressRecord.prototype.getStatusString = function() {
    // returns a single status string for a unit record based on retire_flg and isNew
    var status = 'edit';
    if (this.get('retire_flg') === true) {
        status = 'retire';
    }
    if (this.get('isNew') === true) {
        status = 'new';
    }
    return status;
};

MAD.model.BaseAddressRecord.prototype.isProposedStatusOfficial= function() {
    return this.get('unit_disposition_proposed') === 1;
};

MAD.model.BaseAddressRecord.prototype.getTitleData = function() {
    var address;
    if (this.get('address_base_type') !== MAD.model.landmarkAddressID) {
        var prefix = this.get("num");
        var midfix = this.get("base_address_suffix") ? (" " + this.get("base_address_suffix") + " "): " ";
        var suffix = this.getStreetSegmentDescription();
        address = prefix + midfix + suffix;
    } else {
        address = this.get('complete_landmark_name');
    }
    address = address == " " ? "" : address;

    return {
        address: address,
        status: this.getStatusString(),
        exception_level: this.get('exception_level'),
        exception_key: this.get('exception_key'),
        exception_message: this.get('exception_message'),
        exception_css_class: this.get('exception_css_class')
    };
};

MAD.model.BaseAddressRecord.prototype.getStreetSegmentRecord = function() {
    var streetSegmentId = this.get('street_segment_id');
    var streetSegmentStore = this.get('available_streets');
    if (! streetSegmentStore) {
        throw new Error("no street segment store");
    }
    var streetSegmentIndex = streetSegmentStore.find('street_segment_id', streetSegmentId);
    if (streetSegmentIndex == -1) {
        if (!this.get('isNew') && this.get('address_base_type') !== MAD.model.landmarkAddressID) {
            throw new Error("street segment not found");
        }
    }
    return streetSegmentStore.getAt(streetSegmentIndex);
};

MAD.model.BaseAddressRecord.prototype.getStreetSegmentGeometry = function() {
    var streetSegmentGeometry = null;
    var streetSegmentRecord = this.getStreetSegmentRecord();
    if (streetSegmentRecord) {
        streetSegmentGeometry = streetSegmentRecord.get('geometry');
    }
    return streetSegmentGeometry;
};

MAD.model.BaseAddressRecord.prototype.getFirstApn = function() {
    // return the first apn we find - this is for simple identification purposes such as is this APN valid at this XY
    var parcelPickStore = this.get('parcel_pick_list');
    if (parcelPickStore.getCount() > 0)   {
        return parcelPickStore.getAt(0).get('apn');
    } else {
        return null;
    }
};

MAD.model.BaseAddressRecord.prototype.addNewUnitAddress = function() {
    var unitAddressStore = this.get('unit_addresses');
    var newUnitAddressRecord = unitAddressStore.insertDefaultRecord();
    newUnitAddressRecord.set('isNew', true);
    newUnitAddressRecord.validate();

    return newUnitAddressRecord;
};

MAD.model.BaseAddressRecord.prototype.compareStreetStores = function(compareRecord) {
    var thisStreetStore = this.get('available_streets');
    var thatStreetStore = compareRecord.get('available_streets');
    return thisStreetStore.compare(thatStreetStore, 'street_segment_id');
};

MAD.model.BaseAddressRecord.prototype.compareApnStores = function(compareRecord) {
    var thisParcelPickStore = this.get('parcel_pick_list');
    var thatParcelPickStore = compareRecord.get('parcel_pick_list');
    return thisParcelPickStore.compare(thatParcelPickStore, 'apn');
};

MAD.model.BaseAddressRecord.prototype.initialize = function(baseAddressStore) {

    // Note the use of:
    //     this.set('linked_parcels', null);
    // We found that without this line, the record's field was not being set properly.

    // no listener
    var parcelPickStore = new MAD.model.ParcelPickStore();
    parcelPickStore.loadRawData(this.data, false);
    this.set('parcel_pick_list', null);
    this.set('parcel_pick_list', parcelPickStore);

    // no listener
    var linkedParcelStore = new MAD.model.LinkedParcelStore();
    linkedParcelStore.loadRawData(this.data, false);
    this.set('linked_parcels', null);
    this.set('linked_parcels', linkedParcelStore);

    // yes listener
    var parcelLinkChangeStore = new MAD.model.ParcelLinkChangeStore({changeListener: baseAddressStore.changeListener});
    parcelLinkChangeStore.loadRawData(this.data, false);
    this.set('parcel_link_changes', null);
    this.set('parcel_link_changes', parcelLinkChangeStore);

    // no listener
    var streetSegmentStore = new MAD.model.StreetSegmentStore();
    streetSegmentStore.loadRawData(this.data, false);
    this.set('available_streets', null);
    this.set('available_streets', streetSegmentStore);

    var zoneStore = new MAD.model.ZoneStore();
    zoneStore.loadRawData(this.data, false);
    this.set('zones', null);
    this.set('zones', zoneStore);

    var unitAddressStore = new MAD.model.UnitAddressStore({changeListener: baseAddressStore.changeListener, parcelPickStore: parcelPickStore});
    unitAddressStore.proxy.data = this.data;
    unitAddressStore.load();
    this.set('unit_addresses', null);
    this.set('unit_addresses', unitAddressStore);

    var baseAddressValidateCommit = Ext.bind(function() {
        this.validate();
        this.commit();
    }, this);

    unitAddressStore.on('add', baseAddressValidateCommit, this);
    unitAddressStore.on('remove', baseAddressValidateCommit, this);
    unitAddressStore.on('update', baseAddressValidateCommit, this);

    parcelLinkChangeStore.on('add', baseAddressValidateCommit, this);
    parcelLinkChangeStore.on('remove', baseAddressValidateCommit, this);
    parcelLinkChangeStore.on('update', baseAddressValidateCommit, this);

    baseAddressValidateCommit();

};


Ext.define('MAD.model.BaseAddressStore', {
    extend: 'MAD.model.AbstractStore',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'base_addresses'
        }
    },
    model: 'MAD.model.BaseAddressRecord',
    constructor: function () {
        this.callParent(arguments);
        this.on({
            'add': this.initRecords,
            'load': this.initRecords, 
            scope: this
        });
    },
    initRecords: function(store, records, index, eOpts){
        Ext.each(records, function(record) {
            record.initialize(this);
        }, this)
    }
});


///// street segment record and store
Ext.define('MAD.model.StreetSegmentRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "street_segment_id", defaultValue: 0 },
        { name: "description", defaultValue: "" },
        { name: "geometry", defaultValue: "LINESTRING (0 0, 0 0)" },
        { name: "date_dropped", defaultValue: "" },
        { name: "l_f_add", defaultValue: 0 },
        { name: "l_t_add", defaultValue: 0 },
        { name: "r_f_add", defaultValue: 0 },
        { name: "r_t_add", defaultValue: 0 }
    ]
});

MAD.model.StreetSegmentRecord.prototype.getRangeMin = function() {

    if (this.get("l_f_add") === 0 && this.get("l_t_add") === 0 ) {
        return Math.min(this.get("r_f_add"), this.get("r_t_add"));
    }

    if (this.get("r_f_add") === 0 && this.get("r_t_add") === 0 ) {
        return Math.min(this.get("l_f_add"), this.get("l_t_add"));
    }

    return Math.min(
        this.get("l_f_add"),
        this.get("l_t_add"),
        this.get("r_f_add"),
        this.get("r_t_add")
    );

};

MAD.model.StreetSegmentRecord.prototype.getRangeMax = function() {

    if (this.get("l_f_add") === 0 && this.get("l_t_add") === 0 ) {
        return Math.max(this.get("r_f_add"), this.get("r_t_add"));
    }

    if (this.get("r_f_add") === 0 && this.get("r_t_add") === 0 ) {
        return Math.max(this.get("l_f_add"), this.get("l_t_add"));
    }

    return Math.max(
        this.get("l_f_add"),
        this.get("l_t_add"),
        this.get("r_f_add"),
        this.get("r_t_add")
    );

};


Ext.define('MAD.model.StreetSegmentStore', {
    extend: 'MAD.model.AbstractStore',
    model: 'MAD.model.StreetSegmentRecord',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'available_streets'
        }
    }
});
MAD.model.StreetSegmentStore.prototype.serialize = false;


Ext.define('MAD.model.ZoneRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "zone_id", defaultValue: 0 },
        { name: "zipcode", defaultValue: "" },
        { name: "jurisdiction", defaultValue: "" }
    ]
});


Ext.define('MAD.model.ZoneStore', {
    extend: 'MAD.model.AbstractStore',
    model: 'MAD.model.ZoneRecord',
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            root: 'zones'
        }
    }
});
MAD.model.ZoneStore.prototype.serialize = false;


///// ChangeRequest - record and store
Ext.define('MAD.model.ChangeRequestRecord', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "change_request_id", defaultValue: null },
        { name: "name", defaultValue: "please enter a descriptive title" },
        { name: "status_id", defaultValue: 4, mapping: "review_status_id" },
        { name: "status_description", defaultValue: "x" },
        { name: "requestor_name", defaultValue: "" },
        { name: "requestor_comment", defaultValue: "" },
        { name: "requestor_last_update", defaultValue: "" },
        { name: "reviewer_name", defaultValue: "" },
        { name: "reviewer_comment", defaultValue: "" },
        { name: "reviewer_last_update", defaultValue: "" },
        { name: "concurrency_id", defaultValue: 0 },
        { name: "base_addresses", defaultValue: [] }
    ]
});

MAD.model.ChangeRequestRecord.prototype.hasBaseAddress = function(baseAddressId) {
    var hasBaseAddress = false;
    Ext.each(this.get('base_addresses').getRange(), function(item, index, allItems) {
        if (baseAddressId == item.get('address_base_id')) {
            hasBaseAddress = true;
            return false;
        }
    }, this);
    return hasBaseAddress; 
};

Ext.define('MAD.model.ChangeRequestStore', {
    extend: 'MAD.model.AbstractStore',
    model: 'MAD.model.ChangeRequestRecord',
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            root: 'changeRequest'
        }
    },
    constructor: function () {
        this.callParent(arguments);
        this.on('load', function() {
            this.initStore(true);
        }, this);
    },
    initStore: function(loadData) {
        // parse data into base and unit addresss and add to the changerequest
        this.changeRequest = this.getAt(0);

        // add a reference to the base address store directly into the change request record
        this.baseAddressStore = new MAD.model.BaseAddressStore({changeListener: this.changeListener});
        if (loadData === true) {
            this.baseAddressStore.loadRawData(this.changeRequest.data, false);
        }
        this.baseAddressStore.changeRequestStore = this;
        
        this.changeRequest.set('base_addresses', null);
        this.changeRequest.set('base_addresses', this.baseAddressStore);

        this.changeRequest.baseAddresses = this.baseAddressStore.data;
    },
    addDefaultRecord: function() {
        this.add(this.model.create({}));
        this.initStore(true);
    },
    getDataVolumeFactor: function() {
        var rowCount = 0;
        // Provide a factor that gives a sense of how much data we have - start with something simple.
        var baseAddressRecords = this.baseAddressStore.getRange();
        Ext.each(baseAddressRecords, function(record, index, allItems) {
            var unitAddressStore = record.get('unit_addresses');
            rowCount += unitAddressStore.getCount();
            var parcelPickStore = record.get('parcel_pick_list');
            rowCount += parcelPickStore.getCount();
        }, this);
        return rowCount / 1000;
    },
    addBaseAddressesFromJsonObj: function(jsonObj) {
        this.baseAddressStore.loadRawData(jsonObj, true);
    }

});


Ext.define('MAD.model.ChangeRequestStatusDomainRecord', {
    extend: 'Ext.data.Model',
    fields:[
        { name: "status_description", defaultValue: null },
        { name: "status_id", defaultValue: null }
    ]
});
Ext.define('MAD.model.ChangeRequestStatusDomainStore', {
    extend: 'Ext.data.Store',
    reader: new Ext.data.JsonReader({root: 'd_change_request_status'}),
    model: 'MAD.model.ChangeRequestStatusDomainRecord'
});


Ext.define('MAD.model.getAddressBaseNumberSuffixDomainRecord', {
    extend: 'Ext.data.Model',

    fields: ['id', 'suffix_value', 'suffix_display']
});

MAD.model.getAddressBaseNumberSuffixDomainStore = function() {
    return new Ext.data.Store({
        model: 'MAD.model.getAddressBaseNumberSuffixDomainRecord',
        reader: new Ext.data.JsonReader({
            root: 'd_address_base_number_suffix'
        })
    });
};

Ext.define('MAD.model.getAddressBaseTypeDomainRecord', {
    extend: 'Ext.data.Model',

    fields: ['id', 'type_name']
});

MAD.model.getAddressBaseTypeDomainStore = function () {
    return new Ext.data.Store({
        model: 'MAD.model.getAddressBaseTypeDomainRecord',
        reader: new Ext.data.JsonReader({
            root: 'd_address_base_type'
        })
    });
};

Ext.define('MAD.model.getFloorsDomainRecord', {
    extend: 'Ext.data.Model',

    fields: ['floor_description', 'floor_id']
});

MAD.model.getFloorsDomainStore = function() {
    return new Ext.data.Store({
        reader: new Ext.data.JsonReader({
            root: 'd_floors'
        }),
        model: 'MAD.model.getFloorsDomainRecord'
    });
};


Ext.define('MAD.model.UnitTypeRecord', {
    extend: 'Ext.data.Model',

    fields:[
        { name: "unit_type_description", defaultValue: null },
        { name: "unit_type_id", defaultValue: null },
        { name: "level", defaultValue: null},
    ]
});


MAD.model.getUnitTypeDomainStore = function() {
    return new Ext.data.Store({
        model: 'MAD.model.UnitTypeRecord',
        reader: new Ext.data.JsonReader( {root: 'd_unit_type', id: 'unit_type_id'})
    });
};


Ext.define('MAD.model.DispositionRecord', {
    extend: 'Ext.data.Model',

    fields: [
        { name: "disposition_code", defaultValue: null },
        { name: "disposition_description", defaultValue: null }
    ]
});


MAD.model.getDispositionDomainStore = function() {
    return new Ext.data.Store({
        model: 'MAD.model.DispositionRecord',
        reader: new Ext.data.JsonReader({root: 'd_address_disposition', id: 'disposition_code'})
    });
};

Ext.define('MAD.model.AddressSearch', {
    extend: 'Ext.data.Model',

    fields: [
        { name: "address_base_id", mapping: "address_base_id" },
        { name: "base_address_num", mapping: "base_address_num" },
        { name: "base_address_suffix", mapping: "base_address_suffix" },
        { name: "prefix", mapping: "base_address_prefix" },
        { name: "zipcode", mapping: "zipcode" },
        { name: "jurisdiction", mapping: "jurisdiction" },
        { name: "full_street_name", mapping: "full_street_name" },
        { name: "geometry", mapping: "geometry" },
        { name: "disposition", mapping: "disposition" },
        { name: "create_tms", mapping: "create_tms"},
        { name: "activate_change_request_id", mapping: "activate_change_request_id"},
        { name: "last_change_tms", mapping: "last_change_tms"},
        { name: "update_change_request_id", mapping: "update_change_request_id"},
        { name: "retire_tms", mapping: "retire_tms" },
        { name: "retire_change_request_id", mapping: "retire_change_request_id"},
        { name: "data_source_base", mapping: "data_source_base" },
        { name: "validationWarnings", mapping: "validationWarnings" } ,
        { name: "complete_landmark_name", mapping: "complete_landmark_name" },
        { name: "address_type" },
        { name: "landmark_aliases", mapping: "landmark_aliases" },
    ]
});

///// parcel_link_changes
Ext.define('MAD.model.ClusterReport', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'geometry' },
        { name: 'address_base_id' },
        { name: 'address' },
        { name: 'validation_warning_count' },
        { name: 'retire_tms' }
    ]
});


Ext.define('MAD.model.AddressesFlat', {
    extend: 'Ext.data.Model',
    fields: [
        { name: "address_id" },
        { name: "post_direction" },
        { name: "address_x_parcel_retire_change_request_id" },
        { name: "base_address_retire_tms" },
        { name: "disposition_code" },
        { name: "base_address_suffix" },
        { name: "base_address_create_tms" },
        { name: "id" },
        { name: "address_base_flg" },
        { name: "base_street_name" },
        { name: "unit_address_create_tms" },
        { name: "address_x_parcel_retire_tms", defaultValue: null, type: "string" }, // use type string and default null to support sorting
        { name: "blk_lot" },
        { name: "full_street_name" },
        { name: "unit_address_retire_tms" },
        { name: "map_blk_lot" },
        { name: "unit_num" },
        { name: "address_x_parcel_create_tms", defaultValue: null, type: "string" }, // use type string and default null to support sorting
        { name: "address_base_id" },
        { name: "disposition_description" },
        { name: "street_type" },
        { name: "address_x_parcel_activate_change_request_id" },
        { name: "parcel_date_map_drop" },
        { name: "street_segment_id" },
        { name: "parcel_date_map_add" },
        { name: "base_address_num" },
        { name: "geometry" },
        { name: "zipcode" },
        { name: "floor_id" },
        { name: "address_x_parcel_id" },
        { name: "unit_type_id" },
        { name: "seg_cnn" },
        { name: "report_change_request_id" },
        { name: "change_type" },
        { name: "complete_landmark_name" },
        { name: "address_type" },
        { name: "landmark_aliases", mapping: "landmark_aliases" },
    ]
});


Ext.define('MAD.model.User', {
    singleton: true,
    config: {
        changePass: false,
        email: null,
        firstname: null,
        id: 0,
        lastname: null,
        roles: [],
        username: null
    },
    mixins: {
        observable: 'Ext.util.Observable'
    },
    constructor: function (cfg) {
        Ext.apply(this, this.config);
        Ext.apply(this, cfg);
        this.mixins.observable.constructor.call(this, cfg);
        this.addEvents(
            'login',
            'logout'
        );
    },
    hasRole: function(roleName) {
        var hasRole = false;
        Ext.each(this.roles, function (role) {
            if (role === roleName) {
                hasRole = true;
                return false;
            }
        }, this);
        return hasRole;
    },
    isRequestor: function () {
        return this.hasRole('requestor');
    },
    isApprover: function () {
        return this.hasRole('approver');
    },
    isLandmarkEditor: function () {
        return this.hasRole('landmark_editor');
    },
    isSubaddressEditor: function () {
        return this.hasRole('subaddress_editor');
    },
    login: function (cfg) {
        Ext.apply(this, cfg);
        this.fireEvent('login');
    },
    logout: function() {
        Ext.apply(this, this.config);
        this.fireEvent('logout');
    }
});


Ext.apply(Ext.form.VTypes, {

    // Yes indeed, zero addresses are used by real agencies and they legal in the FGDC!
    AddressNumberText: 'must be a number between 1 and 999999',
    AddressNumber:  function(v) {
        if (! /^[0-9]+$/.test(v) ) {
            return false;
        } else {
            return v >= 0 && v <= 999999;
        }
    },

    BlockNumberText: 'must begin with 4 numbers optionally followed by one upper case letter',
    BlockNumber:  function(v) {

        var testString;

        if (v.length < 4 || v.length > 5) {
            Ext.form.VTypes['BlockNumberText'] = 'total length must be 4 or 5';
            return false;
        }

        testString = v.slice(0,4);
        if (! /^[0-9]+$/.test(testString) ) {
            Ext.form.VTypes['BlockNumberText'] = 'the first 4 characters must be numbers';
            return false;
        }

        if (v.length === 5) {
            testString = v.slice(4);
            if (! /^[A-Z]+$/.test(testString) ) {
                Ext.form.VTypes['BlockNumberText'] = 'the optional 5th character must be an upper case letter';
                return false;
            }
        }

        return true;

    },

    LotNumberText: 'must begin with 3 numbers optionally followed by one upper case letter',
    LotNumber:  function(v) {

        var testString;

        if (v.length < 3 || v.length > 4) {
            Ext.form.VTypes['LotNumberText'] = 'total length must be 3 or 4';
            return false;
        }

        testString = v.slice(0,3);
        if (! /^[0-9]+$/.test(testString) ) {
            Ext.form.VTypes['LotNumberText'] = 'the first 3 characters must be numbers';
            return false;
        }

        if (v.length === 4) {
            testString = v.slice(3);
            if (! /^[A-Z]+$/.test(testString) ) {
                Ext.form.VTypes['LotNumberText'] = 'the optional 4th character must be an upper case letter';
                return false;
            }
        }

        return true;

    },

    UnitNumberText: "Sorry but an illegal character was found. A Unit Number is limited to any combination of the following: numbers, letters, dash(-), forward slash(/), and white space( ).",
    UnitNumber: function(v) {
        if (! /^[a-zA-Z0-9\-\/\s]+$/.test(v)) {
            return false;
        } else {
            return v.length <= 10;
        }
    }

});
