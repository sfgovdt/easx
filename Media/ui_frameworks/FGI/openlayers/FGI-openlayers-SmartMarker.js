/* Copyright (c) 2009 Farallon Geographics, Inc.*/

Ext.namespace('FGI', 'FGI.openlayers');

/**
 * @requires OpenLayers/Marker.js
 */

/**
 * Class: FGI.openLayers.SmartMarker
 *
 * Inherits from:
 *  - <OpenLayers.Marker> 
 *
 */
FGI.openlayers.SmartMarker = OpenLayers.Class(OpenLayers.Marker, {

    /** 
    * Property: data 
    * {<Object>} The data associated with the marker
    */
    data: {},

    /** 
    * Property: idField 
    * {<String>} name of the property in the data object to use as an Id for the marker
    */
    idField: '',

    /** 
    * Property: events 
    * {<OpenLayers.Events>} the event handler.
    */
    events: null,

    /** 
    * Property: map 
    * {<OpenLayers.Map>} the map this marker is attached to
    */
    map: null,

    /** 
    * Constructor: OpenLayers.Marker
    * Parameters:
    * lonlat - {<OpenLayers.LonLat>} the position of this marker
    * icon - {<OpenLayers.Icon>}  the icon for this marker
    * data - {<Object>}  The data associated with the marker
    */
    initialize: function(lonlat, icon, data, idField) {
        //this.lonlat = lonlat;
        if (data != null) {
            this.data = data;
            if (idField != null) {
                this.idField = idField;
            }
        }
        
        OpenLayers.Marker.prototype.initialize.apply(this, arguments);
//        
//        var newIcon = (icon) ? icon : OpenLayers.Marker.defaultIcon();
//        if (this.icon == null) {
//            this.icon = newIcon;
//        } else {
//            this.icon.url = newIcon.url;
//            this.icon.size = newIcon.size;
//            this.icon.offset = newIcon.offset;
//            this.icon.calculateOffset = newIcon.calculateOffset;
//        }
//        this.events = new OpenLayers.Events(this, this.icon.imageDiv, null);
    },

    /**
    * APIMethod: destroy
    * Destroy the marker. You must first remove the marker from any 
    * layer which it has been added to, or you will get buggy behavior.
    * (This can not be done within the marker since the marker does not
    * know which layer it is attached to.)
    */
    destroy: function() {
        // erase any drawn features
        this.erase();

        this.map = null;

        this.events.destroy();
        this.events = null;

        if (this.icon != null) {
            this.icon.destroy();
            this.icon = null;
        }
    },

    /** 
    * Method: draw
    * Calls draw on the icon, and returns that output.
    * 
    * Parameters:
    * px - {<OpenLayers.Pixel>}
    * 
    * Returns:
    * {DOMElement} A new DOM Image with this marker's icon set at the 
    * location passed-in
    */
    draw: function(px) {
        return this.icon.draw(px);
    },

    /** 
    * Method: erase
    * Erases any drawn elements for this marker.
    */
    erase: function() {
        if (this.icon != null) {
            this.icon.erase();
        }
    },

    /**
    * Method: moveTo
    * Move the marker to the new location.
    *
    * Parameters:
    * px - {<OpenLayers.Pixel>} the pixel position to move to
    */
    moveTo: function(px) {
        if ((px != null) && (this.icon != null)) {
            this.icon.moveTo(px);
        }
        this.lonlat = this.map.getLonLatFromLayerPx(px);
    },

    /**
    * APIMethod: isDrawn
    * 
    * Returns:
    * {Boolean} Whether or not the marker is drawn.
    */
    isDrawn: function() {
        var isDrawn = (this.icon && this.icon.isDrawn());
        return isDrawn;
    },

    /**
    * Method: onScreen
    *
    * Returns:
    * {Boolean} Whether or not the marker is currently visible on screen.
    */
    onScreen: function() {

        var onScreen = false;
        if (this.map) {
            var screenBounds = this.map.getExtent();
            onScreen = screenBounds.containsLonLat(this.lonlat);
        }
        return onScreen;
    },

    /**
    * Method: inflate
    * Englarges the markers icon by the specified ratio.
    *
    * Parameters:
    * inflate - {float} the ratio to enlarge the marker by (passing 2
    *                   will double the size).
    */
    inflate: function(inflate) {
        if (this.icon) {
            var newSize = new OpenLayers.Size(this.icon.size.w * inflate,
                                              this.icon.size.h * inflate);
            this.icon.setSize(newSize);
        }
    },

    /** 
    * Method: setOpacity
    * Change the opacity of the marker by changin the opacity of 
    *   its icon
    * 
    * Parameters:
    * opacity - {float}  Specified as fraction (0.4, etc)
    */
    setOpacity: function(opacity) {
        this.icon.setOpacity(opacity);
    },

    /**
    * Method: setUrl
    * Change URL of the Icon Image.
    * 
    * url - {String} 
    */
    setUrl: function(url) {
        this.icon.setUrl(url);
    },

    /** 
    * Method: display
    * Hide or show the icon
    * 
    * display - {Boolean} 
    */
    display: function(display) {
        this.icon.display(display);
    },

    CLASS_NAME: "FGI.openlayers.SmartMarker"
});

