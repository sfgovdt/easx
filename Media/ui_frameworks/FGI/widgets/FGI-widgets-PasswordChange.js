﻿Ext.define('FGI.widgets.PasswordChangeForm', {
    extend: 'Ext.form.FormPanel',
    // Constructor Defaults, can be overridden by user's config object
    // id: 'frmChangePassword',

    url: '',

    frame: true,

    title: 'Change Your Password',

    changeSecurityQuestion: true,

    initComponent: function () {

        this.addEvents({
            'passwordchanged': true
        });

        this.submitButton = Ext.create('Ext.button.Button', {
            text: 'Change Password',
            scope: this,
            handler: this.submit,
            formBind: true
        });

        this.cancelButton = Ext.create('Ext.button.Button', {
            text: 'Cancel',
            scope: this,
            handler: this.cancel
        });

        var fieldListeners = {
            'keydown': function(textField, event, eventOptions) {
                if (event.getKey() === event.ESC) {
                    this.cancel();
                } else if (event.getKey() === event.RETURN) {
                    this.submit();
                }
            },
            scope: this
        };

        Ext.apply(this, {
            bodyStyle: 'padding:5px 5px 0',
            defaults: {
                msgTarget: 'side',
                labelWidth: 150,
                enableKeyEvents: true
            },
            defaultType: 'textfield',
            items: [
                {
                    fieldLabel: 'Old Password',
                    name: 'password_old',
                    allowBlank: false,
                    inputType: 'password',
                    listeners: fieldListeners
                }
                , {
                    fieldLabel: 'New Password',
                    name: 'password_new',
                    allowBlank: false,
                    inputType: 'password',
                    listeners: fieldListeners
                }
                , {
                    fieldLabel: 'Confirm Password',
                    name: 'password_confirm',
                    allowBlank: false,
                    inputType: 'password',
                    listeners: fieldListeners
                }
            ],
            buttons: [
                this.submitButton,
                this.cancelButton
            ]
        });

        this.callParent(arguments);

    },

    resetForm: function() {
        var form = this.getForm();
        var field = form.findField('password_old');
        if (field) {
            field.focus(true, 10);
        }
        form.reset();
    },

    cancel: function () {
        this.ownerCt.close();
    },

    submit: function () {
        if (!this.getForm().isValid()) {
            return;
        }
        var mask = new Ext.LoadMask(this.getEl(), { msg: 'Changing your password...' });
        mask.show();
        this.submitButton.disable();
        this.form.submit({
            url: this.url,
            method: 'POST',
            success: function (theForm, responseObj) {
                mask.hide();
                this.ownerCt.close();
                this.fireEvent('passwordchanged', responseObj);
                Ext.Msg.alert('Password Changed!', 'An email has been sent with your new password.');
            },
            failure: function (theForm, responseObj) {
                var window;
                mask.hide();
                if (responseObj.result) {
                    window = Ext.Msg.alert('There was a problem changing your password.', responseObj.result.message);
                } else {
                    window = Ext.Msg.alert('', 'Please correct any errors in the form.');
                }
                // handle escape and OK
                window.on({
                    hide: this.resetForm,
                    scope: this,
                    single: true
                });
            },
            scope: this
        });
    }

});