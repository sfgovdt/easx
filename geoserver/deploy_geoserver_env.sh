JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.4.11-0.el7_6.x86_64
export JAVA_HOME

CATALINA_HOME=/usr/share/tomcat
export CATALINA_HOME

TOMCAT_WEBAPPS_PATH=$CATALINA_HOME/webapps
export TOMCAT_WEBAPPS_PATH

GEOSERVER_ARCHIVE_PATH=$TOMCAT_WEBAPPS_PATH/archive
export GEOSERVER_ARCHIVE_PATH

GEOSERVER_DATA_PATH=$TOMCAT_WEBAPPS_PATH/geoserver/data
export GEOSERVER_DATA_PATH

GWC_PATH=/srv/gwc
export GWC_PATH

PYTHON_EXE_PATH=/usr/bin/python3
export PYTHON_EXE_PATH
