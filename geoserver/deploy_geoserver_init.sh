echo initializing geoserver deploy
cd /usr/share/tomcat/webapps/

EAS_BRANCH=master
export EAS_BRANCH

EAS_TEAM=sfgovdt
export EAS_TEAM

EAS_REPO=easx
export EAS_REPO

CONFIG_BRANCH=master
export CONFIG_BRANCH

CONFIG_TEAM=sfgovdt
export CONFIG_TEAM

CONFIG_REPO=easconfigxsf
export CONFIG_REPO

read -p "BitBucket usename: " BITBUCKET_USER; echo
export BITBUCKET_USER
stty -echo
read -p "BitBucket password: " BITBUCKET_PASSWORD; echo
export BITBUCKET_PASSWORD
stty echo

curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@api.bitbucket.org/2.0/repositories/$EAS_TEAM/$EAS_REPO/src/$EAS_BRANCH/geoserver/deploy_geoserver_env.sh
chmod 700 ./deploy_geoserver_env.sh

curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@api.bitbucket.org/2.0/repositories/$EAS_TEAM/$EAS_REPO/src/$EAS_BRANCH/geoserver/deploy_geoserver.py
chmod 600 ./deploy_geoserver.py

curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@api.bitbucket.org/2.0/repositories/$CONFIG_TEAM/$CONFIG_REPO/src/$CONFIG_BRANCH/web/settings_env/live/environments.py
chmod 600 ./environments.py

. ./deploy_geoserver_env.sh

curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@api.bitbucket.org/2.0/repositories/$EAS_TEAM/$EAS_REPO/src/$EAS_BRANCH/requirements.txt
$PYTHON_EXE_PATH -m pip install -r requirements.txt

# use unbuffered for prettier stdout
$PYTHON_EXE_PATH -u deploy_geoserver.py
